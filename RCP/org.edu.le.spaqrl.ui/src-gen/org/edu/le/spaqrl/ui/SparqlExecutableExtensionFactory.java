/*
 * generated by Xtext
 */
package org.edu.le.spaqrl.ui;

import org.eclipse.xtext.ui.guice.AbstractGuiceAwareExecutableExtensionFactory;
import org.osgi.framework.Bundle;

import com.google.inject.Injector;

import org.edu.le.spaqrl.ui.internal.SparqlActivator;

/**
 * This class was generated. Customizations should only happen in a newly
 * introduced subclass. 
 */
public class SparqlExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	protected Bundle getBundle() {
		return SparqlActivator.getInstance().getBundle();
	}
	
	@Override
	protected Injector getInjector() {
		return SparqlActivator.getInstance().getInjector(SparqlActivator.ORG_EDU_LE_SPAQRL_SPARQL);
	}
	
}
