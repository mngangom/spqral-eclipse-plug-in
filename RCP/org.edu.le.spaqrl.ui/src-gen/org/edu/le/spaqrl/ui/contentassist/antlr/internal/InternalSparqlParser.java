package org.edu.le.spaqrl.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.edu.le.spaqrl.services.SparqlGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalSparqlParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_INT", "RULE_IRI_URI", "RULE_ID", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'DISTINCT'", "'REDUCED'", "'*'", "'ASC'", "'DESC'", "'PREFIX'", "':'", "'SELECT'", "'FILTER'", "'('", "')'", "','", "'OFFSET'", "'LIMIT'", "'ORDER BY'", "'WHERE'", "'{'", "'}'", "'UNION'", "'OPTIONAL'", "'.'", "';'", "'?:'", "'_:'", "'FROM'", "'NAMED'", "'?'"
    };
    public static final int RULE_STRING=7;
    public static final int RULE_SL_COMMENT=9;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_IRI_URI=5;
    public static final int RULE_ID=6;
    public static final int RULE_WS=10;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=4;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalSparqlParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalSparqlParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalSparqlParser.tokenNames; }
    public String getGrammarFileName() { return "../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g"; }


     
     	private SparqlGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(SparqlGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleQuery"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:60:1: entryRuleQuery : ruleQuery EOF ;
    public final void entryRuleQuery() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:61:1: ( ruleQuery EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:62:1: ruleQuery EOF
            {
             before(grammarAccess.getQueryRule()); 
            pushFollow(FOLLOW_ruleQuery_in_entryRuleQuery61);
            ruleQuery();

            state._fsp--;

             after(grammarAccess.getQueryRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQuery68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQuery"


    // $ANTLR start "ruleQuery"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:69:1: ruleQuery : ( ( rule__Query__SelectQueryAssignment )* ) ;
    public final void ruleQuery() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:73:2: ( ( ( rule__Query__SelectQueryAssignment )* ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:74:1: ( ( rule__Query__SelectQueryAssignment )* )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:74:1: ( ( rule__Query__SelectQueryAssignment )* )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:75:1: ( rule__Query__SelectQueryAssignment )*
            {
             before(grammarAccess.getQueryAccess().getSelectQueryAssignment()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:76:1: ( rule__Query__SelectQueryAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==17||LA1_0==19||(LA1_0>=24 && LA1_0<=25)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:76:2: rule__Query__SelectQueryAssignment
            	    {
            	    pushFollow(FOLLOW_rule__Query__SelectQueryAssignment_in_ruleQuery94);
            	    rule__Query__SelectQueryAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getQueryAccess().getSelectQueryAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQuery"


    // $ANTLR start "entryRulePrefix"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:88:1: entryRulePrefix : rulePrefix EOF ;
    public final void entryRulePrefix() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:89:1: ( rulePrefix EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:90:1: rulePrefix EOF
            {
             before(grammarAccess.getPrefixRule()); 
            pushFollow(FOLLOW_rulePrefix_in_entryRulePrefix122);
            rulePrefix();

            state._fsp--;

             after(grammarAccess.getPrefixRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulePrefix129); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrefix"


    // $ANTLR start "rulePrefix"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:97:1: rulePrefix : ( ( rule__Prefix__Alternatives ) ) ;
    public final void rulePrefix() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:101:2: ( ( ( rule__Prefix__Alternatives ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:102:1: ( ( rule__Prefix__Alternatives ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:102:1: ( ( rule__Prefix__Alternatives ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:103:1: ( rule__Prefix__Alternatives )
            {
             before(grammarAccess.getPrefixAccess().getAlternatives()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:104:1: ( rule__Prefix__Alternatives )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:104:2: rule__Prefix__Alternatives
            {
            pushFollow(FOLLOW_rule__Prefix__Alternatives_in_rulePrefix155);
            rule__Prefix__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPrefixAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrefix"


    // $ANTLR start "entryRuleUnNamedPrefix"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:116:1: entryRuleUnNamedPrefix : ruleUnNamedPrefix EOF ;
    public final void entryRuleUnNamedPrefix() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:117:1: ( ruleUnNamedPrefix EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:118:1: ruleUnNamedPrefix EOF
            {
             before(grammarAccess.getUnNamedPrefixRule()); 
            pushFollow(FOLLOW_ruleUnNamedPrefix_in_entryRuleUnNamedPrefix182);
            ruleUnNamedPrefix();

            state._fsp--;

             after(grammarAccess.getUnNamedPrefixRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleUnNamedPrefix189); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUnNamedPrefix"


    // $ANTLR start "ruleUnNamedPrefix"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:125:1: ruleUnNamedPrefix : ( ( rule__UnNamedPrefix__Group__0 ) ) ;
    public final void ruleUnNamedPrefix() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:129:2: ( ( ( rule__UnNamedPrefix__Group__0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:130:1: ( ( rule__UnNamedPrefix__Group__0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:130:1: ( ( rule__UnNamedPrefix__Group__0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:131:1: ( rule__UnNamedPrefix__Group__0 )
            {
             before(grammarAccess.getUnNamedPrefixAccess().getGroup()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:132:1: ( rule__UnNamedPrefix__Group__0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:132:2: rule__UnNamedPrefix__Group__0
            {
            pushFollow(FOLLOW_rule__UnNamedPrefix__Group__0_in_ruleUnNamedPrefix215);
            rule__UnNamedPrefix__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getUnNamedPrefixAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUnNamedPrefix"


    // $ANTLR start "entryRuleSelectQuery"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:144:1: entryRuleSelectQuery : ruleSelectQuery EOF ;
    public final void entryRuleSelectQuery() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:145:1: ( ruleSelectQuery EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:146:1: ruleSelectQuery EOF
            {
             before(grammarAccess.getSelectQueryRule()); 
            pushFollow(FOLLOW_ruleSelectQuery_in_entryRuleSelectQuery242);
            ruleSelectQuery();

            state._fsp--;

             after(grammarAccess.getSelectQueryRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSelectQuery249); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSelectQuery"


    // $ANTLR start "ruleSelectQuery"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:153:1: ruleSelectQuery : ( ( rule__SelectQuery__Alternatives ) ) ;
    public final void ruleSelectQuery() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:157:2: ( ( ( rule__SelectQuery__Alternatives ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:158:1: ( ( rule__SelectQuery__Alternatives ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:158:1: ( ( rule__SelectQuery__Alternatives ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:159:1: ( rule__SelectQuery__Alternatives )
            {
             before(grammarAccess.getSelectQueryAccess().getAlternatives()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:160:1: ( rule__SelectQuery__Alternatives )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:160:2: rule__SelectQuery__Alternatives
            {
            pushFollow(FOLLOW_rule__SelectQuery__Alternatives_in_ruleSelectQuery275);
            rule__SelectQuery__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getSelectQueryAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSelectQuery"


    // $ANTLR start "entryRuleFilter"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:172:1: entryRuleFilter : ruleFilter EOF ;
    public final void entryRuleFilter() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:173:1: ( ruleFilter EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:174:1: ruleFilter EOF
            {
             before(grammarAccess.getFilterRule()); 
            pushFollow(FOLLOW_ruleFilter_in_entryRuleFilter302);
            ruleFilter();

            state._fsp--;

             after(grammarAccess.getFilterRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFilter309); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFilter"


    // $ANTLR start "ruleFilter"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:181:1: ruleFilter : ( ( rule__Filter__Group__0 ) ) ;
    public final void ruleFilter() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:185:2: ( ( ( rule__Filter__Group__0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:186:1: ( ( rule__Filter__Group__0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:186:1: ( ( rule__Filter__Group__0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:187:1: ( rule__Filter__Group__0 )
            {
             before(grammarAccess.getFilterAccess().getGroup()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:188:1: ( rule__Filter__Group__0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:188:2: rule__Filter__Group__0
            {
            pushFollow(FOLLOW_rule__Filter__Group__0_in_ruleFilter335);
            rule__Filter__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFilterAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFilter"


    // $ANTLR start "entryRuleExpressionFilter"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:200:1: entryRuleExpressionFilter : ruleExpressionFilter EOF ;
    public final void entryRuleExpressionFilter() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:201:1: ( ruleExpressionFilter EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:202:1: ruleExpressionFilter EOF
            {
             before(grammarAccess.getExpressionFilterRule()); 
            pushFollow(FOLLOW_ruleExpressionFilter_in_entryRuleExpressionFilter362);
            ruleExpressionFilter();

            state._fsp--;

             after(grammarAccess.getExpressionFilterRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleExpressionFilter369); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpressionFilter"


    // $ANTLR start "ruleExpressionFilter"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:209:1: ruleExpressionFilter : ( ( rule__ExpressionFilter__Group__0 ) ) ;
    public final void ruleExpressionFilter() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:213:2: ( ( ( rule__ExpressionFilter__Group__0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:214:1: ( ( rule__ExpressionFilter__Group__0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:214:1: ( ( rule__ExpressionFilter__Group__0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:215:1: ( rule__ExpressionFilter__Group__0 )
            {
             before(grammarAccess.getExpressionFilterAccess().getGroup()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:216:1: ( rule__ExpressionFilter__Group__0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:216:2: rule__ExpressionFilter__Group__0
            {
            pushFollow(FOLLOW_rule__ExpressionFilter__Group__0_in_ruleExpressionFilter395);
            rule__ExpressionFilter__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getExpressionFilterAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpressionFilter"


    // $ANTLR start "entryRuleOffsetClause"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:228:1: entryRuleOffsetClause : ruleOffsetClause EOF ;
    public final void entryRuleOffsetClause() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:229:1: ( ruleOffsetClause EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:230:1: ruleOffsetClause EOF
            {
             before(grammarAccess.getOffsetClauseRule()); 
            pushFollow(FOLLOW_ruleOffsetClause_in_entryRuleOffsetClause422);
            ruleOffsetClause();

            state._fsp--;

             after(grammarAccess.getOffsetClauseRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOffsetClause429); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOffsetClause"


    // $ANTLR start "ruleOffsetClause"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:237:1: ruleOffsetClause : ( ( rule__OffsetClause__Group__0 ) ) ;
    public final void ruleOffsetClause() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:241:2: ( ( ( rule__OffsetClause__Group__0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:242:1: ( ( rule__OffsetClause__Group__0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:242:1: ( ( rule__OffsetClause__Group__0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:243:1: ( rule__OffsetClause__Group__0 )
            {
             before(grammarAccess.getOffsetClauseAccess().getGroup()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:244:1: ( rule__OffsetClause__Group__0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:244:2: rule__OffsetClause__Group__0
            {
            pushFollow(FOLLOW_rule__OffsetClause__Group__0_in_ruleOffsetClause455);
            rule__OffsetClause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOffsetClauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOffsetClause"


    // $ANTLR start "entryRuleLimitClause"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:256:1: entryRuleLimitClause : ruleLimitClause EOF ;
    public final void entryRuleLimitClause() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:257:1: ( ruleLimitClause EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:258:1: ruleLimitClause EOF
            {
             before(grammarAccess.getLimitClauseRule()); 
            pushFollow(FOLLOW_ruleLimitClause_in_entryRuleLimitClause482);
            ruleLimitClause();

            state._fsp--;

             after(grammarAccess.getLimitClauseRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleLimitClause489); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLimitClause"


    // $ANTLR start "ruleLimitClause"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:265:1: ruleLimitClause : ( ( rule__LimitClause__Group__0 ) ) ;
    public final void ruleLimitClause() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:269:2: ( ( ( rule__LimitClause__Group__0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:270:1: ( ( rule__LimitClause__Group__0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:270:1: ( ( rule__LimitClause__Group__0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:271:1: ( rule__LimitClause__Group__0 )
            {
             before(grammarAccess.getLimitClauseAccess().getGroup()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:272:1: ( rule__LimitClause__Group__0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:272:2: rule__LimitClause__Group__0
            {
            pushFollow(FOLLOW_rule__LimitClause__Group__0_in_ruleLimitClause515);
            rule__LimitClause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLimitClauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLimitClause"


    // $ANTLR start "entryRuleINTEGER"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:284:1: entryRuleINTEGER : ruleINTEGER EOF ;
    public final void entryRuleINTEGER() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:285:1: ( ruleINTEGER EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:286:1: ruleINTEGER EOF
            {
             before(grammarAccess.getINTEGERRule()); 
            pushFollow(FOLLOW_ruleINTEGER_in_entryRuleINTEGER542);
            ruleINTEGER();

            state._fsp--;

             after(grammarAccess.getINTEGERRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleINTEGER549); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleINTEGER"


    // $ANTLR start "ruleINTEGER"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:293:1: ruleINTEGER : ( RULE_INT ) ;
    public final void ruleINTEGER() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:297:2: ( ( RULE_INT ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:298:1: ( RULE_INT )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:298:1: ( RULE_INT )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:299:1: RULE_INT
            {
             before(grammarAccess.getINTEGERAccess().getINTTerminalRuleCall()); 
            match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleINTEGER575); 
             after(grammarAccess.getINTEGERAccess().getINTTerminalRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleINTEGER"


    // $ANTLR start "entryRuleOrderClause"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:312:1: entryRuleOrderClause : ruleOrderClause EOF ;
    public final void entryRuleOrderClause() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:313:1: ( ruleOrderClause EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:314:1: ruleOrderClause EOF
            {
             before(grammarAccess.getOrderClauseRule()); 
            pushFollow(FOLLOW_ruleOrderClause_in_entryRuleOrderClause601);
            ruleOrderClause();

            state._fsp--;

             after(grammarAccess.getOrderClauseRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOrderClause608); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOrderClause"


    // $ANTLR start "ruleOrderClause"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:321:1: ruleOrderClause : ( ( rule__OrderClause__Group__0 ) ) ;
    public final void ruleOrderClause() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:325:2: ( ( ( rule__OrderClause__Group__0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:326:1: ( ( rule__OrderClause__Group__0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:326:1: ( ( rule__OrderClause__Group__0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:327:1: ( rule__OrderClause__Group__0 )
            {
             before(grammarAccess.getOrderClauseAccess().getGroup()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:328:1: ( rule__OrderClause__Group__0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:328:2: rule__OrderClause__Group__0
            {
            pushFollow(FOLLOW_rule__OrderClause__Group__0_in_ruleOrderClause634);
            rule__OrderClause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOrderClauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOrderClause"


    // $ANTLR start "entryRuleOrderCondition"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:340:1: entryRuleOrderCondition : ruleOrderCondition EOF ;
    public final void entryRuleOrderCondition() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:341:1: ( ruleOrderCondition EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:342:1: ruleOrderCondition EOF
            {
             before(grammarAccess.getOrderConditionRule()); 
            pushFollow(FOLLOW_ruleOrderCondition_in_entryRuleOrderCondition661);
            ruleOrderCondition();

            state._fsp--;

             after(grammarAccess.getOrderConditionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOrderCondition668); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOrderCondition"


    // $ANTLR start "ruleOrderCondition"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:349:1: ruleOrderCondition : ( ( rule__OrderCondition__Alternatives ) ) ;
    public final void ruleOrderCondition() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:353:2: ( ( ( rule__OrderCondition__Alternatives ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:354:1: ( ( rule__OrderCondition__Alternatives ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:354:1: ( ( rule__OrderCondition__Alternatives ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:355:1: ( rule__OrderCondition__Alternatives )
            {
             before(grammarAccess.getOrderConditionAccess().getAlternatives()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:356:1: ( rule__OrderCondition__Alternatives )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:356:2: rule__OrderCondition__Alternatives
            {
            pushFollow(FOLLOW_rule__OrderCondition__Alternatives_in_ruleOrderCondition694);
            rule__OrderCondition__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getOrderConditionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOrderCondition"


    // $ANTLR start "entryRuleBrackettedExpression"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:368:1: entryRuleBrackettedExpression : ruleBrackettedExpression EOF ;
    public final void entryRuleBrackettedExpression() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:369:1: ( ruleBrackettedExpression EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:370:1: ruleBrackettedExpression EOF
            {
             before(grammarAccess.getBrackettedExpressionRule()); 
            pushFollow(FOLLOW_ruleBrackettedExpression_in_entryRuleBrackettedExpression721);
            ruleBrackettedExpression();

            state._fsp--;

             after(grammarAccess.getBrackettedExpressionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBrackettedExpression728); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBrackettedExpression"


    // $ANTLR start "ruleBrackettedExpression"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:377:1: ruleBrackettedExpression : ( ( rule__BrackettedExpression__Group__0 ) ) ;
    public final void ruleBrackettedExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:381:2: ( ( ( rule__BrackettedExpression__Group__0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:382:1: ( ( rule__BrackettedExpression__Group__0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:382:1: ( ( rule__BrackettedExpression__Group__0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:383:1: ( rule__BrackettedExpression__Group__0 )
            {
             before(grammarAccess.getBrackettedExpressionAccess().getGroup()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:384:1: ( rule__BrackettedExpression__Group__0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:384:2: rule__BrackettedExpression__Group__0
            {
            pushFollow(FOLLOW_rule__BrackettedExpression__Group__0_in_ruleBrackettedExpression754);
            rule__BrackettedExpression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBrackettedExpressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBrackettedExpression"


    // $ANTLR start "entryRuleExpression"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:396:1: entryRuleExpression : ruleExpression EOF ;
    public final void entryRuleExpression() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:397:1: ( ruleExpression EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:398:1: ruleExpression EOF
            {
             before(grammarAccess.getExpressionRule()); 
            pushFollow(FOLLOW_ruleExpression_in_entryRuleExpression781);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getExpressionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleExpression788); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:405:1: ruleExpression : ( ruleVar ) ;
    public final void ruleExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:409:2: ( ( ruleVar ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:410:1: ( ruleVar )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:410:1: ( ruleVar )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:411:1: ruleVar
            {
             before(grammarAccess.getExpressionAccess().getVarParserRuleCall()); 
            pushFollow(FOLLOW_ruleVar_in_ruleExpression814);
            ruleVar();

            state._fsp--;

             after(grammarAccess.getExpressionAccess().getVarParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleWhereClause"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:424:1: entryRuleWhereClause : ruleWhereClause EOF ;
    public final void entryRuleWhereClause() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:425:1: ( ruleWhereClause EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:426:1: ruleWhereClause EOF
            {
             before(grammarAccess.getWhereClauseRule()); 
            pushFollow(FOLLOW_ruleWhereClause_in_entryRuleWhereClause840);
            ruleWhereClause();

            state._fsp--;

             after(grammarAccess.getWhereClauseRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWhereClause847); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWhereClause"


    // $ANTLR start "ruleWhereClause"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:433:1: ruleWhereClause : ( ( rule__WhereClause__Group__0 ) ) ;
    public final void ruleWhereClause() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:437:2: ( ( ( rule__WhereClause__Group__0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:438:1: ( ( rule__WhereClause__Group__0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:438:1: ( ( rule__WhereClause__Group__0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:439:1: ( rule__WhereClause__Group__0 )
            {
             before(grammarAccess.getWhereClauseAccess().getGroup()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:440:1: ( rule__WhereClause__Group__0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:440:2: rule__WhereClause__Group__0
            {
            pushFollow(FOLLOW_rule__WhereClause__Group__0_in_ruleWhereClause873);
            rule__WhereClause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getWhereClauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWhereClause"


    // $ANTLR start "entryRulePattern"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:452:1: entryRulePattern : rulePattern EOF ;
    public final void entryRulePattern() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:453:1: ( rulePattern EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:454:1: rulePattern EOF
            {
             before(grammarAccess.getPatternRule()); 
            pushFollow(FOLLOW_rulePattern_in_entryRulePattern900);
            rulePattern();

            state._fsp--;

             after(grammarAccess.getPatternRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulePattern907); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePattern"


    // $ANTLR start "rulePattern"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:461:1: rulePattern : ( ( rule__Pattern__Group__0 ) ) ;
    public final void rulePattern() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:465:2: ( ( ( rule__Pattern__Group__0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:466:1: ( ( rule__Pattern__Group__0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:466:1: ( ( rule__Pattern__Group__0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:467:1: ( rule__Pattern__Group__0 )
            {
             before(grammarAccess.getPatternAccess().getGroup()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:468:1: ( rule__Pattern__Group__0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:468:2: rule__Pattern__Group__0
            {
            pushFollow(FOLLOW_rule__Pattern__Group__0_in_rulePattern933);
            rule__Pattern__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPatternAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePattern"


    // $ANTLR start "entryRulePatternOne"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:480:1: entryRulePatternOne : rulePatternOne EOF ;
    public final void entryRulePatternOne() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:481:1: ( rulePatternOne EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:482:1: rulePatternOne EOF
            {
             before(grammarAccess.getPatternOneRule()); 
            pushFollow(FOLLOW_rulePatternOne_in_entryRulePatternOne960);
            rulePatternOne();

            state._fsp--;

             after(grammarAccess.getPatternOneRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulePatternOne967); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePatternOne"


    // $ANTLR start "rulePatternOne"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:489:1: rulePatternOne : ( ( rule__PatternOne__TriplesSameSubjectAssignment ) ) ;
    public final void rulePatternOne() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:493:2: ( ( ( rule__PatternOne__TriplesSameSubjectAssignment ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:494:1: ( ( rule__PatternOne__TriplesSameSubjectAssignment ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:494:1: ( ( rule__PatternOne__TriplesSameSubjectAssignment ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:495:1: ( rule__PatternOne__TriplesSameSubjectAssignment )
            {
             before(grammarAccess.getPatternOneAccess().getTriplesSameSubjectAssignment()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:496:1: ( rule__PatternOne__TriplesSameSubjectAssignment )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:496:2: rule__PatternOne__TriplesSameSubjectAssignment
            {
            pushFollow(FOLLOW_rule__PatternOne__TriplesSameSubjectAssignment_in_rulePatternOne993);
            rule__PatternOne__TriplesSameSubjectAssignment();

            state._fsp--;


            }

             after(grammarAccess.getPatternOneAccess().getTriplesSameSubjectAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePatternOne"


    // $ANTLR start "entryRuleTriplesSameSubject"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:508:1: entryRuleTriplesSameSubject : ruleTriplesSameSubject EOF ;
    public final void entryRuleTriplesSameSubject() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:509:1: ( ruleTriplesSameSubject EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:510:1: ruleTriplesSameSubject EOF
            {
             before(grammarAccess.getTriplesSameSubjectRule()); 
            pushFollow(FOLLOW_ruleTriplesSameSubject_in_entryRuleTriplesSameSubject1020);
            ruleTriplesSameSubject();

            state._fsp--;

             after(grammarAccess.getTriplesSameSubjectRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTriplesSameSubject1027); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTriplesSameSubject"


    // $ANTLR start "ruleTriplesSameSubject"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:517:1: ruleTriplesSameSubject : ( ( rule__TriplesSameSubject__Group__0 ) ) ;
    public final void ruleTriplesSameSubject() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:521:2: ( ( ( rule__TriplesSameSubject__Group__0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:522:1: ( ( rule__TriplesSameSubject__Group__0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:522:1: ( ( rule__TriplesSameSubject__Group__0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:523:1: ( rule__TriplesSameSubject__Group__0 )
            {
             before(grammarAccess.getTriplesSameSubjectAccess().getGroup()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:524:1: ( rule__TriplesSameSubject__Group__0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:524:2: rule__TriplesSameSubject__Group__0
            {
            pushFollow(FOLLOW_rule__TriplesSameSubject__Group__0_in_ruleTriplesSameSubject1053);
            rule__TriplesSameSubject__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTriplesSameSubjectAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTriplesSameSubject"


    // $ANTLR start "entryRulePropertyList"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:536:1: entryRulePropertyList : rulePropertyList EOF ;
    public final void entryRulePropertyList() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:537:1: ( rulePropertyList EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:538:1: rulePropertyList EOF
            {
             before(grammarAccess.getPropertyListRule()); 
            pushFollow(FOLLOW_rulePropertyList_in_entryRulePropertyList1080);
            rulePropertyList();

            state._fsp--;

             after(grammarAccess.getPropertyListRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulePropertyList1087); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePropertyList"


    // $ANTLR start "rulePropertyList"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:545:1: rulePropertyList : ( ( rule__PropertyList__Group__0 ) ) ;
    public final void rulePropertyList() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:549:2: ( ( ( rule__PropertyList__Group__0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:550:1: ( ( rule__PropertyList__Group__0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:550:1: ( ( rule__PropertyList__Group__0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:551:1: ( rule__PropertyList__Group__0 )
            {
             before(grammarAccess.getPropertyListAccess().getGroup()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:552:1: ( rule__PropertyList__Group__0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:552:2: rule__PropertyList__Group__0
            {
            pushFollow(FOLLOW_rule__PropertyList__Group__0_in_rulePropertyList1113);
            rule__PropertyList__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPropertyListAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePropertyList"


    // $ANTLR start "entryRuleGraphNode"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:564:1: entryRuleGraphNode : ruleGraphNode EOF ;
    public final void entryRuleGraphNode() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:565:1: ( ruleGraphNode EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:566:1: ruleGraphNode EOF
            {
             before(grammarAccess.getGraphNodeRule()); 
            pushFollow(FOLLOW_ruleGraphNode_in_entryRuleGraphNode1140);
            ruleGraphNode();

            state._fsp--;

             after(grammarAccess.getGraphNodeRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleGraphNode1147); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGraphNode"


    // $ANTLR start "ruleGraphNode"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:573:1: ruleGraphNode : ( ( rule__GraphNode__Alternatives ) ) ;
    public final void ruleGraphNode() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:577:2: ( ( ( rule__GraphNode__Alternatives ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:578:1: ( ( rule__GraphNode__Alternatives ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:578:1: ( ( rule__GraphNode__Alternatives ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:579:1: ( rule__GraphNode__Alternatives )
            {
             before(grammarAccess.getGraphNodeAccess().getAlternatives()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:580:1: ( rule__GraphNode__Alternatives )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:580:2: rule__GraphNode__Alternatives
            {
            pushFollow(FOLLOW_rule__GraphNode__Alternatives_in_ruleGraphNode1173);
            rule__GraphNode__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getGraphNodeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGraphNode"


    // $ANTLR start "entryRuleQuote"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:592:1: entryRuleQuote : ruleQuote EOF ;
    public final void entryRuleQuote() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:593:1: ( ruleQuote EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:594:1: ruleQuote EOF
            {
             before(grammarAccess.getQuoteRule()); 
            pushFollow(FOLLOW_ruleQuote_in_entryRuleQuote1200);
            ruleQuote();

            state._fsp--;

             after(grammarAccess.getQuoteRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQuote1207); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQuote"


    // $ANTLR start "ruleQuote"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:601:1: ruleQuote : ( ( rule__Quote__Group__0 ) ) ;
    public final void ruleQuote() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:605:2: ( ( ( rule__Quote__Group__0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:606:1: ( ( rule__Quote__Group__0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:606:1: ( ( rule__Quote__Group__0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:607:1: ( rule__Quote__Group__0 )
            {
             before(grammarAccess.getQuoteAccess().getGroup()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:608:1: ( rule__Quote__Group__0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:608:2: rule__Quote__Group__0
            {
            pushFollow(FOLLOW_rule__Quote__Group__0_in_ruleQuote1233);
            rule__Quote__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQuoteAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQuote"


    // $ANTLR start "entryRuleParameter"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:620:1: entryRuleParameter : ruleParameter EOF ;
    public final void entryRuleParameter() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:621:1: ( ruleParameter EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:622:1: ruleParameter EOF
            {
             before(grammarAccess.getParameterRule()); 
            pushFollow(FOLLOW_ruleParameter_in_entryRuleParameter1260);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getParameterRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleParameter1267); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleParameter"


    // $ANTLR start "ruleParameter"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:629:1: ruleParameter : ( ( rule__Parameter__Group__0 ) ) ;
    public final void ruleParameter() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:633:2: ( ( ( rule__Parameter__Group__0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:634:1: ( ( rule__Parameter__Group__0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:634:1: ( ( rule__Parameter__Group__0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:635:1: ( rule__Parameter__Group__0 )
            {
             before(grammarAccess.getParameterAccess().getGroup()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:636:1: ( rule__Parameter__Group__0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:636:2: rule__Parameter__Group__0
            {
            pushFollow(FOLLOW_rule__Parameter__Group__0_in_ruleParameter1293);
            rule__Parameter__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getParameterAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleParameter"


    // $ANTLR start "entryRuleBlankNode"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:648:1: entryRuleBlankNode : ruleBlankNode EOF ;
    public final void entryRuleBlankNode() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:649:1: ( ruleBlankNode EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:650:1: ruleBlankNode EOF
            {
             before(grammarAccess.getBlankNodeRule()); 
            pushFollow(FOLLOW_ruleBlankNode_in_entryRuleBlankNode1320);
            ruleBlankNode();

            state._fsp--;

             after(grammarAccess.getBlankNodeRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBlankNode1327); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBlankNode"


    // $ANTLR start "ruleBlankNode"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:657:1: ruleBlankNode : ( ( rule__BlankNode__Group__0 ) ) ;
    public final void ruleBlankNode() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:661:2: ( ( ( rule__BlankNode__Group__0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:662:1: ( ( rule__BlankNode__Group__0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:662:1: ( ( rule__BlankNode__Group__0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:663:1: ( rule__BlankNode__Group__0 )
            {
             before(grammarAccess.getBlankNodeAccess().getGroup()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:664:1: ( rule__BlankNode__Group__0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:664:2: rule__BlankNode__Group__0
            {
            pushFollow(FOLLOW_rule__BlankNode__Group__0_in_ruleBlankNode1353);
            rule__BlankNode__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBlankNodeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBlankNode"


    // $ANTLR start "entryRuleValue"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:676:1: entryRuleValue : ruleValue EOF ;
    public final void entryRuleValue() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:677:1: ( ruleValue EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:678:1: ruleValue EOF
            {
             before(grammarAccess.getValueRule()); 
            pushFollow(FOLLOW_ruleValue_in_entryRuleValue1380);
            ruleValue();

            state._fsp--;

             after(grammarAccess.getValueRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleValue1387); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleValue"


    // $ANTLR start "ruleValue"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:685:1: ruleValue : ( ( rule__Value__Alternatives ) ) ;
    public final void ruleValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:689:2: ( ( ( rule__Value__Alternatives ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:690:1: ( ( rule__Value__Alternatives ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:690:1: ( ( rule__Value__Alternatives ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:691:1: ( rule__Value__Alternatives )
            {
             before(grammarAccess.getValueAccess().getAlternatives()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:692:1: ( rule__Value__Alternatives )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:692:2: rule__Value__Alternatives
            {
            pushFollow(FOLLOW_rule__Value__Alternatives_in_ruleValue1413);
            rule__Value__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getValueAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleValue"


    // $ANTLR start "entryRuleDatasetClause"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:704:1: entryRuleDatasetClause : ruleDatasetClause EOF ;
    public final void entryRuleDatasetClause() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:705:1: ( ruleDatasetClause EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:706:1: ruleDatasetClause EOF
            {
             before(grammarAccess.getDatasetClauseRule()); 
            pushFollow(FOLLOW_ruleDatasetClause_in_entryRuleDatasetClause1440);
            ruleDatasetClause();

            state._fsp--;

             after(grammarAccess.getDatasetClauseRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDatasetClause1447); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDatasetClause"


    // $ANTLR start "ruleDatasetClause"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:713:1: ruleDatasetClause : ( ( rule__DatasetClause__Alternatives ) ) ;
    public final void ruleDatasetClause() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:717:2: ( ( ( rule__DatasetClause__Alternatives ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:718:1: ( ( rule__DatasetClause__Alternatives ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:718:1: ( ( rule__DatasetClause__Alternatives ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:719:1: ( rule__DatasetClause__Alternatives )
            {
             before(grammarAccess.getDatasetClauseAccess().getAlternatives()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:720:1: ( rule__DatasetClause__Alternatives )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:720:2: rule__DatasetClause__Alternatives
            {
            pushFollow(FOLLOW_rule__DatasetClause__Alternatives_in_ruleDatasetClause1473);
            rule__DatasetClause__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getDatasetClauseAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDatasetClause"


    // $ANTLR start "entryRuleDefaultGraphClause"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:732:1: entryRuleDefaultGraphClause : ruleDefaultGraphClause EOF ;
    public final void entryRuleDefaultGraphClause() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:733:1: ( ruleDefaultGraphClause EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:734:1: ruleDefaultGraphClause EOF
            {
             before(grammarAccess.getDefaultGraphClauseRule()); 
            pushFollow(FOLLOW_ruleDefaultGraphClause_in_entryRuleDefaultGraphClause1500);
            ruleDefaultGraphClause();

            state._fsp--;

             after(grammarAccess.getDefaultGraphClauseRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDefaultGraphClause1507); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDefaultGraphClause"


    // $ANTLR start "ruleDefaultGraphClause"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:741:1: ruleDefaultGraphClause : ( ( rule__DefaultGraphClause__IrefAssignment ) ) ;
    public final void ruleDefaultGraphClause() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:745:2: ( ( ( rule__DefaultGraphClause__IrefAssignment ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:746:1: ( ( rule__DefaultGraphClause__IrefAssignment ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:746:1: ( ( rule__DefaultGraphClause__IrefAssignment ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:747:1: ( rule__DefaultGraphClause__IrefAssignment )
            {
             before(grammarAccess.getDefaultGraphClauseAccess().getIrefAssignment()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:748:1: ( rule__DefaultGraphClause__IrefAssignment )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:748:2: rule__DefaultGraphClause__IrefAssignment
            {
            pushFollow(FOLLOW_rule__DefaultGraphClause__IrefAssignment_in_ruleDefaultGraphClause1533);
            rule__DefaultGraphClause__IrefAssignment();

            state._fsp--;


            }

             after(grammarAccess.getDefaultGraphClauseAccess().getIrefAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDefaultGraphClause"


    // $ANTLR start "entryRuleNamedGraphClause"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:760:1: entryRuleNamedGraphClause : ruleNamedGraphClause EOF ;
    public final void entryRuleNamedGraphClause() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:761:1: ( ruleNamedGraphClause EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:762:1: ruleNamedGraphClause EOF
            {
             before(grammarAccess.getNamedGraphClauseRule()); 
            pushFollow(FOLLOW_ruleNamedGraphClause_in_entryRuleNamedGraphClause1560);
            ruleNamedGraphClause();

            state._fsp--;

             after(grammarAccess.getNamedGraphClauseRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleNamedGraphClause1567); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNamedGraphClause"


    // $ANTLR start "ruleNamedGraphClause"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:769:1: ruleNamedGraphClause : ( ( rule__NamedGraphClause__Group__0 ) ) ;
    public final void ruleNamedGraphClause() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:773:2: ( ( ( rule__NamedGraphClause__Group__0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:774:1: ( ( rule__NamedGraphClause__Group__0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:774:1: ( ( rule__NamedGraphClause__Group__0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:775:1: ( rule__NamedGraphClause__Group__0 )
            {
             before(grammarAccess.getNamedGraphClauseAccess().getGroup()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:776:1: ( rule__NamedGraphClause__Group__0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:776:2: rule__NamedGraphClause__Group__0
            {
            pushFollow(FOLLOW_rule__NamedGraphClause__Group__0_in_ruleNamedGraphClause1593);
            rule__NamedGraphClause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNamedGraphClauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNamedGraphClause"


    // $ANTLR start "entryRuleSourceSelector"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:788:1: entryRuleSourceSelector : ruleSourceSelector EOF ;
    public final void entryRuleSourceSelector() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:789:1: ( ruleSourceSelector EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:790:1: ruleSourceSelector EOF
            {
             before(grammarAccess.getSourceSelectorRule()); 
            pushFollow(FOLLOW_ruleSourceSelector_in_entryRuleSourceSelector1620);
            ruleSourceSelector();

            state._fsp--;

             after(grammarAccess.getSourceSelectorRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSourceSelector1627); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSourceSelector"


    // $ANTLR start "ruleSourceSelector"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:797:1: ruleSourceSelector : ( ( rule__SourceSelector__IrefAssignment ) ) ;
    public final void ruleSourceSelector() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:801:2: ( ( ( rule__SourceSelector__IrefAssignment ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:802:1: ( ( rule__SourceSelector__IrefAssignment ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:802:1: ( ( rule__SourceSelector__IrefAssignment ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:803:1: ( rule__SourceSelector__IrefAssignment )
            {
             before(grammarAccess.getSourceSelectorAccess().getIrefAssignment()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:804:1: ( rule__SourceSelector__IrefAssignment )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:804:2: rule__SourceSelector__IrefAssignment
            {
            pushFollow(FOLLOW_rule__SourceSelector__IrefAssignment_in_ruleSourceSelector1653);
            rule__SourceSelector__IrefAssignment();

            state._fsp--;


            }

             after(grammarAccess.getSourceSelectorAccess().getIrefAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSourceSelector"


    // $ANTLR start "entryRuleVar"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:816:1: entryRuleVar : ruleVar EOF ;
    public final void entryRuleVar() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:817:1: ( ruleVar EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:818:1: ruleVar EOF
            {
             before(grammarAccess.getVarRule()); 
            pushFollow(FOLLOW_ruleVar_in_entryRuleVar1680);
            ruleVar();

            state._fsp--;

             after(grammarAccess.getVarRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVar1687); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVar"


    // $ANTLR start "ruleVar"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:825:1: ruleVar : ( ( rule__Var__Alternatives ) ) ;
    public final void ruleVar() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:829:2: ( ( ( rule__Var__Alternatives ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:830:1: ( ( rule__Var__Alternatives ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:830:1: ( ( rule__Var__Alternatives ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:831:1: ( rule__Var__Alternatives )
            {
             before(grammarAccess.getVarAccess().getAlternatives()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:832:1: ( rule__Var__Alternatives )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:832:2: rule__Var__Alternatives
            {
            pushFollow(FOLLOW_rule__Var__Alternatives_in_ruleVar1713);
            rule__Var__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getVarAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVar"


    // $ANTLR start "entryRuleVAR1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:844:1: entryRuleVAR1 : ruleVAR1 EOF ;
    public final void entryRuleVAR1() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:845:1: ( ruleVAR1 EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:846:1: ruleVAR1 EOF
            {
             before(grammarAccess.getVAR1Rule()); 
            pushFollow(FOLLOW_ruleVAR1_in_entryRuleVAR11740);
            ruleVAR1();

            state._fsp--;

             after(grammarAccess.getVAR1Rule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVAR11747); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVAR1"


    // $ANTLR start "ruleVAR1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:853:1: ruleVAR1 : ( ( rule__VAR1__Group__0 ) ) ;
    public final void ruleVAR1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:857:2: ( ( ( rule__VAR1__Group__0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:858:1: ( ( rule__VAR1__Group__0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:858:1: ( ( rule__VAR1__Group__0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:859:1: ( rule__VAR1__Group__0 )
            {
             before(grammarAccess.getVAR1Access().getGroup()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:860:1: ( rule__VAR1__Group__0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:860:2: rule__VAR1__Group__0
            {
            pushFollow(FOLLOW_rule__VAR1__Group__0_in_ruleVAR11773);
            rule__VAR1__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVAR1Access().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVAR1"


    // $ANTLR start "entryRuleVAR2"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:872:1: entryRuleVAR2 : ruleVAR2 EOF ;
    public final void entryRuleVAR2() throws RecognitionException {
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:873:1: ( ruleVAR2 EOF )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:874:1: ruleVAR2 EOF
            {
             before(grammarAccess.getVAR2Rule()); 
            pushFollow(FOLLOW_ruleVAR2_in_entryRuleVAR21800);
            ruleVAR2();

            state._fsp--;

             after(grammarAccess.getVAR2Rule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVAR21807); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVAR2"


    // $ANTLR start "ruleVAR2"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:881:1: ruleVAR2 : ( ( rule__VAR2__Group__0 ) ) ;
    public final void ruleVAR2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:885:2: ( ( ( rule__VAR2__Group__0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:886:1: ( ( rule__VAR2__Group__0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:886:1: ( ( rule__VAR2__Group__0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:887:1: ( rule__VAR2__Group__0 )
            {
             before(grammarAccess.getVAR2Access().getGroup()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:888:1: ( rule__VAR2__Group__0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:888:2: rule__VAR2__Group__0
            {
            pushFollow(FOLLOW_rule__VAR2__Group__0_in_ruleVAR21833);
            rule__VAR2__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVAR2Access().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVAR2"


    // $ANTLR start "rule__Prefix__Alternatives"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:900:1: rule__Prefix__Alternatives : ( ( ( rule__Prefix__Group_0__0 ) ) | ( ruleUnNamedPrefix ) );
    public final void rule__Prefix__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:904:1: ( ( ( rule__Prefix__Group_0__0 ) ) | ( ruleUnNamedPrefix ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==17) ) {
                int LA2_1 = input.LA(2);

                if ( (LA2_1==18) ) {
                    alt2=2;
                }
                else if ( (LA2_1==RULE_ID) ) {
                    alt2=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:905:1: ( ( rule__Prefix__Group_0__0 ) )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:905:1: ( ( rule__Prefix__Group_0__0 ) )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:906:1: ( rule__Prefix__Group_0__0 )
                    {
                     before(grammarAccess.getPrefixAccess().getGroup_0()); 
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:907:1: ( rule__Prefix__Group_0__0 )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:907:2: rule__Prefix__Group_0__0
                    {
                    pushFollow(FOLLOW_rule__Prefix__Group_0__0_in_rule__Prefix__Alternatives1869);
                    rule__Prefix__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrefixAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:911:6: ( ruleUnNamedPrefix )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:911:6: ( ruleUnNamedPrefix )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:912:1: ruleUnNamedPrefix
                    {
                     before(grammarAccess.getPrefixAccess().getUnNamedPrefixParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleUnNamedPrefix_in_rule__Prefix__Alternatives1887);
                    ruleUnNamedPrefix();

                    state._fsp--;

                     after(grammarAccess.getPrefixAccess().getUnNamedPrefixParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prefix__Alternatives"


    // $ANTLR start "rule__SelectQuery__Alternatives"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:922:1: rule__SelectQuery__Alternatives : ( ( ( rule__SelectQuery__Group_0__0 ) ) | ( ( rule__SelectQuery__LimitClauseAssignment_1 ) ) | ( ( rule__SelectQuery__OffsetClauseAssignment_2 ) ) );
    public final void rule__SelectQuery__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:926:1: ( ( ( rule__SelectQuery__Group_0__0 ) ) | ( ( rule__SelectQuery__LimitClauseAssignment_1 ) ) | ( ( rule__SelectQuery__OffsetClauseAssignment_2 ) ) )
            int alt3=3;
            switch ( input.LA(1) ) {
            case 17:
            case 19:
                {
                alt3=1;
                }
                break;
            case 25:
                {
                alt3=2;
                }
                break;
            case 24:
                {
                alt3=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:927:1: ( ( rule__SelectQuery__Group_0__0 ) )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:927:1: ( ( rule__SelectQuery__Group_0__0 ) )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:928:1: ( rule__SelectQuery__Group_0__0 )
                    {
                     before(grammarAccess.getSelectQueryAccess().getGroup_0()); 
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:929:1: ( rule__SelectQuery__Group_0__0 )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:929:2: rule__SelectQuery__Group_0__0
                    {
                    pushFollow(FOLLOW_rule__SelectQuery__Group_0__0_in_rule__SelectQuery__Alternatives1919);
                    rule__SelectQuery__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getSelectQueryAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:933:6: ( ( rule__SelectQuery__LimitClauseAssignment_1 ) )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:933:6: ( ( rule__SelectQuery__LimitClauseAssignment_1 ) )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:934:1: ( rule__SelectQuery__LimitClauseAssignment_1 )
                    {
                     before(grammarAccess.getSelectQueryAccess().getLimitClauseAssignment_1()); 
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:935:1: ( rule__SelectQuery__LimitClauseAssignment_1 )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:935:2: rule__SelectQuery__LimitClauseAssignment_1
                    {
                    pushFollow(FOLLOW_rule__SelectQuery__LimitClauseAssignment_1_in_rule__SelectQuery__Alternatives1937);
                    rule__SelectQuery__LimitClauseAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getSelectQueryAccess().getLimitClauseAssignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:939:6: ( ( rule__SelectQuery__OffsetClauseAssignment_2 ) )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:939:6: ( ( rule__SelectQuery__OffsetClauseAssignment_2 ) )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:940:1: ( rule__SelectQuery__OffsetClauseAssignment_2 )
                    {
                     before(grammarAccess.getSelectQueryAccess().getOffsetClauseAssignment_2()); 
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:941:1: ( rule__SelectQuery__OffsetClauseAssignment_2 )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:941:2: rule__SelectQuery__OffsetClauseAssignment_2
                    {
                    pushFollow(FOLLOW_rule__SelectQuery__OffsetClauseAssignment_2_in_rule__SelectQuery__Alternatives1955);
                    rule__SelectQuery__OffsetClauseAssignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getSelectQueryAccess().getOffsetClauseAssignment_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__Alternatives"


    // $ANTLR start "rule__SelectQuery__Alternatives_0_2"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:950:1: rule__SelectQuery__Alternatives_0_2 : ( ( 'DISTINCT' ) | ( 'REDUCED' ) );
    public final void rule__SelectQuery__Alternatives_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:954:1: ( ( 'DISTINCT' ) | ( 'REDUCED' ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==12) ) {
                alt4=1;
            }
            else if ( (LA4_0==13) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:955:1: ( 'DISTINCT' )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:955:1: ( 'DISTINCT' )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:956:1: 'DISTINCT'
                    {
                     before(grammarAccess.getSelectQueryAccess().getDISTINCTKeyword_0_2_0()); 
                    match(input,12,FOLLOW_12_in_rule__SelectQuery__Alternatives_0_21989); 
                     after(grammarAccess.getSelectQueryAccess().getDISTINCTKeyword_0_2_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:963:6: ( 'REDUCED' )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:963:6: ( 'REDUCED' )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:964:1: 'REDUCED'
                    {
                     before(grammarAccess.getSelectQueryAccess().getREDUCEDKeyword_0_2_1()); 
                    match(input,13,FOLLOW_13_in_rule__SelectQuery__Alternatives_0_22009); 
                     after(grammarAccess.getSelectQueryAccess().getREDUCEDKeyword_0_2_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__Alternatives_0_2"


    // $ANTLR start "rule__SelectQuery__Alternatives_0_3"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:976:1: rule__SelectQuery__Alternatives_0_3 : ( ( ( ( rule__SelectQuery__VarAssignment_0_3_0 ) ) ( ( rule__SelectQuery__VarAssignment_0_3_0 )* ) ) | ( '*' ) );
    public final void rule__SelectQuery__Alternatives_0_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:980:1: ( ( ( ( rule__SelectQuery__VarAssignment_0_3_0 ) ) ( ( rule__SelectQuery__VarAssignment_0_3_0 )* ) ) | ( '*' ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==17||LA6_0==38) ) {
                alt6=1;
            }
            else if ( (LA6_0==14) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:981:1: ( ( ( rule__SelectQuery__VarAssignment_0_3_0 ) ) ( ( rule__SelectQuery__VarAssignment_0_3_0 )* ) )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:981:1: ( ( ( rule__SelectQuery__VarAssignment_0_3_0 ) ) ( ( rule__SelectQuery__VarAssignment_0_3_0 )* ) )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:982:1: ( ( rule__SelectQuery__VarAssignment_0_3_0 ) ) ( ( rule__SelectQuery__VarAssignment_0_3_0 )* )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:982:1: ( ( rule__SelectQuery__VarAssignment_0_3_0 ) )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:983:1: ( rule__SelectQuery__VarAssignment_0_3_0 )
                    {
                     before(grammarAccess.getSelectQueryAccess().getVarAssignment_0_3_0()); 
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:984:1: ( rule__SelectQuery__VarAssignment_0_3_0 )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:984:2: rule__SelectQuery__VarAssignment_0_3_0
                    {
                    pushFollow(FOLLOW_rule__SelectQuery__VarAssignment_0_3_0_in_rule__SelectQuery__Alternatives_0_32045);
                    rule__SelectQuery__VarAssignment_0_3_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getSelectQueryAccess().getVarAssignment_0_3_0()); 

                    }

                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:987:1: ( ( rule__SelectQuery__VarAssignment_0_3_0 )* )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:988:1: ( rule__SelectQuery__VarAssignment_0_3_0 )*
                    {
                     before(grammarAccess.getSelectQueryAccess().getVarAssignment_0_3_0()); 
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:989:1: ( rule__SelectQuery__VarAssignment_0_3_0 )*
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0==17||LA5_0==38) ) {
                            alt5=1;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:989:2: rule__SelectQuery__VarAssignment_0_3_0
                    	    {
                    	    pushFollow(FOLLOW_rule__SelectQuery__VarAssignment_0_3_0_in_rule__SelectQuery__Alternatives_0_32057);
                    	    rule__SelectQuery__VarAssignment_0_3_0();

                    	    state._fsp--;


                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);

                     after(grammarAccess.getSelectQueryAccess().getVarAssignment_0_3_0()); 

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:994:6: ( '*' )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:994:6: ( '*' )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:995:1: '*'
                    {
                     before(grammarAccess.getSelectQueryAccess().getAsteriskKeyword_0_3_1()); 
                    match(input,14,FOLLOW_14_in_rule__SelectQuery__Alternatives_0_32079); 
                     after(grammarAccess.getSelectQueryAccess().getAsteriskKeyword_0_3_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__Alternatives_0_3"


    // $ANTLR start "rule__OrderCondition__Alternatives"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1007:1: rule__OrderCondition__Alternatives : ( ( ( rule__OrderCondition__Group_0__0 ) ) | ( ruleVar ) );
    public final void rule__OrderCondition__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1011:1: ( ( ( rule__OrderCondition__Group_0__0 ) ) | ( ruleVar ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( ((LA7_0>=15 && LA7_0<=16)) ) {
                alt7=1;
            }
            else if ( (LA7_0==17||LA7_0==38) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1012:1: ( ( rule__OrderCondition__Group_0__0 ) )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1012:1: ( ( rule__OrderCondition__Group_0__0 ) )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1013:1: ( rule__OrderCondition__Group_0__0 )
                    {
                     before(grammarAccess.getOrderConditionAccess().getGroup_0()); 
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1014:1: ( rule__OrderCondition__Group_0__0 )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1014:2: rule__OrderCondition__Group_0__0
                    {
                    pushFollow(FOLLOW_rule__OrderCondition__Group_0__0_in_rule__OrderCondition__Alternatives2113);
                    rule__OrderCondition__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getOrderConditionAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1018:6: ( ruleVar )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1018:6: ( ruleVar )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1019:1: ruleVar
                    {
                     before(grammarAccess.getOrderConditionAccess().getVarParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleVar_in_rule__OrderCondition__Alternatives2131);
                    ruleVar();

                    state._fsp--;

                     after(grammarAccess.getOrderConditionAccess().getVarParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderCondition__Alternatives"


    // $ANTLR start "rule__OrderCondition__Alternatives_0_0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1029:1: rule__OrderCondition__Alternatives_0_0 : ( ( 'ASC' ) | ( 'DESC' ) );
    public final void rule__OrderCondition__Alternatives_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1033:1: ( ( 'ASC' ) | ( 'DESC' ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==15) ) {
                alt8=1;
            }
            else if ( (LA8_0==16) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1034:1: ( 'ASC' )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1034:1: ( 'ASC' )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1035:1: 'ASC'
                    {
                     before(grammarAccess.getOrderConditionAccess().getASCKeyword_0_0_0()); 
                    match(input,15,FOLLOW_15_in_rule__OrderCondition__Alternatives_0_02164); 
                     after(grammarAccess.getOrderConditionAccess().getASCKeyword_0_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1042:6: ( 'DESC' )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1042:6: ( 'DESC' )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1043:1: 'DESC'
                    {
                     before(grammarAccess.getOrderConditionAccess().getDESCKeyword_0_0_1()); 
                    match(input,16,FOLLOW_16_in_rule__OrderCondition__Alternatives_0_02184); 
                     after(grammarAccess.getOrderConditionAccess().getDESCKeyword_0_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderCondition__Alternatives_0_0"


    // $ANTLR start "rule__GraphNode__Alternatives"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1055:1: rule__GraphNode__Alternatives : ( ( ruleVar ) | ( ruleValue ) | ( RULE_IRI_URI ) | ( ruleBlankNode ) | ( ruleParameter ) | ( ruleQuote ) );
    public final void rule__GraphNode__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1059:1: ( ( ruleVar ) | ( ruleValue ) | ( RULE_IRI_URI ) | ( ruleBlankNode ) | ( ruleParameter ) | ( ruleQuote ) )
            int alt9=6;
            switch ( input.LA(1) ) {
            case 17:
            case 38:
                {
                alt9=1;
                }
                break;
            case RULE_ID:
                {
                int LA9_2 = input.LA(2);

                if ( (LA9_2==EOF||(LA9_2>=RULE_INT && LA9_2<=RULE_STRING)||LA9_2==17||(LA9_2>=22 && LA9_2<=23)||LA9_2==29||(LA9_2>=32 && LA9_2<=35)||LA9_2==38) ) {
                    alt9=2;
                }
                else if ( (LA9_2==18) ) {
                    alt9=6;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 9, 2, input);

                    throw nvae;
                }
                }
                break;
            case RULE_INT:
            case RULE_STRING:
                {
                alt9=2;
                }
                break;
            case RULE_IRI_URI:
                {
                alt9=3;
                }
                break;
            case 35:
                {
                alt9=4;
                }
                break;
            case 34:
                {
                alt9=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1060:1: ( ruleVar )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1060:1: ( ruleVar )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1061:1: ruleVar
                    {
                     before(grammarAccess.getGraphNodeAccess().getVarParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleVar_in_rule__GraphNode__Alternatives2218);
                    ruleVar();

                    state._fsp--;

                     after(grammarAccess.getGraphNodeAccess().getVarParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1066:6: ( ruleValue )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1066:6: ( ruleValue )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1067:1: ruleValue
                    {
                     before(grammarAccess.getGraphNodeAccess().getValueParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleValue_in_rule__GraphNode__Alternatives2235);
                    ruleValue();

                    state._fsp--;

                     after(grammarAccess.getGraphNodeAccess().getValueParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1072:6: ( RULE_IRI_URI )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1072:6: ( RULE_IRI_URI )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1073:1: RULE_IRI_URI
                    {
                     before(grammarAccess.getGraphNodeAccess().getIRI_URITerminalRuleCall_2()); 
                    match(input,RULE_IRI_URI,FOLLOW_RULE_IRI_URI_in_rule__GraphNode__Alternatives2252); 
                     after(grammarAccess.getGraphNodeAccess().getIRI_URITerminalRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1078:6: ( ruleBlankNode )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1078:6: ( ruleBlankNode )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1079:1: ruleBlankNode
                    {
                     before(grammarAccess.getGraphNodeAccess().getBlankNodeParserRuleCall_3()); 
                    pushFollow(FOLLOW_ruleBlankNode_in_rule__GraphNode__Alternatives2269);
                    ruleBlankNode();

                    state._fsp--;

                     after(grammarAccess.getGraphNodeAccess().getBlankNodeParserRuleCall_3()); 

                    }


                    }
                    break;
                case 5 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1084:6: ( ruleParameter )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1084:6: ( ruleParameter )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1085:1: ruleParameter
                    {
                     before(grammarAccess.getGraphNodeAccess().getParameterParserRuleCall_4()); 
                    pushFollow(FOLLOW_ruleParameter_in_rule__GraphNode__Alternatives2286);
                    ruleParameter();

                    state._fsp--;

                     after(grammarAccess.getGraphNodeAccess().getParameterParserRuleCall_4()); 

                    }


                    }
                    break;
                case 6 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1090:6: ( ruleQuote )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1090:6: ( ruleQuote )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1091:1: ruleQuote
                    {
                     before(grammarAccess.getGraphNodeAccess().getQuoteParserRuleCall_5()); 
                    pushFollow(FOLLOW_ruleQuote_in_rule__GraphNode__Alternatives2303);
                    ruleQuote();

                    state._fsp--;

                     after(grammarAccess.getGraphNodeAccess().getQuoteParserRuleCall_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GraphNode__Alternatives"


    // $ANTLR start "rule__Value__Alternatives"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1101:1: rule__Value__Alternatives : ( ( ( rule__Value__NameAssignment_0 ) ) | ( ( rule__Value__StringValueAssignment_1 ) ) | ( ( rule__Value__IntegerValueAssignment_2 ) ) );
    public final void rule__Value__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1105:1: ( ( ( rule__Value__NameAssignment_0 ) ) | ( ( rule__Value__StringValueAssignment_1 ) ) | ( ( rule__Value__IntegerValueAssignment_2 ) ) )
            int alt10=3;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                alt10=1;
                }
                break;
            case RULE_STRING:
                {
                alt10=2;
                }
                break;
            case RULE_INT:
                {
                alt10=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1106:1: ( ( rule__Value__NameAssignment_0 ) )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1106:1: ( ( rule__Value__NameAssignment_0 ) )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1107:1: ( rule__Value__NameAssignment_0 )
                    {
                     before(grammarAccess.getValueAccess().getNameAssignment_0()); 
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1108:1: ( rule__Value__NameAssignment_0 )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1108:2: rule__Value__NameAssignment_0
                    {
                    pushFollow(FOLLOW_rule__Value__NameAssignment_0_in_rule__Value__Alternatives2335);
                    rule__Value__NameAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getValueAccess().getNameAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1112:6: ( ( rule__Value__StringValueAssignment_1 ) )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1112:6: ( ( rule__Value__StringValueAssignment_1 ) )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1113:1: ( rule__Value__StringValueAssignment_1 )
                    {
                     before(grammarAccess.getValueAccess().getStringValueAssignment_1()); 
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1114:1: ( rule__Value__StringValueAssignment_1 )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1114:2: rule__Value__StringValueAssignment_1
                    {
                    pushFollow(FOLLOW_rule__Value__StringValueAssignment_1_in_rule__Value__Alternatives2353);
                    rule__Value__StringValueAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getValueAccess().getStringValueAssignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1118:6: ( ( rule__Value__IntegerValueAssignment_2 ) )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1118:6: ( ( rule__Value__IntegerValueAssignment_2 ) )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1119:1: ( rule__Value__IntegerValueAssignment_2 )
                    {
                     before(grammarAccess.getValueAccess().getIntegerValueAssignment_2()); 
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1120:1: ( rule__Value__IntegerValueAssignment_2 )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1120:2: rule__Value__IntegerValueAssignment_2
                    {
                    pushFollow(FOLLOW_rule__Value__IntegerValueAssignment_2_in_rule__Value__Alternatives2371);
                    rule__Value__IntegerValueAssignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getValueAccess().getIntegerValueAssignment_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__Alternatives"


    // $ANTLR start "rule__DatasetClause__Alternatives"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1129:1: rule__DatasetClause__Alternatives : ( ( ( rule__DatasetClause__Group_0__0 ) ) | ( ( rule__DatasetClause__NamedGraphClauseAssignment_1 ) ) );
    public final void rule__DatasetClause__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1133:1: ( ( ( rule__DatasetClause__Group_0__0 ) ) | ( ( rule__DatasetClause__NamedGraphClauseAssignment_1 ) ) )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==36) ) {
                alt11=1;
            }
            else if ( (LA11_0==37) ) {
                alt11=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1134:1: ( ( rule__DatasetClause__Group_0__0 ) )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1134:1: ( ( rule__DatasetClause__Group_0__0 ) )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1135:1: ( rule__DatasetClause__Group_0__0 )
                    {
                     before(grammarAccess.getDatasetClauseAccess().getGroup_0()); 
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1136:1: ( rule__DatasetClause__Group_0__0 )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1136:2: rule__DatasetClause__Group_0__0
                    {
                    pushFollow(FOLLOW_rule__DatasetClause__Group_0__0_in_rule__DatasetClause__Alternatives2404);
                    rule__DatasetClause__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getDatasetClauseAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1140:6: ( ( rule__DatasetClause__NamedGraphClauseAssignment_1 ) )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1140:6: ( ( rule__DatasetClause__NamedGraphClauseAssignment_1 ) )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1141:1: ( rule__DatasetClause__NamedGraphClauseAssignment_1 )
                    {
                     before(grammarAccess.getDatasetClauseAccess().getNamedGraphClauseAssignment_1()); 
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1142:1: ( rule__DatasetClause__NamedGraphClauseAssignment_1 )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1142:2: rule__DatasetClause__NamedGraphClauseAssignment_1
                    {
                    pushFollow(FOLLOW_rule__DatasetClause__NamedGraphClauseAssignment_1_in_rule__DatasetClause__Alternatives2422);
                    rule__DatasetClause__NamedGraphClauseAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getDatasetClauseAccess().getNamedGraphClauseAssignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DatasetClause__Alternatives"


    // $ANTLR start "rule__Var__Alternatives"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1151:1: rule__Var__Alternatives : ( ( ruleVAR1 ) | ( ruleVAR2 ) );
    public final void rule__Var__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1155:1: ( ( ruleVAR1 ) | ( ruleVAR2 ) )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==38) ) {
                alt12=1;
            }
            else if ( (LA12_0==17) ) {
                alt12=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1156:1: ( ruleVAR1 )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1156:1: ( ruleVAR1 )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1157:1: ruleVAR1
                    {
                     before(grammarAccess.getVarAccess().getVAR1ParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleVAR1_in_rule__Var__Alternatives2455);
                    ruleVAR1();

                    state._fsp--;

                     after(grammarAccess.getVarAccess().getVAR1ParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1162:6: ( ruleVAR2 )
                    {
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1162:6: ( ruleVAR2 )
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1163:1: ruleVAR2
                    {
                     before(grammarAccess.getVarAccess().getVAR2ParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleVAR2_in_rule__Var__Alternatives2472);
                    ruleVAR2();

                    state._fsp--;

                     after(grammarAccess.getVarAccess().getVAR2ParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Var__Alternatives"


    // $ANTLR start "rule__Prefix__Group_0__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1175:1: rule__Prefix__Group_0__0 : rule__Prefix__Group_0__0__Impl rule__Prefix__Group_0__1 ;
    public final void rule__Prefix__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1179:1: ( rule__Prefix__Group_0__0__Impl rule__Prefix__Group_0__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1180:2: rule__Prefix__Group_0__0__Impl rule__Prefix__Group_0__1
            {
            pushFollow(FOLLOW_rule__Prefix__Group_0__0__Impl_in_rule__Prefix__Group_0__02502);
            rule__Prefix__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Prefix__Group_0__1_in_rule__Prefix__Group_0__02505);
            rule__Prefix__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prefix__Group_0__0"


    // $ANTLR start "rule__Prefix__Group_0__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1187:1: rule__Prefix__Group_0__0__Impl : ( 'PREFIX' ) ;
    public final void rule__Prefix__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1191:1: ( ( 'PREFIX' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1192:1: ( 'PREFIX' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1192:1: ( 'PREFIX' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1193:1: 'PREFIX'
            {
             before(grammarAccess.getPrefixAccess().getPREFIXKeyword_0_0()); 
            match(input,17,FOLLOW_17_in_rule__Prefix__Group_0__0__Impl2533); 
             after(grammarAccess.getPrefixAccess().getPREFIXKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prefix__Group_0__0__Impl"


    // $ANTLR start "rule__Prefix__Group_0__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1206:1: rule__Prefix__Group_0__1 : rule__Prefix__Group_0__1__Impl rule__Prefix__Group_0__2 ;
    public final void rule__Prefix__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1210:1: ( rule__Prefix__Group_0__1__Impl rule__Prefix__Group_0__2 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1211:2: rule__Prefix__Group_0__1__Impl rule__Prefix__Group_0__2
            {
            pushFollow(FOLLOW_rule__Prefix__Group_0__1__Impl_in_rule__Prefix__Group_0__12564);
            rule__Prefix__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Prefix__Group_0__2_in_rule__Prefix__Group_0__12567);
            rule__Prefix__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prefix__Group_0__1"


    // $ANTLR start "rule__Prefix__Group_0__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1218:1: rule__Prefix__Group_0__1__Impl : ( ( rule__Prefix__NameAssignment_0_1 ) ) ;
    public final void rule__Prefix__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1222:1: ( ( ( rule__Prefix__NameAssignment_0_1 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1223:1: ( ( rule__Prefix__NameAssignment_0_1 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1223:1: ( ( rule__Prefix__NameAssignment_0_1 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1224:1: ( rule__Prefix__NameAssignment_0_1 )
            {
             before(grammarAccess.getPrefixAccess().getNameAssignment_0_1()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1225:1: ( rule__Prefix__NameAssignment_0_1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1225:2: rule__Prefix__NameAssignment_0_1
            {
            pushFollow(FOLLOW_rule__Prefix__NameAssignment_0_1_in_rule__Prefix__Group_0__1__Impl2594);
            rule__Prefix__NameAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getPrefixAccess().getNameAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prefix__Group_0__1__Impl"


    // $ANTLR start "rule__Prefix__Group_0__2"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1235:1: rule__Prefix__Group_0__2 : rule__Prefix__Group_0__2__Impl rule__Prefix__Group_0__3 ;
    public final void rule__Prefix__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1239:1: ( rule__Prefix__Group_0__2__Impl rule__Prefix__Group_0__3 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1240:2: rule__Prefix__Group_0__2__Impl rule__Prefix__Group_0__3
            {
            pushFollow(FOLLOW_rule__Prefix__Group_0__2__Impl_in_rule__Prefix__Group_0__22624);
            rule__Prefix__Group_0__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Prefix__Group_0__3_in_rule__Prefix__Group_0__22627);
            rule__Prefix__Group_0__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prefix__Group_0__2"


    // $ANTLR start "rule__Prefix__Group_0__2__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1247:1: rule__Prefix__Group_0__2__Impl : ( ':' ) ;
    public final void rule__Prefix__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1251:1: ( ( ':' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1252:1: ( ':' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1252:1: ( ':' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1253:1: ':'
            {
             before(grammarAccess.getPrefixAccess().getColonKeyword_0_2()); 
            match(input,18,FOLLOW_18_in_rule__Prefix__Group_0__2__Impl2655); 
             after(grammarAccess.getPrefixAccess().getColonKeyword_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prefix__Group_0__2__Impl"


    // $ANTLR start "rule__Prefix__Group_0__3"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1266:1: rule__Prefix__Group_0__3 : rule__Prefix__Group_0__3__Impl ;
    public final void rule__Prefix__Group_0__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1270:1: ( rule__Prefix__Group_0__3__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1271:2: rule__Prefix__Group_0__3__Impl
            {
            pushFollow(FOLLOW_rule__Prefix__Group_0__3__Impl_in_rule__Prefix__Group_0__32686);
            rule__Prefix__Group_0__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prefix__Group_0__3"


    // $ANTLR start "rule__Prefix__Group_0__3__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1277:1: rule__Prefix__Group_0__3__Impl : ( ( rule__Prefix__IrefAssignment_0_3 ) ) ;
    public final void rule__Prefix__Group_0__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1281:1: ( ( ( rule__Prefix__IrefAssignment_0_3 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1282:1: ( ( rule__Prefix__IrefAssignment_0_3 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1282:1: ( ( rule__Prefix__IrefAssignment_0_3 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1283:1: ( rule__Prefix__IrefAssignment_0_3 )
            {
             before(grammarAccess.getPrefixAccess().getIrefAssignment_0_3()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1284:1: ( rule__Prefix__IrefAssignment_0_3 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1284:2: rule__Prefix__IrefAssignment_0_3
            {
            pushFollow(FOLLOW_rule__Prefix__IrefAssignment_0_3_in_rule__Prefix__Group_0__3__Impl2713);
            rule__Prefix__IrefAssignment_0_3();

            state._fsp--;


            }

             after(grammarAccess.getPrefixAccess().getIrefAssignment_0_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prefix__Group_0__3__Impl"


    // $ANTLR start "rule__UnNamedPrefix__Group__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1302:1: rule__UnNamedPrefix__Group__0 : rule__UnNamedPrefix__Group__0__Impl rule__UnNamedPrefix__Group__1 ;
    public final void rule__UnNamedPrefix__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1306:1: ( rule__UnNamedPrefix__Group__0__Impl rule__UnNamedPrefix__Group__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1307:2: rule__UnNamedPrefix__Group__0__Impl rule__UnNamedPrefix__Group__1
            {
            pushFollow(FOLLOW_rule__UnNamedPrefix__Group__0__Impl_in_rule__UnNamedPrefix__Group__02751);
            rule__UnNamedPrefix__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__UnNamedPrefix__Group__1_in_rule__UnNamedPrefix__Group__02754);
            rule__UnNamedPrefix__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnNamedPrefix__Group__0"


    // $ANTLR start "rule__UnNamedPrefix__Group__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1314:1: rule__UnNamedPrefix__Group__0__Impl : ( 'PREFIX' ) ;
    public final void rule__UnNamedPrefix__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1318:1: ( ( 'PREFIX' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1319:1: ( 'PREFIX' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1319:1: ( 'PREFIX' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1320:1: 'PREFIX'
            {
             before(grammarAccess.getUnNamedPrefixAccess().getPREFIXKeyword_0()); 
            match(input,17,FOLLOW_17_in_rule__UnNamedPrefix__Group__0__Impl2782); 
             after(grammarAccess.getUnNamedPrefixAccess().getPREFIXKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnNamedPrefix__Group__0__Impl"


    // $ANTLR start "rule__UnNamedPrefix__Group__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1333:1: rule__UnNamedPrefix__Group__1 : rule__UnNamedPrefix__Group__1__Impl rule__UnNamedPrefix__Group__2 ;
    public final void rule__UnNamedPrefix__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1337:1: ( rule__UnNamedPrefix__Group__1__Impl rule__UnNamedPrefix__Group__2 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1338:2: rule__UnNamedPrefix__Group__1__Impl rule__UnNamedPrefix__Group__2
            {
            pushFollow(FOLLOW_rule__UnNamedPrefix__Group__1__Impl_in_rule__UnNamedPrefix__Group__12813);
            rule__UnNamedPrefix__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__UnNamedPrefix__Group__2_in_rule__UnNamedPrefix__Group__12816);
            rule__UnNamedPrefix__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnNamedPrefix__Group__1"


    // $ANTLR start "rule__UnNamedPrefix__Group__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1345:1: rule__UnNamedPrefix__Group__1__Impl : ( ':' ) ;
    public final void rule__UnNamedPrefix__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1349:1: ( ( ':' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1350:1: ( ':' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1350:1: ( ':' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1351:1: ':'
            {
             before(grammarAccess.getUnNamedPrefixAccess().getColonKeyword_1()); 
            match(input,18,FOLLOW_18_in_rule__UnNamedPrefix__Group__1__Impl2844); 
             after(grammarAccess.getUnNamedPrefixAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnNamedPrefix__Group__1__Impl"


    // $ANTLR start "rule__UnNamedPrefix__Group__2"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1364:1: rule__UnNamedPrefix__Group__2 : rule__UnNamedPrefix__Group__2__Impl ;
    public final void rule__UnNamedPrefix__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1368:1: ( rule__UnNamedPrefix__Group__2__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1369:2: rule__UnNamedPrefix__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__UnNamedPrefix__Group__2__Impl_in_rule__UnNamedPrefix__Group__22875);
            rule__UnNamedPrefix__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnNamedPrefix__Group__2"


    // $ANTLR start "rule__UnNamedPrefix__Group__2__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1375:1: rule__UnNamedPrefix__Group__2__Impl : ( ( rule__UnNamedPrefix__IrefAssignment_2 ) ) ;
    public final void rule__UnNamedPrefix__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1379:1: ( ( ( rule__UnNamedPrefix__IrefAssignment_2 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1380:1: ( ( rule__UnNamedPrefix__IrefAssignment_2 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1380:1: ( ( rule__UnNamedPrefix__IrefAssignment_2 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1381:1: ( rule__UnNamedPrefix__IrefAssignment_2 )
            {
             before(grammarAccess.getUnNamedPrefixAccess().getIrefAssignment_2()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1382:1: ( rule__UnNamedPrefix__IrefAssignment_2 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1382:2: rule__UnNamedPrefix__IrefAssignment_2
            {
            pushFollow(FOLLOW_rule__UnNamedPrefix__IrefAssignment_2_in_rule__UnNamedPrefix__Group__2__Impl2902);
            rule__UnNamedPrefix__IrefAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getUnNamedPrefixAccess().getIrefAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnNamedPrefix__Group__2__Impl"


    // $ANTLR start "rule__SelectQuery__Group_0__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1398:1: rule__SelectQuery__Group_0__0 : rule__SelectQuery__Group_0__0__Impl rule__SelectQuery__Group_0__1 ;
    public final void rule__SelectQuery__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1402:1: ( rule__SelectQuery__Group_0__0__Impl rule__SelectQuery__Group_0__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1403:2: rule__SelectQuery__Group_0__0__Impl rule__SelectQuery__Group_0__1
            {
            pushFollow(FOLLOW_rule__SelectQuery__Group_0__0__Impl_in_rule__SelectQuery__Group_0__02938);
            rule__SelectQuery__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__SelectQuery__Group_0__1_in_rule__SelectQuery__Group_0__02941);
            rule__SelectQuery__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__Group_0__0"


    // $ANTLR start "rule__SelectQuery__Group_0__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1410:1: rule__SelectQuery__Group_0__0__Impl : ( ( rule__SelectQuery__PrefixAssignment_0_0 )* ) ;
    public final void rule__SelectQuery__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1414:1: ( ( ( rule__SelectQuery__PrefixAssignment_0_0 )* ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1415:1: ( ( rule__SelectQuery__PrefixAssignment_0_0 )* )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1415:1: ( ( rule__SelectQuery__PrefixAssignment_0_0 )* )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1416:1: ( rule__SelectQuery__PrefixAssignment_0_0 )*
            {
             before(grammarAccess.getSelectQueryAccess().getPrefixAssignment_0_0()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1417:1: ( rule__SelectQuery__PrefixAssignment_0_0 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==17) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1417:2: rule__SelectQuery__PrefixAssignment_0_0
            	    {
            	    pushFollow(FOLLOW_rule__SelectQuery__PrefixAssignment_0_0_in_rule__SelectQuery__Group_0__0__Impl2968);
            	    rule__SelectQuery__PrefixAssignment_0_0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

             after(grammarAccess.getSelectQueryAccess().getPrefixAssignment_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__Group_0__0__Impl"


    // $ANTLR start "rule__SelectQuery__Group_0__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1427:1: rule__SelectQuery__Group_0__1 : rule__SelectQuery__Group_0__1__Impl rule__SelectQuery__Group_0__2 ;
    public final void rule__SelectQuery__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1431:1: ( rule__SelectQuery__Group_0__1__Impl rule__SelectQuery__Group_0__2 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1432:2: rule__SelectQuery__Group_0__1__Impl rule__SelectQuery__Group_0__2
            {
            pushFollow(FOLLOW_rule__SelectQuery__Group_0__1__Impl_in_rule__SelectQuery__Group_0__12999);
            rule__SelectQuery__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__SelectQuery__Group_0__2_in_rule__SelectQuery__Group_0__13002);
            rule__SelectQuery__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__Group_0__1"


    // $ANTLR start "rule__SelectQuery__Group_0__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1439:1: rule__SelectQuery__Group_0__1__Impl : ( 'SELECT' ) ;
    public final void rule__SelectQuery__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1443:1: ( ( 'SELECT' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1444:1: ( 'SELECT' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1444:1: ( 'SELECT' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1445:1: 'SELECT'
            {
             before(grammarAccess.getSelectQueryAccess().getSELECTKeyword_0_1()); 
            match(input,19,FOLLOW_19_in_rule__SelectQuery__Group_0__1__Impl3030); 
             after(grammarAccess.getSelectQueryAccess().getSELECTKeyword_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__Group_0__1__Impl"


    // $ANTLR start "rule__SelectQuery__Group_0__2"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1458:1: rule__SelectQuery__Group_0__2 : rule__SelectQuery__Group_0__2__Impl rule__SelectQuery__Group_0__3 ;
    public final void rule__SelectQuery__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1462:1: ( rule__SelectQuery__Group_0__2__Impl rule__SelectQuery__Group_0__3 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1463:2: rule__SelectQuery__Group_0__2__Impl rule__SelectQuery__Group_0__3
            {
            pushFollow(FOLLOW_rule__SelectQuery__Group_0__2__Impl_in_rule__SelectQuery__Group_0__23061);
            rule__SelectQuery__Group_0__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__SelectQuery__Group_0__3_in_rule__SelectQuery__Group_0__23064);
            rule__SelectQuery__Group_0__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__Group_0__2"


    // $ANTLR start "rule__SelectQuery__Group_0__2__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1470:1: rule__SelectQuery__Group_0__2__Impl : ( ( rule__SelectQuery__Alternatives_0_2 )? ) ;
    public final void rule__SelectQuery__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1474:1: ( ( ( rule__SelectQuery__Alternatives_0_2 )? ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1475:1: ( ( rule__SelectQuery__Alternatives_0_2 )? )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1475:1: ( ( rule__SelectQuery__Alternatives_0_2 )? )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1476:1: ( rule__SelectQuery__Alternatives_0_2 )?
            {
             before(grammarAccess.getSelectQueryAccess().getAlternatives_0_2()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1477:1: ( rule__SelectQuery__Alternatives_0_2 )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( ((LA14_0>=12 && LA14_0<=13)) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1477:2: rule__SelectQuery__Alternatives_0_2
                    {
                    pushFollow(FOLLOW_rule__SelectQuery__Alternatives_0_2_in_rule__SelectQuery__Group_0__2__Impl3091);
                    rule__SelectQuery__Alternatives_0_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSelectQueryAccess().getAlternatives_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__Group_0__2__Impl"


    // $ANTLR start "rule__SelectQuery__Group_0__3"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1487:1: rule__SelectQuery__Group_0__3 : rule__SelectQuery__Group_0__3__Impl rule__SelectQuery__Group_0__4 ;
    public final void rule__SelectQuery__Group_0__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1491:1: ( rule__SelectQuery__Group_0__3__Impl rule__SelectQuery__Group_0__4 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1492:2: rule__SelectQuery__Group_0__3__Impl rule__SelectQuery__Group_0__4
            {
            pushFollow(FOLLOW_rule__SelectQuery__Group_0__3__Impl_in_rule__SelectQuery__Group_0__33122);
            rule__SelectQuery__Group_0__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__SelectQuery__Group_0__4_in_rule__SelectQuery__Group_0__33125);
            rule__SelectQuery__Group_0__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__Group_0__3"


    // $ANTLR start "rule__SelectQuery__Group_0__3__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1499:1: rule__SelectQuery__Group_0__3__Impl : ( ( rule__SelectQuery__Alternatives_0_3 ) ) ;
    public final void rule__SelectQuery__Group_0__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1503:1: ( ( ( rule__SelectQuery__Alternatives_0_3 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1504:1: ( ( rule__SelectQuery__Alternatives_0_3 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1504:1: ( ( rule__SelectQuery__Alternatives_0_3 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1505:1: ( rule__SelectQuery__Alternatives_0_3 )
            {
             before(grammarAccess.getSelectQueryAccess().getAlternatives_0_3()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1506:1: ( rule__SelectQuery__Alternatives_0_3 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1506:2: rule__SelectQuery__Alternatives_0_3
            {
            pushFollow(FOLLOW_rule__SelectQuery__Alternatives_0_3_in_rule__SelectQuery__Group_0__3__Impl3152);
            rule__SelectQuery__Alternatives_0_3();

            state._fsp--;


            }

             after(grammarAccess.getSelectQueryAccess().getAlternatives_0_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__Group_0__3__Impl"


    // $ANTLR start "rule__SelectQuery__Group_0__4"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1516:1: rule__SelectQuery__Group_0__4 : rule__SelectQuery__Group_0__4__Impl rule__SelectQuery__Group_0__5 ;
    public final void rule__SelectQuery__Group_0__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1520:1: ( rule__SelectQuery__Group_0__4__Impl rule__SelectQuery__Group_0__5 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1521:2: rule__SelectQuery__Group_0__4__Impl rule__SelectQuery__Group_0__5
            {
            pushFollow(FOLLOW_rule__SelectQuery__Group_0__4__Impl_in_rule__SelectQuery__Group_0__43182);
            rule__SelectQuery__Group_0__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__SelectQuery__Group_0__5_in_rule__SelectQuery__Group_0__43185);
            rule__SelectQuery__Group_0__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__Group_0__4"


    // $ANTLR start "rule__SelectQuery__Group_0__4__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1528:1: rule__SelectQuery__Group_0__4__Impl : ( ( rule__SelectQuery__DatasetClauseAssignment_0_4 )* ) ;
    public final void rule__SelectQuery__Group_0__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1532:1: ( ( ( rule__SelectQuery__DatasetClauseAssignment_0_4 )* ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1533:1: ( ( rule__SelectQuery__DatasetClauseAssignment_0_4 )* )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1533:1: ( ( rule__SelectQuery__DatasetClauseAssignment_0_4 )* )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1534:1: ( rule__SelectQuery__DatasetClauseAssignment_0_4 )*
            {
             before(grammarAccess.getSelectQueryAccess().getDatasetClauseAssignment_0_4()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1535:1: ( rule__SelectQuery__DatasetClauseAssignment_0_4 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( ((LA15_0>=36 && LA15_0<=37)) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1535:2: rule__SelectQuery__DatasetClauseAssignment_0_4
            	    {
            	    pushFollow(FOLLOW_rule__SelectQuery__DatasetClauseAssignment_0_4_in_rule__SelectQuery__Group_0__4__Impl3212);
            	    rule__SelectQuery__DatasetClauseAssignment_0_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getSelectQueryAccess().getDatasetClauseAssignment_0_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__Group_0__4__Impl"


    // $ANTLR start "rule__SelectQuery__Group_0__5"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1545:1: rule__SelectQuery__Group_0__5 : rule__SelectQuery__Group_0__5__Impl rule__SelectQuery__Group_0__6 ;
    public final void rule__SelectQuery__Group_0__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1549:1: ( rule__SelectQuery__Group_0__5__Impl rule__SelectQuery__Group_0__6 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1550:2: rule__SelectQuery__Group_0__5__Impl rule__SelectQuery__Group_0__6
            {
            pushFollow(FOLLOW_rule__SelectQuery__Group_0__5__Impl_in_rule__SelectQuery__Group_0__53243);
            rule__SelectQuery__Group_0__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__SelectQuery__Group_0__6_in_rule__SelectQuery__Group_0__53246);
            rule__SelectQuery__Group_0__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__Group_0__5"


    // $ANTLR start "rule__SelectQuery__Group_0__5__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1557:1: rule__SelectQuery__Group_0__5__Impl : ( ( rule__SelectQuery__WhereClauseAssignment_0_5 ) ) ;
    public final void rule__SelectQuery__Group_0__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1561:1: ( ( ( rule__SelectQuery__WhereClauseAssignment_0_5 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1562:1: ( ( rule__SelectQuery__WhereClauseAssignment_0_5 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1562:1: ( ( rule__SelectQuery__WhereClauseAssignment_0_5 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1563:1: ( rule__SelectQuery__WhereClauseAssignment_0_5 )
            {
             before(grammarAccess.getSelectQueryAccess().getWhereClauseAssignment_0_5()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1564:1: ( rule__SelectQuery__WhereClauseAssignment_0_5 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1564:2: rule__SelectQuery__WhereClauseAssignment_0_5
            {
            pushFollow(FOLLOW_rule__SelectQuery__WhereClauseAssignment_0_5_in_rule__SelectQuery__Group_0__5__Impl3273);
            rule__SelectQuery__WhereClauseAssignment_0_5();

            state._fsp--;


            }

             after(grammarAccess.getSelectQueryAccess().getWhereClauseAssignment_0_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__Group_0__5__Impl"


    // $ANTLR start "rule__SelectQuery__Group_0__6"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1574:1: rule__SelectQuery__Group_0__6 : rule__SelectQuery__Group_0__6__Impl ;
    public final void rule__SelectQuery__Group_0__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1578:1: ( rule__SelectQuery__Group_0__6__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1579:2: rule__SelectQuery__Group_0__6__Impl
            {
            pushFollow(FOLLOW_rule__SelectQuery__Group_0__6__Impl_in_rule__SelectQuery__Group_0__63303);
            rule__SelectQuery__Group_0__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__Group_0__6"


    // $ANTLR start "rule__SelectQuery__Group_0__6__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1585:1: rule__SelectQuery__Group_0__6__Impl : ( ( rule__SelectQuery__OrderClauseAssignment_0_6 )? ) ;
    public final void rule__SelectQuery__Group_0__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1589:1: ( ( ( rule__SelectQuery__OrderClauseAssignment_0_6 )? ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1590:1: ( ( rule__SelectQuery__OrderClauseAssignment_0_6 )? )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1590:1: ( ( rule__SelectQuery__OrderClauseAssignment_0_6 )? )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1591:1: ( rule__SelectQuery__OrderClauseAssignment_0_6 )?
            {
             before(grammarAccess.getSelectQueryAccess().getOrderClauseAssignment_0_6()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1592:1: ( rule__SelectQuery__OrderClauseAssignment_0_6 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==26) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1592:2: rule__SelectQuery__OrderClauseAssignment_0_6
                    {
                    pushFollow(FOLLOW_rule__SelectQuery__OrderClauseAssignment_0_6_in_rule__SelectQuery__Group_0__6__Impl3330);
                    rule__SelectQuery__OrderClauseAssignment_0_6();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSelectQueryAccess().getOrderClauseAssignment_0_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__Group_0__6__Impl"


    // $ANTLR start "rule__Filter__Group__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1616:1: rule__Filter__Group__0 : rule__Filter__Group__0__Impl rule__Filter__Group__1 ;
    public final void rule__Filter__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1620:1: ( rule__Filter__Group__0__Impl rule__Filter__Group__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1621:2: rule__Filter__Group__0__Impl rule__Filter__Group__1
            {
            pushFollow(FOLLOW_rule__Filter__Group__0__Impl_in_rule__Filter__Group__03375);
            rule__Filter__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Filter__Group__1_in_rule__Filter__Group__03378);
            rule__Filter__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Filter__Group__0"


    // $ANTLR start "rule__Filter__Group__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1628:1: rule__Filter__Group__0__Impl : ( 'FILTER' ) ;
    public final void rule__Filter__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1632:1: ( ( 'FILTER' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1633:1: ( 'FILTER' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1633:1: ( 'FILTER' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1634:1: 'FILTER'
            {
             before(grammarAccess.getFilterAccess().getFILTERKeyword_0()); 
            match(input,20,FOLLOW_20_in_rule__Filter__Group__0__Impl3406); 
             after(grammarAccess.getFilterAccess().getFILTERKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Filter__Group__0__Impl"


    // $ANTLR start "rule__Filter__Group__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1647:1: rule__Filter__Group__1 : rule__Filter__Group__1__Impl ;
    public final void rule__Filter__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1651:1: ( rule__Filter__Group__1__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1652:2: rule__Filter__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Filter__Group__1__Impl_in_rule__Filter__Group__13437);
            rule__Filter__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Filter__Group__1"


    // $ANTLR start "rule__Filter__Group__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1658:1: rule__Filter__Group__1__Impl : ( ( rule__Filter__ExpressionFilterAssignment_1 ) ) ;
    public final void rule__Filter__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1662:1: ( ( ( rule__Filter__ExpressionFilterAssignment_1 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1663:1: ( ( rule__Filter__ExpressionFilterAssignment_1 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1663:1: ( ( rule__Filter__ExpressionFilterAssignment_1 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1664:1: ( rule__Filter__ExpressionFilterAssignment_1 )
            {
             before(grammarAccess.getFilterAccess().getExpressionFilterAssignment_1()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1665:1: ( rule__Filter__ExpressionFilterAssignment_1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1665:2: rule__Filter__ExpressionFilterAssignment_1
            {
            pushFollow(FOLLOW_rule__Filter__ExpressionFilterAssignment_1_in_rule__Filter__Group__1__Impl3464);
            rule__Filter__ExpressionFilterAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getFilterAccess().getExpressionFilterAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Filter__Group__1__Impl"


    // $ANTLR start "rule__ExpressionFilter__Group__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1679:1: rule__ExpressionFilter__Group__0 : rule__ExpressionFilter__Group__0__Impl rule__ExpressionFilter__Group__1 ;
    public final void rule__ExpressionFilter__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1683:1: ( rule__ExpressionFilter__Group__0__Impl rule__ExpressionFilter__Group__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1684:2: rule__ExpressionFilter__Group__0__Impl rule__ExpressionFilter__Group__1
            {
            pushFollow(FOLLOW_rule__ExpressionFilter__Group__0__Impl_in_rule__ExpressionFilter__Group__03498);
            rule__ExpressionFilter__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ExpressionFilter__Group__1_in_rule__ExpressionFilter__Group__03501);
            rule__ExpressionFilter__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__Group__0"


    // $ANTLR start "rule__ExpressionFilter__Group__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1691:1: rule__ExpressionFilter__Group__0__Impl : ( ( rule__ExpressionFilter__NameAssignment_0 ) ) ;
    public final void rule__ExpressionFilter__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1695:1: ( ( ( rule__ExpressionFilter__NameAssignment_0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1696:1: ( ( rule__ExpressionFilter__NameAssignment_0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1696:1: ( ( rule__ExpressionFilter__NameAssignment_0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1697:1: ( rule__ExpressionFilter__NameAssignment_0 )
            {
             before(grammarAccess.getExpressionFilterAccess().getNameAssignment_0()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1698:1: ( rule__ExpressionFilter__NameAssignment_0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1698:2: rule__ExpressionFilter__NameAssignment_0
            {
            pushFollow(FOLLOW_rule__ExpressionFilter__NameAssignment_0_in_rule__ExpressionFilter__Group__0__Impl3528);
            rule__ExpressionFilter__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getExpressionFilterAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__Group__0__Impl"


    // $ANTLR start "rule__ExpressionFilter__Group__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1708:1: rule__ExpressionFilter__Group__1 : rule__ExpressionFilter__Group__1__Impl rule__ExpressionFilter__Group__2 ;
    public final void rule__ExpressionFilter__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1712:1: ( rule__ExpressionFilter__Group__1__Impl rule__ExpressionFilter__Group__2 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1713:2: rule__ExpressionFilter__Group__1__Impl rule__ExpressionFilter__Group__2
            {
            pushFollow(FOLLOW_rule__ExpressionFilter__Group__1__Impl_in_rule__ExpressionFilter__Group__13558);
            rule__ExpressionFilter__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ExpressionFilter__Group__2_in_rule__ExpressionFilter__Group__13561);
            rule__ExpressionFilter__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__Group__1"


    // $ANTLR start "rule__ExpressionFilter__Group__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1720:1: rule__ExpressionFilter__Group__1__Impl : ( '(' ) ;
    public final void rule__ExpressionFilter__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1724:1: ( ( '(' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1725:1: ( '(' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1725:1: ( '(' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1726:1: '('
            {
             before(grammarAccess.getExpressionFilterAccess().getLeftParenthesisKeyword_1()); 
            match(input,21,FOLLOW_21_in_rule__ExpressionFilter__Group__1__Impl3589); 
             after(grammarAccess.getExpressionFilterAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__Group__1__Impl"


    // $ANTLR start "rule__ExpressionFilter__Group__2"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1739:1: rule__ExpressionFilter__Group__2 : rule__ExpressionFilter__Group__2__Impl rule__ExpressionFilter__Group__3 ;
    public final void rule__ExpressionFilter__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1743:1: ( rule__ExpressionFilter__Group__2__Impl rule__ExpressionFilter__Group__3 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1744:2: rule__ExpressionFilter__Group__2__Impl rule__ExpressionFilter__Group__3
            {
            pushFollow(FOLLOW_rule__ExpressionFilter__Group__2__Impl_in_rule__ExpressionFilter__Group__23620);
            rule__ExpressionFilter__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ExpressionFilter__Group__3_in_rule__ExpressionFilter__Group__23623);
            rule__ExpressionFilter__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__Group__2"


    // $ANTLR start "rule__ExpressionFilter__Group__2__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1751:1: rule__ExpressionFilter__Group__2__Impl : ( ( rule__ExpressionFilter__ParametersAssignment_2 ) ) ;
    public final void rule__ExpressionFilter__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1755:1: ( ( ( rule__ExpressionFilter__ParametersAssignment_2 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1756:1: ( ( rule__ExpressionFilter__ParametersAssignment_2 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1756:1: ( ( rule__ExpressionFilter__ParametersAssignment_2 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1757:1: ( rule__ExpressionFilter__ParametersAssignment_2 )
            {
             before(grammarAccess.getExpressionFilterAccess().getParametersAssignment_2()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1758:1: ( rule__ExpressionFilter__ParametersAssignment_2 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1758:2: rule__ExpressionFilter__ParametersAssignment_2
            {
            pushFollow(FOLLOW_rule__ExpressionFilter__ParametersAssignment_2_in_rule__ExpressionFilter__Group__2__Impl3650);
            rule__ExpressionFilter__ParametersAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getExpressionFilterAccess().getParametersAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__Group__2__Impl"


    // $ANTLR start "rule__ExpressionFilter__Group__3"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1768:1: rule__ExpressionFilter__Group__3 : rule__ExpressionFilter__Group__3__Impl rule__ExpressionFilter__Group__4 ;
    public final void rule__ExpressionFilter__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1772:1: ( rule__ExpressionFilter__Group__3__Impl rule__ExpressionFilter__Group__4 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1773:2: rule__ExpressionFilter__Group__3__Impl rule__ExpressionFilter__Group__4
            {
            pushFollow(FOLLOW_rule__ExpressionFilter__Group__3__Impl_in_rule__ExpressionFilter__Group__33680);
            rule__ExpressionFilter__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ExpressionFilter__Group__4_in_rule__ExpressionFilter__Group__33683);
            rule__ExpressionFilter__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__Group__3"


    // $ANTLR start "rule__ExpressionFilter__Group__3__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1780:1: rule__ExpressionFilter__Group__3__Impl : ( ( rule__ExpressionFilter__Group_3__0 )? ) ;
    public final void rule__ExpressionFilter__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1784:1: ( ( ( rule__ExpressionFilter__Group_3__0 )? ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1785:1: ( ( rule__ExpressionFilter__Group_3__0 )? )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1785:1: ( ( rule__ExpressionFilter__Group_3__0 )? )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1786:1: ( rule__ExpressionFilter__Group_3__0 )?
            {
             before(grammarAccess.getExpressionFilterAccess().getGroup_3()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1787:1: ( rule__ExpressionFilter__Group_3__0 )?
            int alt17=2;
            alt17 = dfa17.predict(input);
            switch (alt17) {
                case 1 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1787:2: rule__ExpressionFilter__Group_3__0
                    {
                    pushFollow(FOLLOW_rule__ExpressionFilter__Group_3__0_in_rule__ExpressionFilter__Group__3__Impl3710);
                    rule__ExpressionFilter__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getExpressionFilterAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__Group__3__Impl"


    // $ANTLR start "rule__ExpressionFilter__Group__4"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1797:1: rule__ExpressionFilter__Group__4 : rule__ExpressionFilter__Group__4__Impl rule__ExpressionFilter__Group__5 ;
    public final void rule__ExpressionFilter__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1801:1: ( rule__ExpressionFilter__Group__4__Impl rule__ExpressionFilter__Group__5 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1802:2: rule__ExpressionFilter__Group__4__Impl rule__ExpressionFilter__Group__5
            {
            pushFollow(FOLLOW_rule__ExpressionFilter__Group__4__Impl_in_rule__ExpressionFilter__Group__43741);
            rule__ExpressionFilter__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ExpressionFilter__Group__5_in_rule__ExpressionFilter__Group__43744);
            rule__ExpressionFilter__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__Group__4"


    // $ANTLR start "rule__ExpressionFilter__Group__4__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1809:1: rule__ExpressionFilter__Group__4__Impl : ( ( rule__ExpressionFilter__Group_4__0 )? ) ;
    public final void rule__ExpressionFilter__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1813:1: ( ( ( rule__ExpressionFilter__Group_4__0 )? ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1814:1: ( ( rule__ExpressionFilter__Group_4__0 )? )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1814:1: ( ( rule__ExpressionFilter__Group_4__0 )? )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1815:1: ( rule__ExpressionFilter__Group_4__0 )?
            {
             before(grammarAccess.getExpressionFilterAccess().getGroup_4()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1816:1: ( rule__ExpressionFilter__Group_4__0 )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==23) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1816:2: rule__ExpressionFilter__Group_4__0
                    {
                    pushFollow(FOLLOW_rule__ExpressionFilter__Group_4__0_in_rule__ExpressionFilter__Group__4__Impl3771);
                    rule__ExpressionFilter__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getExpressionFilterAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__Group__4__Impl"


    // $ANTLR start "rule__ExpressionFilter__Group__5"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1826:1: rule__ExpressionFilter__Group__5 : rule__ExpressionFilter__Group__5__Impl ;
    public final void rule__ExpressionFilter__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1830:1: ( rule__ExpressionFilter__Group__5__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1831:2: rule__ExpressionFilter__Group__5__Impl
            {
            pushFollow(FOLLOW_rule__ExpressionFilter__Group__5__Impl_in_rule__ExpressionFilter__Group__53802);
            rule__ExpressionFilter__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__Group__5"


    // $ANTLR start "rule__ExpressionFilter__Group__5__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1837:1: rule__ExpressionFilter__Group__5__Impl : ( ')' ) ;
    public final void rule__ExpressionFilter__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1841:1: ( ( ')' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1842:1: ( ')' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1842:1: ( ')' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1843:1: ')'
            {
             before(grammarAccess.getExpressionFilterAccess().getRightParenthesisKeyword_5()); 
            match(input,22,FOLLOW_22_in_rule__ExpressionFilter__Group__5__Impl3830); 
             after(grammarAccess.getExpressionFilterAccess().getRightParenthesisKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__Group__5__Impl"


    // $ANTLR start "rule__ExpressionFilter__Group_3__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1868:1: rule__ExpressionFilter__Group_3__0 : rule__ExpressionFilter__Group_3__0__Impl rule__ExpressionFilter__Group_3__1 ;
    public final void rule__ExpressionFilter__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1872:1: ( rule__ExpressionFilter__Group_3__0__Impl rule__ExpressionFilter__Group_3__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1873:2: rule__ExpressionFilter__Group_3__0__Impl rule__ExpressionFilter__Group_3__1
            {
            pushFollow(FOLLOW_rule__ExpressionFilter__Group_3__0__Impl_in_rule__ExpressionFilter__Group_3__03873);
            rule__ExpressionFilter__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ExpressionFilter__Group_3__1_in_rule__ExpressionFilter__Group_3__03876);
            rule__ExpressionFilter__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__Group_3__0"


    // $ANTLR start "rule__ExpressionFilter__Group_3__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1880:1: rule__ExpressionFilter__Group_3__0__Impl : ( ',' ) ;
    public final void rule__ExpressionFilter__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1884:1: ( ( ',' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1885:1: ( ',' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1885:1: ( ',' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1886:1: ','
            {
             before(grammarAccess.getExpressionFilterAccess().getCommaKeyword_3_0()); 
            match(input,23,FOLLOW_23_in_rule__ExpressionFilter__Group_3__0__Impl3904); 
             after(grammarAccess.getExpressionFilterAccess().getCommaKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__Group_3__0__Impl"


    // $ANTLR start "rule__ExpressionFilter__Group_3__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1899:1: rule__ExpressionFilter__Group_3__1 : rule__ExpressionFilter__Group_3__1__Impl ;
    public final void rule__ExpressionFilter__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1903:1: ( rule__ExpressionFilter__Group_3__1__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1904:2: rule__ExpressionFilter__Group_3__1__Impl
            {
            pushFollow(FOLLOW_rule__ExpressionFilter__Group_3__1__Impl_in_rule__ExpressionFilter__Group_3__13935);
            rule__ExpressionFilter__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__Group_3__1"


    // $ANTLR start "rule__ExpressionFilter__Group_3__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1910:1: rule__ExpressionFilter__Group_3__1__Impl : ( ( rule__ExpressionFilter__ParametersAssignment_3_1 ) ) ;
    public final void rule__ExpressionFilter__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1914:1: ( ( ( rule__ExpressionFilter__ParametersAssignment_3_1 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1915:1: ( ( rule__ExpressionFilter__ParametersAssignment_3_1 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1915:1: ( ( rule__ExpressionFilter__ParametersAssignment_3_1 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1916:1: ( rule__ExpressionFilter__ParametersAssignment_3_1 )
            {
             before(grammarAccess.getExpressionFilterAccess().getParametersAssignment_3_1()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1917:1: ( rule__ExpressionFilter__ParametersAssignment_3_1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1917:2: rule__ExpressionFilter__ParametersAssignment_3_1
            {
            pushFollow(FOLLOW_rule__ExpressionFilter__ParametersAssignment_3_1_in_rule__ExpressionFilter__Group_3__1__Impl3962);
            rule__ExpressionFilter__ParametersAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getExpressionFilterAccess().getParametersAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__Group_3__1__Impl"


    // $ANTLR start "rule__ExpressionFilter__Group_4__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1931:1: rule__ExpressionFilter__Group_4__0 : rule__ExpressionFilter__Group_4__0__Impl rule__ExpressionFilter__Group_4__1 ;
    public final void rule__ExpressionFilter__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1935:1: ( rule__ExpressionFilter__Group_4__0__Impl rule__ExpressionFilter__Group_4__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1936:2: rule__ExpressionFilter__Group_4__0__Impl rule__ExpressionFilter__Group_4__1
            {
            pushFollow(FOLLOW_rule__ExpressionFilter__Group_4__0__Impl_in_rule__ExpressionFilter__Group_4__03996);
            rule__ExpressionFilter__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ExpressionFilter__Group_4__1_in_rule__ExpressionFilter__Group_4__03999);
            rule__ExpressionFilter__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__Group_4__0"


    // $ANTLR start "rule__ExpressionFilter__Group_4__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1943:1: rule__ExpressionFilter__Group_4__0__Impl : ( ',' ) ;
    public final void rule__ExpressionFilter__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1947:1: ( ( ',' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1948:1: ( ',' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1948:1: ( ',' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1949:1: ','
            {
             before(grammarAccess.getExpressionFilterAccess().getCommaKeyword_4_0()); 
            match(input,23,FOLLOW_23_in_rule__ExpressionFilter__Group_4__0__Impl4027); 
             after(grammarAccess.getExpressionFilterAccess().getCommaKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__Group_4__0__Impl"


    // $ANTLR start "rule__ExpressionFilter__Group_4__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1962:1: rule__ExpressionFilter__Group_4__1 : rule__ExpressionFilter__Group_4__1__Impl ;
    public final void rule__ExpressionFilter__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1966:1: ( rule__ExpressionFilter__Group_4__1__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1967:2: rule__ExpressionFilter__Group_4__1__Impl
            {
            pushFollow(FOLLOW_rule__ExpressionFilter__Group_4__1__Impl_in_rule__ExpressionFilter__Group_4__14058);
            rule__ExpressionFilter__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__Group_4__1"


    // $ANTLR start "rule__ExpressionFilter__Group_4__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1973:1: rule__ExpressionFilter__Group_4__1__Impl : ( ( rule__ExpressionFilter__ParametersAssignment_4_1 ) ) ;
    public final void rule__ExpressionFilter__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1977:1: ( ( ( rule__ExpressionFilter__ParametersAssignment_4_1 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1978:1: ( ( rule__ExpressionFilter__ParametersAssignment_4_1 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1978:1: ( ( rule__ExpressionFilter__ParametersAssignment_4_1 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1979:1: ( rule__ExpressionFilter__ParametersAssignment_4_1 )
            {
             before(grammarAccess.getExpressionFilterAccess().getParametersAssignment_4_1()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1980:1: ( rule__ExpressionFilter__ParametersAssignment_4_1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1980:2: rule__ExpressionFilter__ParametersAssignment_4_1
            {
            pushFollow(FOLLOW_rule__ExpressionFilter__ParametersAssignment_4_1_in_rule__ExpressionFilter__Group_4__1__Impl4085);
            rule__ExpressionFilter__ParametersAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getExpressionFilterAccess().getParametersAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__Group_4__1__Impl"


    // $ANTLR start "rule__OffsetClause__Group__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1994:1: rule__OffsetClause__Group__0 : rule__OffsetClause__Group__0__Impl rule__OffsetClause__Group__1 ;
    public final void rule__OffsetClause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1998:1: ( rule__OffsetClause__Group__0__Impl rule__OffsetClause__Group__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:1999:2: rule__OffsetClause__Group__0__Impl rule__OffsetClause__Group__1
            {
            pushFollow(FOLLOW_rule__OffsetClause__Group__0__Impl_in_rule__OffsetClause__Group__04119);
            rule__OffsetClause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__OffsetClause__Group__1_in_rule__OffsetClause__Group__04122);
            rule__OffsetClause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OffsetClause__Group__0"


    // $ANTLR start "rule__OffsetClause__Group__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2006:1: rule__OffsetClause__Group__0__Impl : ( 'OFFSET' ) ;
    public final void rule__OffsetClause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2010:1: ( ( 'OFFSET' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2011:1: ( 'OFFSET' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2011:1: ( 'OFFSET' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2012:1: 'OFFSET'
            {
             before(grammarAccess.getOffsetClauseAccess().getOFFSETKeyword_0()); 
            match(input,24,FOLLOW_24_in_rule__OffsetClause__Group__0__Impl4150); 
             after(grammarAccess.getOffsetClauseAccess().getOFFSETKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OffsetClause__Group__0__Impl"


    // $ANTLR start "rule__OffsetClause__Group__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2025:1: rule__OffsetClause__Group__1 : rule__OffsetClause__Group__1__Impl ;
    public final void rule__OffsetClause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2029:1: ( rule__OffsetClause__Group__1__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2030:2: rule__OffsetClause__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__OffsetClause__Group__1__Impl_in_rule__OffsetClause__Group__14181);
            rule__OffsetClause__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OffsetClause__Group__1"


    // $ANTLR start "rule__OffsetClause__Group__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2036:1: rule__OffsetClause__Group__1__Impl : ( ruleINTEGER ) ;
    public final void rule__OffsetClause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2040:1: ( ( ruleINTEGER ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2041:1: ( ruleINTEGER )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2041:1: ( ruleINTEGER )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2042:1: ruleINTEGER
            {
             before(grammarAccess.getOffsetClauseAccess().getINTEGERParserRuleCall_1()); 
            pushFollow(FOLLOW_ruleINTEGER_in_rule__OffsetClause__Group__1__Impl4208);
            ruleINTEGER();

            state._fsp--;

             after(grammarAccess.getOffsetClauseAccess().getINTEGERParserRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OffsetClause__Group__1__Impl"


    // $ANTLR start "rule__LimitClause__Group__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2057:1: rule__LimitClause__Group__0 : rule__LimitClause__Group__0__Impl rule__LimitClause__Group__1 ;
    public final void rule__LimitClause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2061:1: ( rule__LimitClause__Group__0__Impl rule__LimitClause__Group__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2062:2: rule__LimitClause__Group__0__Impl rule__LimitClause__Group__1
            {
            pushFollow(FOLLOW_rule__LimitClause__Group__0__Impl_in_rule__LimitClause__Group__04241);
            rule__LimitClause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__LimitClause__Group__1_in_rule__LimitClause__Group__04244);
            rule__LimitClause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LimitClause__Group__0"


    // $ANTLR start "rule__LimitClause__Group__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2069:1: rule__LimitClause__Group__0__Impl : ( 'LIMIT' ) ;
    public final void rule__LimitClause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2073:1: ( ( 'LIMIT' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2074:1: ( 'LIMIT' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2074:1: ( 'LIMIT' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2075:1: 'LIMIT'
            {
             before(grammarAccess.getLimitClauseAccess().getLIMITKeyword_0()); 
            match(input,25,FOLLOW_25_in_rule__LimitClause__Group__0__Impl4272); 
             after(grammarAccess.getLimitClauseAccess().getLIMITKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LimitClause__Group__0__Impl"


    // $ANTLR start "rule__LimitClause__Group__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2088:1: rule__LimitClause__Group__1 : rule__LimitClause__Group__1__Impl ;
    public final void rule__LimitClause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2092:1: ( rule__LimitClause__Group__1__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2093:2: rule__LimitClause__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__LimitClause__Group__1__Impl_in_rule__LimitClause__Group__14303);
            rule__LimitClause__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LimitClause__Group__1"


    // $ANTLR start "rule__LimitClause__Group__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2099:1: rule__LimitClause__Group__1__Impl : ( ruleINTEGER ) ;
    public final void rule__LimitClause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2103:1: ( ( ruleINTEGER ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2104:1: ( ruleINTEGER )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2104:1: ( ruleINTEGER )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2105:1: ruleINTEGER
            {
             before(grammarAccess.getLimitClauseAccess().getINTEGERParserRuleCall_1()); 
            pushFollow(FOLLOW_ruleINTEGER_in_rule__LimitClause__Group__1__Impl4330);
            ruleINTEGER();

            state._fsp--;

             after(grammarAccess.getLimitClauseAccess().getINTEGERParserRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LimitClause__Group__1__Impl"


    // $ANTLR start "rule__OrderClause__Group__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2120:1: rule__OrderClause__Group__0 : rule__OrderClause__Group__0__Impl rule__OrderClause__Group__1 ;
    public final void rule__OrderClause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2124:1: ( rule__OrderClause__Group__0__Impl rule__OrderClause__Group__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2125:2: rule__OrderClause__Group__0__Impl rule__OrderClause__Group__1
            {
            pushFollow(FOLLOW_rule__OrderClause__Group__0__Impl_in_rule__OrderClause__Group__04363);
            rule__OrderClause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__OrderClause__Group__1_in_rule__OrderClause__Group__04366);
            rule__OrderClause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderClause__Group__0"


    // $ANTLR start "rule__OrderClause__Group__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2132:1: rule__OrderClause__Group__0__Impl : ( 'ORDER BY' ) ;
    public final void rule__OrderClause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2136:1: ( ( 'ORDER BY' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2137:1: ( 'ORDER BY' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2137:1: ( 'ORDER BY' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2138:1: 'ORDER BY'
            {
             before(grammarAccess.getOrderClauseAccess().getORDERBYKeyword_0()); 
            match(input,26,FOLLOW_26_in_rule__OrderClause__Group__0__Impl4394); 
             after(grammarAccess.getOrderClauseAccess().getORDERBYKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderClause__Group__0__Impl"


    // $ANTLR start "rule__OrderClause__Group__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2151:1: rule__OrderClause__Group__1 : rule__OrderClause__Group__1__Impl ;
    public final void rule__OrderClause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2155:1: ( rule__OrderClause__Group__1__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2156:2: rule__OrderClause__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__OrderClause__Group__1__Impl_in_rule__OrderClause__Group__14425);
            rule__OrderClause__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderClause__Group__1"


    // $ANTLR start "rule__OrderClause__Group__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2162:1: rule__OrderClause__Group__1__Impl : ( ( ( rule__OrderClause__OrderConditionAssignment_1 ) ) ( ( rule__OrderClause__OrderConditionAssignment_1 )* ) ) ;
    public final void rule__OrderClause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2166:1: ( ( ( ( rule__OrderClause__OrderConditionAssignment_1 ) ) ( ( rule__OrderClause__OrderConditionAssignment_1 )* ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2167:1: ( ( ( rule__OrderClause__OrderConditionAssignment_1 ) ) ( ( rule__OrderClause__OrderConditionAssignment_1 )* ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2167:1: ( ( ( rule__OrderClause__OrderConditionAssignment_1 ) ) ( ( rule__OrderClause__OrderConditionAssignment_1 )* ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2168:1: ( ( rule__OrderClause__OrderConditionAssignment_1 ) ) ( ( rule__OrderClause__OrderConditionAssignment_1 )* )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2168:1: ( ( rule__OrderClause__OrderConditionAssignment_1 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2169:1: ( rule__OrderClause__OrderConditionAssignment_1 )
            {
             before(grammarAccess.getOrderClauseAccess().getOrderConditionAssignment_1()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2170:1: ( rule__OrderClause__OrderConditionAssignment_1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2170:2: rule__OrderClause__OrderConditionAssignment_1
            {
            pushFollow(FOLLOW_rule__OrderClause__OrderConditionAssignment_1_in_rule__OrderClause__Group__1__Impl4454);
            rule__OrderClause__OrderConditionAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getOrderClauseAccess().getOrderConditionAssignment_1()); 

            }

            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2173:1: ( ( rule__OrderClause__OrderConditionAssignment_1 )* )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2174:1: ( rule__OrderClause__OrderConditionAssignment_1 )*
            {
             before(grammarAccess.getOrderClauseAccess().getOrderConditionAssignment_1()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2175:1: ( rule__OrderClause__OrderConditionAssignment_1 )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==17) ) {
                    int LA19_2 = input.LA(2);

                    if ( (LA19_2==18) ) {
                        int LA19_4 = input.LA(3);

                        if ( (LA19_4==RULE_IRI_URI) ) {
                            int LA19_6 = input.LA(4);

                            if ( (LA19_6==18) ) {
                                alt19=1;
                            }


                        }


                    }
                    else if ( (LA19_2==RULE_ID) ) {
                        int LA19_5 = input.LA(3);

                        if ( (LA19_5==18) ) {
                            int LA19_7 = input.LA(4);

                            if ( (LA19_7==RULE_IRI_URI) ) {
                                int LA19_8 = input.LA(5);

                                if ( (LA19_8==18) ) {
                                    alt19=1;
                                }


                            }


                        }


                    }


                }
                else if ( ((LA19_0>=15 && LA19_0<=16)||LA19_0==38) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2175:2: rule__OrderClause__OrderConditionAssignment_1
            	    {
            	    pushFollow(FOLLOW_rule__OrderClause__OrderConditionAssignment_1_in_rule__OrderClause__Group__1__Impl4466);
            	    rule__OrderClause__OrderConditionAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

             after(grammarAccess.getOrderClauseAccess().getOrderConditionAssignment_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderClause__Group__1__Impl"


    // $ANTLR start "rule__OrderCondition__Group_0__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2190:1: rule__OrderCondition__Group_0__0 : rule__OrderCondition__Group_0__0__Impl rule__OrderCondition__Group_0__1 ;
    public final void rule__OrderCondition__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2194:1: ( rule__OrderCondition__Group_0__0__Impl rule__OrderCondition__Group_0__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2195:2: rule__OrderCondition__Group_0__0__Impl rule__OrderCondition__Group_0__1
            {
            pushFollow(FOLLOW_rule__OrderCondition__Group_0__0__Impl_in_rule__OrderCondition__Group_0__04503);
            rule__OrderCondition__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__OrderCondition__Group_0__1_in_rule__OrderCondition__Group_0__04506);
            rule__OrderCondition__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderCondition__Group_0__0"


    // $ANTLR start "rule__OrderCondition__Group_0__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2202:1: rule__OrderCondition__Group_0__0__Impl : ( ( rule__OrderCondition__Alternatives_0_0 ) ) ;
    public final void rule__OrderCondition__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2206:1: ( ( ( rule__OrderCondition__Alternatives_0_0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2207:1: ( ( rule__OrderCondition__Alternatives_0_0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2207:1: ( ( rule__OrderCondition__Alternatives_0_0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2208:1: ( rule__OrderCondition__Alternatives_0_0 )
            {
             before(grammarAccess.getOrderConditionAccess().getAlternatives_0_0()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2209:1: ( rule__OrderCondition__Alternatives_0_0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2209:2: rule__OrderCondition__Alternatives_0_0
            {
            pushFollow(FOLLOW_rule__OrderCondition__Alternatives_0_0_in_rule__OrderCondition__Group_0__0__Impl4533);
            rule__OrderCondition__Alternatives_0_0();

            state._fsp--;


            }

             after(grammarAccess.getOrderConditionAccess().getAlternatives_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderCondition__Group_0__0__Impl"


    // $ANTLR start "rule__OrderCondition__Group_0__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2219:1: rule__OrderCondition__Group_0__1 : rule__OrderCondition__Group_0__1__Impl ;
    public final void rule__OrderCondition__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2223:1: ( rule__OrderCondition__Group_0__1__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2224:2: rule__OrderCondition__Group_0__1__Impl
            {
            pushFollow(FOLLOW_rule__OrderCondition__Group_0__1__Impl_in_rule__OrderCondition__Group_0__14563);
            rule__OrderCondition__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderCondition__Group_0__1"


    // $ANTLR start "rule__OrderCondition__Group_0__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2230:1: rule__OrderCondition__Group_0__1__Impl : ( ( rule__OrderCondition__BrackettedExpressionAssignment_0_1 ) ) ;
    public final void rule__OrderCondition__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2234:1: ( ( ( rule__OrderCondition__BrackettedExpressionAssignment_0_1 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2235:1: ( ( rule__OrderCondition__BrackettedExpressionAssignment_0_1 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2235:1: ( ( rule__OrderCondition__BrackettedExpressionAssignment_0_1 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2236:1: ( rule__OrderCondition__BrackettedExpressionAssignment_0_1 )
            {
             before(grammarAccess.getOrderConditionAccess().getBrackettedExpressionAssignment_0_1()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2237:1: ( rule__OrderCondition__BrackettedExpressionAssignment_0_1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2237:2: rule__OrderCondition__BrackettedExpressionAssignment_0_1
            {
            pushFollow(FOLLOW_rule__OrderCondition__BrackettedExpressionAssignment_0_1_in_rule__OrderCondition__Group_0__1__Impl4590);
            rule__OrderCondition__BrackettedExpressionAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getOrderConditionAccess().getBrackettedExpressionAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderCondition__Group_0__1__Impl"


    // $ANTLR start "rule__BrackettedExpression__Group__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2251:1: rule__BrackettedExpression__Group__0 : rule__BrackettedExpression__Group__0__Impl rule__BrackettedExpression__Group__1 ;
    public final void rule__BrackettedExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2255:1: ( rule__BrackettedExpression__Group__0__Impl rule__BrackettedExpression__Group__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2256:2: rule__BrackettedExpression__Group__0__Impl rule__BrackettedExpression__Group__1
            {
            pushFollow(FOLLOW_rule__BrackettedExpression__Group__0__Impl_in_rule__BrackettedExpression__Group__04624);
            rule__BrackettedExpression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__BrackettedExpression__Group__1_in_rule__BrackettedExpression__Group__04627);
            rule__BrackettedExpression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BrackettedExpression__Group__0"


    // $ANTLR start "rule__BrackettedExpression__Group__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2263:1: rule__BrackettedExpression__Group__0__Impl : ( '(' ) ;
    public final void rule__BrackettedExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2267:1: ( ( '(' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2268:1: ( '(' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2268:1: ( '(' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2269:1: '('
            {
             before(grammarAccess.getBrackettedExpressionAccess().getLeftParenthesisKeyword_0()); 
            match(input,21,FOLLOW_21_in_rule__BrackettedExpression__Group__0__Impl4655); 
             after(grammarAccess.getBrackettedExpressionAccess().getLeftParenthesisKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BrackettedExpression__Group__0__Impl"


    // $ANTLR start "rule__BrackettedExpression__Group__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2282:1: rule__BrackettedExpression__Group__1 : rule__BrackettedExpression__Group__1__Impl rule__BrackettedExpression__Group__2 ;
    public final void rule__BrackettedExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2286:1: ( rule__BrackettedExpression__Group__1__Impl rule__BrackettedExpression__Group__2 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2287:2: rule__BrackettedExpression__Group__1__Impl rule__BrackettedExpression__Group__2
            {
            pushFollow(FOLLOW_rule__BrackettedExpression__Group__1__Impl_in_rule__BrackettedExpression__Group__14686);
            rule__BrackettedExpression__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__BrackettedExpression__Group__2_in_rule__BrackettedExpression__Group__14689);
            rule__BrackettedExpression__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BrackettedExpression__Group__1"


    // $ANTLR start "rule__BrackettedExpression__Group__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2294:1: rule__BrackettedExpression__Group__1__Impl : ( ( rule__BrackettedExpression__ExpressionAssignment_1 ) ) ;
    public final void rule__BrackettedExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2298:1: ( ( ( rule__BrackettedExpression__ExpressionAssignment_1 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2299:1: ( ( rule__BrackettedExpression__ExpressionAssignment_1 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2299:1: ( ( rule__BrackettedExpression__ExpressionAssignment_1 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2300:1: ( rule__BrackettedExpression__ExpressionAssignment_1 )
            {
             before(grammarAccess.getBrackettedExpressionAccess().getExpressionAssignment_1()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2301:1: ( rule__BrackettedExpression__ExpressionAssignment_1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2301:2: rule__BrackettedExpression__ExpressionAssignment_1
            {
            pushFollow(FOLLOW_rule__BrackettedExpression__ExpressionAssignment_1_in_rule__BrackettedExpression__Group__1__Impl4716);
            rule__BrackettedExpression__ExpressionAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getBrackettedExpressionAccess().getExpressionAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BrackettedExpression__Group__1__Impl"


    // $ANTLR start "rule__BrackettedExpression__Group__2"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2311:1: rule__BrackettedExpression__Group__2 : rule__BrackettedExpression__Group__2__Impl ;
    public final void rule__BrackettedExpression__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2315:1: ( rule__BrackettedExpression__Group__2__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2316:2: rule__BrackettedExpression__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__BrackettedExpression__Group__2__Impl_in_rule__BrackettedExpression__Group__24746);
            rule__BrackettedExpression__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BrackettedExpression__Group__2"


    // $ANTLR start "rule__BrackettedExpression__Group__2__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2322:1: rule__BrackettedExpression__Group__2__Impl : ( ')' ) ;
    public final void rule__BrackettedExpression__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2326:1: ( ( ')' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2327:1: ( ')' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2327:1: ( ')' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2328:1: ')'
            {
             before(grammarAccess.getBrackettedExpressionAccess().getRightParenthesisKeyword_2()); 
            match(input,22,FOLLOW_22_in_rule__BrackettedExpression__Group__2__Impl4774); 
             after(grammarAccess.getBrackettedExpressionAccess().getRightParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BrackettedExpression__Group__2__Impl"


    // $ANTLR start "rule__WhereClause__Group__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2347:1: rule__WhereClause__Group__0 : rule__WhereClause__Group__0__Impl rule__WhereClause__Group__1 ;
    public final void rule__WhereClause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2351:1: ( rule__WhereClause__Group__0__Impl rule__WhereClause__Group__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2352:2: rule__WhereClause__Group__0__Impl rule__WhereClause__Group__1
            {
            pushFollow(FOLLOW_rule__WhereClause__Group__0__Impl_in_rule__WhereClause__Group__04811);
            rule__WhereClause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WhereClause__Group__1_in_rule__WhereClause__Group__04814);
            rule__WhereClause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group__0"


    // $ANTLR start "rule__WhereClause__Group__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2359:1: rule__WhereClause__Group__0__Impl : ( 'WHERE' ) ;
    public final void rule__WhereClause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2363:1: ( ( 'WHERE' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2364:1: ( 'WHERE' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2364:1: ( 'WHERE' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2365:1: 'WHERE'
            {
             before(grammarAccess.getWhereClauseAccess().getWHEREKeyword_0()); 
            match(input,27,FOLLOW_27_in_rule__WhereClause__Group__0__Impl4842); 
             after(grammarAccess.getWhereClauseAccess().getWHEREKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group__0__Impl"


    // $ANTLR start "rule__WhereClause__Group__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2378:1: rule__WhereClause__Group__1 : rule__WhereClause__Group__1__Impl rule__WhereClause__Group__2 ;
    public final void rule__WhereClause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2382:1: ( rule__WhereClause__Group__1__Impl rule__WhereClause__Group__2 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2383:2: rule__WhereClause__Group__1__Impl rule__WhereClause__Group__2
            {
            pushFollow(FOLLOW_rule__WhereClause__Group__1__Impl_in_rule__WhereClause__Group__14873);
            rule__WhereClause__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WhereClause__Group__2_in_rule__WhereClause__Group__14876);
            rule__WhereClause__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group__1"


    // $ANTLR start "rule__WhereClause__Group__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2390:1: rule__WhereClause__Group__1__Impl : ( '{' ) ;
    public final void rule__WhereClause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2394:1: ( ( '{' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2395:1: ( '{' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2395:1: ( '{' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2396:1: '{'
            {
             before(grammarAccess.getWhereClauseAccess().getLeftCurlyBracketKeyword_1()); 
            match(input,28,FOLLOW_28_in_rule__WhereClause__Group__1__Impl4904); 
             after(grammarAccess.getWhereClauseAccess().getLeftCurlyBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group__1__Impl"


    // $ANTLR start "rule__WhereClause__Group__2"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2409:1: rule__WhereClause__Group__2 : rule__WhereClause__Group__2__Impl rule__WhereClause__Group__3 ;
    public final void rule__WhereClause__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2413:1: ( rule__WhereClause__Group__2__Impl rule__WhereClause__Group__3 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2414:2: rule__WhereClause__Group__2__Impl rule__WhereClause__Group__3
            {
            pushFollow(FOLLOW_rule__WhereClause__Group__2__Impl_in_rule__WhereClause__Group__24935);
            rule__WhereClause__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WhereClause__Group__3_in_rule__WhereClause__Group__24938);
            rule__WhereClause__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group__2"


    // $ANTLR start "rule__WhereClause__Group__2__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2421:1: rule__WhereClause__Group__2__Impl : ( ( rule__WhereClause__Group_2__0 ) ) ;
    public final void rule__WhereClause__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2425:1: ( ( ( rule__WhereClause__Group_2__0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2426:1: ( ( rule__WhereClause__Group_2__0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2426:1: ( ( rule__WhereClause__Group_2__0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2427:1: ( rule__WhereClause__Group_2__0 )
            {
             before(grammarAccess.getWhereClauseAccess().getGroup_2()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2428:1: ( rule__WhereClause__Group_2__0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2428:2: rule__WhereClause__Group_2__0
            {
            pushFollow(FOLLOW_rule__WhereClause__Group_2__0_in_rule__WhereClause__Group__2__Impl4965);
            rule__WhereClause__Group_2__0();

            state._fsp--;


            }

             after(grammarAccess.getWhereClauseAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group__2__Impl"


    // $ANTLR start "rule__WhereClause__Group__3"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2438:1: rule__WhereClause__Group__3 : rule__WhereClause__Group__3__Impl rule__WhereClause__Group__4 ;
    public final void rule__WhereClause__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2442:1: ( rule__WhereClause__Group__3__Impl rule__WhereClause__Group__4 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2443:2: rule__WhereClause__Group__3__Impl rule__WhereClause__Group__4
            {
            pushFollow(FOLLOW_rule__WhereClause__Group__3__Impl_in_rule__WhereClause__Group__34995);
            rule__WhereClause__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WhereClause__Group__4_in_rule__WhereClause__Group__34998);
            rule__WhereClause__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group__3"


    // $ANTLR start "rule__WhereClause__Group__3__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2450:1: rule__WhereClause__Group__3__Impl : ( ( rule__WhereClause__FilterAssignment_3 )? ) ;
    public final void rule__WhereClause__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2454:1: ( ( ( rule__WhereClause__FilterAssignment_3 )? ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2455:1: ( ( rule__WhereClause__FilterAssignment_3 )? )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2455:1: ( ( rule__WhereClause__FilterAssignment_3 )? )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2456:1: ( rule__WhereClause__FilterAssignment_3 )?
            {
             before(grammarAccess.getWhereClauseAccess().getFilterAssignment_3()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2457:1: ( rule__WhereClause__FilterAssignment_3 )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==20) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2457:2: rule__WhereClause__FilterAssignment_3
                    {
                    pushFollow(FOLLOW_rule__WhereClause__FilterAssignment_3_in_rule__WhereClause__Group__3__Impl5025);
                    rule__WhereClause__FilterAssignment_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getWhereClauseAccess().getFilterAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group__3__Impl"


    // $ANTLR start "rule__WhereClause__Group__4"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2467:1: rule__WhereClause__Group__4 : rule__WhereClause__Group__4__Impl ;
    public final void rule__WhereClause__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2471:1: ( rule__WhereClause__Group__4__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2472:2: rule__WhereClause__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__WhereClause__Group__4__Impl_in_rule__WhereClause__Group__45056);
            rule__WhereClause__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group__4"


    // $ANTLR start "rule__WhereClause__Group__4__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2478:1: rule__WhereClause__Group__4__Impl : ( '}' ) ;
    public final void rule__WhereClause__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2482:1: ( ( '}' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2483:1: ( '}' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2483:1: ( '}' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2484:1: '}'
            {
             before(grammarAccess.getWhereClauseAccess().getRightCurlyBracketKeyword_4()); 
            match(input,29,FOLLOW_29_in_rule__WhereClause__Group__4__Impl5084); 
             after(grammarAccess.getWhereClauseAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group__4__Impl"


    // $ANTLR start "rule__WhereClause__Group_2__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2507:1: rule__WhereClause__Group_2__0 : rule__WhereClause__Group_2__0__Impl rule__WhereClause__Group_2__1 ;
    public final void rule__WhereClause__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2511:1: ( rule__WhereClause__Group_2__0__Impl rule__WhereClause__Group_2__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2512:2: rule__WhereClause__Group_2__0__Impl rule__WhereClause__Group_2__1
            {
            pushFollow(FOLLOW_rule__WhereClause__Group_2__0__Impl_in_rule__WhereClause__Group_2__05125);
            rule__WhereClause__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WhereClause__Group_2__1_in_rule__WhereClause__Group_2__05128);
            rule__WhereClause__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group_2__0"


    // $ANTLR start "rule__WhereClause__Group_2__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2519:1: rule__WhereClause__Group_2__0__Impl : ( ( rule__WhereClause__PatternAssignment_2_0 ) ) ;
    public final void rule__WhereClause__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2523:1: ( ( ( rule__WhereClause__PatternAssignment_2_0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2524:1: ( ( rule__WhereClause__PatternAssignment_2_0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2524:1: ( ( rule__WhereClause__PatternAssignment_2_0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2525:1: ( rule__WhereClause__PatternAssignment_2_0 )
            {
             before(grammarAccess.getWhereClauseAccess().getPatternAssignment_2_0()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2526:1: ( rule__WhereClause__PatternAssignment_2_0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2526:2: rule__WhereClause__PatternAssignment_2_0
            {
            pushFollow(FOLLOW_rule__WhereClause__PatternAssignment_2_0_in_rule__WhereClause__Group_2__0__Impl5155);
            rule__WhereClause__PatternAssignment_2_0();

            state._fsp--;


            }

             after(grammarAccess.getWhereClauseAccess().getPatternAssignment_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group_2__0__Impl"


    // $ANTLR start "rule__WhereClause__Group_2__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2536:1: rule__WhereClause__Group_2__1 : rule__WhereClause__Group_2__1__Impl rule__WhereClause__Group_2__2 ;
    public final void rule__WhereClause__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2540:1: ( rule__WhereClause__Group_2__1__Impl rule__WhereClause__Group_2__2 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2541:2: rule__WhereClause__Group_2__1__Impl rule__WhereClause__Group_2__2
            {
            pushFollow(FOLLOW_rule__WhereClause__Group_2__1__Impl_in_rule__WhereClause__Group_2__15185);
            rule__WhereClause__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WhereClause__Group_2__2_in_rule__WhereClause__Group_2__15188);
            rule__WhereClause__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group_2__1"


    // $ANTLR start "rule__WhereClause__Group_2__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2548:1: rule__WhereClause__Group_2__1__Impl : ( ( rule__WhereClause__Group_2_1__0 )? ) ;
    public final void rule__WhereClause__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2552:1: ( ( ( rule__WhereClause__Group_2_1__0 )? ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2553:1: ( ( rule__WhereClause__Group_2_1__0 )? )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2553:1: ( ( rule__WhereClause__Group_2_1__0 )? )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2554:1: ( rule__WhereClause__Group_2_1__0 )?
            {
             before(grammarAccess.getWhereClauseAccess().getGroup_2_1()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2555:1: ( rule__WhereClause__Group_2_1__0 )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==30) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2555:2: rule__WhereClause__Group_2_1__0
                    {
                    pushFollow(FOLLOW_rule__WhereClause__Group_2_1__0_in_rule__WhereClause__Group_2__1__Impl5215);
                    rule__WhereClause__Group_2_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getWhereClauseAccess().getGroup_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group_2__1__Impl"


    // $ANTLR start "rule__WhereClause__Group_2__2"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2565:1: rule__WhereClause__Group_2__2 : rule__WhereClause__Group_2__2__Impl ;
    public final void rule__WhereClause__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2569:1: ( rule__WhereClause__Group_2__2__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2570:2: rule__WhereClause__Group_2__2__Impl
            {
            pushFollow(FOLLOW_rule__WhereClause__Group_2__2__Impl_in_rule__WhereClause__Group_2__25246);
            rule__WhereClause__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group_2__2"


    // $ANTLR start "rule__WhereClause__Group_2__2__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2576:1: rule__WhereClause__Group_2__2__Impl : ( ( rule__WhereClause__Group_2_2__0 )? ) ;
    public final void rule__WhereClause__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2580:1: ( ( ( rule__WhereClause__Group_2_2__0 )? ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2581:1: ( ( rule__WhereClause__Group_2_2__0 )? )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2581:1: ( ( rule__WhereClause__Group_2_2__0 )? )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2582:1: ( rule__WhereClause__Group_2_2__0 )?
            {
             before(grammarAccess.getWhereClauseAccess().getGroup_2_2()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2583:1: ( rule__WhereClause__Group_2_2__0 )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==31) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2583:2: rule__WhereClause__Group_2_2__0
                    {
                    pushFollow(FOLLOW_rule__WhereClause__Group_2_2__0_in_rule__WhereClause__Group_2__2__Impl5273);
                    rule__WhereClause__Group_2_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getWhereClauseAccess().getGroup_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group_2__2__Impl"


    // $ANTLR start "rule__WhereClause__Group_2_1__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2599:1: rule__WhereClause__Group_2_1__0 : rule__WhereClause__Group_2_1__0__Impl rule__WhereClause__Group_2_1__1 ;
    public final void rule__WhereClause__Group_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2603:1: ( rule__WhereClause__Group_2_1__0__Impl rule__WhereClause__Group_2_1__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2604:2: rule__WhereClause__Group_2_1__0__Impl rule__WhereClause__Group_2_1__1
            {
            pushFollow(FOLLOW_rule__WhereClause__Group_2_1__0__Impl_in_rule__WhereClause__Group_2_1__05310);
            rule__WhereClause__Group_2_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WhereClause__Group_2_1__1_in_rule__WhereClause__Group_2_1__05313);
            rule__WhereClause__Group_2_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group_2_1__0"


    // $ANTLR start "rule__WhereClause__Group_2_1__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2611:1: rule__WhereClause__Group_2_1__0__Impl : ( 'UNION' ) ;
    public final void rule__WhereClause__Group_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2615:1: ( ( 'UNION' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2616:1: ( 'UNION' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2616:1: ( 'UNION' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2617:1: 'UNION'
            {
             before(grammarAccess.getWhereClauseAccess().getUNIONKeyword_2_1_0()); 
            match(input,30,FOLLOW_30_in_rule__WhereClause__Group_2_1__0__Impl5341); 
             after(grammarAccess.getWhereClauseAccess().getUNIONKeyword_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group_2_1__0__Impl"


    // $ANTLR start "rule__WhereClause__Group_2_1__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2630:1: rule__WhereClause__Group_2_1__1 : rule__WhereClause__Group_2_1__1__Impl ;
    public final void rule__WhereClause__Group_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2634:1: ( rule__WhereClause__Group_2_1__1__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2635:2: rule__WhereClause__Group_2_1__1__Impl
            {
            pushFollow(FOLLOW_rule__WhereClause__Group_2_1__1__Impl_in_rule__WhereClause__Group_2_1__15372);
            rule__WhereClause__Group_2_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group_2_1__1"


    // $ANTLR start "rule__WhereClause__Group_2_1__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2641:1: rule__WhereClause__Group_2_1__1__Impl : ( ( rule__WhereClause__PatternAssignment_2_1_1 ) ) ;
    public final void rule__WhereClause__Group_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2645:1: ( ( ( rule__WhereClause__PatternAssignment_2_1_1 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2646:1: ( ( rule__WhereClause__PatternAssignment_2_1_1 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2646:1: ( ( rule__WhereClause__PatternAssignment_2_1_1 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2647:1: ( rule__WhereClause__PatternAssignment_2_1_1 )
            {
             before(grammarAccess.getWhereClauseAccess().getPatternAssignment_2_1_1()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2648:1: ( rule__WhereClause__PatternAssignment_2_1_1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2648:2: rule__WhereClause__PatternAssignment_2_1_1
            {
            pushFollow(FOLLOW_rule__WhereClause__PatternAssignment_2_1_1_in_rule__WhereClause__Group_2_1__1__Impl5399);
            rule__WhereClause__PatternAssignment_2_1_1();

            state._fsp--;


            }

             after(grammarAccess.getWhereClauseAccess().getPatternAssignment_2_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group_2_1__1__Impl"


    // $ANTLR start "rule__WhereClause__Group_2_2__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2662:1: rule__WhereClause__Group_2_2__0 : rule__WhereClause__Group_2_2__0__Impl rule__WhereClause__Group_2_2__1 ;
    public final void rule__WhereClause__Group_2_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2666:1: ( rule__WhereClause__Group_2_2__0__Impl rule__WhereClause__Group_2_2__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2667:2: rule__WhereClause__Group_2_2__0__Impl rule__WhereClause__Group_2_2__1
            {
            pushFollow(FOLLOW_rule__WhereClause__Group_2_2__0__Impl_in_rule__WhereClause__Group_2_2__05433);
            rule__WhereClause__Group_2_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WhereClause__Group_2_2__1_in_rule__WhereClause__Group_2_2__05436);
            rule__WhereClause__Group_2_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group_2_2__0"


    // $ANTLR start "rule__WhereClause__Group_2_2__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2674:1: rule__WhereClause__Group_2_2__0__Impl : ( 'OPTIONAL' ) ;
    public final void rule__WhereClause__Group_2_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2678:1: ( ( 'OPTIONAL' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2679:1: ( 'OPTIONAL' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2679:1: ( 'OPTIONAL' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2680:1: 'OPTIONAL'
            {
             before(grammarAccess.getWhereClauseAccess().getOPTIONALKeyword_2_2_0()); 
            match(input,31,FOLLOW_31_in_rule__WhereClause__Group_2_2__0__Impl5464); 
             after(grammarAccess.getWhereClauseAccess().getOPTIONALKeyword_2_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group_2_2__0__Impl"


    // $ANTLR start "rule__WhereClause__Group_2_2__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2693:1: rule__WhereClause__Group_2_2__1 : rule__WhereClause__Group_2_2__1__Impl ;
    public final void rule__WhereClause__Group_2_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2697:1: ( rule__WhereClause__Group_2_2__1__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2698:2: rule__WhereClause__Group_2_2__1__Impl
            {
            pushFollow(FOLLOW_rule__WhereClause__Group_2_2__1__Impl_in_rule__WhereClause__Group_2_2__15495);
            rule__WhereClause__Group_2_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group_2_2__1"


    // $ANTLR start "rule__WhereClause__Group_2_2__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2704:1: rule__WhereClause__Group_2_2__1__Impl : ( ( rule__WhereClause__PatternAssignment_2_2_1 ) ) ;
    public final void rule__WhereClause__Group_2_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2708:1: ( ( ( rule__WhereClause__PatternAssignment_2_2_1 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2709:1: ( ( rule__WhereClause__PatternAssignment_2_2_1 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2709:1: ( ( rule__WhereClause__PatternAssignment_2_2_1 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2710:1: ( rule__WhereClause__PatternAssignment_2_2_1 )
            {
             before(grammarAccess.getWhereClauseAccess().getPatternAssignment_2_2_1()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2711:1: ( rule__WhereClause__PatternAssignment_2_2_1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2711:2: rule__WhereClause__PatternAssignment_2_2_1
            {
            pushFollow(FOLLOW_rule__WhereClause__PatternAssignment_2_2_1_in_rule__WhereClause__Group_2_2__1__Impl5522);
            rule__WhereClause__PatternAssignment_2_2_1();

            state._fsp--;


            }

             after(grammarAccess.getWhereClauseAccess().getPatternAssignment_2_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group_2_2__1__Impl"


    // $ANTLR start "rule__Pattern__Group__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2725:1: rule__Pattern__Group__0 : rule__Pattern__Group__0__Impl rule__Pattern__Group__1 ;
    public final void rule__Pattern__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2729:1: ( rule__Pattern__Group__0__Impl rule__Pattern__Group__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2730:2: rule__Pattern__Group__0__Impl rule__Pattern__Group__1
            {
            pushFollow(FOLLOW_rule__Pattern__Group__0__Impl_in_rule__Pattern__Group__05556);
            rule__Pattern__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Pattern__Group__1_in_rule__Pattern__Group__05559);
            rule__Pattern__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pattern__Group__0"


    // $ANTLR start "rule__Pattern__Group__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2737:1: rule__Pattern__Group__0__Impl : ( '{' ) ;
    public final void rule__Pattern__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2741:1: ( ( '{' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2742:1: ( '{' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2742:1: ( '{' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2743:1: '{'
            {
             before(grammarAccess.getPatternAccess().getLeftCurlyBracketKeyword_0()); 
            match(input,28,FOLLOW_28_in_rule__Pattern__Group__0__Impl5587); 
             after(grammarAccess.getPatternAccess().getLeftCurlyBracketKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pattern__Group__0__Impl"


    // $ANTLR start "rule__Pattern__Group__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2756:1: rule__Pattern__Group__1 : rule__Pattern__Group__1__Impl rule__Pattern__Group__2 ;
    public final void rule__Pattern__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2760:1: ( rule__Pattern__Group__1__Impl rule__Pattern__Group__2 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2761:2: rule__Pattern__Group__1__Impl rule__Pattern__Group__2
            {
            pushFollow(FOLLOW_rule__Pattern__Group__1__Impl_in_rule__Pattern__Group__15618);
            rule__Pattern__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Pattern__Group__2_in_rule__Pattern__Group__15621);
            rule__Pattern__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pattern__Group__1"


    // $ANTLR start "rule__Pattern__Group__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2768:1: rule__Pattern__Group__1__Impl : ( ( rule__Pattern__PatternsAssignment_1 ) ) ;
    public final void rule__Pattern__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2772:1: ( ( ( rule__Pattern__PatternsAssignment_1 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2773:1: ( ( rule__Pattern__PatternsAssignment_1 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2773:1: ( ( rule__Pattern__PatternsAssignment_1 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2774:1: ( rule__Pattern__PatternsAssignment_1 )
            {
             before(grammarAccess.getPatternAccess().getPatternsAssignment_1()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2775:1: ( rule__Pattern__PatternsAssignment_1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2775:2: rule__Pattern__PatternsAssignment_1
            {
            pushFollow(FOLLOW_rule__Pattern__PatternsAssignment_1_in_rule__Pattern__Group__1__Impl5648);
            rule__Pattern__PatternsAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getPatternAccess().getPatternsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pattern__Group__1__Impl"


    // $ANTLR start "rule__Pattern__Group__2"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2785:1: rule__Pattern__Group__2 : rule__Pattern__Group__2__Impl rule__Pattern__Group__3 ;
    public final void rule__Pattern__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2789:1: ( rule__Pattern__Group__2__Impl rule__Pattern__Group__3 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2790:2: rule__Pattern__Group__2__Impl rule__Pattern__Group__3
            {
            pushFollow(FOLLOW_rule__Pattern__Group__2__Impl_in_rule__Pattern__Group__25678);
            rule__Pattern__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Pattern__Group__3_in_rule__Pattern__Group__25681);
            rule__Pattern__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pattern__Group__2"


    // $ANTLR start "rule__Pattern__Group__2__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2797:1: rule__Pattern__Group__2__Impl : ( ( rule__Pattern__Group_2__0 )* ) ;
    public final void rule__Pattern__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2801:1: ( ( ( rule__Pattern__Group_2__0 )* ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2802:1: ( ( rule__Pattern__Group_2__0 )* )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2802:1: ( ( rule__Pattern__Group_2__0 )* )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2803:1: ( rule__Pattern__Group_2__0 )*
            {
             before(grammarAccess.getPatternAccess().getGroup_2()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2804:1: ( rule__Pattern__Group_2__0 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==32) ) {
                    int LA23_1 = input.LA(2);

                    if ( ((LA23_1>=RULE_INT && LA23_1<=RULE_STRING)||LA23_1==17||(LA23_1>=34 && LA23_1<=35)||LA23_1==38) ) {
                        alt23=1;
                    }


                }


                switch (alt23) {
            	case 1 :
            	    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2804:2: rule__Pattern__Group_2__0
            	    {
            	    pushFollow(FOLLOW_rule__Pattern__Group_2__0_in_rule__Pattern__Group__2__Impl5708);
            	    rule__Pattern__Group_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

             after(grammarAccess.getPatternAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pattern__Group__2__Impl"


    // $ANTLR start "rule__Pattern__Group__3"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2814:1: rule__Pattern__Group__3 : rule__Pattern__Group__3__Impl rule__Pattern__Group__4 ;
    public final void rule__Pattern__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2818:1: ( rule__Pattern__Group__3__Impl rule__Pattern__Group__4 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2819:2: rule__Pattern__Group__3__Impl rule__Pattern__Group__4
            {
            pushFollow(FOLLOW_rule__Pattern__Group__3__Impl_in_rule__Pattern__Group__35739);
            rule__Pattern__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Pattern__Group__4_in_rule__Pattern__Group__35742);
            rule__Pattern__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pattern__Group__3"


    // $ANTLR start "rule__Pattern__Group__3__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2826:1: rule__Pattern__Group__3__Impl : ( ( '.' )? ) ;
    public final void rule__Pattern__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2830:1: ( ( ( '.' )? ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2831:1: ( ( '.' )? )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2831:1: ( ( '.' )? )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2832:1: ( '.' )?
            {
             before(grammarAccess.getPatternAccess().getFullStopKeyword_3()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2833:1: ( '.' )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==32) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2834:2: '.'
                    {
                    match(input,32,FOLLOW_32_in_rule__Pattern__Group__3__Impl5771); 

                    }
                    break;

            }

             after(grammarAccess.getPatternAccess().getFullStopKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pattern__Group__3__Impl"


    // $ANTLR start "rule__Pattern__Group__4"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2845:1: rule__Pattern__Group__4 : rule__Pattern__Group__4__Impl ;
    public final void rule__Pattern__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2849:1: ( rule__Pattern__Group__4__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2850:2: rule__Pattern__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__Pattern__Group__4__Impl_in_rule__Pattern__Group__45804);
            rule__Pattern__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pattern__Group__4"


    // $ANTLR start "rule__Pattern__Group__4__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2856:1: rule__Pattern__Group__4__Impl : ( '}' ) ;
    public final void rule__Pattern__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2860:1: ( ( '}' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2861:1: ( '}' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2861:1: ( '}' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2862:1: '}'
            {
             before(grammarAccess.getPatternAccess().getRightCurlyBracketKeyword_4()); 
            match(input,29,FOLLOW_29_in_rule__Pattern__Group__4__Impl5832); 
             after(grammarAccess.getPatternAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pattern__Group__4__Impl"


    // $ANTLR start "rule__Pattern__Group_2__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2885:1: rule__Pattern__Group_2__0 : rule__Pattern__Group_2__0__Impl rule__Pattern__Group_2__1 ;
    public final void rule__Pattern__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2889:1: ( rule__Pattern__Group_2__0__Impl rule__Pattern__Group_2__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2890:2: rule__Pattern__Group_2__0__Impl rule__Pattern__Group_2__1
            {
            pushFollow(FOLLOW_rule__Pattern__Group_2__0__Impl_in_rule__Pattern__Group_2__05873);
            rule__Pattern__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Pattern__Group_2__1_in_rule__Pattern__Group_2__05876);
            rule__Pattern__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pattern__Group_2__0"


    // $ANTLR start "rule__Pattern__Group_2__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2897:1: rule__Pattern__Group_2__0__Impl : ( '.' ) ;
    public final void rule__Pattern__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2901:1: ( ( '.' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2902:1: ( '.' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2902:1: ( '.' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2903:1: '.'
            {
             before(grammarAccess.getPatternAccess().getFullStopKeyword_2_0()); 
            match(input,32,FOLLOW_32_in_rule__Pattern__Group_2__0__Impl5904); 
             after(grammarAccess.getPatternAccess().getFullStopKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pattern__Group_2__0__Impl"


    // $ANTLR start "rule__Pattern__Group_2__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2916:1: rule__Pattern__Group_2__1 : rule__Pattern__Group_2__1__Impl ;
    public final void rule__Pattern__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2920:1: ( rule__Pattern__Group_2__1__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2921:2: rule__Pattern__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__Pattern__Group_2__1__Impl_in_rule__Pattern__Group_2__15935);
            rule__Pattern__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pattern__Group_2__1"


    // $ANTLR start "rule__Pattern__Group_2__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2927:1: rule__Pattern__Group_2__1__Impl : ( ( rule__Pattern__PatternsAssignment_2_1 ) ) ;
    public final void rule__Pattern__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2931:1: ( ( ( rule__Pattern__PatternsAssignment_2_1 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2932:1: ( ( rule__Pattern__PatternsAssignment_2_1 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2932:1: ( ( rule__Pattern__PatternsAssignment_2_1 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2933:1: ( rule__Pattern__PatternsAssignment_2_1 )
            {
             before(grammarAccess.getPatternAccess().getPatternsAssignment_2_1()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2934:1: ( rule__Pattern__PatternsAssignment_2_1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2934:2: rule__Pattern__PatternsAssignment_2_1
            {
            pushFollow(FOLLOW_rule__Pattern__PatternsAssignment_2_1_in_rule__Pattern__Group_2__1__Impl5962);
            rule__Pattern__PatternsAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getPatternAccess().getPatternsAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pattern__Group_2__1__Impl"


    // $ANTLR start "rule__TriplesSameSubject__Group__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2948:1: rule__TriplesSameSubject__Group__0 : rule__TriplesSameSubject__Group__0__Impl rule__TriplesSameSubject__Group__1 ;
    public final void rule__TriplesSameSubject__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2952:1: ( rule__TriplesSameSubject__Group__0__Impl rule__TriplesSameSubject__Group__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2953:2: rule__TriplesSameSubject__Group__0__Impl rule__TriplesSameSubject__Group__1
            {
            pushFollow(FOLLOW_rule__TriplesSameSubject__Group__0__Impl_in_rule__TriplesSameSubject__Group__05996);
            rule__TriplesSameSubject__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__TriplesSameSubject__Group__1_in_rule__TriplesSameSubject__Group__05999);
            rule__TriplesSameSubject__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriplesSameSubject__Group__0"


    // $ANTLR start "rule__TriplesSameSubject__Group__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2960:1: rule__TriplesSameSubject__Group__0__Impl : ( ( rule__TriplesSameSubject__SubjectAssignment_0 ) ) ;
    public final void rule__TriplesSameSubject__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2964:1: ( ( ( rule__TriplesSameSubject__SubjectAssignment_0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2965:1: ( ( rule__TriplesSameSubject__SubjectAssignment_0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2965:1: ( ( rule__TriplesSameSubject__SubjectAssignment_0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2966:1: ( rule__TriplesSameSubject__SubjectAssignment_0 )
            {
             before(grammarAccess.getTriplesSameSubjectAccess().getSubjectAssignment_0()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2967:1: ( rule__TriplesSameSubject__SubjectAssignment_0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2967:2: rule__TriplesSameSubject__SubjectAssignment_0
            {
            pushFollow(FOLLOW_rule__TriplesSameSubject__SubjectAssignment_0_in_rule__TriplesSameSubject__Group__0__Impl6026);
            rule__TriplesSameSubject__SubjectAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getTriplesSameSubjectAccess().getSubjectAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriplesSameSubject__Group__0__Impl"


    // $ANTLR start "rule__TriplesSameSubject__Group__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2977:1: rule__TriplesSameSubject__Group__1 : rule__TriplesSameSubject__Group__1__Impl rule__TriplesSameSubject__Group__2 ;
    public final void rule__TriplesSameSubject__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2981:1: ( rule__TriplesSameSubject__Group__1__Impl rule__TriplesSameSubject__Group__2 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2982:2: rule__TriplesSameSubject__Group__1__Impl rule__TriplesSameSubject__Group__2
            {
            pushFollow(FOLLOW_rule__TriplesSameSubject__Group__1__Impl_in_rule__TriplesSameSubject__Group__16056);
            rule__TriplesSameSubject__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__TriplesSameSubject__Group__2_in_rule__TriplesSameSubject__Group__16059);
            rule__TriplesSameSubject__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriplesSameSubject__Group__1"


    // $ANTLR start "rule__TriplesSameSubject__Group__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2989:1: rule__TriplesSameSubject__Group__1__Impl : ( ( rule__TriplesSameSubject__PropertyListAssignment_1 ) ) ;
    public final void rule__TriplesSameSubject__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2993:1: ( ( ( rule__TriplesSameSubject__PropertyListAssignment_1 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2994:1: ( ( rule__TriplesSameSubject__PropertyListAssignment_1 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2994:1: ( ( rule__TriplesSameSubject__PropertyListAssignment_1 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2995:1: ( rule__TriplesSameSubject__PropertyListAssignment_1 )
            {
             before(grammarAccess.getTriplesSameSubjectAccess().getPropertyListAssignment_1()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2996:1: ( rule__TriplesSameSubject__PropertyListAssignment_1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:2996:2: rule__TriplesSameSubject__PropertyListAssignment_1
            {
            pushFollow(FOLLOW_rule__TriplesSameSubject__PropertyListAssignment_1_in_rule__TriplesSameSubject__Group__1__Impl6086);
            rule__TriplesSameSubject__PropertyListAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTriplesSameSubjectAccess().getPropertyListAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriplesSameSubject__Group__1__Impl"


    // $ANTLR start "rule__TriplesSameSubject__Group__2"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3006:1: rule__TriplesSameSubject__Group__2 : rule__TriplesSameSubject__Group__2__Impl ;
    public final void rule__TriplesSameSubject__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3010:1: ( rule__TriplesSameSubject__Group__2__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3011:2: rule__TriplesSameSubject__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__TriplesSameSubject__Group__2__Impl_in_rule__TriplesSameSubject__Group__26116);
            rule__TriplesSameSubject__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriplesSameSubject__Group__2"


    // $ANTLR start "rule__TriplesSameSubject__Group__2__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3017:1: rule__TriplesSameSubject__Group__2__Impl : ( ( rule__TriplesSameSubject__Group_2__0 )* ) ;
    public final void rule__TriplesSameSubject__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3021:1: ( ( ( rule__TriplesSameSubject__Group_2__0 )* ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3022:1: ( ( rule__TriplesSameSubject__Group_2__0 )* )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3022:1: ( ( rule__TriplesSameSubject__Group_2__0 )* )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3023:1: ( rule__TriplesSameSubject__Group_2__0 )*
            {
             before(grammarAccess.getTriplesSameSubjectAccess().getGroup_2()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3024:1: ( rule__TriplesSameSubject__Group_2__0 )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==33) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3024:2: rule__TriplesSameSubject__Group_2__0
            	    {
            	    pushFollow(FOLLOW_rule__TriplesSameSubject__Group_2__0_in_rule__TriplesSameSubject__Group__2__Impl6143);
            	    rule__TriplesSameSubject__Group_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

             after(grammarAccess.getTriplesSameSubjectAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriplesSameSubject__Group__2__Impl"


    // $ANTLR start "rule__TriplesSameSubject__Group_2__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3040:1: rule__TriplesSameSubject__Group_2__0 : rule__TriplesSameSubject__Group_2__0__Impl rule__TriplesSameSubject__Group_2__1 ;
    public final void rule__TriplesSameSubject__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3044:1: ( rule__TriplesSameSubject__Group_2__0__Impl rule__TriplesSameSubject__Group_2__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3045:2: rule__TriplesSameSubject__Group_2__0__Impl rule__TriplesSameSubject__Group_2__1
            {
            pushFollow(FOLLOW_rule__TriplesSameSubject__Group_2__0__Impl_in_rule__TriplesSameSubject__Group_2__06180);
            rule__TriplesSameSubject__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__TriplesSameSubject__Group_2__1_in_rule__TriplesSameSubject__Group_2__06183);
            rule__TriplesSameSubject__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriplesSameSubject__Group_2__0"


    // $ANTLR start "rule__TriplesSameSubject__Group_2__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3052:1: rule__TriplesSameSubject__Group_2__0__Impl : ( ';' ) ;
    public final void rule__TriplesSameSubject__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3056:1: ( ( ';' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3057:1: ( ';' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3057:1: ( ';' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3058:1: ';'
            {
             before(grammarAccess.getTriplesSameSubjectAccess().getSemicolonKeyword_2_0()); 
            match(input,33,FOLLOW_33_in_rule__TriplesSameSubject__Group_2__0__Impl6211); 
             after(grammarAccess.getTriplesSameSubjectAccess().getSemicolonKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriplesSameSubject__Group_2__0__Impl"


    // $ANTLR start "rule__TriplesSameSubject__Group_2__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3071:1: rule__TriplesSameSubject__Group_2__1 : rule__TriplesSameSubject__Group_2__1__Impl ;
    public final void rule__TriplesSameSubject__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3075:1: ( rule__TriplesSameSubject__Group_2__1__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3076:2: rule__TriplesSameSubject__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__TriplesSameSubject__Group_2__1__Impl_in_rule__TriplesSameSubject__Group_2__16242);
            rule__TriplesSameSubject__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriplesSameSubject__Group_2__1"


    // $ANTLR start "rule__TriplesSameSubject__Group_2__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3082:1: rule__TriplesSameSubject__Group_2__1__Impl : ( ( rule__TriplesSameSubject__PropertyListAssignment_2_1 ) ) ;
    public final void rule__TriplesSameSubject__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3086:1: ( ( ( rule__TriplesSameSubject__PropertyListAssignment_2_1 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3087:1: ( ( rule__TriplesSameSubject__PropertyListAssignment_2_1 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3087:1: ( ( rule__TriplesSameSubject__PropertyListAssignment_2_1 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3088:1: ( rule__TriplesSameSubject__PropertyListAssignment_2_1 )
            {
             before(grammarAccess.getTriplesSameSubjectAccess().getPropertyListAssignment_2_1()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3089:1: ( rule__TriplesSameSubject__PropertyListAssignment_2_1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3089:2: rule__TriplesSameSubject__PropertyListAssignment_2_1
            {
            pushFollow(FOLLOW_rule__TriplesSameSubject__PropertyListAssignment_2_1_in_rule__TriplesSameSubject__Group_2__1__Impl6269);
            rule__TriplesSameSubject__PropertyListAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getTriplesSameSubjectAccess().getPropertyListAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriplesSameSubject__Group_2__1__Impl"


    // $ANTLR start "rule__PropertyList__Group__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3103:1: rule__PropertyList__Group__0 : rule__PropertyList__Group__0__Impl rule__PropertyList__Group__1 ;
    public final void rule__PropertyList__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3107:1: ( rule__PropertyList__Group__0__Impl rule__PropertyList__Group__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3108:2: rule__PropertyList__Group__0__Impl rule__PropertyList__Group__1
            {
            pushFollow(FOLLOW_rule__PropertyList__Group__0__Impl_in_rule__PropertyList__Group__06303);
            rule__PropertyList__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__PropertyList__Group__1_in_rule__PropertyList__Group__06306);
            rule__PropertyList__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PropertyList__Group__0"


    // $ANTLR start "rule__PropertyList__Group__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3115:1: rule__PropertyList__Group__0__Impl : ( ( rule__PropertyList__PropertyAssignment_0 ) ) ;
    public final void rule__PropertyList__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3119:1: ( ( ( rule__PropertyList__PropertyAssignment_0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3120:1: ( ( rule__PropertyList__PropertyAssignment_0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3120:1: ( ( rule__PropertyList__PropertyAssignment_0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3121:1: ( rule__PropertyList__PropertyAssignment_0 )
            {
             before(grammarAccess.getPropertyListAccess().getPropertyAssignment_0()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3122:1: ( rule__PropertyList__PropertyAssignment_0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3122:2: rule__PropertyList__PropertyAssignment_0
            {
            pushFollow(FOLLOW_rule__PropertyList__PropertyAssignment_0_in_rule__PropertyList__Group__0__Impl6333);
            rule__PropertyList__PropertyAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getPropertyListAccess().getPropertyAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PropertyList__Group__0__Impl"


    // $ANTLR start "rule__PropertyList__Group__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3132:1: rule__PropertyList__Group__1 : rule__PropertyList__Group__1__Impl ;
    public final void rule__PropertyList__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3136:1: ( rule__PropertyList__Group__1__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3137:2: rule__PropertyList__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__PropertyList__Group__1__Impl_in_rule__PropertyList__Group__16363);
            rule__PropertyList__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PropertyList__Group__1"


    // $ANTLR start "rule__PropertyList__Group__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3143:1: rule__PropertyList__Group__1__Impl : ( ( rule__PropertyList__ObjectAssignment_1 )* ) ;
    public final void rule__PropertyList__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3147:1: ( ( ( rule__PropertyList__ObjectAssignment_1 )* ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3148:1: ( ( rule__PropertyList__ObjectAssignment_1 )* )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3148:1: ( ( rule__PropertyList__ObjectAssignment_1 )* )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3149:1: ( rule__PropertyList__ObjectAssignment_1 )*
            {
             before(grammarAccess.getPropertyListAccess().getObjectAssignment_1()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3150:1: ( rule__PropertyList__ObjectAssignment_1 )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( ((LA26_0>=RULE_INT && LA26_0<=RULE_STRING)||LA26_0==17||(LA26_0>=34 && LA26_0<=35)||LA26_0==38) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3150:2: rule__PropertyList__ObjectAssignment_1
            	    {
            	    pushFollow(FOLLOW_rule__PropertyList__ObjectAssignment_1_in_rule__PropertyList__Group__1__Impl6390);
            	    rule__PropertyList__ObjectAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);

             after(grammarAccess.getPropertyListAccess().getObjectAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PropertyList__Group__1__Impl"


    // $ANTLR start "rule__Quote__Group__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3164:1: rule__Quote__Group__0 : rule__Quote__Group__0__Impl rule__Quote__Group__1 ;
    public final void rule__Quote__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3168:1: ( rule__Quote__Group__0__Impl rule__Quote__Group__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3169:2: rule__Quote__Group__0__Impl rule__Quote__Group__1
            {
            pushFollow(FOLLOW_rule__Quote__Group__0__Impl_in_rule__Quote__Group__06425);
            rule__Quote__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Quote__Group__1_in_rule__Quote__Group__06428);
            rule__Quote__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quote__Group__0"


    // $ANTLR start "rule__Quote__Group__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3176:1: rule__Quote__Group__0__Impl : ( ( rule__Quote__NameAssignment_0 ) ) ;
    public final void rule__Quote__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3180:1: ( ( ( rule__Quote__NameAssignment_0 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3181:1: ( ( rule__Quote__NameAssignment_0 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3181:1: ( ( rule__Quote__NameAssignment_0 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3182:1: ( rule__Quote__NameAssignment_0 )
            {
             before(grammarAccess.getQuoteAccess().getNameAssignment_0()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3183:1: ( rule__Quote__NameAssignment_0 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3183:2: rule__Quote__NameAssignment_0
            {
            pushFollow(FOLLOW_rule__Quote__NameAssignment_0_in_rule__Quote__Group__0__Impl6455);
            rule__Quote__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getQuoteAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quote__Group__0__Impl"


    // $ANTLR start "rule__Quote__Group__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3193:1: rule__Quote__Group__1 : rule__Quote__Group__1__Impl ;
    public final void rule__Quote__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3197:1: ( rule__Quote__Group__1__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3198:2: rule__Quote__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Quote__Group__1__Impl_in_rule__Quote__Group__16485);
            rule__Quote__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quote__Group__1"


    // $ANTLR start "rule__Quote__Group__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3204:1: rule__Quote__Group__1__Impl : ( ':' ) ;
    public final void rule__Quote__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3208:1: ( ( ':' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3209:1: ( ':' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3209:1: ( ':' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3210:1: ':'
            {
             before(grammarAccess.getQuoteAccess().getColonKeyword_1()); 
            match(input,18,FOLLOW_18_in_rule__Quote__Group__1__Impl6513); 
             after(grammarAccess.getQuoteAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quote__Group__1__Impl"


    // $ANTLR start "rule__Parameter__Group__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3227:1: rule__Parameter__Group__0 : rule__Parameter__Group__0__Impl rule__Parameter__Group__1 ;
    public final void rule__Parameter__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3231:1: ( rule__Parameter__Group__0__Impl rule__Parameter__Group__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3232:2: rule__Parameter__Group__0__Impl rule__Parameter__Group__1
            {
            pushFollow(FOLLOW_rule__Parameter__Group__0__Impl_in_rule__Parameter__Group__06548);
            rule__Parameter__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Parameter__Group__1_in_rule__Parameter__Group__06551);
            rule__Parameter__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__0"


    // $ANTLR start "rule__Parameter__Group__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3239:1: rule__Parameter__Group__0__Impl : ( '?:' ) ;
    public final void rule__Parameter__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3243:1: ( ( '?:' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3244:1: ( '?:' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3244:1: ( '?:' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3245:1: '?:'
            {
             before(grammarAccess.getParameterAccess().getQuestionMarkColonKeyword_0()); 
            match(input,34,FOLLOW_34_in_rule__Parameter__Group__0__Impl6579); 
             after(grammarAccess.getParameterAccess().getQuestionMarkColonKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__0__Impl"


    // $ANTLR start "rule__Parameter__Group__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3258:1: rule__Parameter__Group__1 : rule__Parameter__Group__1__Impl ;
    public final void rule__Parameter__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3262:1: ( rule__Parameter__Group__1__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3263:2: rule__Parameter__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Parameter__Group__1__Impl_in_rule__Parameter__Group__16610);
            rule__Parameter__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__1"


    // $ANTLR start "rule__Parameter__Group__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3269:1: rule__Parameter__Group__1__Impl : ( ( rule__Parameter__NameAssignment_1 ) ) ;
    public final void rule__Parameter__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3273:1: ( ( ( rule__Parameter__NameAssignment_1 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3274:1: ( ( rule__Parameter__NameAssignment_1 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3274:1: ( ( rule__Parameter__NameAssignment_1 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3275:1: ( rule__Parameter__NameAssignment_1 )
            {
             before(grammarAccess.getParameterAccess().getNameAssignment_1()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3276:1: ( rule__Parameter__NameAssignment_1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3276:2: rule__Parameter__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Parameter__NameAssignment_1_in_rule__Parameter__Group__1__Impl6637);
            rule__Parameter__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getParameterAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__1__Impl"


    // $ANTLR start "rule__BlankNode__Group__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3290:1: rule__BlankNode__Group__0 : rule__BlankNode__Group__0__Impl rule__BlankNode__Group__1 ;
    public final void rule__BlankNode__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3294:1: ( rule__BlankNode__Group__0__Impl rule__BlankNode__Group__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3295:2: rule__BlankNode__Group__0__Impl rule__BlankNode__Group__1
            {
            pushFollow(FOLLOW_rule__BlankNode__Group__0__Impl_in_rule__BlankNode__Group__06671);
            rule__BlankNode__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__BlankNode__Group__1_in_rule__BlankNode__Group__06674);
            rule__BlankNode__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BlankNode__Group__0"


    // $ANTLR start "rule__BlankNode__Group__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3302:1: rule__BlankNode__Group__0__Impl : ( '_:' ) ;
    public final void rule__BlankNode__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3306:1: ( ( '_:' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3307:1: ( '_:' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3307:1: ( '_:' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3308:1: '_:'
            {
             before(grammarAccess.getBlankNodeAccess().get_Keyword_0()); 
            match(input,35,FOLLOW_35_in_rule__BlankNode__Group__0__Impl6702); 
             after(grammarAccess.getBlankNodeAccess().get_Keyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BlankNode__Group__0__Impl"


    // $ANTLR start "rule__BlankNode__Group__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3321:1: rule__BlankNode__Group__1 : rule__BlankNode__Group__1__Impl ;
    public final void rule__BlankNode__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3325:1: ( rule__BlankNode__Group__1__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3326:2: rule__BlankNode__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__BlankNode__Group__1__Impl_in_rule__BlankNode__Group__16733);
            rule__BlankNode__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BlankNode__Group__1"


    // $ANTLR start "rule__BlankNode__Group__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3332:1: rule__BlankNode__Group__1__Impl : ( ( rule__BlankNode__NameAssignment_1 ) ) ;
    public final void rule__BlankNode__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3336:1: ( ( ( rule__BlankNode__NameAssignment_1 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3337:1: ( ( rule__BlankNode__NameAssignment_1 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3337:1: ( ( rule__BlankNode__NameAssignment_1 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3338:1: ( rule__BlankNode__NameAssignment_1 )
            {
             before(grammarAccess.getBlankNodeAccess().getNameAssignment_1()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3339:1: ( rule__BlankNode__NameAssignment_1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3339:2: rule__BlankNode__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__BlankNode__NameAssignment_1_in_rule__BlankNode__Group__1__Impl6760);
            rule__BlankNode__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getBlankNodeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BlankNode__Group__1__Impl"


    // $ANTLR start "rule__DatasetClause__Group_0__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3353:1: rule__DatasetClause__Group_0__0 : rule__DatasetClause__Group_0__0__Impl rule__DatasetClause__Group_0__1 ;
    public final void rule__DatasetClause__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3357:1: ( rule__DatasetClause__Group_0__0__Impl rule__DatasetClause__Group_0__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3358:2: rule__DatasetClause__Group_0__0__Impl rule__DatasetClause__Group_0__1
            {
            pushFollow(FOLLOW_rule__DatasetClause__Group_0__0__Impl_in_rule__DatasetClause__Group_0__06794);
            rule__DatasetClause__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__DatasetClause__Group_0__1_in_rule__DatasetClause__Group_0__06797);
            rule__DatasetClause__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DatasetClause__Group_0__0"


    // $ANTLR start "rule__DatasetClause__Group_0__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3365:1: rule__DatasetClause__Group_0__0__Impl : ( 'FROM' ) ;
    public final void rule__DatasetClause__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3369:1: ( ( 'FROM' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3370:1: ( 'FROM' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3370:1: ( 'FROM' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3371:1: 'FROM'
            {
             before(grammarAccess.getDatasetClauseAccess().getFROMKeyword_0_0()); 
            match(input,36,FOLLOW_36_in_rule__DatasetClause__Group_0__0__Impl6825); 
             after(grammarAccess.getDatasetClauseAccess().getFROMKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DatasetClause__Group_0__0__Impl"


    // $ANTLR start "rule__DatasetClause__Group_0__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3384:1: rule__DatasetClause__Group_0__1 : rule__DatasetClause__Group_0__1__Impl ;
    public final void rule__DatasetClause__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3388:1: ( rule__DatasetClause__Group_0__1__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3389:2: rule__DatasetClause__Group_0__1__Impl
            {
            pushFollow(FOLLOW_rule__DatasetClause__Group_0__1__Impl_in_rule__DatasetClause__Group_0__16856);
            rule__DatasetClause__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DatasetClause__Group_0__1"


    // $ANTLR start "rule__DatasetClause__Group_0__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3395:1: rule__DatasetClause__Group_0__1__Impl : ( ( rule__DatasetClause__DefaultGraphClauseAssignment_0_1 ) ) ;
    public final void rule__DatasetClause__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3399:1: ( ( ( rule__DatasetClause__DefaultGraphClauseAssignment_0_1 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3400:1: ( ( rule__DatasetClause__DefaultGraphClauseAssignment_0_1 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3400:1: ( ( rule__DatasetClause__DefaultGraphClauseAssignment_0_1 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3401:1: ( rule__DatasetClause__DefaultGraphClauseAssignment_0_1 )
            {
             before(grammarAccess.getDatasetClauseAccess().getDefaultGraphClauseAssignment_0_1()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3402:1: ( rule__DatasetClause__DefaultGraphClauseAssignment_0_1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3402:2: rule__DatasetClause__DefaultGraphClauseAssignment_0_1
            {
            pushFollow(FOLLOW_rule__DatasetClause__DefaultGraphClauseAssignment_0_1_in_rule__DatasetClause__Group_0__1__Impl6883);
            rule__DatasetClause__DefaultGraphClauseAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getDatasetClauseAccess().getDefaultGraphClauseAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DatasetClause__Group_0__1__Impl"


    // $ANTLR start "rule__NamedGraphClause__Group__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3416:1: rule__NamedGraphClause__Group__0 : rule__NamedGraphClause__Group__0__Impl rule__NamedGraphClause__Group__1 ;
    public final void rule__NamedGraphClause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3420:1: ( rule__NamedGraphClause__Group__0__Impl rule__NamedGraphClause__Group__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3421:2: rule__NamedGraphClause__Group__0__Impl rule__NamedGraphClause__Group__1
            {
            pushFollow(FOLLOW_rule__NamedGraphClause__Group__0__Impl_in_rule__NamedGraphClause__Group__06917);
            rule__NamedGraphClause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__NamedGraphClause__Group__1_in_rule__NamedGraphClause__Group__06920);
            rule__NamedGraphClause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NamedGraphClause__Group__0"


    // $ANTLR start "rule__NamedGraphClause__Group__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3428:1: rule__NamedGraphClause__Group__0__Impl : ( 'NAMED' ) ;
    public final void rule__NamedGraphClause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3432:1: ( ( 'NAMED' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3433:1: ( 'NAMED' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3433:1: ( 'NAMED' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3434:1: 'NAMED'
            {
             before(grammarAccess.getNamedGraphClauseAccess().getNAMEDKeyword_0()); 
            match(input,37,FOLLOW_37_in_rule__NamedGraphClause__Group__0__Impl6948); 
             after(grammarAccess.getNamedGraphClauseAccess().getNAMEDKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NamedGraphClause__Group__0__Impl"


    // $ANTLR start "rule__NamedGraphClause__Group__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3447:1: rule__NamedGraphClause__Group__1 : rule__NamedGraphClause__Group__1__Impl ;
    public final void rule__NamedGraphClause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3451:1: ( rule__NamedGraphClause__Group__1__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3452:2: rule__NamedGraphClause__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__NamedGraphClause__Group__1__Impl_in_rule__NamedGraphClause__Group__16979);
            rule__NamedGraphClause__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NamedGraphClause__Group__1"


    // $ANTLR start "rule__NamedGraphClause__Group__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3458:1: rule__NamedGraphClause__Group__1__Impl : ( ruleSourceSelector ) ;
    public final void rule__NamedGraphClause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3462:1: ( ( ruleSourceSelector ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3463:1: ( ruleSourceSelector )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3463:1: ( ruleSourceSelector )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3464:1: ruleSourceSelector
            {
             before(grammarAccess.getNamedGraphClauseAccess().getSourceSelectorParserRuleCall_1()); 
            pushFollow(FOLLOW_ruleSourceSelector_in_rule__NamedGraphClause__Group__1__Impl7006);
            ruleSourceSelector();

            state._fsp--;

             after(grammarAccess.getNamedGraphClauseAccess().getSourceSelectorParserRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NamedGraphClause__Group__1__Impl"


    // $ANTLR start "rule__VAR1__Group__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3479:1: rule__VAR1__Group__0 : rule__VAR1__Group__0__Impl rule__VAR1__Group__1 ;
    public final void rule__VAR1__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3483:1: ( rule__VAR1__Group__0__Impl rule__VAR1__Group__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3484:2: rule__VAR1__Group__0__Impl rule__VAR1__Group__1
            {
            pushFollow(FOLLOW_rule__VAR1__Group__0__Impl_in_rule__VAR1__Group__07039);
            rule__VAR1__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VAR1__Group__1_in_rule__VAR1__Group__07042);
            rule__VAR1__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VAR1__Group__0"


    // $ANTLR start "rule__VAR1__Group__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3491:1: rule__VAR1__Group__0__Impl : ( '?' ) ;
    public final void rule__VAR1__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3495:1: ( ( '?' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3496:1: ( '?' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3496:1: ( '?' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3497:1: '?'
            {
             before(grammarAccess.getVAR1Access().getQuestionMarkKeyword_0()); 
            match(input,38,FOLLOW_38_in_rule__VAR1__Group__0__Impl7070); 
             after(grammarAccess.getVAR1Access().getQuestionMarkKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VAR1__Group__0__Impl"


    // $ANTLR start "rule__VAR1__Group__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3510:1: rule__VAR1__Group__1 : rule__VAR1__Group__1__Impl ;
    public final void rule__VAR1__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3514:1: ( rule__VAR1__Group__1__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3515:2: rule__VAR1__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__VAR1__Group__1__Impl_in_rule__VAR1__Group__17101);
            rule__VAR1__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VAR1__Group__1"


    // $ANTLR start "rule__VAR1__Group__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3521:1: rule__VAR1__Group__1__Impl : ( ( rule__VAR1__NameAssignment_1 ) ) ;
    public final void rule__VAR1__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3525:1: ( ( ( rule__VAR1__NameAssignment_1 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3526:1: ( ( rule__VAR1__NameAssignment_1 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3526:1: ( ( rule__VAR1__NameAssignment_1 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3527:1: ( rule__VAR1__NameAssignment_1 )
            {
             before(grammarAccess.getVAR1Access().getNameAssignment_1()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3528:1: ( rule__VAR1__NameAssignment_1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3528:2: rule__VAR1__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__VAR1__NameAssignment_1_in_rule__VAR1__Group__1__Impl7128);
            rule__VAR1__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getVAR1Access().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VAR1__Group__1__Impl"


    // $ANTLR start "rule__VAR2__Group__0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3542:1: rule__VAR2__Group__0 : rule__VAR2__Group__0__Impl rule__VAR2__Group__1 ;
    public final void rule__VAR2__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3546:1: ( rule__VAR2__Group__0__Impl rule__VAR2__Group__1 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3547:2: rule__VAR2__Group__0__Impl rule__VAR2__Group__1
            {
            pushFollow(FOLLOW_rule__VAR2__Group__0__Impl_in_rule__VAR2__Group__07162);
            rule__VAR2__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VAR2__Group__1_in_rule__VAR2__Group__07165);
            rule__VAR2__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VAR2__Group__0"


    // $ANTLR start "rule__VAR2__Group__0__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3554:1: rule__VAR2__Group__0__Impl : ( rulePrefix ) ;
    public final void rule__VAR2__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3558:1: ( ( rulePrefix ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3559:1: ( rulePrefix )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3559:1: ( rulePrefix )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3560:1: rulePrefix
            {
             before(grammarAccess.getVAR2Access().getPrefixParserRuleCall_0()); 
            pushFollow(FOLLOW_rulePrefix_in_rule__VAR2__Group__0__Impl7192);
            rulePrefix();

            state._fsp--;

             after(grammarAccess.getVAR2Access().getPrefixParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VAR2__Group__0__Impl"


    // $ANTLR start "rule__VAR2__Group__1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3571:1: rule__VAR2__Group__1 : rule__VAR2__Group__1__Impl rule__VAR2__Group__2 ;
    public final void rule__VAR2__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3575:1: ( rule__VAR2__Group__1__Impl rule__VAR2__Group__2 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3576:2: rule__VAR2__Group__1__Impl rule__VAR2__Group__2
            {
            pushFollow(FOLLOW_rule__VAR2__Group__1__Impl_in_rule__VAR2__Group__17221);
            rule__VAR2__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VAR2__Group__2_in_rule__VAR2__Group__17224);
            rule__VAR2__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VAR2__Group__1"


    // $ANTLR start "rule__VAR2__Group__1__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3583:1: rule__VAR2__Group__1__Impl : ( ':' ) ;
    public final void rule__VAR2__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3587:1: ( ( ':' ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3588:1: ( ':' )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3588:1: ( ':' )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3589:1: ':'
            {
             before(grammarAccess.getVAR2Access().getColonKeyword_1()); 
            match(input,18,FOLLOW_18_in_rule__VAR2__Group__1__Impl7252); 
             after(grammarAccess.getVAR2Access().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VAR2__Group__1__Impl"


    // $ANTLR start "rule__VAR2__Group__2"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3602:1: rule__VAR2__Group__2 : rule__VAR2__Group__2__Impl ;
    public final void rule__VAR2__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3606:1: ( rule__VAR2__Group__2__Impl )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3607:2: rule__VAR2__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__VAR2__Group__2__Impl_in_rule__VAR2__Group__27283);
            rule__VAR2__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VAR2__Group__2"


    // $ANTLR start "rule__VAR2__Group__2__Impl"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3613:1: rule__VAR2__Group__2__Impl : ( ( rule__VAR2__NameAssignment_2 ) ) ;
    public final void rule__VAR2__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3617:1: ( ( ( rule__VAR2__NameAssignment_2 ) ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3618:1: ( ( rule__VAR2__NameAssignment_2 ) )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3618:1: ( ( rule__VAR2__NameAssignment_2 ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3619:1: ( rule__VAR2__NameAssignment_2 )
            {
             before(grammarAccess.getVAR2Access().getNameAssignment_2()); 
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3620:1: ( rule__VAR2__NameAssignment_2 )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3620:2: rule__VAR2__NameAssignment_2
            {
            pushFollow(FOLLOW_rule__VAR2__NameAssignment_2_in_rule__VAR2__Group__2__Impl7310);
            rule__VAR2__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getVAR2Access().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VAR2__Group__2__Impl"


    // $ANTLR start "rule__Query__SelectQueryAssignment"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3637:1: rule__Query__SelectQueryAssignment : ( ruleSelectQuery ) ;
    public final void rule__Query__SelectQueryAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3641:1: ( ( ruleSelectQuery ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3642:1: ( ruleSelectQuery )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3642:1: ( ruleSelectQuery )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3643:1: ruleSelectQuery
            {
             before(grammarAccess.getQueryAccess().getSelectQuerySelectQueryParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleSelectQuery_in_rule__Query__SelectQueryAssignment7351);
            ruleSelectQuery();

            state._fsp--;

             after(grammarAccess.getQueryAccess().getSelectQuerySelectQueryParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Query__SelectQueryAssignment"


    // $ANTLR start "rule__Prefix__NameAssignment_0_1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3652:1: rule__Prefix__NameAssignment_0_1 : ( RULE_ID ) ;
    public final void rule__Prefix__NameAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3656:1: ( ( RULE_ID ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3657:1: ( RULE_ID )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3657:1: ( RULE_ID )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3658:1: RULE_ID
            {
             before(grammarAccess.getPrefixAccess().getNameIDTerminalRuleCall_0_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Prefix__NameAssignment_0_17382); 
             after(grammarAccess.getPrefixAccess().getNameIDTerminalRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prefix__NameAssignment_0_1"


    // $ANTLR start "rule__Prefix__IrefAssignment_0_3"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3667:1: rule__Prefix__IrefAssignment_0_3 : ( RULE_IRI_URI ) ;
    public final void rule__Prefix__IrefAssignment_0_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3671:1: ( ( RULE_IRI_URI ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3672:1: ( RULE_IRI_URI )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3672:1: ( RULE_IRI_URI )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3673:1: RULE_IRI_URI
            {
             before(grammarAccess.getPrefixAccess().getIrefIRI_URITerminalRuleCall_0_3_0()); 
            match(input,RULE_IRI_URI,FOLLOW_RULE_IRI_URI_in_rule__Prefix__IrefAssignment_0_37413); 
             after(grammarAccess.getPrefixAccess().getIrefIRI_URITerminalRuleCall_0_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prefix__IrefAssignment_0_3"


    // $ANTLR start "rule__UnNamedPrefix__IrefAssignment_2"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3682:1: rule__UnNamedPrefix__IrefAssignment_2 : ( RULE_IRI_URI ) ;
    public final void rule__UnNamedPrefix__IrefAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3686:1: ( ( RULE_IRI_URI ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3687:1: ( RULE_IRI_URI )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3687:1: ( RULE_IRI_URI )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3688:1: RULE_IRI_URI
            {
             before(grammarAccess.getUnNamedPrefixAccess().getIrefIRI_URITerminalRuleCall_2_0()); 
            match(input,RULE_IRI_URI,FOLLOW_RULE_IRI_URI_in_rule__UnNamedPrefix__IrefAssignment_27444); 
             after(grammarAccess.getUnNamedPrefixAccess().getIrefIRI_URITerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnNamedPrefix__IrefAssignment_2"


    // $ANTLR start "rule__SelectQuery__PrefixAssignment_0_0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3697:1: rule__SelectQuery__PrefixAssignment_0_0 : ( rulePrefix ) ;
    public final void rule__SelectQuery__PrefixAssignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3701:1: ( ( rulePrefix ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3702:1: ( rulePrefix )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3702:1: ( rulePrefix )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3703:1: rulePrefix
            {
             before(grammarAccess.getSelectQueryAccess().getPrefixPrefixParserRuleCall_0_0_0()); 
            pushFollow(FOLLOW_rulePrefix_in_rule__SelectQuery__PrefixAssignment_0_07475);
            rulePrefix();

            state._fsp--;

             after(grammarAccess.getSelectQueryAccess().getPrefixPrefixParserRuleCall_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__PrefixAssignment_0_0"


    // $ANTLR start "rule__SelectQuery__VarAssignment_0_3_0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3712:1: rule__SelectQuery__VarAssignment_0_3_0 : ( ruleVar ) ;
    public final void rule__SelectQuery__VarAssignment_0_3_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3716:1: ( ( ruleVar ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3717:1: ( ruleVar )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3717:1: ( ruleVar )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3718:1: ruleVar
            {
             before(grammarAccess.getSelectQueryAccess().getVarVarParserRuleCall_0_3_0_0()); 
            pushFollow(FOLLOW_ruleVar_in_rule__SelectQuery__VarAssignment_0_3_07506);
            ruleVar();

            state._fsp--;

             after(grammarAccess.getSelectQueryAccess().getVarVarParserRuleCall_0_3_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__VarAssignment_0_3_0"


    // $ANTLR start "rule__SelectQuery__DatasetClauseAssignment_0_4"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3727:1: rule__SelectQuery__DatasetClauseAssignment_0_4 : ( ruleDatasetClause ) ;
    public final void rule__SelectQuery__DatasetClauseAssignment_0_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3731:1: ( ( ruleDatasetClause ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3732:1: ( ruleDatasetClause )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3732:1: ( ruleDatasetClause )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3733:1: ruleDatasetClause
            {
             before(grammarAccess.getSelectQueryAccess().getDatasetClauseDatasetClauseParserRuleCall_0_4_0()); 
            pushFollow(FOLLOW_ruleDatasetClause_in_rule__SelectQuery__DatasetClauseAssignment_0_47537);
            ruleDatasetClause();

            state._fsp--;

             after(grammarAccess.getSelectQueryAccess().getDatasetClauseDatasetClauseParserRuleCall_0_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__DatasetClauseAssignment_0_4"


    // $ANTLR start "rule__SelectQuery__WhereClauseAssignment_0_5"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3742:1: rule__SelectQuery__WhereClauseAssignment_0_5 : ( ruleWhereClause ) ;
    public final void rule__SelectQuery__WhereClauseAssignment_0_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3746:1: ( ( ruleWhereClause ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3747:1: ( ruleWhereClause )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3747:1: ( ruleWhereClause )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3748:1: ruleWhereClause
            {
             before(grammarAccess.getSelectQueryAccess().getWhereClauseWhereClauseParserRuleCall_0_5_0()); 
            pushFollow(FOLLOW_ruleWhereClause_in_rule__SelectQuery__WhereClauseAssignment_0_57568);
            ruleWhereClause();

            state._fsp--;

             after(grammarAccess.getSelectQueryAccess().getWhereClauseWhereClauseParserRuleCall_0_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__WhereClauseAssignment_0_5"


    // $ANTLR start "rule__SelectQuery__OrderClauseAssignment_0_6"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3757:1: rule__SelectQuery__OrderClauseAssignment_0_6 : ( ruleOrderClause ) ;
    public final void rule__SelectQuery__OrderClauseAssignment_0_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3761:1: ( ( ruleOrderClause ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3762:1: ( ruleOrderClause )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3762:1: ( ruleOrderClause )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3763:1: ruleOrderClause
            {
             before(grammarAccess.getSelectQueryAccess().getOrderClauseOrderClauseParserRuleCall_0_6_0()); 
            pushFollow(FOLLOW_ruleOrderClause_in_rule__SelectQuery__OrderClauseAssignment_0_67599);
            ruleOrderClause();

            state._fsp--;

             after(grammarAccess.getSelectQueryAccess().getOrderClauseOrderClauseParserRuleCall_0_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__OrderClauseAssignment_0_6"


    // $ANTLR start "rule__SelectQuery__LimitClauseAssignment_1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3772:1: rule__SelectQuery__LimitClauseAssignment_1 : ( ruleLimitClause ) ;
    public final void rule__SelectQuery__LimitClauseAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3776:1: ( ( ruleLimitClause ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3777:1: ( ruleLimitClause )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3777:1: ( ruleLimitClause )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3778:1: ruleLimitClause
            {
             before(grammarAccess.getSelectQueryAccess().getLimitClauseLimitClauseParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleLimitClause_in_rule__SelectQuery__LimitClauseAssignment_17630);
            ruleLimitClause();

            state._fsp--;

             after(grammarAccess.getSelectQueryAccess().getLimitClauseLimitClauseParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__LimitClauseAssignment_1"


    // $ANTLR start "rule__SelectQuery__OffsetClauseAssignment_2"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3787:1: rule__SelectQuery__OffsetClauseAssignment_2 : ( ruleOffsetClause ) ;
    public final void rule__SelectQuery__OffsetClauseAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3791:1: ( ( ruleOffsetClause ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3792:1: ( ruleOffsetClause )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3792:1: ( ruleOffsetClause )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3793:1: ruleOffsetClause
            {
             before(grammarAccess.getSelectQueryAccess().getOffsetClauseOffsetClauseParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleOffsetClause_in_rule__SelectQuery__OffsetClauseAssignment_27661);
            ruleOffsetClause();

            state._fsp--;

             after(grammarAccess.getSelectQueryAccess().getOffsetClauseOffsetClauseParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectQuery__OffsetClauseAssignment_2"


    // $ANTLR start "rule__Filter__ExpressionFilterAssignment_1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3802:1: rule__Filter__ExpressionFilterAssignment_1 : ( ruleExpressionFilter ) ;
    public final void rule__Filter__ExpressionFilterAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3806:1: ( ( ruleExpressionFilter ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3807:1: ( ruleExpressionFilter )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3807:1: ( ruleExpressionFilter )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3808:1: ruleExpressionFilter
            {
             before(grammarAccess.getFilterAccess().getExpressionFilterExpressionFilterParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleExpressionFilter_in_rule__Filter__ExpressionFilterAssignment_17692);
            ruleExpressionFilter();

            state._fsp--;

             after(grammarAccess.getFilterAccess().getExpressionFilterExpressionFilterParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Filter__ExpressionFilterAssignment_1"


    // $ANTLR start "rule__ExpressionFilter__NameAssignment_0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3817:1: rule__ExpressionFilter__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__ExpressionFilter__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3821:1: ( ( RULE_ID ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3822:1: ( RULE_ID )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3822:1: ( RULE_ID )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3823:1: RULE_ID
            {
             before(grammarAccess.getExpressionFilterAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__ExpressionFilter__NameAssignment_07723); 
             after(grammarAccess.getExpressionFilterAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__NameAssignment_0"


    // $ANTLR start "rule__ExpressionFilter__ParametersAssignment_2"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3832:1: rule__ExpressionFilter__ParametersAssignment_2 : ( ruleGraphNode ) ;
    public final void rule__ExpressionFilter__ParametersAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3836:1: ( ( ruleGraphNode ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3837:1: ( ruleGraphNode )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3837:1: ( ruleGraphNode )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3838:1: ruleGraphNode
            {
             before(grammarAccess.getExpressionFilterAccess().getParametersGraphNodeParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleGraphNode_in_rule__ExpressionFilter__ParametersAssignment_27754);
            ruleGraphNode();

            state._fsp--;

             after(grammarAccess.getExpressionFilterAccess().getParametersGraphNodeParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__ParametersAssignment_2"


    // $ANTLR start "rule__ExpressionFilter__ParametersAssignment_3_1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3847:1: rule__ExpressionFilter__ParametersAssignment_3_1 : ( ruleGraphNode ) ;
    public final void rule__ExpressionFilter__ParametersAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3851:1: ( ( ruleGraphNode ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3852:1: ( ruleGraphNode )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3852:1: ( ruleGraphNode )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3853:1: ruleGraphNode
            {
             before(grammarAccess.getExpressionFilterAccess().getParametersGraphNodeParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_ruleGraphNode_in_rule__ExpressionFilter__ParametersAssignment_3_17785);
            ruleGraphNode();

            state._fsp--;

             after(grammarAccess.getExpressionFilterAccess().getParametersGraphNodeParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__ParametersAssignment_3_1"


    // $ANTLR start "rule__ExpressionFilter__ParametersAssignment_4_1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3862:1: rule__ExpressionFilter__ParametersAssignment_4_1 : ( ruleGraphNode ) ;
    public final void rule__ExpressionFilter__ParametersAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3866:1: ( ( ruleGraphNode ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3867:1: ( ruleGraphNode )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3867:1: ( ruleGraphNode )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3868:1: ruleGraphNode
            {
             before(grammarAccess.getExpressionFilterAccess().getParametersGraphNodeParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_ruleGraphNode_in_rule__ExpressionFilter__ParametersAssignment_4_17816);
            ruleGraphNode();

            state._fsp--;

             after(grammarAccess.getExpressionFilterAccess().getParametersGraphNodeParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionFilter__ParametersAssignment_4_1"


    // $ANTLR start "rule__OrderClause__OrderConditionAssignment_1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3877:1: rule__OrderClause__OrderConditionAssignment_1 : ( ruleOrderCondition ) ;
    public final void rule__OrderClause__OrderConditionAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3881:1: ( ( ruleOrderCondition ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3882:1: ( ruleOrderCondition )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3882:1: ( ruleOrderCondition )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3883:1: ruleOrderCondition
            {
             before(grammarAccess.getOrderClauseAccess().getOrderConditionOrderConditionParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleOrderCondition_in_rule__OrderClause__OrderConditionAssignment_17847);
            ruleOrderCondition();

            state._fsp--;

             after(grammarAccess.getOrderClauseAccess().getOrderConditionOrderConditionParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderClause__OrderConditionAssignment_1"


    // $ANTLR start "rule__OrderCondition__BrackettedExpressionAssignment_0_1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3892:1: rule__OrderCondition__BrackettedExpressionAssignment_0_1 : ( ruleBrackettedExpression ) ;
    public final void rule__OrderCondition__BrackettedExpressionAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3896:1: ( ( ruleBrackettedExpression ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3897:1: ( ruleBrackettedExpression )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3897:1: ( ruleBrackettedExpression )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3898:1: ruleBrackettedExpression
            {
             before(grammarAccess.getOrderConditionAccess().getBrackettedExpressionBrackettedExpressionParserRuleCall_0_1_0()); 
            pushFollow(FOLLOW_ruleBrackettedExpression_in_rule__OrderCondition__BrackettedExpressionAssignment_0_17878);
            ruleBrackettedExpression();

            state._fsp--;

             after(grammarAccess.getOrderConditionAccess().getBrackettedExpressionBrackettedExpressionParserRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderCondition__BrackettedExpressionAssignment_0_1"


    // $ANTLR start "rule__BrackettedExpression__ExpressionAssignment_1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3907:1: rule__BrackettedExpression__ExpressionAssignment_1 : ( ruleExpression ) ;
    public final void rule__BrackettedExpression__ExpressionAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3911:1: ( ( ruleExpression ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3912:1: ( ruleExpression )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3912:1: ( ruleExpression )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3913:1: ruleExpression
            {
             before(grammarAccess.getBrackettedExpressionAccess().getExpressionExpressionParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleExpression_in_rule__BrackettedExpression__ExpressionAssignment_17909);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getBrackettedExpressionAccess().getExpressionExpressionParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BrackettedExpression__ExpressionAssignment_1"


    // $ANTLR start "rule__WhereClause__PatternAssignment_2_0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3922:1: rule__WhereClause__PatternAssignment_2_0 : ( rulePattern ) ;
    public final void rule__WhereClause__PatternAssignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3926:1: ( ( rulePattern ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3927:1: ( rulePattern )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3927:1: ( rulePattern )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3928:1: rulePattern
            {
             before(grammarAccess.getWhereClauseAccess().getPatternPatternParserRuleCall_2_0_0()); 
            pushFollow(FOLLOW_rulePattern_in_rule__WhereClause__PatternAssignment_2_07940);
            rulePattern();

            state._fsp--;

             after(grammarAccess.getWhereClauseAccess().getPatternPatternParserRuleCall_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__PatternAssignment_2_0"


    // $ANTLR start "rule__WhereClause__PatternAssignment_2_1_1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3937:1: rule__WhereClause__PatternAssignment_2_1_1 : ( rulePattern ) ;
    public final void rule__WhereClause__PatternAssignment_2_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3941:1: ( ( rulePattern ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3942:1: ( rulePattern )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3942:1: ( rulePattern )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3943:1: rulePattern
            {
             before(grammarAccess.getWhereClauseAccess().getPatternPatternParserRuleCall_2_1_1_0()); 
            pushFollow(FOLLOW_rulePattern_in_rule__WhereClause__PatternAssignment_2_1_17971);
            rulePattern();

            state._fsp--;

             after(grammarAccess.getWhereClauseAccess().getPatternPatternParserRuleCall_2_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__PatternAssignment_2_1_1"


    // $ANTLR start "rule__WhereClause__PatternAssignment_2_2_1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3952:1: rule__WhereClause__PatternAssignment_2_2_1 : ( rulePattern ) ;
    public final void rule__WhereClause__PatternAssignment_2_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3956:1: ( ( rulePattern ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3957:1: ( rulePattern )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3957:1: ( rulePattern )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3958:1: rulePattern
            {
             before(grammarAccess.getWhereClauseAccess().getPatternPatternParserRuleCall_2_2_1_0()); 
            pushFollow(FOLLOW_rulePattern_in_rule__WhereClause__PatternAssignment_2_2_18002);
            rulePattern();

            state._fsp--;

             after(grammarAccess.getWhereClauseAccess().getPatternPatternParserRuleCall_2_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__PatternAssignment_2_2_1"


    // $ANTLR start "rule__WhereClause__FilterAssignment_3"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3967:1: rule__WhereClause__FilterAssignment_3 : ( ruleFilter ) ;
    public final void rule__WhereClause__FilterAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3971:1: ( ( ruleFilter ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3972:1: ( ruleFilter )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3972:1: ( ruleFilter )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3973:1: ruleFilter
            {
             before(grammarAccess.getWhereClauseAccess().getFilterFilterParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleFilter_in_rule__WhereClause__FilterAssignment_38033);
            ruleFilter();

            state._fsp--;

             after(grammarAccess.getWhereClauseAccess().getFilterFilterParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__FilterAssignment_3"


    // $ANTLR start "rule__Pattern__PatternsAssignment_1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3982:1: rule__Pattern__PatternsAssignment_1 : ( rulePatternOne ) ;
    public final void rule__Pattern__PatternsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3986:1: ( ( rulePatternOne ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3987:1: ( rulePatternOne )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3987:1: ( rulePatternOne )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3988:1: rulePatternOne
            {
             before(grammarAccess.getPatternAccess().getPatternsPatternOneParserRuleCall_1_0()); 
            pushFollow(FOLLOW_rulePatternOne_in_rule__Pattern__PatternsAssignment_18064);
            rulePatternOne();

            state._fsp--;

             after(grammarAccess.getPatternAccess().getPatternsPatternOneParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pattern__PatternsAssignment_1"


    // $ANTLR start "rule__Pattern__PatternsAssignment_2_1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:3997:1: rule__Pattern__PatternsAssignment_2_1 : ( rulePatternOne ) ;
    public final void rule__Pattern__PatternsAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4001:1: ( ( rulePatternOne ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4002:1: ( rulePatternOne )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4002:1: ( rulePatternOne )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4003:1: rulePatternOne
            {
             before(grammarAccess.getPatternAccess().getPatternsPatternOneParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_rulePatternOne_in_rule__Pattern__PatternsAssignment_2_18095);
            rulePatternOne();

            state._fsp--;

             after(grammarAccess.getPatternAccess().getPatternsPatternOneParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pattern__PatternsAssignment_2_1"


    // $ANTLR start "rule__PatternOne__TriplesSameSubjectAssignment"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4012:1: rule__PatternOne__TriplesSameSubjectAssignment : ( ruleTriplesSameSubject ) ;
    public final void rule__PatternOne__TriplesSameSubjectAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4016:1: ( ( ruleTriplesSameSubject ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4017:1: ( ruleTriplesSameSubject )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4017:1: ( ruleTriplesSameSubject )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4018:1: ruleTriplesSameSubject
            {
             before(grammarAccess.getPatternOneAccess().getTriplesSameSubjectTriplesSameSubjectParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleTriplesSameSubject_in_rule__PatternOne__TriplesSameSubjectAssignment8126);
            ruleTriplesSameSubject();

            state._fsp--;

             after(grammarAccess.getPatternOneAccess().getTriplesSameSubjectTriplesSameSubjectParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PatternOne__TriplesSameSubjectAssignment"


    // $ANTLR start "rule__TriplesSameSubject__SubjectAssignment_0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4027:1: rule__TriplesSameSubject__SubjectAssignment_0 : ( ruleGraphNode ) ;
    public final void rule__TriplesSameSubject__SubjectAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4031:1: ( ( ruleGraphNode ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4032:1: ( ruleGraphNode )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4032:1: ( ruleGraphNode )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4033:1: ruleGraphNode
            {
             before(grammarAccess.getTriplesSameSubjectAccess().getSubjectGraphNodeParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleGraphNode_in_rule__TriplesSameSubject__SubjectAssignment_08157);
            ruleGraphNode();

            state._fsp--;

             after(grammarAccess.getTriplesSameSubjectAccess().getSubjectGraphNodeParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriplesSameSubject__SubjectAssignment_0"


    // $ANTLR start "rule__TriplesSameSubject__PropertyListAssignment_1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4042:1: rule__TriplesSameSubject__PropertyListAssignment_1 : ( rulePropertyList ) ;
    public final void rule__TriplesSameSubject__PropertyListAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4046:1: ( ( rulePropertyList ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4047:1: ( rulePropertyList )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4047:1: ( rulePropertyList )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4048:1: rulePropertyList
            {
             before(grammarAccess.getTriplesSameSubjectAccess().getPropertyListPropertyListParserRuleCall_1_0()); 
            pushFollow(FOLLOW_rulePropertyList_in_rule__TriplesSameSubject__PropertyListAssignment_18188);
            rulePropertyList();

            state._fsp--;

             after(grammarAccess.getTriplesSameSubjectAccess().getPropertyListPropertyListParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriplesSameSubject__PropertyListAssignment_1"


    // $ANTLR start "rule__TriplesSameSubject__PropertyListAssignment_2_1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4057:1: rule__TriplesSameSubject__PropertyListAssignment_2_1 : ( rulePropertyList ) ;
    public final void rule__TriplesSameSubject__PropertyListAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4061:1: ( ( rulePropertyList ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4062:1: ( rulePropertyList )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4062:1: ( rulePropertyList )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4063:1: rulePropertyList
            {
             before(grammarAccess.getTriplesSameSubjectAccess().getPropertyListPropertyListParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_rulePropertyList_in_rule__TriplesSameSubject__PropertyListAssignment_2_18219);
            rulePropertyList();

            state._fsp--;

             after(grammarAccess.getTriplesSameSubjectAccess().getPropertyListPropertyListParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriplesSameSubject__PropertyListAssignment_2_1"


    // $ANTLR start "rule__PropertyList__PropertyAssignment_0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4072:1: rule__PropertyList__PropertyAssignment_0 : ( ruleGraphNode ) ;
    public final void rule__PropertyList__PropertyAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4076:1: ( ( ruleGraphNode ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4077:1: ( ruleGraphNode )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4077:1: ( ruleGraphNode )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4078:1: ruleGraphNode
            {
             before(grammarAccess.getPropertyListAccess().getPropertyGraphNodeParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleGraphNode_in_rule__PropertyList__PropertyAssignment_08250);
            ruleGraphNode();

            state._fsp--;

             after(grammarAccess.getPropertyListAccess().getPropertyGraphNodeParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PropertyList__PropertyAssignment_0"


    // $ANTLR start "rule__PropertyList__ObjectAssignment_1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4087:1: rule__PropertyList__ObjectAssignment_1 : ( ruleGraphNode ) ;
    public final void rule__PropertyList__ObjectAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4091:1: ( ( ruleGraphNode ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4092:1: ( ruleGraphNode )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4092:1: ( ruleGraphNode )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4093:1: ruleGraphNode
            {
             before(grammarAccess.getPropertyListAccess().getObjectGraphNodeParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleGraphNode_in_rule__PropertyList__ObjectAssignment_18281);
            ruleGraphNode();

            state._fsp--;

             after(grammarAccess.getPropertyListAccess().getObjectGraphNodeParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PropertyList__ObjectAssignment_1"


    // $ANTLR start "rule__Quote__NameAssignment_0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4102:1: rule__Quote__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__Quote__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4106:1: ( ( RULE_ID ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4107:1: ( RULE_ID )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4107:1: ( RULE_ID )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4108:1: RULE_ID
            {
             before(grammarAccess.getQuoteAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Quote__NameAssignment_08312); 
             after(grammarAccess.getQuoteAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quote__NameAssignment_0"


    // $ANTLR start "rule__Parameter__NameAssignment_1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4117:1: rule__Parameter__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Parameter__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4121:1: ( ( RULE_ID ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4122:1: ( RULE_ID )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4122:1: ( RULE_ID )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4123:1: RULE_ID
            {
             before(grammarAccess.getParameterAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Parameter__NameAssignment_18343); 
             after(grammarAccess.getParameterAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__NameAssignment_1"


    // $ANTLR start "rule__BlankNode__NameAssignment_1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4132:1: rule__BlankNode__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__BlankNode__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4136:1: ( ( RULE_ID ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4137:1: ( RULE_ID )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4137:1: ( RULE_ID )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4138:1: RULE_ID
            {
             before(grammarAccess.getBlankNodeAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__BlankNode__NameAssignment_18374); 
             after(grammarAccess.getBlankNodeAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BlankNode__NameAssignment_1"


    // $ANTLR start "rule__Value__NameAssignment_0"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4147:1: rule__Value__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__Value__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4151:1: ( ( RULE_ID ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4152:1: ( RULE_ID )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4152:1: ( RULE_ID )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4153:1: RULE_ID
            {
             before(grammarAccess.getValueAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Value__NameAssignment_08405); 
             after(grammarAccess.getValueAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__NameAssignment_0"


    // $ANTLR start "rule__Value__StringValueAssignment_1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4162:1: rule__Value__StringValueAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Value__StringValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4166:1: ( ( RULE_STRING ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4167:1: ( RULE_STRING )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4167:1: ( RULE_STRING )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4168:1: RULE_STRING
            {
             before(grammarAccess.getValueAccess().getStringValueSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Value__StringValueAssignment_18436); 
             after(grammarAccess.getValueAccess().getStringValueSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__StringValueAssignment_1"


    // $ANTLR start "rule__Value__IntegerValueAssignment_2"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4177:1: rule__Value__IntegerValueAssignment_2 : ( RULE_INT ) ;
    public final void rule__Value__IntegerValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4181:1: ( ( RULE_INT ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4182:1: ( RULE_INT )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4182:1: ( RULE_INT )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4183:1: RULE_INT
            {
             before(grammarAccess.getValueAccess().getIntegerValueINTTerminalRuleCall_2_0()); 
            match(input,RULE_INT,FOLLOW_RULE_INT_in_rule__Value__IntegerValueAssignment_28467); 
             after(grammarAccess.getValueAccess().getIntegerValueINTTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__IntegerValueAssignment_2"


    // $ANTLR start "rule__DatasetClause__DefaultGraphClauseAssignment_0_1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4192:1: rule__DatasetClause__DefaultGraphClauseAssignment_0_1 : ( ruleDefaultGraphClause ) ;
    public final void rule__DatasetClause__DefaultGraphClauseAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4196:1: ( ( ruleDefaultGraphClause ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4197:1: ( ruleDefaultGraphClause )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4197:1: ( ruleDefaultGraphClause )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4198:1: ruleDefaultGraphClause
            {
             before(grammarAccess.getDatasetClauseAccess().getDefaultGraphClauseDefaultGraphClauseParserRuleCall_0_1_0()); 
            pushFollow(FOLLOW_ruleDefaultGraphClause_in_rule__DatasetClause__DefaultGraphClauseAssignment_0_18498);
            ruleDefaultGraphClause();

            state._fsp--;

             after(grammarAccess.getDatasetClauseAccess().getDefaultGraphClauseDefaultGraphClauseParserRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DatasetClause__DefaultGraphClauseAssignment_0_1"


    // $ANTLR start "rule__DatasetClause__NamedGraphClauseAssignment_1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4207:1: rule__DatasetClause__NamedGraphClauseAssignment_1 : ( ruleNamedGraphClause ) ;
    public final void rule__DatasetClause__NamedGraphClauseAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4211:1: ( ( ruleNamedGraphClause ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4212:1: ( ruleNamedGraphClause )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4212:1: ( ruleNamedGraphClause )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4213:1: ruleNamedGraphClause
            {
             before(grammarAccess.getDatasetClauseAccess().getNamedGraphClauseNamedGraphClauseParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleNamedGraphClause_in_rule__DatasetClause__NamedGraphClauseAssignment_18529);
            ruleNamedGraphClause();

            state._fsp--;

             after(grammarAccess.getDatasetClauseAccess().getNamedGraphClauseNamedGraphClauseParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DatasetClause__NamedGraphClauseAssignment_1"


    // $ANTLR start "rule__DefaultGraphClause__IrefAssignment"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4222:1: rule__DefaultGraphClause__IrefAssignment : ( RULE_IRI_URI ) ;
    public final void rule__DefaultGraphClause__IrefAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4226:1: ( ( RULE_IRI_URI ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4227:1: ( RULE_IRI_URI )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4227:1: ( RULE_IRI_URI )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4228:1: RULE_IRI_URI
            {
             before(grammarAccess.getDefaultGraphClauseAccess().getIrefIRI_URITerminalRuleCall_0()); 
            match(input,RULE_IRI_URI,FOLLOW_RULE_IRI_URI_in_rule__DefaultGraphClause__IrefAssignment8560); 
             after(grammarAccess.getDefaultGraphClauseAccess().getIrefIRI_URITerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultGraphClause__IrefAssignment"


    // $ANTLR start "rule__SourceSelector__IrefAssignment"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4237:1: rule__SourceSelector__IrefAssignment : ( RULE_IRI_URI ) ;
    public final void rule__SourceSelector__IrefAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4241:1: ( ( RULE_IRI_URI ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4242:1: ( RULE_IRI_URI )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4242:1: ( RULE_IRI_URI )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4243:1: RULE_IRI_URI
            {
             before(grammarAccess.getSourceSelectorAccess().getIrefIRI_URITerminalRuleCall_0()); 
            match(input,RULE_IRI_URI,FOLLOW_RULE_IRI_URI_in_rule__SourceSelector__IrefAssignment8591); 
             after(grammarAccess.getSourceSelectorAccess().getIrefIRI_URITerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SourceSelector__IrefAssignment"


    // $ANTLR start "rule__VAR1__NameAssignment_1"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4252:1: rule__VAR1__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__VAR1__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4256:1: ( ( RULE_ID ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4257:1: ( RULE_ID )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4257:1: ( RULE_ID )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4258:1: RULE_ID
            {
             before(grammarAccess.getVAR1Access().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__VAR1__NameAssignment_18622); 
             after(grammarAccess.getVAR1Access().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VAR1__NameAssignment_1"


    // $ANTLR start "rule__VAR2__NameAssignment_2"
    // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4267:1: rule__VAR2__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__VAR2__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4271:1: ( ( RULE_ID ) )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4272:1: ( RULE_ID )
            {
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4272:1: ( RULE_ID )
            // ../org.edu.le.spaqrl.ui/src-gen/org/edu/le/spaqrl/ui/contentassist/antlr/internal/InternalSparql.g:4273:1: RULE_ID
            {
             before(grammarAccess.getVAR2Access().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__VAR2__NameAssignment_28653); 
             after(grammarAccess.getVAR2Access().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VAR2__NameAssignment_2"

    // Delegated rules


    protected DFA17 dfa17 = new DFA17(this);
    static final String DFA17_eotS =
        "\25\uffff";
    static final String DFA17_eofS =
        "\25\uffff";
    static final String DFA17_minS =
        "\1\26\1\4\1\uffff\2\6\4\uffff\2\6\1\uffff\1\5\1\22\2\uffff\1\22"+
        "\1\5\1\6\1\22\1\uffff";
    static final String DFA17_maxS =
        "\1\27\1\46\1\uffff\1\6\1\22\4\uffff\2\6\1\uffff\1\5\1\22\2\uffff"+
        "\1\22\1\5\1\6\1\22\1\uffff";
    static final String DFA17_acceptS =
        "\2\uffff\1\2\2\uffff\4\1\2\uffff\1\1\2\uffff\2\1\4\uffff\1\1";
    static final String DFA17_specialS =
        "\25\uffff}>";
    static final String[] DFA17_transitionS = {
            "\1\2\1\1",
            "\1\7\1\10\1\5\1\6\11\uffff\1\4\20\uffff\1\12\1\11\2\uffff"+
            "\1\3",
            "",
            "\1\13",
            "\1\15\13\uffff\1\14",
            "",
            "",
            "",
            "",
            "\1\16",
            "\1\17",
            "",
            "\1\20",
            "\1\21",
            "",
            "",
            "\1\22",
            "\1\23",
            "\1\24",
            "\1\22",
            ""
    };

    static final short[] DFA17_eot = DFA.unpackEncodedString(DFA17_eotS);
    static final short[] DFA17_eof = DFA.unpackEncodedString(DFA17_eofS);
    static final char[] DFA17_min = DFA.unpackEncodedStringToUnsignedChars(DFA17_minS);
    static final char[] DFA17_max = DFA.unpackEncodedStringToUnsignedChars(DFA17_maxS);
    static final short[] DFA17_accept = DFA.unpackEncodedString(DFA17_acceptS);
    static final short[] DFA17_special = DFA.unpackEncodedString(DFA17_specialS);
    static final short[][] DFA17_transition;

    static {
        int numStates = DFA17_transitionS.length;
        DFA17_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA17_transition[i] = DFA.unpackEncodedString(DFA17_transitionS[i]);
        }
    }

    class DFA17 extends DFA {

        public DFA17(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 17;
            this.eot = DFA17_eot;
            this.eof = DFA17_eof;
            this.min = DFA17_min;
            this.max = DFA17_max;
            this.accept = DFA17_accept;
            this.special = DFA17_special;
            this.transition = DFA17_transition;
        }
        public String getDescription() {
            return "1787:1: ( rule__ExpressionFilter__Group_3__0 )?";
        }
    }
 

    public static final BitSet FOLLOW_ruleQuery_in_entryRuleQuery61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQuery68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Query__SelectQueryAssignment_in_ruleQuery94 = new BitSet(new long[]{0x00000000030A0002L});
    public static final BitSet FOLLOW_rulePrefix_in_entryRulePrefix122 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePrefix129 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Prefix__Alternatives_in_rulePrefix155 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUnNamedPrefix_in_entryRuleUnNamedPrefix182 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleUnNamedPrefix189 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__UnNamedPrefix__Group__0_in_ruleUnNamedPrefix215 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSelectQuery_in_entryRuleSelectQuery242 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSelectQuery249 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SelectQuery__Alternatives_in_ruleSelectQuery275 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFilter_in_entryRuleFilter302 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFilter309 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Filter__Group__0_in_ruleFilter335 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpressionFilter_in_entryRuleExpressionFilter362 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExpressionFilter369 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__Group__0_in_ruleExpressionFilter395 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOffsetClause_in_entryRuleOffsetClause422 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOffsetClause429 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OffsetClause__Group__0_in_ruleOffsetClause455 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLimitClause_in_entryRuleLimitClause482 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLimitClause489 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LimitClause__Group__0_in_ruleLimitClause515 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleINTEGER_in_entryRuleINTEGER542 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleINTEGER549 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleINTEGER575 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOrderClause_in_entryRuleOrderClause601 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOrderClause608 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderClause__Group__0_in_ruleOrderClause634 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOrderCondition_in_entryRuleOrderCondition661 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOrderCondition668 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderCondition__Alternatives_in_ruleOrderCondition694 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBrackettedExpression_in_entryRuleBrackettedExpression721 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBrackettedExpression728 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BrackettedExpression__Group__0_in_ruleBrackettedExpression754 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_entryRuleExpression781 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExpression788 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVar_in_ruleExpression814 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhereClause_in_entryRuleWhereClause840 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWhereClause847 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__Group__0_in_ruleWhereClause873 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePattern_in_entryRulePattern900 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePattern907 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Pattern__Group__0_in_rulePattern933 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePatternOne_in_entryRulePatternOne960 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePatternOne967 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PatternOne__TriplesSameSubjectAssignment_in_rulePatternOne993 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTriplesSameSubject_in_entryRuleTriplesSameSubject1020 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTriplesSameSubject1027 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TriplesSameSubject__Group__0_in_ruleTriplesSameSubject1053 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePropertyList_in_entryRulePropertyList1080 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePropertyList1087 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PropertyList__Group__0_in_rulePropertyList1113 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGraphNode_in_entryRuleGraphNode1140 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGraphNode1147 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__GraphNode__Alternatives_in_ruleGraphNode1173 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQuote_in_entryRuleQuote1200 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQuote1207 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Quote__Group__0_in_ruleQuote1233 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParameter_in_entryRuleParameter1260 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleParameter1267 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Parameter__Group__0_in_ruleParameter1293 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBlankNode_in_entryRuleBlankNode1320 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBlankNode1327 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BlankNode__Group__0_in_ruleBlankNode1353 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleValue_in_entryRuleValue1380 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleValue1387 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value__Alternatives_in_ruleValue1413 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDatasetClause_in_entryRuleDatasetClause1440 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDatasetClause1447 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__DatasetClause__Alternatives_in_ruleDatasetClause1473 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDefaultGraphClause_in_entryRuleDefaultGraphClause1500 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDefaultGraphClause1507 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__DefaultGraphClause__IrefAssignment_in_ruleDefaultGraphClause1533 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNamedGraphClause_in_entryRuleNamedGraphClause1560 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNamedGraphClause1567 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NamedGraphClause__Group__0_in_ruleNamedGraphClause1593 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSourceSelector_in_entryRuleSourceSelector1620 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSourceSelector1627 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SourceSelector__IrefAssignment_in_ruleSourceSelector1653 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVar_in_entryRuleVar1680 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVar1687 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Var__Alternatives_in_ruleVar1713 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVAR1_in_entryRuleVAR11740 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVAR11747 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VAR1__Group__0_in_ruleVAR11773 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVAR2_in_entryRuleVAR21800 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVAR21807 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VAR2__Group__0_in_ruleVAR21833 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Prefix__Group_0__0_in_rule__Prefix__Alternatives1869 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUnNamedPrefix_in_rule__Prefix__Alternatives1887 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SelectQuery__Group_0__0_in_rule__SelectQuery__Alternatives1919 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SelectQuery__LimitClauseAssignment_1_in_rule__SelectQuery__Alternatives1937 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SelectQuery__OffsetClauseAssignment_2_in_rule__SelectQuery__Alternatives1955 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__SelectQuery__Alternatives_0_21989 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__SelectQuery__Alternatives_0_22009 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SelectQuery__VarAssignment_0_3_0_in_rule__SelectQuery__Alternatives_0_32045 = new BitSet(new long[]{0x0000004000020002L});
    public static final BitSet FOLLOW_rule__SelectQuery__VarAssignment_0_3_0_in_rule__SelectQuery__Alternatives_0_32057 = new BitSet(new long[]{0x0000004000020002L});
    public static final BitSet FOLLOW_14_in_rule__SelectQuery__Alternatives_0_32079 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderCondition__Group_0__0_in_rule__OrderCondition__Alternatives2113 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVar_in_rule__OrderCondition__Alternatives2131 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__OrderCondition__Alternatives_0_02164 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__OrderCondition__Alternatives_0_02184 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVar_in_rule__GraphNode__Alternatives2218 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleValue_in_rule__GraphNode__Alternatives2235 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_IRI_URI_in_rule__GraphNode__Alternatives2252 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBlankNode_in_rule__GraphNode__Alternatives2269 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParameter_in_rule__GraphNode__Alternatives2286 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQuote_in_rule__GraphNode__Alternatives2303 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value__NameAssignment_0_in_rule__Value__Alternatives2335 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value__StringValueAssignment_1_in_rule__Value__Alternatives2353 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value__IntegerValueAssignment_2_in_rule__Value__Alternatives2371 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__DatasetClause__Group_0__0_in_rule__DatasetClause__Alternatives2404 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__DatasetClause__NamedGraphClauseAssignment_1_in_rule__DatasetClause__Alternatives2422 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVAR1_in_rule__Var__Alternatives2455 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVAR2_in_rule__Var__Alternatives2472 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Prefix__Group_0__0__Impl_in_rule__Prefix__Group_0__02502 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_rule__Prefix__Group_0__1_in_rule__Prefix__Group_0__02505 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Prefix__Group_0__0__Impl2533 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Prefix__Group_0__1__Impl_in_rule__Prefix__Group_0__12564 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_rule__Prefix__Group_0__2_in_rule__Prefix__Group_0__12567 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Prefix__NameAssignment_0_1_in_rule__Prefix__Group_0__1__Impl2594 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Prefix__Group_0__2__Impl_in_rule__Prefix__Group_0__22624 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__Prefix__Group_0__3_in_rule__Prefix__Group_0__22627 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__Prefix__Group_0__2__Impl2655 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Prefix__Group_0__3__Impl_in_rule__Prefix__Group_0__32686 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Prefix__IrefAssignment_0_3_in_rule__Prefix__Group_0__3__Impl2713 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__UnNamedPrefix__Group__0__Impl_in_rule__UnNamedPrefix__Group__02751 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_rule__UnNamedPrefix__Group__1_in_rule__UnNamedPrefix__Group__02754 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__UnNamedPrefix__Group__0__Impl2782 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__UnNamedPrefix__Group__1__Impl_in_rule__UnNamedPrefix__Group__12813 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__UnNamedPrefix__Group__2_in_rule__UnNamedPrefix__Group__12816 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__UnNamedPrefix__Group__1__Impl2844 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__UnNamedPrefix__Group__2__Impl_in_rule__UnNamedPrefix__Group__22875 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__UnNamedPrefix__IrefAssignment_2_in_rule__UnNamedPrefix__Group__2__Impl2902 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SelectQuery__Group_0__0__Impl_in_rule__SelectQuery__Group_0__02938 = new BitSet(new long[]{0x00000000000A0000L});
    public static final BitSet FOLLOW_rule__SelectQuery__Group_0__1_in_rule__SelectQuery__Group_0__02941 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SelectQuery__PrefixAssignment_0_0_in_rule__SelectQuery__Group_0__0__Impl2968 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_rule__SelectQuery__Group_0__1__Impl_in_rule__SelectQuery__Group_0__12999 = new BitSet(new long[]{0x0000004000027000L});
    public static final BitSet FOLLOW_rule__SelectQuery__Group_0__2_in_rule__SelectQuery__Group_0__13002 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__SelectQuery__Group_0__1__Impl3030 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SelectQuery__Group_0__2__Impl_in_rule__SelectQuery__Group_0__23061 = new BitSet(new long[]{0x0000004000027000L});
    public static final BitSet FOLLOW_rule__SelectQuery__Group_0__3_in_rule__SelectQuery__Group_0__23064 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SelectQuery__Alternatives_0_2_in_rule__SelectQuery__Group_0__2__Impl3091 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SelectQuery__Group_0__3__Impl_in_rule__SelectQuery__Group_0__33122 = new BitSet(new long[]{0x0000003008000000L});
    public static final BitSet FOLLOW_rule__SelectQuery__Group_0__4_in_rule__SelectQuery__Group_0__33125 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SelectQuery__Alternatives_0_3_in_rule__SelectQuery__Group_0__3__Impl3152 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SelectQuery__Group_0__4__Impl_in_rule__SelectQuery__Group_0__43182 = new BitSet(new long[]{0x0000003008000000L});
    public static final BitSet FOLLOW_rule__SelectQuery__Group_0__5_in_rule__SelectQuery__Group_0__43185 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SelectQuery__DatasetClauseAssignment_0_4_in_rule__SelectQuery__Group_0__4__Impl3212 = new BitSet(new long[]{0x0000003000000002L});
    public static final BitSet FOLLOW_rule__SelectQuery__Group_0__5__Impl_in_rule__SelectQuery__Group_0__53243 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_rule__SelectQuery__Group_0__6_in_rule__SelectQuery__Group_0__53246 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SelectQuery__WhereClauseAssignment_0_5_in_rule__SelectQuery__Group_0__5__Impl3273 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SelectQuery__Group_0__6__Impl_in_rule__SelectQuery__Group_0__63303 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SelectQuery__OrderClauseAssignment_0_6_in_rule__SelectQuery__Group_0__6__Impl3330 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Filter__Group__0__Impl_in_rule__Filter__Group__03375 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_rule__Filter__Group__1_in_rule__Filter__Group__03378 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__Filter__Group__0__Impl3406 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Filter__Group__1__Impl_in_rule__Filter__Group__13437 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Filter__ExpressionFilterAssignment_1_in_rule__Filter__Group__1__Impl3464 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__Group__0__Impl_in_rule__ExpressionFilter__Group__03498 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__Group__1_in_rule__ExpressionFilter__Group__03501 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__NameAssignment_0_in_rule__ExpressionFilter__Group__0__Impl3528 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__Group__1__Impl_in_rule__ExpressionFilter__Group__13558 = new BitSet(new long[]{0x0000004C000200F0L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__Group__2_in_rule__ExpressionFilter__Group__13561 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__ExpressionFilter__Group__1__Impl3589 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__Group__2__Impl_in_rule__ExpressionFilter__Group__23620 = new BitSet(new long[]{0x0000000000C00000L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__Group__3_in_rule__ExpressionFilter__Group__23623 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__ParametersAssignment_2_in_rule__ExpressionFilter__Group__2__Impl3650 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__Group__3__Impl_in_rule__ExpressionFilter__Group__33680 = new BitSet(new long[]{0x0000000000C00000L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__Group__4_in_rule__ExpressionFilter__Group__33683 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__Group_3__0_in_rule__ExpressionFilter__Group__3__Impl3710 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__Group__4__Impl_in_rule__ExpressionFilter__Group__43741 = new BitSet(new long[]{0x0000000000C00000L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__Group__5_in_rule__ExpressionFilter__Group__43744 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__Group_4__0_in_rule__ExpressionFilter__Group__4__Impl3771 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__Group__5__Impl_in_rule__ExpressionFilter__Group__53802 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__ExpressionFilter__Group__5__Impl3830 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__Group_3__0__Impl_in_rule__ExpressionFilter__Group_3__03873 = new BitSet(new long[]{0x0000004C000200F0L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__Group_3__1_in_rule__ExpressionFilter__Group_3__03876 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__ExpressionFilter__Group_3__0__Impl3904 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__Group_3__1__Impl_in_rule__ExpressionFilter__Group_3__13935 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__ParametersAssignment_3_1_in_rule__ExpressionFilter__Group_3__1__Impl3962 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__Group_4__0__Impl_in_rule__ExpressionFilter__Group_4__03996 = new BitSet(new long[]{0x0000004C000200F0L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__Group_4__1_in_rule__ExpressionFilter__Group_4__03999 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__ExpressionFilter__Group_4__0__Impl4027 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__Group_4__1__Impl_in_rule__ExpressionFilter__Group_4__14058 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExpressionFilter__ParametersAssignment_4_1_in_rule__ExpressionFilter__Group_4__1__Impl4085 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OffsetClause__Group__0__Impl_in_rule__OffsetClause__Group__04119 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__OffsetClause__Group__1_in_rule__OffsetClause__Group__04122 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__OffsetClause__Group__0__Impl4150 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OffsetClause__Group__1__Impl_in_rule__OffsetClause__Group__14181 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleINTEGER_in_rule__OffsetClause__Group__1__Impl4208 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LimitClause__Group__0__Impl_in_rule__LimitClause__Group__04241 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__LimitClause__Group__1_in_rule__LimitClause__Group__04244 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_rule__LimitClause__Group__0__Impl4272 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LimitClause__Group__1__Impl_in_rule__LimitClause__Group__14303 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleINTEGER_in_rule__LimitClause__Group__1__Impl4330 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderClause__Group__0__Impl_in_rule__OrderClause__Group__04363 = new BitSet(new long[]{0x0000004000038000L});
    public static final BitSet FOLLOW_rule__OrderClause__Group__1_in_rule__OrderClause__Group__04366 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_rule__OrderClause__Group__0__Impl4394 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderClause__Group__1__Impl_in_rule__OrderClause__Group__14425 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderClause__OrderConditionAssignment_1_in_rule__OrderClause__Group__1__Impl4454 = new BitSet(new long[]{0x0000004000038002L});
    public static final BitSet FOLLOW_rule__OrderClause__OrderConditionAssignment_1_in_rule__OrderClause__Group__1__Impl4466 = new BitSet(new long[]{0x0000004000038002L});
    public static final BitSet FOLLOW_rule__OrderCondition__Group_0__0__Impl_in_rule__OrderCondition__Group_0__04503 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_rule__OrderCondition__Group_0__1_in_rule__OrderCondition__Group_0__04506 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderCondition__Alternatives_0_0_in_rule__OrderCondition__Group_0__0__Impl4533 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderCondition__Group_0__1__Impl_in_rule__OrderCondition__Group_0__14563 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderCondition__BrackettedExpressionAssignment_0_1_in_rule__OrderCondition__Group_0__1__Impl4590 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BrackettedExpression__Group__0__Impl_in_rule__BrackettedExpression__Group__04624 = new BitSet(new long[]{0x0000004000020000L});
    public static final BitSet FOLLOW_rule__BrackettedExpression__Group__1_in_rule__BrackettedExpression__Group__04627 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__BrackettedExpression__Group__0__Impl4655 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BrackettedExpression__Group__1__Impl_in_rule__BrackettedExpression__Group__14686 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_rule__BrackettedExpression__Group__2_in_rule__BrackettedExpression__Group__14689 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BrackettedExpression__ExpressionAssignment_1_in_rule__BrackettedExpression__Group__1__Impl4716 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BrackettedExpression__Group__2__Impl_in_rule__BrackettedExpression__Group__24746 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__BrackettedExpression__Group__2__Impl4774 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__Group__0__Impl_in_rule__WhereClause__Group__04811 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_rule__WhereClause__Group__1_in_rule__WhereClause__Group__04814 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_rule__WhereClause__Group__0__Impl4842 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__Group__1__Impl_in_rule__WhereClause__Group__14873 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_rule__WhereClause__Group__2_in_rule__WhereClause__Group__14876 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_rule__WhereClause__Group__1__Impl4904 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__Group__2__Impl_in_rule__WhereClause__Group__24935 = new BitSet(new long[]{0x0000000020100000L});
    public static final BitSet FOLLOW_rule__WhereClause__Group__3_in_rule__WhereClause__Group__24938 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__Group_2__0_in_rule__WhereClause__Group__2__Impl4965 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__Group__3__Impl_in_rule__WhereClause__Group__34995 = new BitSet(new long[]{0x0000000020100000L});
    public static final BitSet FOLLOW_rule__WhereClause__Group__4_in_rule__WhereClause__Group__34998 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__FilterAssignment_3_in_rule__WhereClause__Group__3__Impl5025 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__Group__4__Impl_in_rule__WhereClause__Group__45056 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_rule__WhereClause__Group__4__Impl5084 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__Group_2__0__Impl_in_rule__WhereClause__Group_2__05125 = new BitSet(new long[]{0x00000000C0000000L});
    public static final BitSet FOLLOW_rule__WhereClause__Group_2__1_in_rule__WhereClause__Group_2__05128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__PatternAssignment_2_0_in_rule__WhereClause__Group_2__0__Impl5155 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__Group_2__1__Impl_in_rule__WhereClause__Group_2__15185 = new BitSet(new long[]{0x00000000C0000000L});
    public static final BitSet FOLLOW_rule__WhereClause__Group_2__2_in_rule__WhereClause__Group_2__15188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__Group_2_1__0_in_rule__WhereClause__Group_2__1__Impl5215 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__Group_2__2__Impl_in_rule__WhereClause__Group_2__25246 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__Group_2_2__0_in_rule__WhereClause__Group_2__2__Impl5273 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__Group_2_1__0__Impl_in_rule__WhereClause__Group_2_1__05310 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_rule__WhereClause__Group_2_1__1_in_rule__WhereClause__Group_2_1__05313 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_rule__WhereClause__Group_2_1__0__Impl5341 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__Group_2_1__1__Impl_in_rule__WhereClause__Group_2_1__15372 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__PatternAssignment_2_1_1_in_rule__WhereClause__Group_2_1__1__Impl5399 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__Group_2_2__0__Impl_in_rule__WhereClause__Group_2_2__05433 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_rule__WhereClause__Group_2_2__1_in_rule__WhereClause__Group_2_2__05436 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_rule__WhereClause__Group_2_2__0__Impl5464 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__Group_2_2__1__Impl_in_rule__WhereClause__Group_2_2__15495 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__PatternAssignment_2_2_1_in_rule__WhereClause__Group_2_2__1__Impl5522 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Pattern__Group__0__Impl_in_rule__Pattern__Group__05556 = new BitSet(new long[]{0x0000004C000200F0L});
    public static final BitSet FOLLOW_rule__Pattern__Group__1_in_rule__Pattern__Group__05559 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_rule__Pattern__Group__0__Impl5587 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Pattern__Group__1__Impl_in_rule__Pattern__Group__15618 = new BitSet(new long[]{0x0000000120000000L});
    public static final BitSet FOLLOW_rule__Pattern__Group__2_in_rule__Pattern__Group__15621 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Pattern__PatternsAssignment_1_in_rule__Pattern__Group__1__Impl5648 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Pattern__Group__2__Impl_in_rule__Pattern__Group__25678 = new BitSet(new long[]{0x0000000120000000L});
    public static final BitSet FOLLOW_rule__Pattern__Group__3_in_rule__Pattern__Group__25681 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Pattern__Group_2__0_in_rule__Pattern__Group__2__Impl5708 = new BitSet(new long[]{0x0000000100000002L});
    public static final BitSet FOLLOW_rule__Pattern__Group__3__Impl_in_rule__Pattern__Group__35739 = new BitSet(new long[]{0x0000000120000000L});
    public static final BitSet FOLLOW_rule__Pattern__Group__4_in_rule__Pattern__Group__35742 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_rule__Pattern__Group__3__Impl5771 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Pattern__Group__4__Impl_in_rule__Pattern__Group__45804 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_rule__Pattern__Group__4__Impl5832 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Pattern__Group_2__0__Impl_in_rule__Pattern__Group_2__05873 = new BitSet(new long[]{0x0000004C000200F0L});
    public static final BitSet FOLLOW_rule__Pattern__Group_2__1_in_rule__Pattern__Group_2__05876 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_rule__Pattern__Group_2__0__Impl5904 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Pattern__Group_2__1__Impl_in_rule__Pattern__Group_2__15935 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Pattern__PatternsAssignment_2_1_in_rule__Pattern__Group_2__1__Impl5962 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TriplesSameSubject__Group__0__Impl_in_rule__TriplesSameSubject__Group__05996 = new BitSet(new long[]{0x0000004C000200F0L});
    public static final BitSet FOLLOW_rule__TriplesSameSubject__Group__1_in_rule__TriplesSameSubject__Group__05999 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TriplesSameSubject__SubjectAssignment_0_in_rule__TriplesSameSubject__Group__0__Impl6026 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TriplesSameSubject__Group__1__Impl_in_rule__TriplesSameSubject__Group__16056 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_rule__TriplesSameSubject__Group__2_in_rule__TriplesSameSubject__Group__16059 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TriplesSameSubject__PropertyListAssignment_1_in_rule__TriplesSameSubject__Group__1__Impl6086 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TriplesSameSubject__Group__2__Impl_in_rule__TriplesSameSubject__Group__26116 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TriplesSameSubject__Group_2__0_in_rule__TriplesSameSubject__Group__2__Impl6143 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_rule__TriplesSameSubject__Group_2__0__Impl_in_rule__TriplesSameSubject__Group_2__06180 = new BitSet(new long[]{0x0000004C000200F0L});
    public static final BitSet FOLLOW_rule__TriplesSameSubject__Group_2__1_in_rule__TriplesSameSubject__Group_2__06183 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_rule__TriplesSameSubject__Group_2__0__Impl6211 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TriplesSameSubject__Group_2__1__Impl_in_rule__TriplesSameSubject__Group_2__16242 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TriplesSameSubject__PropertyListAssignment_2_1_in_rule__TriplesSameSubject__Group_2__1__Impl6269 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PropertyList__Group__0__Impl_in_rule__PropertyList__Group__06303 = new BitSet(new long[]{0x0000004C000200F0L});
    public static final BitSet FOLLOW_rule__PropertyList__Group__1_in_rule__PropertyList__Group__06306 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PropertyList__PropertyAssignment_0_in_rule__PropertyList__Group__0__Impl6333 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PropertyList__Group__1__Impl_in_rule__PropertyList__Group__16363 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PropertyList__ObjectAssignment_1_in_rule__PropertyList__Group__1__Impl6390 = new BitSet(new long[]{0x0000004C000200F2L});
    public static final BitSet FOLLOW_rule__Quote__Group__0__Impl_in_rule__Quote__Group__06425 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_rule__Quote__Group__1_in_rule__Quote__Group__06428 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Quote__NameAssignment_0_in_rule__Quote__Group__0__Impl6455 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Quote__Group__1__Impl_in_rule__Quote__Group__16485 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__Quote__Group__1__Impl6513 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Parameter__Group__0__Impl_in_rule__Parameter__Group__06548 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_rule__Parameter__Group__1_in_rule__Parameter__Group__06551 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_rule__Parameter__Group__0__Impl6579 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Parameter__Group__1__Impl_in_rule__Parameter__Group__16610 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Parameter__NameAssignment_1_in_rule__Parameter__Group__1__Impl6637 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BlankNode__Group__0__Impl_in_rule__BlankNode__Group__06671 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_rule__BlankNode__Group__1_in_rule__BlankNode__Group__06674 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_rule__BlankNode__Group__0__Impl6702 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BlankNode__Group__1__Impl_in_rule__BlankNode__Group__16733 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BlankNode__NameAssignment_1_in_rule__BlankNode__Group__1__Impl6760 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__DatasetClause__Group_0__0__Impl_in_rule__DatasetClause__Group_0__06794 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__DatasetClause__Group_0__1_in_rule__DatasetClause__Group_0__06797 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_rule__DatasetClause__Group_0__0__Impl6825 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__DatasetClause__Group_0__1__Impl_in_rule__DatasetClause__Group_0__16856 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__DatasetClause__DefaultGraphClauseAssignment_0_1_in_rule__DatasetClause__Group_0__1__Impl6883 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NamedGraphClause__Group__0__Impl_in_rule__NamedGraphClause__Group__06917 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__NamedGraphClause__Group__1_in_rule__NamedGraphClause__Group__06920 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_rule__NamedGraphClause__Group__0__Impl6948 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NamedGraphClause__Group__1__Impl_in_rule__NamedGraphClause__Group__16979 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSourceSelector_in_rule__NamedGraphClause__Group__1__Impl7006 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VAR1__Group__0__Impl_in_rule__VAR1__Group__07039 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_rule__VAR1__Group__1_in_rule__VAR1__Group__07042 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_rule__VAR1__Group__0__Impl7070 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VAR1__Group__1__Impl_in_rule__VAR1__Group__17101 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VAR1__NameAssignment_1_in_rule__VAR1__Group__1__Impl7128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VAR2__Group__0__Impl_in_rule__VAR2__Group__07162 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_rule__VAR2__Group__1_in_rule__VAR2__Group__07165 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrefix_in_rule__VAR2__Group__0__Impl7192 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VAR2__Group__1__Impl_in_rule__VAR2__Group__17221 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_rule__VAR2__Group__2_in_rule__VAR2__Group__17224 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__VAR2__Group__1__Impl7252 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VAR2__Group__2__Impl_in_rule__VAR2__Group__27283 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VAR2__NameAssignment_2_in_rule__VAR2__Group__2__Impl7310 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSelectQuery_in_rule__Query__SelectQueryAssignment7351 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Prefix__NameAssignment_0_17382 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_IRI_URI_in_rule__Prefix__IrefAssignment_0_37413 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_IRI_URI_in_rule__UnNamedPrefix__IrefAssignment_27444 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrefix_in_rule__SelectQuery__PrefixAssignment_0_07475 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVar_in_rule__SelectQuery__VarAssignment_0_3_07506 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDatasetClause_in_rule__SelectQuery__DatasetClauseAssignment_0_47537 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhereClause_in_rule__SelectQuery__WhereClauseAssignment_0_57568 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOrderClause_in_rule__SelectQuery__OrderClauseAssignment_0_67599 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLimitClause_in_rule__SelectQuery__LimitClauseAssignment_17630 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOffsetClause_in_rule__SelectQuery__OffsetClauseAssignment_27661 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpressionFilter_in_rule__Filter__ExpressionFilterAssignment_17692 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__ExpressionFilter__NameAssignment_07723 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGraphNode_in_rule__ExpressionFilter__ParametersAssignment_27754 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGraphNode_in_rule__ExpressionFilter__ParametersAssignment_3_17785 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGraphNode_in_rule__ExpressionFilter__ParametersAssignment_4_17816 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOrderCondition_in_rule__OrderClause__OrderConditionAssignment_17847 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBrackettedExpression_in_rule__OrderCondition__BrackettedExpressionAssignment_0_17878 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_rule__BrackettedExpression__ExpressionAssignment_17909 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePattern_in_rule__WhereClause__PatternAssignment_2_07940 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePattern_in_rule__WhereClause__PatternAssignment_2_1_17971 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePattern_in_rule__WhereClause__PatternAssignment_2_2_18002 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFilter_in_rule__WhereClause__FilterAssignment_38033 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePatternOne_in_rule__Pattern__PatternsAssignment_18064 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePatternOne_in_rule__Pattern__PatternsAssignment_2_18095 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTriplesSameSubject_in_rule__PatternOne__TriplesSameSubjectAssignment8126 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGraphNode_in_rule__TriplesSameSubject__SubjectAssignment_08157 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePropertyList_in_rule__TriplesSameSubject__PropertyListAssignment_18188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePropertyList_in_rule__TriplesSameSubject__PropertyListAssignment_2_18219 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGraphNode_in_rule__PropertyList__PropertyAssignment_08250 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGraphNode_in_rule__PropertyList__ObjectAssignment_18281 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Quote__NameAssignment_08312 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Parameter__NameAssignment_18343 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__BlankNode__NameAssignment_18374 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Value__NameAssignment_08405 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Value__StringValueAssignment_18436 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rule__Value__IntegerValueAssignment_28467 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDefaultGraphClause_in_rule__DatasetClause__DefaultGraphClauseAssignment_0_18498 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNamedGraphClause_in_rule__DatasetClause__NamedGraphClauseAssignment_18529 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_IRI_URI_in_rule__DefaultGraphClause__IrefAssignment8560 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_IRI_URI_in_rule__SourceSelector__IrefAssignment8591 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__VAR1__NameAssignment_18622 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__VAR2__NameAssignment_28653 = new BitSet(new long[]{0x0000000000000002L});

}