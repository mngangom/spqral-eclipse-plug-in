/*
 * generated by Xtext
 */
package org.edu.le.spaqrl.ui.contentassist

import org.edu.le.spaqrl.ui.contentassist.AbstractSparqlProposalProvider

/**
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#content-assist
 * on how to customize the content assistant.
 */
class SparqlProposalProvider extends AbstractSparqlProposalProvider {
}
