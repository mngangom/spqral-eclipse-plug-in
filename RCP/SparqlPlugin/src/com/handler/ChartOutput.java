package com.handler;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

import sparqlplugin.ViewChart;
import view.Perspective;
import view.SparqlEngine;

public class ChartOutput extends AbstractHandler implements IHandler {
	
public static PieDataset pieData;
public List<ArrayList> list= new ArrayList<ArrayList>();
int total;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ChartOutput co= new ChartOutput();
		pieData=   createDataset();
		IWorkbenchWindow window = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow();

		IWorkbenchPage page = window.getActivePage();
		try {
			ViewChart v = (ViewChart) page.showView(ViewChart.ID,
					Perspective.getRandomViewID(),
					IWorkbenchPage.VIEW_ACTIVATE);
		} catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		return null;
	}
	public  PieDataset getPieData()
	{
		return pieData;
	}
	
	
	 private PieDataset createDataset() {  
		 total =0;
		 SparqlEngine se= new SparqlEngine();
		 se.rawFormat();
		 list= se.getLists();
		  final DefaultPieDataset result = new DefaultPieDataset();
		  for(int i=0;i<list.size();i++)
		  {
			  total+=Integer.parseInt(list.get(i).get(1).toString().replaceAll("\\s+",""));  
		  }
		  for(int i=0;i<list.size();i++)
		  {
			  result.setValue(list.get(i).get(0).toString() + (Integer.parseInt( list.get(i).get(1).toString().replaceAll("\\s+","")))*(100/total)+ "%" ,(Integer.parseInt( list.get(i).get(1).toString().replaceAll("\\s+",""))));  
		  }
		  return result;  
		 } 
	
}
	


