package sparqlplugin;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.util.Rotation;
import org.jfree.experimental.chart.swt.ChartComposite;
import org.eclipse.swt.SWT; 

import com.handler.ChartOutput;



public class ViewChart extends ViewPart {
	public static final String ID = "SparqlPlugin.view";
	public ViewChart() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void createPartControl(Composite parent) {
		// TODO Auto-generated method stub
		
		final PieDataset dataset =  new ChartOutput().getPieData();  
		  final JFreeChart chart = createChart(dataset, "Value Percentage Presence");  
		  new ChartComposite(parent, SWT.Activate | SWT.FULL_SELECTION | SWT.BORDER , chart, true);  

	}
	
//	 private PieDataset createDataset() {  
//		  final DefaultPieDataset result = new DefaultPieDataset();  
//		  result.setValue("Linux", 29);  
//		  result.setValue("Mac", 20);  
//		  result.setValue("Windows", 51);  
//		  return result;  
//		 }  

	 private org.jfree.chart.JFreeChart createChart(final PieDataset dataset, final String title) {  
		  final JFreeChart chart = ChartFactory.createPieChart3D(title, dataset, true, true, false); 
		  
		  final PiePlot3D plot = (PiePlot3D) chart.getPlot();  
		  plot.setStartAngle(290);  
		  plot.setDirection(Rotation.CLOCKWISE);  
		  plot.setForegroundAlpha(0.5f);  
		
	        
		  
		  return chart;  
		 }  
	 
	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

}
