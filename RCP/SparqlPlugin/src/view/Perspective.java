package view;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class Perspective implements IPerspectiveFactory {

	private static IFolderLayout views;
	private static int count=1;

	public void createInitialLayout(IPageLayout layout) {
		views = layout.createFolder("Views", IPageLayout.BOTTOM, 1.0f,
				layout.getEditorArea());

		views.addView(View.ID + ":" + count++);

		layout.setEditorAreaVisible(true);
		layout.setFixed(true);

	}

	public static void addView() {
		views.addView(View.ID + ":" + count++);
	}
	public static String getRandomViewID()
	{
		return (int)(Math.random()*count) + "";
	}

}
