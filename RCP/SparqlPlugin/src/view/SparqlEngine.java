package view;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.DatasetFactory;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;

public class SparqlEngine {

	private List listFormattedSingle = new ArrayList();
	private List l2 = new ArrayList();
	List listNew = new ArrayList();
	private List<ArrayList> lFormattedAll = new ArrayList<ArrayList>();
	List<String> ar;

	public List listFormat() {
		listNew.clear(); // to remove all the elements from the list
		String dftGraphURI = "file:///" + new LoadHandler().getFilePath();
		String sparqlContent = new LoadSparql().getFilePath().toString();
		StringBuffer sb = new StringBuffer();
		Dataset data = DatasetFactory.create(dftGraphURI);
		Query query = QueryFactory.create(sparqlContent);
		QueryExecution qexec = QueryExecutionFactory.create(query, data);
		ResultSet results = qexec.execSelect();
		ar = results.getResultVars();

		for (; results.hasNext();) {
			//QuerySolution soln = results.nextSolution();
			listNew.addAll(ResultSetFormatter.toList(results));
		}
		formatting(listNew);
		return lFormattedAll;
	}

	public void formatting(List l) {
		for (int i = 0; i < l.size(); i++) {
			int count = (int) l.get(i).toString().length();
			String temp = (String) l.get(i).toString();
			Pattern regexpression = Pattern.compile("\"([^\"]*)\"|[0-9]*\\.?[0-9]+ |[0-9]+ | [#a-z*]"); //
			ArrayList<String> allMatches = new ArrayList<String>();
			Matcher matcherRegex = regexpression.matcher(temp);
			while (matcherRegex.find()) {
				allMatches.add(matcherRegex.group());
			}
			if(!allMatches.equals(null) && (allMatches.size()!=0)){
			System.out.println(allMatches);
			listFormattedSingle.add(allMatches);
			}
			else
			{
				for (int j = 0; j < l.size(); j++) {
					String tempNew = (String) l.get(j).toString();
					allMatches.add(tempNew.substring(tempNew.lastIndexOf("#")+1, tempNew.lastIndexOf(">")));
					
				}listFormattedSingle.add(allMatches);
				}
			
		}
		System.out.println(listFormattedSingle);
		lFormattedAll.add((ArrayList) listFormattedSingle);// get the list that contains all the values

	}

	List<String> getRows() {
		return ar;
	}

	public void XMLFormat(String file) throws Exception {
		String dftGraphURI = "file:///" + new LoadHandler().getFilePath();
		String sparqlContent = new LoadSparql().getFilePath().toString();
		StringBuffer sb = new StringBuffer();

		Dataset data = DatasetFactory.create(dftGraphURI);
		Query query = QueryFactory.create(sparqlContent);
		QueryExecution qexec = QueryExecutionFactory.create(query, data);
		ResultSet results = qexec.execSelect();
		ar = results.getResultVars();

		for (; results.hasNext();) {
			sb.append(ResultSetFormatter.asXMLString(results));
		}
		System.out.println(sb.toString());
		PrintWriter out = new PrintWriter(file);
		out.write(sb.toString());
		out.flush();
		out.close();
	}

	public List getLists() {
		return listFormattedSingle;
	}

	public int[] sizeCalc(List<ArrayList> lSize) {
		int[] sizeArray = new int[lSize.get(0).size()];
		for (int i = 0; i < lSize.get(0).size(); i++) {
			if (lSize.get(0).get(i).toString().length() < 10) {
				sizeArray[i] = lSize.get(0).get(i).toString().length() * 15;
			} else {
				sizeArray[i] = lSize.get(0).get(i).toString().length() * 10;
			}
		}
		return sizeArray;
	}

}
