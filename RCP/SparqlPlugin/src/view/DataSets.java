package view;

import java.util.ArrayList;
import java.util.List;

public enum DataSets {
	INSTANCE;
	  private List<ArrayList> listAll= new ArrayList<ArrayList>();
	 
	  void ModelProvider() {
		  ExecuteHandler eh = new ExecuteHandler();
		  SparqlEngine se= new SparqlEngine();
		  se.rawFormat();
			
			listAll=se.getLists();
	  }

	  public List<ArrayList> getListAll() {
		  ModelProvider();
	    return listAll;
	  }
	  

}
