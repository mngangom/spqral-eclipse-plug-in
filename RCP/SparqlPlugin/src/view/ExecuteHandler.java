package view;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.text.IDocument;
//import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.DatasetFactory;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;

public class ExecuteHandler extends AbstractHandler implements IHandler {
	private List l1 = new ArrayList();
	private List l2 = new ArrayList();
	List listNew = new ArrayList();
	private List<ArrayList> lTogether = new ArrayList<ArrayList>();
	View v;
	Utility ut = new Utility();
	List<String> ar;

	public static String outputChoice;

	public void setOutputChoice(String choice) {
		outputChoice = choice;
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ExecuteHandler eh = new ExecuteHandler();
		System.out.println(eh.getCurrentEditorContent());
		
		XMLSelectHandler xh = new XMLSelectHandler();
		LoadHandler load = new LoadHandler();
		SparqlEngine se= new SparqlEngine();
		System.out.println(se.listFormat());
		switch (outputChoice) {
		case "XML":
			String file = xh.getXMLFilePath().toString();
			try {
				se.XMLFormat(file);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			break;
		case "Table":
			IWorkbenchWindow window = PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow();

			IWorkbenchPage page = window.getActivePage();
			try {	
				 v = (View) page.showView(View.ID,Perspective.getRandomViewID(),
						IWorkbenchPage.VIEW_ACTIVATE);
				v.executeSparql();
			} catch (PartInitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;

		default:
			break;
		}
		return null;
	}

	public String getCurrentEditorContent() {
		final IEditorPart activeEditor = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		if (activeEditor == null)
			return null;
		final IDocument doc = (IDocument) activeEditor
				.getAdapter(IDocument.class);
		if (doc == null)
			return null;

		return doc.get();
	}

}