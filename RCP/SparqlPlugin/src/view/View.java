package view;

//import org.eclipse.core.resources.mapping.ModelProvider;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchSite;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

public class View extends ViewPart {
	public static final String ID = "View.view";
	boolean check = false;
	static List<ArrayList> lists;
	private TableViewer viewer;
	static int count = 0;
	static Composite composite;
	static IWorkbenchSite site;
	static TableViewerColumn colSl;
	TableViewerColumn col1, col2, col3, col4, col5;

	Text text;

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	public void createPartControl(Composite parent) {
		viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL
				| SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		composite = parent;

	}
	public void executeSparql() {

		createColumns(composite, viewer);
		final Table table = viewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		viewer.refresh();
		viewer.setContentProvider(new ArrayContentProvider());
		viewer.setInput(DataSets.INSTANCE.getListAll());
		getSite().setSelectionProvider(viewer);
		GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.horizontalSpan = 2;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		viewer.getControl().setLayoutData(gridData);

	}

	public void createColumns(final Composite parent, final TableViewer viewer) {
		int columns = 1;
		TableViewerColumn[] cols = { col1, col2, col3, col3, col4, col5 };
		SparqlEngine se = new SparqlEngine();
		se.listFormat();// -->check
		lists = se.getLists();
		int[] bounds = se.sizeCalc(lists);
		List<String> lRow = se.getRows();
		int size = lRow.size();
		if (count != 0 && (cols[0] != null)) {
			for (int i = 0; i < size; i++) {
				cols[i].getColumn().dispose(); // -> problem NUllPointer
			}
		}
		if (columns <= size) {
			if (!lRow.equals(null)) {

				col1 = createTableViewerColumn(lRow.get(0), bounds[0], 1);

				col1.setLabelProvider(new ColumnLabelProvider() {

					@Override
					public String getText(Object element) {
						int j = 0;
						ArrayList p = (ArrayList) element;
						return p.get(0).toString();
					}

				});
				columns++;
			}
			// ///////////////////////////////////////
			if (columns <= size) {
				col2 = createTableViewerColumn(lRow.get(1), bounds[1], 2);

				col2.setLabelProvider(new ColumnLabelProvider() {

					@Override
					public String getText(Object element) {
						int j = 0;
						ArrayList p = (ArrayList) element;
						return p.get(1).toString();
					}

				});
				columns++;
			}
			// ///////////////////////////////////////
			if (columns <= size) {
				col3 = createTableViewerColumn(lRow.get(2), bounds[2], 3);

				col3.setLabelProvider(new ColumnLabelProvider() {

					@Override
					public String getText(Object element) {
						int j = 0;
						ArrayList p = (ArrayList) element;
						return p.get(2).toString();
					}

				});
				columns++;
			}
			// ///////////////////////////////////////
			if (columns <= size) {
				col4 = createTableViewerColumn(lRow.get(3), bounds[3], 4);

				col4.setLabelProvider(new ColumnLabelProvider() {

					@Override
					public String getText(Object element) {
						int j = 0;
						ArrayList p = (ArrayList) element;
						return p.get(3).toString();
					}

				});
				columns++;
			}
			// ///////////////////////////////////////////
			if (columns <= size) {
				col4 = createTableViewerColumn(lRow.get(4), bounds[4], 5);

				col4.setLabelProvider(new ColumnLabelProvider() {

					@Override
					public String getText(Object element) {
						int j = 0;
						ArrayList p = (ArrayList) element;
						return p.get(4).toString();
					}

				});
				columns++;
			}
			// ///////////////////////////////////////////
			count++;
			// col2.getColumn().dispose();
		} else {
			System.out.println("please load the file");
		}

	}

	// }

	private TableViewerColumn createTableViewerColumn(String title, int bound,
			final int colNumber) {
		final TableViewerColumn viewerColumn = new TableViewerColumn(viewer,
				SWT.NONE);
		final TableColumn column = viewerColumn.getColumn();
		column.setText(title);
		column.setWidth(bound);
		column.setResizable(true);
		column.setMoveable(true);
		return viewerColumn;
	}

	public void setFocus() {

	}
}
