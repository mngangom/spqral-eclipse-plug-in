package view;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;

import org.apache.commons.lang.CharSet;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.internal.UISynchronizer;

import com.hp.hpl.jena.util.FileUtils;

public class LoadSparql extends AbstractHandler implements IHandler {
public static String fileSparql;
public static String sparqlContent;


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
			Shell shell = new Shell(Display.getCurrent());
			FileDialog dialog = new FileDialog(shell, SWT.OPEN);
			 dialog.open();
			 fileSparql= dialog.getFilterPath() +"\\"+ dialog.getFileName();
		//	LoadHandler.this.getFilePath();
			System.out.println("File " +fileSparql+" Loaded");

		
		return null;
	}
	public String getFilePath()
	{
		
		try {
			sparqlContent=FileUtils.readWholeFileAsUTF8(fileSparql);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sparqlContent;
	}
	
	
	public static void main(String[] args) {
		LoadSparql ls= new LoadSparql();
		ls.getFilePath();
	}

}
