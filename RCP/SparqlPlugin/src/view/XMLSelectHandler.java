package view;

import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

public class XMLSelectHandler extends AbstractHandler {

	public static String filePath;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		// TODO Auto-generated method stub
		ExecuteHandler eh = new ExecuteHandler();
		eh.setOutputChoice("XML");
		Shell shell = new Shell(Display.getCurrent());
		FileDialog dialog = new FileDialog(shell, SWT.SAVE);
		dialog.open();

		filePath = dialog.getFilterPath() + "\\" + dialog.getFileName();
		System.out.println("Output XML File : " + filePath);
		return null;
	}
	public String getXMLFilePath()
	{return filePath;}

}
