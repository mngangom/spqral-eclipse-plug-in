package view;


import java.util.Map;

import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;


public class TableOutputHandler extends AbstractHandler implements IHandler {
	
	public String selection;


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		ExecuteHandler eh = new ExecuteHandler();
		eh.setOutputChoice("Table");
		IWorkbenchWindow window =PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		System.out.println("Table output is selected.");
		IWorkbenchPage page = window.getActivePage();
		try {
			View v = (View) page.showView(View.ID, Perspective.getRandomViewID(), IWorkbenchPage.VIEW_ACTIVATE);		
		} catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	// TODO Auto-generated method stub
		return null;
	}
	

}
