package org.edu.le.spaqrl.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.edu.le.spaqrl.services.SparqlGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalSparqlParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_IRI_URI", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'PREFIX'", "':'", "'SELECT'", "'DISTINCT'", "'REDUCED'", "'*'", "'FILTER'", "'('", "','", "')'", "'OFFSET'", "'LIMIT'", "'ORDER BY'", "'ASC'", "'DESC'", "'WHERE'", "'{'", "'UNION'", "'OPTIONAL'", "'}'", "'.'", "';'", "'?:'", "'_:'", "'FROM'", "'NAMED'", "'?'"
    };
    public static final int RULE_STRING=7;
    public static final int RULE_SL_COMMENT=9;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_IRI_URI=5;
    public static final int RULE_ID=4;
    public static final int RULE_WS=10;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalSparqlParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalSparqlParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalSparqlParser.tokenNames; }
    public String getGrammarFileName() { return "../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g"; }



     	private SparqlGrammarAccess grammarAccess;
     	
        public InternalSparqlParser(TokenStream input, SparqlGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Query";	
       	}
       	
       	@Override
       	protected SparqlGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleQuery"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:67:1: entryRuleQuery returns [EObject current=null] : iv_ruleQuery= ruleQuery EOF ;
    public final EObject entryRuleQuery() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuery = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:68:2: (iv_ruleQuery= ruleQuery EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:69:2: iv_ruleQuery= ruleQuery EOF
            {
             newCompositeNode(grammarAccess.getQueryRule()); 
            pushFollow(FOLLOW_ruleQuery_in_entryRuleQuery75);
            iv_ruleQuery=ruleQuery();

            state._fsp--;

             current =iv_ruleQuery; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQuery85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuery"


    // $ANTLR start "ruleQuery"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:76:1: ruleQuery returns [EObject current=null] : ( (lv_SelectQuery_0_0= ruleSelectQuery ) )* ;
    public final EObject ruleQuery() throws RecognitionException {
        EObject current = null;

        EObject lv_SelectQuery_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:79:28: ( ( (lv_SelectQuery_0_0= ruleSelectQuery ) )* )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:80:1: ( (lv_SelectQuery_0_0= ruleSelectQuery ) )*
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:80:1: ( (lv_SelectQuery_0_0= ruleSelectQuery ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==12||LA1_0==14||(LA1_0>=22 && LA1_0<=23)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:81:1: (lv_SelectQuery_0_0= ruleSelectQuery )
            	    {
            	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:81:1: (lv_SelectQuery_0_0= ruleSelectQuery )
            	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:82:3: lv_SelectQuery_0_0= ruleSelectQuery
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getQueryAccess().getSelectQuerySelectQueryParserRuleCall_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleSelectQuery_in_ruleQuery130);
            	    lv_SelectQuery_0_0=ruleSelectQuery();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getQueryRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"SelectQuery",
            	            		lv_SelectQuery_0_0, 
            	            		"SelectQuery");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuery"


    // $ANTLR start "entryRulePrefix"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:106:1: entryRulePrefix returns [EObject current=null] : iv_rulePrefix= rulePrefix EOF ;
    public final EObject entryRulePrefix() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrefix = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:107:2: (iv_rulePrefix= rulePrefix EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:108:2: iv_rulePrefix= rulePrefix EOF
            {
             newCompositeNode(grammarAccess.getPrefixRule()); 
            pushFollow(FOLLOW_rulePrefix_in_entryRulePrefix166);
            iv_rulePrefix=rulePrefix();

            state._fsp--;

             current =iv_rulePrefix; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePrefix176); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrefix"


    // $ANTLR start "rulePrefix"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:115:1: rulePrefix returns [EObject current=null] : ( (otherlv_0= 'PREFIX' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_iref_3_0= RULE_IRI_URI ) ) ) | this_UnNamedPrefix_4= ruleUnNamedPrefix ) ;
    public final EObject rulePrefix() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_iref_3_0=null;
        EObject this_UnNamedPrefix_4 = null;


         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:118:28: ( ( (otherlv_0= 'PREFIX' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_iref_3_0= RULE_IRI_URI ) ) ) | this_UnNamedPrefix_4= ruleUnNamedPrefix ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:119:1: ( (otherlv_0= 'PREFIX' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_iref_3_0= RULE_IRI_URI ) ) ) | this_UnNamedPrefix_4= ruleUnNamedPrefix )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:119:1: ( (otherlv_0= 'PREFIX' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_iref_3_0= RULE_IRI_URI ) ) ) | this_UnNamedPrefix_4= ruleUnNamedPrefix )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==12) ) {
                int LA2_1 = input.LA(2);

                if ( (LA2_1==13) ) {
                    alt2=2;
                }
                else if ( (LA2_1==RULE_ID) ) {
                    alt2=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:119:2: (otherlv_0= 'PREFIX' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_iref_3_0= RULE_IRI_URI ) ) )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:119:2: (otherlv_0= 'PREFIX' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_iref_3_0= RULE_IRI_URI ) ) )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:119:4: otherlv_0= 'PREFIX' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_iref_3_0= RULE_IRI_URI ) )
                    {
                    otherlv_0=(Token)match(input,12,FOLLOW_12_in_rulePrefix214); 

                        	newLeafNode(otherlv_0, grammarAccess.getPrefixAccess().getPREFIXKeyword_0_0());
                        
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:123:1: ( (lv_name_1_0= RULE_ID ) )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:124:1: (lv_name_1_0= RULE_ID )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:124:1: (lv_name_1_0= RULE_ID )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:125:3: lv_name_1_0= RULE_ID
                    {
                    lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_rulePrefix231); 

                    			newLeafNode(lv_name_1_0, grammarAccess.getPrefixAccess().getNameIDTerminalRuleCall_0_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getPrefixRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"name",
                            		lv_name_1_0, 
                            		"ID");
                    	    

                    }


                    }

                    otherlv_2=(Token)match(input,13,FOLLOW_13_in_rulePrefix248); 

                        	newLeafNode(otherlv_2, grammarAccess.getPrefixAccess().getColonKeyword_0_2());
                        
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:145:1: ( (lv_iref_3_0= RULE_IRI_URI ) )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:146:1: (lv_iref_3_0= RULE_IRI_URI )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:146:1: (lv_iref_3_0= RULE_IRI_URI )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:147:3: lv_iref_3_0= RULE_IRI_URI
                    {
                    lv_iref_3_0=(Token)match(input,RULE_IRI_URI,FOLLOW_RULE_IRI_URI_in_rulePrefix265); 

                    			newLeafNode(lv_iref_3_0, grammarAccess.getPrefixAccess().getIrefIRI_URITerminalRuleCall_0_3_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getPrefixRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"iref",
                            		lv_iref_3_0, 
                            		"IRI_URI");
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:165:5: this_UnNamedPrefix_4= ruleUnNamedPrefix
                    {
                     
                            newCompositeNode(grammarAccess.getPrefixAccess().getUnNamedPrefixParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleUnNamedPrefix_in_rulePrefix299);
                    this_UnNamedPrefix_4=ruleUnNamedPrefix();

                    state._fsp--;

                     
                            current = this_UnNamedPrefix_4; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrefix"


    // $ANTLR start "entryRuleUnNamedPrefix"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:181:1: entryRuleUnNamedPrefix returns [EObject current=null] : iv_ruleUnNamedPrefix= ruleUnNamedPrefix EOF ;
    public final EObject entryRuleUnNamedPrefix() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnNamedPrefix = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:182:2: (iv_ruleUnNamedPrefix= ruleUnNamedPrefix EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:183:2: iv_ruleUnNamedPrefix= ruleUnNamedPrefix EOF
            {
             newCompositeNode(grammarAccess.getUnNamedPrefixRule()); 
            pushFollow(FOLLOW_ruleUnNamedPrefix_in_entryRuleUnNamedPrefix334);
            iv_ruleUnNamedPrefix=ruleUnNamedPrefix();

            state._fsp--;

             current =iv_ruleUnNamedPrefix; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleUnNamedPrefix344); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnNamedPrefix"


    // $ANTLR start "ruleUnNamedPrefix"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:190:1: ruleUnNamedPrefix returns [EObject current=null] : (otherlv_0= 'PREFIX' otherlv_1= ':' ( (lv_iref_2_0= RULE_IRI_URI ) ) ) ;
    public final EObject ruleUnNamedPrefix() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_iref_2_0=null;

         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:193:28: ( (otherlv_0= 'PREFIX' otherlv_1= ':' ( (lv_iref_2_0= RULE_IRI_URI ) ) ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:194:1: (otherlv_0= 'PREFIX' otherlv_1= ':' ( (lv_iref_2_0= RULE_IRI_URI ) ) )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:194:1: (otherlv_0= 'PREFIX' otherlv_1= ':' ( (lv_iref_2_0= RULE_IRI_URI ) ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:194:3: otherlv_0= 'PREFIX' otherlv_1= ':' ( (lv_iref_2_0= RULE_IRI_URI ) )
            {
            otherlv_0=(Token)match(input,12,FOLLOW_12_in_ruleUnNamedPrefix381); 

                	newLeafNode(otherlv_0, grammarAccess.getUnNamedPrefixAccess().getPREFIXKeyword_0());
                
            otherlv_1=(Token)match(input,13,FOLLOW_13_in_ruleUnNamedPrefix393); 

                	newLeafNode(otherlv_1, grammarAccess.getUnNamedPrefixAccess().getColonKeyword_1());
                
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:202:1: ( (lv_iref_2_0= RULE_IRI_URI ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:203:1: (lv_iref_2_0= RULE_IRI_URI )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:203:1: (lv_iref_2_0= RULE_IRI_URI )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:204:3: lv_iref_2_0= RULE_IRI_URI
            {
            lv_iref_2_0=(Token)match(input,RULE_IRI_URI,FOLLOW_RULE_IRI_URI_in_ruleUnNamedPrefix410); 

            			newLeafNode(lv_iref_2_0, grammarAccess.getUnNamedPrefixAccess().getIrefIRI_URITerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getUnNamedPrefixRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"iref",
                    		lv_iref_2_0, 
                    		"IRI_URI");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnNamedPrefix"


    // $ANTLR start "entryRuleSelectQuery"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:228:1: entryRuleSelectQuery returns [EObject current=null] : iv_ruleSelectQuery= ruleSelectQuery EOF ;
    public final EObject entryRuleSelectQuery() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSelectQuery = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:229:2: (iv_ruleSelectQuery= ruleSelectQuery EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:230:2: iv_ruleSelectQuery= ruleSelectQuery EOF
            {
             newCompositeNode(grammarAccess.getSelectQueryRule()); 
            pushFollow(FOLLOW_ruleSelectQuery_in_entryRuleSelectQuery451);
            iv_ruleSelectQuery=ruleSelectQuery();

            state._fsp--;

             current =iv_ruleSelectQuery; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSelectQuery461); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSelectQuery"


    // $ANTLR start "ruleSelectQuery"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:237:1: ruleSelectQuery returns [EObject current=null] : ( ( ( (lv_Prefix_0_0= rulePrefix ) )* otherlv_1= 'SELECT' (otherlv_2= 'DISTINCT' | otherlv_3= 'REDUCED' )? ( ( (lv_var_4_0= ruleVar ) )+ | otherlv_5= '*' ) ( (lv_datasetClause_6_0= ruleDatasetClause ) )* ( (lv_whereClause_7_0= ruleWhereClause ) ) ( (lv_orderClause_8_0= ruleOrderClause ) )? ) | ( (lv_limitClause_9_0= ruleLimitClause ) ) | ( (lv_offsetClause_10_0= ruleOffsetClause ) ) ) ;
    public final EObject ruleSelectQuery() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_Prefix_0_0 = null;

        EObject lv_var_4_0 = null;

        EObject lv_datasetClause_6_0 = null;

        EObject lv_whereClause_7_0 = null;

        EObject lv_orderClause_8_0 = null;

        AntlrDatatypeRuleToken lv_limitClause_9_0 = null;

        AntlrDatatypeRuleToken lv_offsetClause_10_0 = null;


         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:240:28: ( ( ( ( (lv_Prefix_0_0= rulePrefix ) )* otherlv_1= 'SELECT' (otherlv_2= 'DISTINCT' | otherlv_3= 'REDUCED' )? ( ( (lv_var_4_0= ruleVar ) )+ | otherlv_5= '*' ) ( (lv_datasetClause_6_0= ruleDatasetClause ) )* ( (lv_whereClause_7_0= ruleWhereClause ) ) ( (lv_orderClause_8_0= ruleOrderClause ) )? ) | ( (lv_limitClause_9_0= ruleLimitClause ) ) | ( (lv_offsetClause_10_0= ruleOffsetClause ) ) ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:241:1: ( ( ( (lv_Prefix_0_0= rulePrefix ) )* otherlv_1= 'SELECT' (otherlv_2= 'DISTINCT' | otherlv_3= 'REDUCED' )? ( ( (lv_var_4_0= ruleVar ) )+ | otherlv_5= '*' ) ( (lv_datasetClause_6_0= ruleDatasetClause ) )* ( (lv_whereClause_7_0= ruleWhereClause ) ) ( (lv_orderClause_8_0= ruleOrderClause ) )? ) | ( (lv_limitClause_9_0= ruleLimitClause ) ) | ( (lv_offsetClause_10_0= ruleOffsetClause ) ) )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:241:1: ( ( ( (lv_Prefix_0_0= rulePrefix ) )* otherlv_1= 'SELECT' (otherlv_2= 'DISTINCT' | otherlv_3= 'REDUCED' )? ( ( (lv_var_4_0= ruleVar ) )+ | otherlv_5= '*' ) ( (lv_datasetClause_6_0= ruleDatasetClause ) )* ( (lv_whereClause_7_0= ruleWhereClause ) ) ( (lv_orderClause_8_0= ruleOrderClause ) )? ) | ( (lv_limitClause_9_0= ruleLimitClause ) ) | ( (lv_offsetClause_10_0= ruleOffsetClause ) ) )
            int alt9=3;
            switch ( input.LA(1) ) {
            case 12:
            case 14:
                {
                alt9=1;
                }
                break;
            case 23:
                {
                alt9=2;
                }
                break;
            case 22:
                {
                alt9=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:241:2: ( ( (lv_Prefix_0_0= rulePrefix ) )* otherlv_1= 'SELECT' (otherlv_2= 'DISTINCT' | otherlv_3= 'REDUCED' )? ( ( (lv_var_4_0= ruleVar ) )+ | otherlv_5= '*' ) ( (lv_datasetClause_6_0= ruleDatasetClause ) )* ( (lv_whereClause_7_0= ruleWhereClause ) ) ( (lv_orderClause_8_0= ruleOrderClause ) )? )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:241:2: ( ( (lv_Prefix_0_0= rulePrefix ) )* otherlv_1= 'SELECT' (otherlv_2= 'DISTINCT' | otherlv_3= 'REDUCED' )? ( ( (lv_var_4_0= ruleVar ) )+ | otherlv_5= '*' ) ( (lv_datasetClause_6_0= ruleDatasetClause ) )* ( (lv_whereClause_7_0= ruleWhereClause ) ) ( (lv_orderClause_8_0= ruleOrderClause ) )? )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:241:3: ( (lv_Prefix_0_0= rulePrefix ) )* otherlv_1= 'SELECT' (otherlv_2= 'DISTINCT' | otherlv_3= 'REDUCED' )? ( ( (lv_var_4_0= ruleVar ) )+ | otherlv_5= '*' ) ( (lv_datasetClause_6_0= ruleDatasetClause ) )* ( (lv_whereClause_7_0= ruleWhereClause ) ) ( (lv_orderClause_8_0= ruleOrderClause ) )?
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:241:3: ( (lv_Prefix_0_0= rulePrefix ) )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==12) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:242:1: (lv_Prefix_0_0= rulePrefix )
                    	    {
                    	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:242:1: (lv_Prefix_0_0= rulePrefix )
                    	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:243:3: lv_Prefix_0_0= rulePrefix
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getSelectQueryAccess().getPrefixPrefixParserRuleCall_0_0_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_rulePrefix_in_ruleSelectQuery508);
                    	    lv_Prefix_0_0=rulePrefix();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getSelectQueryRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"Prefix",
                    	            		lv_Prefix_0_0, 
                    	            		"Prefix");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);

                    otherlv_1=(Token)match(input,14,FOLLOW_14_in_ruleSelectQuery521); 

                        	newLeafNode(otherlv_1, grammarAccess.getSelectQueryAccess().getSELECTKeyword_0_1());
                        
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:263:1: (otherlv_2= 'DISTINCT' | otherlv_3= 'REDUCED' )?
                    int alt4=3;
                    int LA4_0 = input.LA(1);

                    if ( (LA4_0==15) ) {
                        alt4=1;
                    }
                    else if ( (LA4_0==16) ) {
                        alt4=2;
                    }
                    switch (alt4) {
                        case 1 :
                            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:263:3: otherlv_2= 'DISTINCT'
                            {
                            otherlv_2=(Token)match(input,15,FOLLOW_15_in_ruleSelectQuery534); 

                                	newLeafNode(otherlv_2, grammarAccess.getSelectQueryAccess().getDISTINCTKeyword_0_2_0());
                                

                            }
                            break;
                        case 2 :
                            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:268:7: otherlv_3= 'REDUCED'
                            {
                            otherlv_3=(Token)match(input,16,FOLLOW_16_in_ruleSelectQuery552); 

                                	newLeafNode(otherlv_3, grammarAccess.getSelectQueryAccess().getREDUCEDKeyword_0_2_1());
                                

                            }
                            break;

                    }

                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:272:3: ( ( (lv_var_4_0= ruleVar ) )+ | otherlv_5= '*' )
                    int alt6=2;
                    int LA6_0 = input.LA(1);

                    if ( (LA6_0==12||LA6_0==38) ) {
                        alt6=1;
                    }
                    else if ( (LA6_0==17) ) {
                        alt6=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 6, 0, input);

                        throw nvae;
                    }
                    switch (alt6) {
                        case 1 :
                            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:272:4: ( (lv_var_4_0= ruleVar ) )+
                            {
                            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:272:4: ( (lv_var_4_0= ruleVar ) )+
                            int cnt5=0;
                            loop5:
                            do {
                                int alt5=2;
                                int LA5_0 = input.LA(1);

                                if ( (LA5_0==12||LA5_0==38) ) {
                                    alt5=1;
                                }


                                switch (alt5) {
                            	case 1 :
                            	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:273:1: (lv_var_4_0= ruleVar )
                            	    {
                            	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:273:1: (lv_var_4_0= ruleVar )
                            	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:274:3: lv_var_4_0= ruleVar
                            	    {
                            	     
                            	    	        newCompositeNode(grammarAccess.getSelectQueryAccess().getVarVarParserRuleCall_0_3_0_0()); 
                            	    	    
                            	    pushFollow(FOLLOW_ruleVar_in_ruleSelectQuery576);
                            	    lv_var_4_0=ruleVar();

                            	    state._fsp--;


                            	    	        if (current==null) {
                            	    	            current = createModelElementForParent(grammarAccess.getSelectQueryRule());
                            	    	        }
                            	           		set(
                            	           			current, 
                            	           			"var",
                            	            		lv_var_4_0, 
                            	            		"Var");
                            	    	        afterParserOrEnumRuleCall();
                            	    	    

                            	    }


                            	    }
                            	    break;

                            	default :
                            	    if ( cnt5 >= 1 ) break loop5;
                                        EarlyExitException eee =
                                            new EarlyExitException(5, input);
                                        throw eee;
                                }
                                cnt5++;
                            } while (true);


                            }
                            break;
                        case 2 :
                            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:291:7: otherlv_5= '*'
                            {
                            otherlv_5=(Token)match(input,17,FOLLOW_17_in_ruleSelectQuery595); 

                                	newLeafNode(otherlv_5, grammarAccess.getSelectQueryAccess().getAsteriskKeyword_0_3_1());
                                

                            }
                            break;

                    }

                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:295:2: ( (lv_datasetClause_6_0= ruleDatasetClause ) )*
                    loop7:
                    do {
                        int alt7=2;
                        int LA7_0 = input.LA(1);

                        if ( ((LA7_0>=36 && LA7_0<=37)) ) {
                            alt7=1;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:296:1: (lv_datasetClause_6_0= ruleDatasetClause )
                    	    {
                    	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:296:1: (lv_datasetClause_6_0= ruleDatasetClause )
                    	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:297:3: lv_datasetClause_6_0= ruleDatasetClause
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getSelectQueryAccess().getDatasetClauseDatasetClauseParserRuleCall_0_4_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_ruleDatasetClause_in_ruleSelectQuery617);
                    	    lv_datasetClause_6_0=ruleDatasetClause();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getSelectQueryRule());
                    	    	        }
                    	           		set(
                    	           			current, 
                    	           			"datasetClause",
                    	            		lv_datasetClause_6_0, 
                    	            		"DatasetClause");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop7;
                        }
                    } while (true);

                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:313:3: ( (lv_whereClause_7_0= ruleWhereClause ) )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:314:1: (lv_whereClause_7_0= ruleWhereClause )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:314:1: (lv_whereClause_7_0= ruleWhereClause )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:315:3: lv_whereClause_7_0= ruleWhereClause
                    {
                     
                    	        newCompositeNode(grammarAccess.getSelectQueryAccess().getWhereClauseWhereClauseParserRuleCall_0_5_0()); 
                    	    
                    pushFollow(FOLLOW_ruleWhereClause_in_ruleSelectQuery639);
                    lv_whereClause_7_0=ruleWhereClause();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getSelectQueryRule());
                    	        }
                           		set(
                           			current, 
                           			"whereClause",
                            		lv_whereClause_7_0, 
                            		"WhereClause");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:331:2: ( (lv_orderClause_8_0= ruleOrderClause ) )?
                    int alt8=2;
                    int LA8_0 = input.LA(1);

                    if ( (LA8_0==24) ) {
                        alt8=1;
                    }
                    switch (alt8) {
                        case 1 :
                            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:332:1: (lv_orderClause_8_0= ruleOrderClause )
                            {
                            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:332:1: (lv_orderClause_8_0= ruleOrderClause )
                            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:333:3: lv_orderClause_8_0= ruleOrderClause
                            {
                             
                            	        newCompositeNode(grammarAccess.getSelectQueryAccess().getOrderClauseOrderClauseParserRuleCall_0_6_0()); 
                            	    
                            pushFollow(FOLLOW_ruleOrderClause_in_ruleSelectQuery660);
                            lv_orderClause_8_0=ruleOrderClause();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getSelectQueryRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"orderClause",
                                    		lv_orderClause_8_0, 
                                    		"OrderClause");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:350:6: ( (lv_limitClause_9_0= ruleLimitClause ) )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:350:6: ( (lv_limitClause_9_0= ruleLimitClause ) )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:351:1: (lv_limitClause_9_0= ruleLimitClause )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:351:1: (lv_limitClause_9_0= ruleLimitClause )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:352:3: lv_limitClause_9_0= ruleLimitClause
                    {
                     
                    	        newCompositeNode(grammarAccess.getSelectQueryAccess().getLimitClauseLimitClauseParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleLimitClause_in_ruleSelectQuery689);
                    lv_limitClause_9_0=ruleLimitClause();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getSelectQueryRule());
                    	        }
                           		set(
                           			current, 
                           			"limitClause",
                            		lv_limitClause_9_0, 
                            		"LimitClause");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:369:6: ( (lv_offsetClause_10_0= ruleOffsetClause ) )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:369:6: ( (lv_offsetClause_10_0= ruleOffsetClause ) )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:370:1: (lv_offsetClause_10_0= ruleOffsetClause )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:370:1: (lv_offsetClause_10_0= ruleOffsetClause )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:371:3: lv_offsetClause_10_0= ruleOffsetClause
                    {
                     
                    	        newCompositeNode(grammarAccess.getSelectQueryAccess().getOffsetClauseOffsetClauseParserRuleCall_2_0()); 
                    	    
                    pushFollow(FOLLOW_ruleOffsetClause_in_ruleSelectQuery716);
                    lv_offsetClause_10_0=ruleOffsetClause();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getSelectQueryRule());
                    	        }
                           		set(
                           			current, 
                           			"offsetClause",
                            		lv_offsetClause_10_0, 
                            		"OffsetClause");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSelectQuery"


    // $ANTLR start "entryRuleFilter"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:395:1: entryRuleFilter returns [EObject current=null] : iv_ruleFilter= ruleFilter EOF ;
    public final EObject entryRuleFilter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFilter = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:396:2: (iv_ruleFilter= ruleFilter EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:397:2: iv_ruleFilter= ruleFilter EOF
            {
             newCompositeNode(grammarAccess.getFilterRule()); 
            pushFollow(FOLLOW_ruleFilter_in_entryRuleFilter752);
            iv_ruleFilter=ruleFilter();

            state._fsp--;

             current =iv_ruleFilter; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFilter762); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFilter"


    // $ANTLR start "ruleFilter"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:404:1: ruleFilter returns [EObject current=null] : (otherlv_0= 'FILTER' ( (lv_expressionFilter_1_0= ruleExpressionFilter ) ) ) ;
    public final EObject ruleFilter() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_expressionFilter_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:407:28: ( (otherlv_0= 'FILTER' ( (lv_expressionFilter_1_0= ruleExpressionFilter ) ) ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:408:1: (otherlv_0= 'FILTER' ( (lv_expressionFilter_1_0= ruleExpressionFilter ) ) )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:408:1: (otherlv_0= 'FILTER' ( (lv_expressionFilter_1_0= ruleExpressionFilter ) ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:408:3: otherlv_0= 'FILTER' ( (lv_expressionFilter_1_0= ruleExpressionFilter ) )
            {
            otherlv_0=(Token)match(input,18,FOLLOW_18_in_ruleFilter799); 

                	newLeafNode(otherlv_0, grammarAccess.getFilterAccess().getFILTERKeyword_0());
                
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:412:1: ( (lv_expressionFilter_1_0= ruleExpressionFilter ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:413:1: (lv_expressionFilter_1_0= ruleExpressionFilter )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:413:1: (lv_expressionFilter_1_0= ruleExpressionFilter )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:414:3: lv_expressionFilter_1_0= ruleExpressionFilter
            {
             
            	        newCompositeNode(grammarAccess.getFilterAccess().getExpressionFilterExpressionFilterParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleExpressionFilter_in_ruleFilter820);
            lv_expressionFilter_1_0=ruleExpressionFilter();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFilterRule());
            	        }
                   		set(
                   			current, 
                   			"expressionFilter",
                    		lv_expressionFilter_1_0, 
                    		"ExpressionFilter");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFilter"


    // $ANTLR start "entryRuleExpressionFilter"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:438:1: entryRuleExpressionFilter returns [EObject current=null] : iv_ruleExpressionFilter= ruleExpressionFilter EOF ;
    public final EObject entryRuleExpressionFilter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionFilter = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:439:2: (iv_ruleExpressionFilter= ruleExpressionFilter EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:440:2: iv_ruleExpressionFilter= ruleExpressionFilter EOF
            {
             newCompositeNode(grammarAccess.getExpressionFilterRule()); 
            pushFollow(FOLLOW_ruleExpressionFilter_in_entryRuleExpressionFilter856);
            iv_ruleExpressionFilter=ruleExpressionFilter();

            state._fsp--;

             current =iv_ruleExpressionFilter; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleExpressionFilter866); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionFilter"


    // $ANTLR start "ruleExpressionFilter"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:447:1: ruleExpressionFilter returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( (lv_parameters_2_0= ruleGraphNode ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleGraphNode ) ) )? (otherlv_5= ',' ( (lv_parameters_6_0= ruleGraphNode ) ) )? otherlv_7= ')' ) ;
    public final EObject ruleExpressionFilter() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_parameters_2_0 = null;

        EObject lv_parameters_4_0 = null;

        EObject lv_parameters_6_0 = null;


         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:450:28: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( (lv_parameters_2_0= ruleGraphNode ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleGraphNode ) ) )? (otherlv_5= ',' ( (lv_parameters_6_0= ruleGraphNode ) ) )? otherlv_7= ')' ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:451:1: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( (lv_parameters_2_0= ruleGraphNode ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleGraphNode ) ) )? (otherlv_5= ',' ( (lv_parameters_6_0= ruleGraphNode ) ) )? otherlv_7= ')' )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:451:1: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( (lv_parameters_2_0= ruleGraphNode ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleGraphNode ) ) )? (otherlv_5= ',' ( (lv_parameters_6_0= ruleGraphNode ) ) )? otherlv_7= ')' )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:451:2: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( (lv_parameters_2_0= ruleGraphNode ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleGraphNode ) ) )? (otherlv_5= ',' ( (lv_parameters_6_0= ruleGraphNode ) ) )? otherlv_7= ')'
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:451:2: ( (lv_name_0_0= RULE_ID ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:452:1: (lv_name_0_0= RULE_ID )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:452:1: (lv_name_0_0= RULE_ID )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:453:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleExpressionFilter908); 

            			newLeafNode(lv_name_0_0, grammarAccess.getExpressionFilterAccess().getNameIDTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getExpressionFilterRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"ID");
            	    

            }


            }

            otherlv_1=(Token)match(input,19,FOLLOW_19_in_ruleExpressionFilter925); 

                	newLeafNode(otherlv_1, grammarAccess.getExpressionFilterAccess().getLeftParenthesisKeyword_1());
                
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:473:1: ( (lv_parameters_2_0= ruleGraphNode ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:474:1: (lv_parameters_2_0= ruleGraphNode )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:474:1: (lv_parameters_2_0= ruleGraphNode )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:475:3: lv_parameters_2_0= ruleGraphNode
            {
             
            	        newCompositeNode(grammarAccess.getExpressionFilterAccess().getParametersGraphNodeParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleGraphNode_in_ruleExpressionFilter946);
            lv_parameters_2_0=ruleGraphNode();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getExpressionFilterRule());
            	        }
                   		add(
                   			current, 
                   			"parameters",
                    		lv_parameters_2_0, 
                    		"GraphNode");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:491:2: (otherlv_3= ',' ( (lv_parameters_4_0= ruleGraphNode ) ) )?
            int alt10=2;
            alt10 = dfa10.predict(input);
            switch (alt10) {
                case 1 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:491:4: otherlv_3= ',' ( (lv_parameters_4_0= ruleGraphNode ) )
                    {
                    otherlv_3=(Token)match(input,20,FOLLOW_20_in_ruleExpressionFilter959); 

                        	newLeafNode(otherlv_3, grammarAccess.getExpressionFilterAccess().getCommaKeyword_3_0());
                        
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:495:1: ( (lv_parameters_4_0= ruleGraphNode ) )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:496:1: (lv_parameters_4_0= ruleGraphNode )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:496:1: (lv_parameters_4_0= ruleGraphNode )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:497:3: lv_parameters_4_0= ruleGraphNode
                    {
                     
                    	        newCompositeNode(grammarAccess.getExpressionFilterAccess().getParametersGraphNodeParserRuleCall_3_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleGraphNode_in_ruleExpressionFilter980);
                    lv_parameters_4_0=ruleGraphNode();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getExpressionFilterRule());
                    	        }
                           		add(
                           			current, 
                           			"parameters",
                            		lv_parameters_4_0, 
                            		"GraphNode");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:513:4: (otherlv_5= ',' ( (lv_parameters_6_0= ruleGraphNode ) ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==20) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:513:6: otherlv_5= ',' ( (lv_parameters_6_0= ruleGraphNode ) )
                    {
                    otherlv_5=(Token)match(input,20,FOLLOW_20_in_ruleExpressionFilter995); 

                        	newLeafNode(otherlv_5, grammarAccess.getExpressionFilterAccess().getCommaKeyword_4_0());
                        
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:517:1: ( (lv_parameters_6_0= ruleGraphNode ) )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:518:1: (lv_parameters_6_0= ruleGraphNode )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:518:1: (lv_parameters_6_0= ruleGraphNode )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:519:3: lv_parameters_6_0= ruleGraphNode
                    {
                     
                    	        newCompositeNode(grammarAccess.getExpressionFilterAccess().getParametersGraphNodeParserRuleCall_4_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleGraphNode_in_ruleExpressionFilter1016);
                    lv_parameters_6_0=ruleGraphNode();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getExpressionFilterRule());
                    	        }
                           		add(
                           			current, 
                           			"parameters",
                            		lv_parameters_6_0, 
                            		"GraphNode");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            otherlv_7=(Token)match(input,21,FOLLOW_21_in_ruleExpressionFilter1030); 

                	newLeafNode(otherlv_7, grammarAccess.getExpressionFilterAccess().getRightParenthesisKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionFilter"


    // $ANTLR start "entryRuleOffsetClause"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:547:1: entryRuleOffsetClause returns [String current=null] : iv_ruleOffsetClause= ruleOffsetClause EOF ;
    public final String entryRuleOffsetClause() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOffsetClause = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:548:2: (iv_ruleOffsetClause= ruleOffsetClause EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:549:2: iv_ruleOffsetClause= ruleOffsetClause EOF
            {
             newCompositeNode(grammarAccess.getOffsetClauseRule()); 
            pushFollow(FOLLOW_ruleOffsetClause_in_entryRuleOffsetClause1067);
            iv_ruleOffsetClause=ruleOffsetClause();

            state._fsp--;

             current =iv_ruleOffsetClause.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOffsetClause1078); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOffsetClause"


    // $ANTLR start "ruleOffsetClause"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:556:1: ruleOffsetClause returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'OFFSET' this_INTEGER_1= ruleINTEGER ) ;
    public final AntlrDatatypeRuleToken ruleOffsetClause() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_INTEGER_1 = null;


         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:559:28: ( (kw= 'OFFSET' this_INTEGER_1= ruleINTEGER ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:560:1: (kw= 'OFFSET' this_INTEGER_1= ruleINTEGER )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:560:1: (kw= 'OFFSET' this_INTEGER_1= ruleINTEGER )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:561:2: kw= 'OFFSET' this_INTEGER_1= ruleINTEGER
            {
            kw=(Token)match(input,22,FOLLOW_22_in_ruleOffsetClause1116); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getOffsetClauseAccess().getOFFSETKeyword_0()); 
                
             
                    newCompositeNode(grammarAccess.getOffsetClauseAccess().getINTEGERParserRuleCall_1()); 
                
            pushFollow(FOLLOW_ruleINTEGER_in_ruleOffsetClause1138);
            this_INTEGER_1=ruleINTEGER();

            state._fsp--;


            		current.merge(this_INTEGER_1);
                
             
                    afterParserOrEnumRuleCall();
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOffsetClause"


    // $ANTLR start "entryRuleLimitClause"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:585:1: entryRuleLimitClause returns [String current=null] : iv_ruleLimitClause= ruleLimitClause EOF ;
    public final String entryRuleLimitClause() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleLimitClause = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:586:2: (iv_ruleLimitClause= ruleLimitClause EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:587:2: iv_ruleLimitClause= ruleLimitClause EOF
            {
             newCompositeNode(grammarAccess.getLimitClauseRule()); 
            pushFollow(FOLLOW_ruleLimitClause_in_entryRuleLimitClause1184);
            iv_ruleLimitClause=ruleLimitClause();

            state._fsp--;

             current =iv_ruleLimitClause.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleLimitClause1195); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLimitClause"


    // $ANTLR start "ruleLimitClause"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:594:1: ruleLimitClause returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'LIMIT' this_INTEGER_1= ruleINTEGER ) ;
    public final AntlrDatatypeRuleToken ruleLimitClause() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_INTEGER_1 = null;


         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:597:28: ( (kw= 'LIMIT' this_INTEGER_1= ruleINTEGER ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:598:1: (kw= 'LIMIT' this_INTEGER_1= ruleINTEGER )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:598:1: (kw= 'LIMIT' this_INTEGER_1= ruleINTEGER )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:599:2: kw= 'LIMIT' this_INTEGER_1= ruleINTEGER
            {
            kw=(Token)match(input,23,FOLLOW_23_in_ruleLimitClause1233); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getLimitClauseAccess().getLIMITKeyword_0()); 
                
             
                    newCompositeNode(grammarAccess.getLimitClauseAccess().getINTEGERParserRuleCall_1()); 
                
            pushFollow(FOLLOW_ruleINTEGER_in_ruleLimitClause1255);
            this_INTEGER_1=ruleINTEGER();

            state._fsp--;


            		current.merge(this_INTEGER_1);
                
             
                    afterParserOrEnumRuleCall();
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLimitClause"


    // $ANTLR start "entryRuleINTEGER"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:623:1: entryRuleINTEGER returns [String current=null] : iv_ruleINTEGER= ruleINTEGER EOF ;
    public final String entryRuleINTEGER() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleINTEGER = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:624:2: (iv_ruleINTEGER= ruleINTEGER EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:625:2: iv_ruleINTEGER= ruleINTEGER EOF
            {
             newCompositeNode(grammarAccess.getINTEGERRule()); 
            pushFollow(FOLLOW_ruleINTEGER_in_entryRuleINTEGER1301);
            iv_ruleINTEGER=ruleINTEGER();

            state._fsp--;

             current =iv_ruleINTEGER.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleINTEGER1312); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleINTEGER"


    // $ANTLR start "ruleINTEGER"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:632:1: ruleINTEGER returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_INT_0= RULE_INT ;
    public final AntlrDatatypeRuleToken ruleINTEGER() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;

         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:635:28: (this_INT_0= RULE_INT )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:636:5: this_INT_0= RULE_INT
            {
            this_INT_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleINTEGER1351); 

            		current.merge(this_INT_0);
                
             
                newLeafNode(this_INT_0, grammarAccess.getINTEGERAccess().getINTTerminalRuleCall()); 
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleINTEGER"


    // $ANTLR start "entryRuleOrderClause"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:651:1: entryRuleOrderClause returns [EObject current=null] : iv_ruleOrderClause= ruleOrderClause EOF ;
    public final EObject entryRuleOrderClause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOrderClause = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:652:2: (iv_ruleOrderClause= ruleOrderClause EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:653:2: iv_ruleOrderClause= ruleOrderClause EOF
            {
             newCompositeNode(grammarAccess.getOrderClauseRule()); 
            pushFollow(FOLLOW_ruleOrderClause_in_entryRuleOrderClause1395);
            iv_ruleOrderClause=ruleOrderClause();

            state._fsp--;

             current =iv_ruleOrderClause; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOrderClause1405); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOrderClause"


    // $ANTLR start "ruleOrderClause"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:660:1: ruleOrderClause returns [EObject current=null] : (otherlv_0= 'ORDER BY' ( (lv_orderCondition_1_0= ruleOrderCondition ) )+ ) ;
    public final EObject ruleOrderClause() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_orderCondition_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:663:28: ( (otherlv_0= 'ORDER BY' ( (lv_orderCondition_1_0= ruleOrderCondition ) )+ ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:664:1: (otherlv_0= 'ORDER BY' ( (lv_orderCondition_1_0= ruleOrderCondition ) )+ )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:664:1: (otherlv_0= 'ORDER BY' ( (lv_orderCondition_1_0= ruleOrderCondition ) )+ )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:664:3: otherlv_0= 'ORDER BY' ( (lv_orderCondition_1_0= ruleOrderCondition ) )+
            {
            otherlv_0=(Token)match(input,24,FOLLOW_24_in_ruleOrderClause1442); 

                	newLeafNode(otherlv_0, grammarAccess.getOrderClauseAccess().getORDERBYKeyword_0());
                
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:668:1: ( (lv_orderCondition_1_0= ruleOrderCondition ) )+
            int cnt12=0;
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==12) ) {
                    int LA12_2 = input.LA(2);

                    if ( (LA12_2==13) ) {
                        int LA12_4 = input.LA(3);

                        if ( (LA12_4==RULE_IRI_URI) ) {
                            int LA12_6 = input.LA(4);

                            if ( (LA12_6==13) ) {
                                alt12=1;
                            }


                        }


                    }
                    else if ( (LA12_2==RULE_ID) ) {
                        int LA12_5 = input.LA(3);

                        if ( (LA12_5==13) ) {
                            int LA12_7 = input.LA(4);

                            if ( (LA12_7==RULE_IRI_URI) ) {
                                int LA12_8 = input.LA(5);

                                if ( (LA12_8==13) ) {
                                    alt12=1;
                                }


                            }


                        }


                    }


                }
                else if ( ((LA12_0>=25 && LA12_0<=26)||LA12_0==38) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:669:1: (lv_orderCondition_1_0= ruleOrderCondition )
            	    {
            	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:669:1: (lv_orderCondition_1_0= ruleOrderCondition )
            	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:670:3: lv_orderCondition_1_0= ruleOrderCondition
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getOrderClauseAccess().getOrderConditionOrderConditionParserRuleCall_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleOrderCondition_in_ruleOrderClause1463);
            	    lv_orderCondition_1_0=ruleOrderCondition();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getOrderClauseRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"orderCondition",
            	            		lv_orderCondition_1_0, 
            	            		"OrderCondition");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt12 >= 1 ) break loop12;
                        EarlyExitException eee =
                            new EarlyExitException(12, input);
                        throw eee;
                }
                cnt12++;
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOrderClause"


    // $ANTLR start "entryRuleOrderCondition"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:694:1: entryRuleOrderCondition returns [EObject current=null] : iv_ruleOrderCondition= ruleOrderCondition EOF ;
    public final EObject entryRuleOrderCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOrderCondition = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:695:2: (iv_ruleOrderCondition= ruleOrderCondition EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:696:2: iv_ruleOrderCondition= ruleOrderCondition EOF
            {
             newCompositeNode(grammarAccess.getOrderConditionRule()); 
            pushFollow(FOLLOW_ruleOrderCondition_in_entryRuleOrderCondition1500);
            iv_ruleOrderCondition=ruleOrderCondition();

            state._fsp--;

             current =iv_ruleOrderCondition; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOrderCondition1510); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOrderCondition"


    // $ANTLR start "ruleOrderCondition"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:703:1: ruleOrderCondition returns [EObject current=null] : ( ( (otherlv_0= 'ASC' | otherlv_1= 'DESC' ) ( (lv_brackettedExpression_2_0= ruleBrackettedExpression ) ) ) | this_Var_3= ruleVar ) ;
    public final EObject ruleOrderCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_brackettedExpression_2_0 = null;

        EObject this_Var_3 = null;


         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:706:28: ( ( ( (otherlv_0= 'ASC' | otherlv_1= 'DESC' ) ( (lv_brackettedExpression_2_0= ruleBrackettedExpression ) ) ) | this_Var_3= ruleVar ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:707:1: ( ( (otherlv_0= 'ASC' | otherlv_1= 'DESC' ) ( (lv_brackettedExpression_2_0= ruleBrackettedExpression ) ) ) | this_Var_3= ruleVar )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:707:1: ( ( (otherlv_0= 'ASC' | otherlv_1= 'DESC' ) ( (lv_brackettedExpression_2_0= ruleBrackettedExpression ) ) ) | this_Var_3= ruleVar )
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( ((LA14_0>=25 && LA14_0<=26)) ) {
                alt14=1;
            }
            else if ( (LA14_0==12||LA14_0==38) ) {
                alt14=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }
            switch (alt14) {
                case 1 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:707:2: ( (otherlv_0= 'ASC' | otherlv_1= 'DESC' ) ( (lv_brackettedExpression_2_0= ruleBrackettedExpression ) ) )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:707:2: ( (otherlv_0= 'ASC' | otherlv_1= 'DESC' ) ( (lv_brackettedExpression_2_0= ruleBrackettedExpression ) ) )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:707:3: (otherlv_0= 'ASC' | otherlv_1= 'DESC' ) ( (lv_brackettedExpression_2_0= ruleBrackettedExpression ) )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:707:3: (otherlv_0= 'ASC' | otherlv_1= 'DESC' )
                    int alt13=2;
                    int LA13_0 = input.LA(1);

                    if ( (LA13_0==25) ) {
                        alt13=1;
                    }
                    else if ( (LA13_0==26) ) {
                        alt13=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 13, 0, input);

                        throw nvae;
                    }
                    switch (alt13) {
                        case 1 :
                            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:707:5: otherlv_0= 'ASC'
                            {
                            otherlv_0=(Token)match(input,25,FOLLOW_25_in_ruleOrderCondition1549); 

                                	newLeafNode(otherlv_0, grammarAccess.getOrderConditionAccess().getASCKeyword_0_0_0());
                                

                            }
                            break;
                        case 2 :
                            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:712:7: otherlv_1= 'DESC'
                            {
                            otherlv_1=(Token)match(input,26,FOLLOW_26_in_ruleOrderCondition1567); 

                                	newLeafNode(otherlv_1, grammarAccess.getOrderConditionAccess().getDESCKeyword_0_0_1());
                                

                            }
                            break;

                    }

                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:716:2: ( (lv_brackettedExpression_2_0= ruleBrackettedExpression ) )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:717:1: (lv_brackettedExpression_2_0= ruleBrackettedExpression )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:717:1: (lv_brackettedExpression_2_0= ruleBrackettedExpression )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:718:3: lv_brackettedExpression_2_0= ruleBrackettedExpression
                    {
                     
                    	        newCompositeNode(grammarAccess.getOrderConditionAccess().getBrackettedExpressionBrackettedExpressionParserRuleCall_0_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleBrackettedExpression_in_ruleOrderCondition1589);
                    lv_brackettedExpression_2_0=ruleBrackettedExpression();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOrderConditionRule());
                    	        }
                           		set(
                           			current, 
                           			"brackettedExpression",
                            		lv_brackettedExpression_2_0, 
                            		"BrackettedExpression");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:736:5: this_Var_3= ruleVar
                    {
                     
                            newCompositeNode(grammarAccess.getOrderConditionAccess().getVarParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleVar_in_ruleOrderCondition1618);
                    this_Var_3=ruleVar();

                    state._fsp--;

                     
                            current = this_Var_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOrderCondition"


    // $ANTLR start "entryRuleBrackettedExpression"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:752:1: entryRuleBrackettedExpression returns [EObject current=null] : iv_ruleBrackettedExpression= ruleBrackettedExpression EOF ;
    public final EObject entryRuleBrackettedExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBrackettedExpression = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:753:2: (iv_ruleBrackettedExpression= ruleBrackettedExpression EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:754:2: iv_ruleBrackettedExpression= ruleBrackettedExpression EOF
            {
             newCompositeNode(grammarAccess.getBrackettedExpressionRule()); 
            pushFollow(FOLLOW_ruleBrackettedExpression_in_entryRuleBrackettedExpression1653);
            iv_ruleBrackettedExpression=ruleBrackettedExpression();

            state._fsp--;

             current =iv_ruleBrackettedExpression; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBrackettedExpression1663); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBrackettedExpression"


    // $ANTLR start "ruleBrackettedExpression"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:761:1: ruleBrackettedExpression returns [EObject current=null] : (otherlv_0= '(' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= ')' ) ;
    public final EObject ruleBrackettedExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_expression_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:764:28: ( (otherlv_0= '(' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= ')' ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:765:1: (otherlv_0= '(' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= ')' )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:765:1: (otherlv_0= '(' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= ')' )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:765:3: otherlv_0= '(' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= ')'
            {
            otherlv_0=(Token)match(input,19,FOLLOW_19_in_ruleBrackettedExpression1700); 

                	newLeafNode(otherlv_0, grammarAccess.getBrackettedExpressionAccess().getLeftParenthesisKeyword_0());
                
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:769:1: ( (lv_expression_1_0= ruleExpression ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:770:1: (lv_expression_1_0= ruleExpression )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:770:1: (lv_expression_1_0= ruleExpression )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:771:3: lv_expression_1_0= ruleExpression
            {
             
            	        newCompositeNode(grammarAccess.getBrackettedExpressionAccess().getExpressionExpressionParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleExpression_in_ruleBrackettedExpression1721);
            lv_expression_1_0=ruleExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBrackettedExpressionRule());
            	        }
                   		set(
                   			current, 
                   			"expression",
                    		lv_expression_1_0, 
                    		"Expression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,21,FOLLOW_21_in_ruleBrackettedExpression1733); 

                	newLeafNode(otherlv_2, grammarAccess.getBrackettedExpressionAccess().getRightParenthesisKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBrackettedExpression"


    // $ANTLR start "entryRuleExpression"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:799:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:800:2: (iv_ruleExpression= ruleExpression EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:801:2: iv_ruleExpression= ruleExpression EOF
            {
             newCompositeNode(grammarAccess.getExpressionRule()); 
            pushFollow(FOLLOW_ruleExpression_in_entryRuleExpression1769);
            iv_ruleExpression=ruleExpression();

            state._fsp--;

             current =iv_ruleExpression; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleExpression1779); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:808:1: ruleExpression returns [EObject current=null] : this_Var_0= ruleVar ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        EObject this_Var_0 = null;


         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:811:28: (this_Var_0= ruleVar )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:813:5: this_Var_0= ruleVar
            {
             
                    newCompositeNode(grammarAccess.getExpressionAccess().getVarParserRuleCall()); 
                
            pushFollow(FOLLOW_ruleVar_in_ruleExpression1825);
            this_Var_0=ruleVar();

            state._fsp--;

             
                    current = this_Var_0; 
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleWhereClause"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:829:1: entryRuleWhereClause returns [EObject current=null] : iv_ruleWhereClause= ruleWhereClause EOF ;
    public final EObject entryRuleWhereClause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWhereClause = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:830:2: (iv_ruleWhereClause= ruleWhereClause EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:831:2: iv_ruleWhereClause= ruleWhereClause EOF
            {
             newCompositeNode(grammarAccess.getWhereClauseRule()); 
            pushFollow(FOLLOW_ruleWhereClause_in_entryRuleWhereClause1859);
            iv_ruleWhereClause=ruleWhereClause();

            state._fsp--;

             current =iv_ruleWhereClause; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWhereClause1869); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWhereClause"


    // $ANTLR start "ruleWhereClause"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:838:1: ruleWhereClause returns [EObject current=null] : (otherlv_0= 'WHERE' otherlv_1= '{' ( ( (lv_pattern_2_0= rulePattern ) ) (otherlv_3= 'UNION' ( (lv_pattern_4_0= rulePattern ) ) )? (otherlv_5= 'OPTIONAL' ( (lv_pattern_6_0= rulePattern ) ) )? ) ( (lv_filter_7_0= ruleFilter ) )? otherlv_8= '}' ) ;
    public final EObject ruleWhereClause() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_8=null;
        EObject lv_pattern_2_0 = null;

        EObject lv_pattern_4_0 = null;

        EObject lv_pattern_6_0 = null;

        EObject lv_filter_7_0 = null;


         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:841:28: ( (otherlv_0= 'WHERE' otherlv_1= '{' ( ( (lv_pattern_2_0= rulePattern ) ) (otherlv_3= 'UNION' ( (lv_pattern_4_0= rulePattern ) ) )? (otherlv_5= 'OPTIONAL' ( (lv_pattern_6_0= rulePattern ) ) )? ) ( (lv_filter_7_0= ruleFilter ) )? otherlv_8= '}' ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:842:1: (otherlv_0= 'WHERE' otherlv_1= '{' ( ( (lv_pattern_2_0= rulePattern ) ) (otherlv_3= 'UNION' ( (lv_pattern_4_0= rulePattern ) ) )? (otherlv_5= 'OPTIONAL' ( (lv_pattern_6_0= rulePattern ) ) )? ) ( (lv_filter_7_0= ruleFilter ) )? otherlv_8= '}' )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:842:1: (otherlv_0= 'WHERE' otherlv_1= '{' ( ( (lv_pattern_2_0= rulePattern ) ) (otherlv_3= 'UNION' ( (lv_pattern_4_0= rulePattern ) ) )? (otherlv_5= 'OPTIONAL' ( (lv_pattern_6_0= rulePattern ) ) )? ) ( (lv_filter_7_0= ruleFilter ) )? otherlv_8= '}' )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:842:3: otherlv_0= 'WHERE' otherlv_1= '{' ( ( (lv_pattern_2_0= rulePattern ) ) (otherlv_3= 'UNION' ( (lv_pattern_4_0= rulePattern ) ) )? (otherlv_5= 'OPTIONAL' ( (lv_pattern_6_0= rulePattern ) ) )? ) ( (lv_filter_7_0= ruleFilter ) )? otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,27,FOLLOW_27_in_ruleWhereClause1906); 

                	newLeafNode(otherlv_0, grammarAccess.getWhereClauseAccess().getWHEREKeyword_0());
                
            otherlv_1=(Token)match(input,28,FOLLOW_28_in_ruleWhereClause1918); 

                	newLeafNode(otherlv_1, grammarAccess.getWhereClauseAccess().getLeftCurlyBracketKeyword_1());
                
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:850:1: ( ( (lv_pattern_2_0= rulePattern ) ) (otherlv_3= 'UNION' ( (lv_pattern_4_0= rulePattern ) ) )? (otherlv_5= 'OPTIONAL' ( (lv_pattern_6_0= rulePattern ) ) )? )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:850:2: ( (lv_pattern_2_0= rulePattern ) ) (otherlv_3= 'UNION' ( (lv_pattern_4_0= rulePattern ) ) )? (otherlv_5= 'OPTIONAL' ( (lv_pattern_6_0= rulePattern ) ) )?
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:850:2: ( (lv_pattern_2_0= rulePattern ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:851:1: (lv_pattern_2_0= rulePattern )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:851:1: (lv_pattern_2_0= rulePattern )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:852:3: lv_pattern_2_0= rulePattern
            {
             
            	        newCompositeNode(grammarAccess.getWhereClauseAccess().getPatternPatternParserRuleCall_2_0_0()); 
            	    
            pushFollow(FOLLOW_rulePattern_in_ruleWhereClause1940);
            lv_pattern_2_0=rulePattern();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getWhereClauseRule());
            	        }
                   		set(
                   			current, 
                   			"pattern",
                    		lv_pattern_2_0, 
                    		"Pattern");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:868:2: (otherlv_3= 'UNION' ( (lv_pattern_4_0= rulePattern ) ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==29) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:868:4: otherlv_3= 'UNION' ( (lv_pattern_4_0= rulePattern ) )
                    {
                    otherlv_3=(Token)match(input,29,FOLLOW_29_in_ruleWhereClause1953); 

                        	newLeafNode(otherlv_3, grammarAccess.getWhereClauseAccess().getUNIONKeyword_2_1_0());
                        
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:872:1: ( (lv_pattern_4_0= rulePattern ) )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:873:1: (lv_pattern_4_0= rulePattern )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:873:1: (lv_pattern_4_0= rulePattern )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:874:3: lv_pattern_4_0= rulePattern
                    {
                     
                    	        newCompositeNode(grammarAccess.getWhereClauseAccess().getPatternPatternParserRuleCall_2_1_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulePattern_in_ruleWhereClause1974);
                    lv_pattern_4_0=rulePattern();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getWhereClauseRule());
                    	        }
                           		set(
                           			current, 
                           			"pattern",
                            		lv_pattern_4_0, 
                            		"Pattern");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:890:4: (otherlv_5= 'OPTIONAL' ( (lv_pattern_6_0= rulePattern ) ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==30) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:890:6: otherlv_5= 'OPTIONAL' ( (lv_pattern_6_0= rulePattern ) )
                    {
                    otherlv_5=(Token)match(input,30,FOLLOW_30_in_ruleWhereClause1989); 

                        	newLeafNode(otherlv_5, grammarAccess.getWhereClauseAccess().getOPTIONALKeyword_2_2_0());
                        
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:894:1: ( (lv_pattern_6_0= rulePattern ) )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:895:1: (lv_pattern_6_0= rulePattern )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:895:1: (lv_pattern_6_0= rulePattern )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:896:3: lv_pattern_6_0= rulePattern
                    {
                     
                    	        newCompositeNode(grammarAccess.getWhereClauseAccess().getPatternPatternParserRuleCall_2_2_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulePattern_in_ruleWhereClause2010);
                    lv_pattern_6_0=rulePattern();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getWhereClauseRule());
                    	        }
                           		set(
                           			current, 
                           			"pattern",
                            		lv_pattern_6_0, 
                            		"Pattern");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }

            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:912:5: ( (lv_filter_7_0= ruleFilter ) )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==18) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:913:1: (lv_filter_7_0= ruleFilter )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:913:1: (lv_filter_7_0= ruleFilter )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:914:3: lv_filter_7_0= ruleFilter
                    {
                     
                    	        newCompositeNode(grammarAccess.getWhereClauseAccess().getFilterFilterParserRuleCall_3_0()); 
                    	    
                    pushFollow(FOLLOW_ruleFilter_in_ruleWhereClause2034);
                    lv_filter_7_0=ruleFilter();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getWhereClauseRule());
                    	        }
                           		set(
                           			current, 
                           			"filter",
                            		lv_filter_7_0, 
                            		"Filter");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            otherlv_8=(Token)match(input,31,FOLLOW_31_in_ruleWhereClause2047); 

                	newLeafNode(otherlv_8, grammarAccess.getWhereClauseAccess().getRightCurlyBracketKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWhereClause"


    // $ANTLR start "entryRulePattern"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:942:1: entryRulePattern returns [EObject current=null] : iv_rulePattern= rulePattern EOF ;
    public final EObject entryRulePattern() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePattern = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:943:2: (iv_rulePattern= rulePattern EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:944:2: iv_rulePattern= rulePattern EOF
            {
             newCompositeNode(grammarAccess.getPatternRule()); 
            pushFollow(FOLLOW_rulePattern_in_entryRulePattern2083);
            iv_rulePattern=rulePattern();

            state._fsp--;

             current =iv_rulePattern; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePattern2093); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePattern"


    // $ANTLR start "rulePattern"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:951:1: rulePattern returns [EObject current=null] : (otherlv_0= '{' ( (lv_Patterns_1_0= rulePatternOne ) ) (otherlv_2= '.' ( (lv_Patterns_3_0= rulePatternOne ) ) )* (otherlv_4= '.' )? otherlv_5= '}' ) ;
    public final EObject rulePattern() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        EObject lv_Patterns_1_0 = null;

        EObject lv_Patterns_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:954:28: ( (otherlv_0= '{' ( (lv_Patterns_1_0= rulePatternOne ) ) (otherlv_2= '.' ( (lv_Patterns_3_0= rulePatternOne ) ) )* (otherlv_4= '.' )? otherlv_5= '}' ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:955:1: (otherlv_0= '{' ( (lv_Patterns_1_0= rulePatternOne ) ) (otherlv_2= '.' ( (lv_Patterns_3_0= rulePatternOne ) ) )* (otherlv_4= '.' )? otherlv_5= '}' )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:955:1: (otherlv_0= '{' ( (lv_Patterns_1_0= rulePatternOne ) ) (otherlv_2= '.' ( (lv_Patterns_3_0= rulePatternOne ) ) )* (otherlv_4= '.' )? otherlv_5= '}' )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:955:3: otherlv_0= '{' ( (lv_Patterns_1_0= rulePatternOne ) ) (otherlv_2= '.' ( (lv_Patterns_3_0= rulePatternOne ) ) )* (otherlv_4= '.' )? otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,28,FOLLOW_28_in_rulePattern2130); 

                	newLeafNode(otherlv_0, grammarAccess.getPatternAccess().getLeftCurlyBracketKeyword_0());
                
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:959:1: ( (lv_Patterns_1_0= rulePatternOne ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:960:1: (lv_Patterns_1_0= rulePatternOne )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:960:1: (lv_Patterns_1_0= rulePatternOne )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:961:3: lv_Patterns_1_0= rulePatternOne
            {
             
            	        newCompositeNode(grammarAccess.getPatternAccess().getPatternsPatternOneParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_rulePatternOne_in_rulePattern2151);
            lv_Patterns_1_0=rulePatternOne();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPatternRule());
            	        }
                   		add(
                   			current, 
                   			"Patterns",
                    		lv_Patterns_1_0, 
                    		"PatternOne");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:977:2: (otherlv_2= '.' ( (lv_Patterns_3_0= rulePatternOne ) ) )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==32) ) {
                    int LA18_1 = input.LA(2);

                    if ( ((LA18_1>=RULE_ID && LA18_1<=RULE_STRING)||LA18_1==12||(LA18_1>=34 && LA18_1<=35)||LA18_1==38) ) {
                        alt18=1;
                    }


                }


                switch (alt18) {
            	case 1 :
            	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:977:4: otherlv_2= '.' ( (lv_Patterns_3_0= rulePatternOne ) )
            	    {
            	    otherlv_2=(Token)match(input,32,FOLLOW_32_in_rulePattern2164); 

            	        	newLeafNode(otherlv_2, grammarAccess.getPatternAccess().getFullStopKeyword_2_0());
            	        
            	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:981:1: ( (lv_Patterns_3_0= rulePatternOne ) )
            	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:982:1: (lv_Patterns_3_0= rulePatternOne )
            	    {
            	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:982:1: (lv_Patterns_3_0= rulePatternOne )
            	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:983:3: lv_Patterns_3_0= rulePatternOne
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getPatternAccess().getPatternsPatternOneParserRuleCall_2_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulePatternOne_in_rulePattern2185);
            	    lv_Patterns_3_0=rulePatternOne();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getPatternRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"Patterns",
            	            		lv_Patterns_3_0, 
            	            		"PatternOne");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:999:4: (otherlv_4= '.' )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==32) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:999:6: otherlv_4= '.'
                    {
                    otherlv_4=(Token)match(input,32,FOLLOW_32_in_rulePattern2200); 

                        	newLeafNode(otherlv_4, grammarAccess.getPatternAccess().getFullStopKeyword_3());
                        

                    }
                    break;

            }

            otherlv_5=(Token)match(input,31,FOLLOW_31_in_rulePattern2214); 

                	newLeafNode(otherlv_5, grammarAccess.getPatternAccess().getRightCurlyBracketKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePattern"


    // $ANTLR start "entryRulePatternOne"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1015:1: entryRulePatternOne returns [EObject current=null] : iv_rulePatternOne= rulePatternOne EOF ;
    public final EObject entryRulePatternOne() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePatternOne = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1016:2: (iv_rulePatternOne= rulePatternOne EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1017:2: iv_rulePatternOne= rulePatternOne EOF
            {
             newCompositeNode(grammarAccess.getPatternOneRule()); 
            pushFollow(FOLLOW_rulePatternOne_in_entryRulePatternOne2250);
            iv_rulePatternOne=rulePatternOne();

            state._fsp--;

             current =iv_rulePatternOne; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePatternOne2260); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePatternOne"


    // $ANTLR start "rulePatternOne"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1024:1: rulePatternOne returns [EObject current=null] : ( (lv_triplesSameSubject_0_0= ruleTriplesSameSubject ) ) ;
    public final EObject rulePatternOne() throws RecognitionException {
        EObject current = null;

        EObject lv_triplesSameSubject_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1027:28: ( ( (lv_triplesSameSubject_0_0= ruleTriplesSameSubject ) ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1028:1: ( (lv_triplesSameSubject_0_0= ruleTriplesSameSubject ) )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1028:1: ( (lv_triplesSameSubject_0_0= ruleTriplesSameSubject ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1029:1: (lv_triplesSameSubject_0_0= ruleTriplesSameSubject )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1029:1: (lv_triplesSameSubject_0_0= ruleTriplesSameSubject )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1030:3: lv_triplesSameSubject_0_0= ruleTriplesSameSubject
            {
             
            	        newCompositeNode(grammarAccess.getPatternOneAccess().getTriplesSameSubjectTriplesSameSubjectParserRuleCall_0()); 
            	    
            pushFollow(FOLLOW_ruleTriplesSameSubject_in_rulePatternOne2305);
            lv_triplesSameSubject_0_0=ruleTriplesSameSubject();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPatternOneRule());
            	        }
                   		set(
                   			current, 
                   			"triplesSameSubject",
                    		lv_triplesSameSubject_0_0, 
                    		"TriplesSameSubject");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePatternOne"


    // $ANTLR start "entryRuleTriplesSameSubject"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1054:1: entryRuleTriplesSameSubject returns [EObject current=null] : iv_ruleTriplesSameSubject= ruleTriplesSameSubject EOF ;
    public final EObject entryRuleTriplesSameSubject() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTriplesSameSubject = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1055:2: (iv_ruleTriplesSameSubject= ruleTriplesSameSubject EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1056:2: iv_ruleTriplesSameSubject= ruleTriplesSameSubject EOF
            {
             newCompositeNode(grammarAccess.getTriplesSameSubjectRule()); 
            pushFollow(FOLLOW_ruleTriplesSameSubject_in_entryRuleTriplesSameSubject2340);
            iv_ruleTriplesSameSubject=ruleTriplesSameSubject();

            state._fsp--;

             current =iv_ruleTriplesSameSubject; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTriplesSameSubject2350); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTriplesSameSubject"


    // $ANTLR start "ruleTriplesSameSubject"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1063:1: ruleTriplesSameSubject returns [EObject current=null] : ( ( (lv_subject_0_0= ruleGraphNode ) ) ( (lv_propertyList_1_0= rulePropertyList ) ) (otherlv_2= ';' ( (lv_propertyList_3_0= rulePropertyList ) ) )* ) ;
    public final EObject ruleTriplesSameSubject() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject lv_subject_0_0 = null;

        EObject lv_propertyList_1_0 = null;

        EObject lv_propertyList_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1066:28: ( ( ( (lv_subject_0_0= ruleGraphNode ) ) ( (lv_propertyList_1_0= rulePropertyList ) ) (otherlv_2= ';' ( (lv_propertyList_3_0= rulePropertyList ) ) )* ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1067:1: ( ( (lv_subject_0_0= ruleGraphNode ) ) ( (lv_propertyList_1_0= rulePropertyList ) ) (otherlv_2= ';' ( (lv_propertyList_3_0= rulePropertyList ) ) )* )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1067:1: ( ( (lv_subject_0_0= ruleGraphNode ) ) ( (lv_propertyList_1_0= rulePropertyList ) ) (otherlv_2= ';' ( (lv_propertyList_3_0= rulePropertyList ) ) )* )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1067:2: ( (lv_subject_0_0= ruleGraphNode ) ) ( (lv_propertyList_1_0= rulePropertyList ) ) (otherlv_2= ';' ( (lv_propertyList_3_0= rulePropertyList ) ) )*
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1067:2: ( (lv_subject_0_0= ruleGraphNode ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1068:1: (lv_subject_0_0= ruleGraphNode )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1068:1: (lv_subject_0_0= ruleGraphNode )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1069:3: lv_subject_0_0= ruleGraphNode
            {
             
            	        newCompositeNode(grammarAccess.getTriplesSameSubjectAccess().getSubjectGraphNodeParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleGraphNode_in_ruleTriplesSameSubject2396);
            lv_subject_0_0=ruleGraphNode();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getTriplesSameSubjectRule());
            	        }
                   		set(
                   			current, 
                   			"subject",
                    		lv_subject_0_0, 
                    		"GraphNode");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1085:2: ( (lv_propertyList_1_0= rulePropertyList ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1086:1: (lv_propertyList_1_0= rulePropertyList )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1086:1: (lv_propertyList_1_0= rulePropertyList )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1087:3: lv_propertyList_1_0= rulePropertyList
            {
             
            	        newCompositeNode(grammarAccess.getTriplesSameSubjectAccess().getPropertyListPropertyListParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_rulePropertyList_in_ruleTriplesSameSubject2417);
            lv_propertyList_1_0=rulePropertyList();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getTriplesSameSubjectRule());
            	        }
                   		add(
                   			current, 
                   			"propertyList",
                    		lv_propertyList_1_0, 
                    		"PropertyList");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1103:2: (otherlv_2= ';' ( (lv_propertyList_3_0= rulePropertyList ) ) )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==33) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1103:4: otherlv_2= ';' ( (lv_propertyList_3_0= rulePropertyList ) )
            	    {
            	    otherlv_2=(Token)match(input,33,FOLLOW_33_in_ruleTriplesSameSubject2430); 

            	        	newLeafNode(otherlv_2, grammarAccess.getTriplesSameSubjectAccess().getSemicolonKeyword_2_0());
            	        
            	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1107:1: ( (lv_propertyList_3_0= rulePropertyList ) )
            	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1108:1: (lv_propertyList_3_0= rulePropertyList )
            	    {
            	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1108:1: (lv_propertyList_3_0= rulePropertyList )
            	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1109:3: lv_propertyList_3_0= rulePropertyList
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getTriplesSameSubjectAccess().getPropertyListPropertyListParserRuleCall_2_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulePropertyList_in_ruleTriplesSameSubject2451);
            	    lv_propertyList_3_0=rulePropertyList();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getTriplesSameSubjectRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"propertyList",
            	            		lv_propertyList_3_0, 
            	            		"PropertyList");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTriplesSameSubject"


    // $ANTLR start "entryRulePropertyList"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1133:1: entryRulePropertyList returns [EObject current=null] : iv_rulePropertyList= rulePropertyList EOF ;
    public final EObject entryRulePropertyList() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePropertyList = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1134:2: (iv_rulePropertyList= rulePropertyList EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1135:2: iv_rulePropertyList= rulePropertyList EOF
            {
             newCompositeNode(grammarAccess.getPropertyListRule()); 
            pushFollow(FOLLOW_rulePropertyList_in_entryRulePropertyList2489);
            iv_rulePropertyList=rulePropertyList();

            state._fsp--;

             current =iv_rulePropertyList; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePropertyList2499); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePropertyList"


    // $ANTLR start "rulePropertyList"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1142:1: rulePropertyList returns [EObject current=null] : ( ( (lv_property_0_0= ruleGraphNode ) ) ( (lv_object_1_0= ruleGraphNode ) )* ) ;
    public final EObject rulePropertyList() throws RecognitionException {
        EObject current = null;

        EObject lv_property_0_0 = null;

        EObject lv_object_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1145:28: ( ( ( (lv_property_0_0= ruleGraphNode ) ) ( (lv_object_1_0= ruleGraphNode ) )* ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1146:1: ( ( (lv_property_0_0= ruleGraphNode ) ) ( (lv_object_1_0= ruleGraphNode ) )* )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1146:1: ( ( (lv_property_0_0= ruleGraphNode ) ) ( (lv_object_1_0= ruleGraphNode ) )* )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1146:2: ( (lv_property_0_0= ruleGraphNode ) ) ( (lv_object_1_0= ruleGraphNode ) )*
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1146:2: ( (lv_property_0_0= ruleGraphNode ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1147:1: (lv_property_0_0= ruleGraphNode )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1147:1: (lv_property_0_0= ruleGraphNode )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1148:3: lv_property_0_0= ruleGraphNode
            {
             
            	        newCompositeNode(grammarAccess.getPropertyListAccess().getPropertyGraphNodeParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleGraphNode_in_rulePropertyList2545);
            lv_property_0_0=ruleGraphNode();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPropertyListRule());
            	        }
                   		set(
                   			current, 
                   			"property",
                    		lv_property_0_0, 
                    		"GraphNode");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1164:2: ( (lv_object_1_0= ruleGraphNode ) )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( ((LA21_0>=RULE_ID && LA21_0<=RULE_STRING)||LA21_0==12||(LA21_0>=34 && LA21_0<=35)||LA21_0==38) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1165:1: (lv_object_1_0= ruleGraphNode )
            	    {
            	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1165:1: (lv_object_1_0= ruleGraphNode )
            	    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1166:3: lv_object_1_0= ruleGraphNode
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getPropertyListAccess().getObjectGraphNodeParserRuleCall_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleGraphNode_in_rulePropertyList2566);
            	    lv_object_1_0=ruleGraphNode();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getPropertyListRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"object",
            	            		lv_object_1_0, 
            	            		"GraphNode");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePropertyList"


    // $ANTLR start "entryRuleGraphNode"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1190:1: entryRuleGraphNode returns [EObject current=null] : iv_ruleGraphNode= ruleGraphNode EOF ;
    public final EObject entryRuleGraphNode() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGraphNode = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1191:2: (iv_ruleGraphNode= ruleGraphNode EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1192:2: iv_ruleGraphNode= ruleGraphNode EOF
            {
             newCompositeNode(grammarAccess.getGraphNodeRule()); 
            pushFollow(FOLLOW_ruleGraphNode_in_entryRuleGraphNode2603);
            iv_ruleGraphNode=ruleGraphNode();

            state._fsp--;

             current =iv_ruleGraphNode; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleGraphNode2613); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGraphNode"


    // $ANTLR start "ruleGraphNode"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1199:1: ruleGraphNode returns [EObject current=null] : (this_Var_0= ruleVar | this_Value_1= ruleValue | this_IRI_URI_2= RULE_IRI_URI | this_BlankNode_3= ruleBlankNode | this_Parameter_4= ruleParameter | this_Quote_5= ruleQuote ) ;
    public final EObject ruleGraphNode() throws RecognitionException {
        EObject current = null;

        Token this_IRI_URI_2=null;
        EObject this_Var_0 = null;

        EObject this_Value_1 = null;

        EObject this_BlankNode_3 = null;

        EObject this_Parameter_4 = null;

        EObject this_Quote_5 = null;


         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1202:28: ( (this_Var_0= ruleVar | this_Value_1= ruleValue | this_IRI_URI_2= RULE_IRI_URI | this_BlankNode_3= ruleBlankNode | this_Parameter_4= ruleParameter | this_Quote_5= ruleQuote ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1203:1: (this_Var_0= ruleVar | this_Value_1= ruleValue | this_IRI_URI_2= RULE_IRI_URI | this_BlankNode_3= ruleBlankNode | this_Parameter_4= ruleParameter | this_Quote_5= ruleQuote )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1203:1: (this_Var_0= ruleVar | this_Value_1= ruleValue | this_IRI_URI_2= RULE_IRI_URI | this_BlankNode_3= ruleBlankNode | this_Parameter_4= ruleParameter | this_Quote_5= ruleQuote )
            int alt22=6;
            switch ( input.LA(1) ) {
            case 12:
            case 38:
                {
                alt22=1;
                }
                break;
            case RULE_ID:
                {
                int LA22_2 = input.LA(2);

                if ( (LA22_2==EOF||(LA22_2>=RULE_ID && LA22_2<=RULE_STRING)||LA22_2==12||(LA22_2>=20 && LA22_2<=21)||(LA22_2>=31 && LA22_2<=35)||LA22_2==38) ) {
                    alt22=2;
                }
                else if ( (LA22_2==13) ) {
                    alt22=6;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 22, 2, input);

                    throw nvae;
                }
                }
                break;
            case RULE_INT:
            case RULE_STRING:
                {
                alt22=2;
                }
                break;
            case RULE_IRI_URI:
                {
                alt22=3;
                }
                break;
            case 35:
                {
                alt22=4;
                }
                break;
            case 34:
                {
                alt22=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }

            switch (alt22) {
                case 1 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1204:5: this_Var_0= ruleVar
                    {
                     
                            newCompositeNode(grammarAccess.getGraphNodeAccess().getVarParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleVar_in_ruleGraphNode2660);
                    this_Var_0=ruleVar();

                    state._fsp--;

                     
                            current = this_Var_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1214:5: this_Value_1= ruleValue
                    {
                     
                            newCompositeNode(grammarAccess.getGraphNodeAccess().getValueParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleValue_in_ruleGraphNode2687);
                    this_Value_1=ruleValue();

                    state._fsp--;

                     
                            current = this_Value_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1223:6: this_IRI_URI_2= RULE_IRI_URI
                    {
                    this_IRI_URI_2=(Token)match(input,RULE_IRI_URI,FOLLOW_RULE_IRI_URI_in_ruleGraphNode2703); 
                     
                        newLeafNode(this_IRI_URI_2, grammarAccess.getGraphNodeAccess().getIRI_URITerminalRuleCall_2()); 
                        

                    }
                    break;
                case 4 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1229:5: this_BlankNode_3= ruleBlankNode
                    {
                     
                            newCompositeNode(grammarAccess.getGraphNodeAccess().getBlankNodeParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_ruleBlankNode_in_ruleGraphNode2730);
                    this_BlankNode_3=ruleBlankNode();

                    state._fsp--;

                     
                            current = this_BlankNode_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1239:5: this_Parameter_4= ruleParameter
                    {
                     
                            newCompositeNode(grammarAccess.getGraphNodeAccess().getParameterParserRuleCall_4()); 
                        
                    pushFollow(FOLLOW_ruleParameter_in_ruleGraphNode2757);
                    this_Parameter_4=ruleParameter();

                    state._fsp--;

                     
                            current = this_Parameter_4; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 6 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1249:5: this_Quote_5= ruleQuote
                    {
                     
                            newCompositeNode(grammarAccess.getGraphNodeAccess().getQuoteParserRuleCall_5()); 
                        
                    pushFollow(FOLLOW_ruleQuote_in_ruleGraphNode2784);
                    this_Quote_5=ruleQuote();

                    state._fsp--;

                     
                            current = this_Quote_5; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGraphNode"


    // $ANTLR start "entryRuleQuote"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1265:1: entryRuleQuote returns [EObject current=null] : iv_ruleQuote= ruleQuote EOF ;
    public final EObject entryRuleQuote() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuote = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1266:2: (iv_ruleQuote= ruleQuote EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1267:2: iv_ruleQuote= ruleQuote EOF
            {
             newCompositeNode(grammarAccess.getQuoteRule()); 
            pushFollow(FOLLOW_ruleQuote_in_entryRuleQuote2819);
            iv_ruleQuote=ruleQuote();

            state._fsp--;

             current =iv_ruleQuote; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQuote2829); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuote"


    // $ANTLR start "ruleQuote"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1274:1: ruleQuote returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ) ;
    public final EObject ruleQuote() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1277:28: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1278:1: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1278:1: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1278:2: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':'
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1278:2: ( (lv_name_0_0= RULE_ID ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1279:1: (lv_name_0_0= RULE_ID )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1279:1: (lv_name_0_0= RULE_ID )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1280:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleQuote2871); 

            			newLeafNode(lv_name_0_0, grammarAccess.getQuoteAccess().getNameIDTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getQuoteRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"ID");
            	    

            }


            }

            otherlv_1=(Token)match(input,13,FOLLOW_13_in_ruleQuote2888); 

                	newLeafNode(otherlv_1, grammarAccess.getQuoteAccess().getColonKeyword_1());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuote"


    // $ANTLR start "entryRuleParameter"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1308:1: entryRuleParameter returns [EObject current=null] : iv_ruleParameter= ruleParameter EOF ;
    public final EObject entryRuleParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameter = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1309:2: (iv_ruleParameter= ruleParameter EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1310:2: iv_ruleParameter= ruleParameter EOF
            {
             newCompositeNode(grammarAccess.getParameterRule()); 
            pushFollow(FOLLOW_ruleParameter_in_entryRuleParameter2924);
            iv_ruleParameter=ruleParameter();

            state._fsp--;

             current =iv_ruleParameter; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleParameter2934); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameter"


    // $ANTLR start "ruleParameter"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1317:1: ruleParameter returns [EObject current=null] : (otherlv_0= '?:' ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleParameter() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;

         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1320:28: ( (otherlv_0= '?:' ( (lv_name_1_0= RULE_ID ) ) ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1321:1: (otherlv_0= '?:' ( (lv_name_1_0= RULE_ID ) ) )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1321:1: (otherlv_0= '?:' ( (lv_name_1_0= RULE_ID ) ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1321:3: otherlv_0= '?:' ( (lv_name_1_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,34,FOLLOW_34_in_ruleParameter2971); 

                	newLeafNode(otherlv_0, grammarAccess.getParameterAccess().getQuestionMarkColonKeyword_0());
                
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1325:1: ( (lv_name_1_0= RULE_ID ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1326:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1326:1: (lv_name_1_0= RULE_ID )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1327:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleParameter2988); 

            			newLeafNode(lv_name_1_0, grammarAccess.getParameterAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getParameterRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameter"


    // $ANTLR start "entryRuleBlankNode"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1351:1: entryRuleBlankNode returns [EObject current=null] : iv_ruleBlankNode= ruleBlankNode EOF ;
    public final EObject entryRuleBlankNode() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBlankNode = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1352:2: (iv_ruleBlankNode= ruleBlankNode EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1353:2: iv_ruleBlankNode= ruleBlankNode EOF
            {
             newCompositeNode(grammarAccess.getBlankNodeRule()); 
            pushFollow(FOLLOW_ruleBlankNode_in_entryRuleBlankNode3029);
            iv_ruleBlankNode=ruleBlankNode();

            state._fsp--;

             current =iv_ruleBlankNode; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBlankNode3039); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBlankNode"


    // $ANTLR start "ruleBlankNode"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1360:1: ruleBlankNode returns [EObject current=null] : (otherlv_0= '_:' ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleBlankNode() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;

         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1363:28: ( (otherlv_0= '_:' ( (lv_name_1_0= RULE_ID ) ) ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1364:1: (otherlv_0= '_:' ( (lv_name_1_0= RULE_ID ) ) )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1364:1: (otherlv_0= '_:' ( (lv_name_1_0= RULE_ID ) ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1364:3: otherlv_0= '_:' ( (lv_name_1_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,35,FOLLOW_35_in_ruleBlankNode3076); 

                	newLeafNode(otherlv_0, grammarAccess.getBlankNodeAccess().get_Keyword_0());
                
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1368:1: ( (lv_name_1_0= RULE_ID ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1369:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1369:1: (lv_name_1_0= RULE_ID )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1370:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleBlankNode3093); 

            			newLeafNode(lv_name_1_0, grammarAccess.getBlankNodeAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getBlankNodeRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBlankNode"


    // $ANTLR start "entryRuleValue"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1394:1: entryRuleValue returns [EObject current=null] : iv_ruleValue= ruleValue EOF ;
    public final EObject entryRuleValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleValue = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1395:2: (iv_ruleValue= ruleValue EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1396:2: iv_ruleValue= ruleValue EOF
            {
             newCompositeNode(grammarAccess.getValueRule()); 
            pushFollow(FOLLOW_ruleValue_in_entryRuleValue3134);
            iv_ruleValue=ruleValue();

            state._fsp--;

             current =iv_ruleValue; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleValue3144); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValue"


    // $ANTLR start "ruleValue"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1403:1: ruleValue returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) | ( (lv_StringValue_1_0= RULE_STRING ) ) | ( (lv_IntegerValue_2_0= RULE_INT ) ) ) ;
    public final EObject ruleValue() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token lv_StringValue_1_0=null;
        Token lv_IntegerValue_2_0=null;

         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1406:28: ( ( ( (lv_name_0_0= RULE_ID ) ) | ( (lv_StringValue_1_0= RULE_STRING ) ) | ( (lv_IntegerValue_2_0= RULE_INT ) ) ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1407:1: ( ( (lv_name_0_0= RULE_ID ) ) | ( (lv_StringValue_1_0= RULE_STRING ) ) | ( (lv_IntegerValue_2_0= RULE_INT ) ) )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1407:1: ( ( (lv_name_0_0= RULE_ID ) ) | ( (lv_StringValue_1_0= RULE_STRING ) ) | ( (lv_IntegerValue_2_0= RULE_INT ) ) )
            int alt23=3;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                alt23=1;
                }
                break;
            case RULE_STRING:
                {
                alt23=2;
                }
                break;
            case RULE_INT:
                {
                alt23=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }

            switch (alt23) {
                case 1 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1407:2: ( (lv_name_0_0= RULE_ID ) )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1407:2: ( (lv_name_0_0= RULE_ID ) )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1408:1: (lv_name_0_0= RULE_ID )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1408:1: (lv_name_0_0= RULE_ID )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1409:3: lv_name_0_0= RULE_ID
                    {
                    lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleValue3186); 

                    			newLeafNode(lv_name_0_0, grammarAccess.getValueAccess().getNameIDTerminalRuleCall_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getValueRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"name",
                            		lv_name_0_0, 
                            		"ID");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1426:6: ( (lv_StringValue_1_0= RULE_STRING ) )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1426:6: ( (lv_StringValue_1_0= RULE_STRING ) )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1427:1: (lv_StringValue_1_0= RULE_STRING )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1427:1: (lv_StringValue_1_0= RULE_STRING )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1428:3: lv_StringValue_1_0= RULE_STRING
                    {
                    lv_StringValue_1_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleValue3214); 

                    			newLeafNode(lv_StringValue_1_0, grammarAccess.getValueAccess().getStringValueSTRINGTerminalRuleCall_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getValueRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"StringValue",
                            		lv_StringValue_1_0, 
                            		"STRING");
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1445:6: ( (lv_IntegerValue_2_0= RULE_INT ) )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1445:6: ( (lv_IntegerValue_2_0= RULE_INT ) )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1446:1: (lv_IntegerValue_2_0= RULE_INT )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1446:1: (lv_IntegerValue_2_0= RULE_INT )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1447:3: lv_IntegerValue_2_0= RULE_INT
                    {
                    lv_IntegerValue_2_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleValue3242); 

                    			newLeafNode(lv_IntegerValue_2_0, grammarAccess.getValueAccess().getIntegerValueINTTerminalRuleCall_2_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getValueRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"IntegerValue",
                            		lv_IntegerValue_2_0, 
                            		"INT");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValue"


    // $ANTLR start "entryRuleDatasetClause"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1471:1: entryRuleDatasetClause returns [EObject current=null] : iv_ruleDatasetClause= ruleDatasetClause EOF ;
    public final EObject entryRuleDatasetClause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDatasetClause = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1472:2: (iv_ruleDatasetClause= ruleDatasetClause EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1473:2: iv_ruleDatasetClause= ruleDatasetClause EOF
            {
             newCompositeNode(grammarAccess.getDatasetClauseRule()); 
            pushFollow(FOLLOW_ruleDatasetClause_in_entryRuleDatasetClause3283);
            iv_ruleDatasetClause=ruleDatasetClause();

            state._fsp--;

             current =iv_ruleDatasetClause; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDatasetClause3293); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDatasetClause"


    // $ANTLR start "ruleDatasetClause"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1480:1: ruleDatasetClause returns [EObject current=null] : ( (otherlv_0= 'FROM' ( (lv_defaultGraphClause_1_0= ruleDefaultGraphClause ) ) ) | ( (lv_namedGraphClause_2_0= ruleNamedGraphClause ) ) ) ;
    public final EObject ruleDatasetClause() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_defaultGraphClause_1_0 = null;

        EObject lv_namedGraphClause_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1483:28: ( ( (otherlv_0= 'FROM' ( (lv_defaultGraphClause_1_0= ruleDefaultGraphClause ) ) ) | ( (lv_namedGraphClause_2_0= ruleNamedGraphClause ) ) ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1484:1: ( (otherlv_0= 'FROM' ( (lv_defaultGraphClause_1_0= ruleDefaultGraphClause ) ) ) | ( (lv_namedGraphClause_2_0= ruleNamedGraphClause ) ) )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1484:1: ( (otherlv_0= 'FROM' ( (lv_defaultGraphClause_1_0= ruleDefaultGraphClause ) ) ) | ( (lv_namedGraphClause_2_0= ruleNamedGraphClause ) ) )
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==36) ) {
                alt24=1;
            }
            else if ( (LA24_0==37) ) {
                alt24=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;
            }
            switch (alt24) {
                case 1 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1484:2: (otherlv_0= 'FROM' ( (lv_defaultGraphClause_1_0= ruleDefaultGraphClause ) ) )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1484:2: (otherlv_0= 'FROM' ( (lv_defaultGraphClause_1_0= ruleDefaultGraphClause ) ) )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1484:4: otherlv_0= 'FROM' ( (lv_defaultGraphClause_1_0= ruleDefaultGraphClause ) )
                    {
                    otherlv_0=(Token)match(input,36,FOLLOW_36_in_ruleDatasetClause3331); 

                        	newLeafNode(otherlv_0, grammarAccess.getDatasetClauseAccess().getFROMKeyword_0_0());
                        
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1488:1: ( (lv_defaultGraphClause_1_0= ruleDefaultGraphClause ) )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1489:1: (lv_defaultGraphClause_1_0= ruleDefaultGraphClause )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1489:1: (lv_defaultGraphClause_1_0= ruleDefaultGraphClause )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1490:3: lv_defaultGraphClause_1_0= ruleDefaultGraphClause
                    {
                     
                    	        newCompositeNode(grammarAccess.getDatasetClauseAccess().getDefaultGraphClauseDefaultGraphClauseParserRuleCall_0_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleDefaultGraphClause_in_ruleDatasetClause3352);
                    lv_defaultGraphClause_1_0=ruleDefaultGraphClause();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getDatasetClauseRule());
                    	        }
                           		set(
                           			current, 
                           			"defaultGraphClause",
                            		lv_defaultGraphClause_1_0, 
                            		"DefaultGraphClause");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1507:6: ( (lv_namedGraphClause_2_0= ruleNamedGraphClause ) )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1507:6: ( (lv_namedGraphClause_2_0= ruleNamedGraphClause ) )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1508:1: (lv_namedGraphClause_2_0= ruleNamedGraphClause )
                    {
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1508:1: (lv_namedGraphClause_2_0= ruleNamedGraphClause )
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1509:3: lv_namedGraphClause_2_0= ruleNamedGraphClause
                    {
                     
                    	        newCompositeNode(grammarAccess.getDatasetClauseAccess().getNamedGraphClauseNamedGraphClauseParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleNamedGraphClause_in_ruleDatasetClause3380);
                    lv_namedGraphClause_2_0=ruleNamedGraphClause();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getDatasetClauseRule());
                    	        }
                           		set(
                           			current, 
                           			"namedGraphClause",
                            		lv_namedGraphClause_2_0, 
                            		"NamedGraphClause");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDatasetClause"


    // $ANTLR start "entryRuleDefaultGraphClause"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1533:1: entryRuleDefaultGraphClause returns [EObject current=null] : iv_ruleDefaultGraphClause= ruleDefaultGraphClause EOF ;
    public final EObject entryRuleDefaultGraphClause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDefaultGraphClause = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1534:2: (iv_ruleDefaultGraphClause= ruleDefaultGraphClause EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1535:2: iv_ruleDefaultGraphClause= ruleDefaultGraphClause EOF
            {
             newCompositeNode(grammarAccess.getDefaultGraphClauseRule()); 
            pushFollow(FOLLOW_ruleDefaultGraphClause_in_entryRuleDefaultGraphClause3416);
            iv_ruleDefaultGraphClause=ruleDefaultGraphClause();

            state._fsp--;

             current =iv_ruleDefaultGraphClause; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDefaultGraphClause3426); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDefaultGraphClause"


    // $ANTLR start "ruleDefaultGraphClause"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1542:1: ruleDefaultGraphClause returns [EObject current=null] : ( (lv_iref_0_0= RULE_IRI_URI ) ) ;
    public final EObject ruleDefaultGraphClause() throws RecognitionException {
        EObject current = null;

        Token lv_iref_0_0=null;

         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1545:28: ( ( (lv_iref_0_0= RULE_IRI_URI ) ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1546:1: ( (lv_iref_0_0= RULE_IRI_URI ) )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1546:1: ( (lv_iref_0_0= RULE_IRI_URI ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1547:1: (lv_iref_0_0= RULE_IRI_URI )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1547:1: (lv_iref_0_0= RULE_IRI_URI )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1548:3: lv_iref_0_0= RULE_IRI_URI
            {
            lv_iref_0_0=(Token)match(input,RULE_IRI_URI,FOLLOW_RULE_IRI_URI_in_ruleDefaultGraphClause3467); 

            			newLeafNode(lv_iref_0_0, grammarAccess.getDefaultGraphClauseAccess().getIrefIRI_URITerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getDefaultGraphClauseRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"iref",
                    		lv_iref_0_0, 
                    		"IRI_URI");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDefaultGraphClause"


    // $ANTLR start "entryRuleNamedGraphClause"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1572:1: entryRuleNamedGraphClause returns [EObject current=null] : iv_ruleNamedGraphClause= ruleNamedGraphClause EOF ;
    public final EObject entryRuleNamedGraphClause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNamedGraphClause = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1573:2: (iv_ruleNamedGraphClause= ruleNamedGraphClause EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1574:2: iv_ruleNamedGraphClause= ruleNamedGraphClause EOF
            {
             newCompositeNode(grammarAccess.getNamedGraphClauseRule()); 
            pushFollow(FOLLOW_ruleNamedGraphClause_in_entryRuleNamedGraphClause3507);
            iv_ruleNamedGraphClause=ruleNamedGraphClause();

            state._fsp--;

             current =iv_ruleNamedGraphClause; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleNamedGraphClause3517); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNamedGraphClause"


    // $ANTLR start "ruleNamedGraphClause"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1581:1: ruleNamedGraphClause returns [EObject current=null] : (otherlv_0= 'NAMED' this_SourceSelector_1= ruleSourceSelector ) ;
    public final EObject ruleNamedGraphClause() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject this_SourceSelector_1 = null;


         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1584:28: ( (otherlv_0= 'NAMED' this_SourceSelector_1= ruleSourceSelector ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1585:1: (otherlv_0= 'NAMED' this_SourceSelector_1= ruleSourceSelector )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1585:1: (otherlv_0= 'NAMED' this_SourceSelector_1= ruleSourceSelector )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1585:3: otherlv_0= 'NAMED' this_SourceSelector_1= ruleSourceSelector
            {
            otherlv_0=(Token)match(input,37,FOLLOW_37_in_ruleNamedGraphClause3554); 

                	newLeafNode(otherlv_0, grammarAccess.getNamedGraphClauseAccess().getNAMEDKeyword_0());
                
             
                    newCompositeNode(grammarAccess.getNamedGraphClauseAccess().getSourceSelectorParserRuleCall_1()); 
                
            pushFollow(FOLLOW_ruleSourceSelector_in_ruleNamedGraphClause3576);
            this_SourceSelector_1=ruleSourceSelector();

            state._fsp--;

             
                    current = this_SourceSelector_1; 
                    afterParserOrEnumRuleCall();
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNamedGraphClause"


    // $ANTLR start "entryRuleSourceSelector"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1606:1: entryRuleSourceSelector returns [EObject current=null] : iv_ruleSourceSelector= ruleSourceSelector EOF ;
    public final EObject entryRuleSourceSelector() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSourceSelector = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1607:2: (iv_ruleSourceSelector= ruleSourceSelector EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1608:2: iv_ruleSourceSelector= ruleSourceSelector EOF
            {
             newCompositeNode(grammarAccess.getSourceSelectorRule()); 
            pushFollow(FOLLOW_ruleSourceSelector_in_entryRuleSourceSelector3611);
            iv_ruleSourceSelector=ruleSourceSelector();

            state._fsp--;

             current =iv_ruleSourceSelector; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSourceSelector3621); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSourceSelector"


    // $ANTLR start "ruleSourceSelector"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1615:1: ruleSourceSelector returns [EObject current=null] : ( (lv_iref_0_0= RULE_IRI_URI ) ) ;
    public final EObject ruleSourceSelector() throws RecognitionException {
        EObject current = null;

        Token lv_iref_0_0=null;

         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1618:28: ( ( (lv_iref_0_0= RULE_IRI_URI ) ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1619:1: ( (lv_iref_0_0= RULE_IRI_URI ) )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1619:1: ( (lv_iref_0_0= RULE_IRI_URI ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1620:1: (lv_iref_0_0= RULE_IRI_URI )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1620:1: (lv_iref_0_0= RULE_IRI_URI )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1621:3: lv_iref_0_0= RULE_IRI_URI
            {
            lv_iref_0_0=(Token)match(input,RULE_IRI_URI,FOLLOW_RULE_IRI_URI_in_ruleSourceSelector3662); 

            			newLeafNode(lv_iref_0_0, grammarAccess.getSourceSelectorAccess().getIrefIRI_URITerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getSourceSelectorRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"iref",
                    		lv_iref_0_0, 
                    		"IRI_URI");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSourceSelector"


    // $ANTLR start "entryRuleVar"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1645:1: entryRuleVar returns [EObject current=null] : iv_ruleVar= ruleVar EOF ;
    public final EObject entryRuleVar() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVar = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1646:2: (iv_ruleVar= ruleVar EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1647:2: iv_ruleVar= ruleVar EOF
            {
             newCompositeNode(grammarAccess.getVarRule()); 
            pushFollow(FOLLOW_ruleVar_in_entryRuleVar3702);
            iv_ruleVar=ruleVar();

            state._fsp--;

             current =iv_ruleVar; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVar3712); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVar"


    // $ANTLR start "ruleVar"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1654:1: ruleVar returns [EObject current=null] : (this_VAR1_0= ruleVAR1 | this_VAR2_1= ruleVAR2 ) ;
    public final EObject ruleVar() throws RecognitionException {
        EObject current = null;

        EObject this_VAR1_0 = null;

        EObject this_VAR2_1 = null;


         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1657:28: ( (this_VAR1_0= ruleVAR1 | this_VAR2_1= ruleVAR2 ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1658:1: (this_VAR1_0= ruleVAR1 | this_VAR2_1= ruleVAR2 )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1658:1: (this_VAR1_0= ruleVAR1 | this_VAR2_1= ruleVAR2 )
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==38) ) {
                alt25=1;
            }
            else if ( (LA25_0==12) ) {
                alt25=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 25, 0, input);

                throw nvae;
            }
            switch (alt25) {
                case 1 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1659:5: this_VAR1_0= ruleVAR1
                    {
                     
                            newCompositeNode(grammarAccess.getVarAccess().getVAR1ParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleVAR1_in_ruleVar3759);
                    this_VAR1_0=ruleVAR1();

                    state._fsp--;

                     
                            current = this_VAR1_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1669:5: this_VAR2_1= ruleVAR2
                    {
                     
                            newCompositeNode(grammarAccess.getVarAccess().getVAR2ParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleVAR2_in_ruleVar3786);
                    this_VAR2_1=ruleVAR2();

                    state._fsp--;

                     
                            current = this_VAR2_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVar"


    // $ANTLR start "entryRuleVAR1"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1685:1: entryRuleVAR1 returns [EObject current=null] : iv_ruleVAR1= ruleVAR1 EOF ;
    public final EObject entryRuleVAR1() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVAR1 = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1686:2: (iv_ruleVAR1= ruleVAR1 EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1687:2: iv_ruleVAR1= ruleVAR1 EOF
            {
             newCompositeNode(grammarAccess.getVAR1Rule()); 
            pushFollow(FOLLOW_ruleVAR1_in_entryRuleVAR13821);
            iv_ruleVAR1=ruleVAR1();

            state._fsp--;

             current =iv_ruleVAR1; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVAR13831); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVAR1"


    // $ANTLR start "ruleVAR1"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1694:1: ruleVAR1 returns [EObject current=null] : (otherlv_0= '?' ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleVAR1() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;

         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1697:28: ( (otherlv_0= '?' ( (lv_name_1_0= RULE_ID ) ) ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1698:1: (otherlv_0= '?' ( (lv_name_1_0= RULE_ID ) ) )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1698:1: (otherlv_0= '?' ( (lv_name_1_0= RULE_ID ) ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1698:3: otherlv_0= '?' ( (lv_name_1_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,38,FOLLOW_38_in_ruleVAR13868); 

                	newLeafNode(otherlv_0, grammarAccess.getVAR1Access().getQuestionMarkKeyword_0());
                
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1702:1: ( (lv_name_1_0= RULE_ID ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1703:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1703:1: (lv_name_1_0= RULE_ID )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1704:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleVAR13885); 

            			newLeafNode(lv_name_1_0, grammarAccess.getVAR1Access().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getVAR1Rule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVAR1"


    // $ANTLR start "entryRuleVAR2"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1728:1: entryRuleVAR2 returns [EObject current=null] : iv_ruleVAR2= ruleVAR2 EOF ;
    public final EObject entryRuleVAR2() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVAR2 = null;


        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1729:2: (iv_ruleVAR2= ruleVAR2 EOF )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1730:2: iv_ruleVAR2= ruleVAR2 EOF
            {
             newCompositeNode(grammarAccess.getVAR2Rule()); 
            pushFollow(FOLLOW_ruleVAR2_in_entryRuleVAR23926);
            iv_ruleVAR2=ruleVAR2();

            state._fsp--;

             current =iv_ruleVAR2; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVAR23936); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVAR2"


    // $ANTLR start "ruleVAR2"
    // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1737:1: ruleVAR2 returns [EObject current=null] : (this_Prefix_0= rulePrefix otherlv_1= ':' ( (lv_name_2_0= RULE_ID ) ) ) ;
    public final EObject ruleVAR2() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        EObject this_Prefix_0 = null;


         enterRule(); 
            
        try {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1740:28: ( (this_Prefix_0= rulePrefix otherlv_1= ':' ( (lv_name_2_0= RULE_ID ) ) ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1741:1: (this_Prefix_0= rulePrefix otherlv_1= ':' ( (lv_name_2_0= RULE_ID ) ) )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1741:1: (this_Prefix_0= rulePrefix otherlv_1= ':' ( (lv_name_2_0= RULE_ID ) ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1742:5: this_Prefix_0= rulePrefix otherlv_1= ':' ( (lv_name_2_0= RULE_ID ) )
            {
             
                    newCompositeNode(grammarAccess.getVAR2Access().getPrefixParserRuleCall_0()); 
                
            pushFollow(FOLLOW_rulePrefix_in_ruleVAR23983);
            this_Prefix_0=rulePrefix();

            state._fsp--;

             
                    current = this_Prefix_0; 
                    afterParserOrEnumRuleCall();
                
            otherlv_1=(Token)match(input,13,FOLLOW_13_in_ruleVAR23994); 

                	newLeafNode(otherlv_1, grammarAccess.getVAR2Access().getColonKeyword_1());
                
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1754:1: ( (lv_name_2_0= RULE_ID ) )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1755:1: (lv_name_2_0= RULE_ID )
            {
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1755:1: (lv_name_2_0= RULE_ID )
            // ../org.edu.le.spaqrl/src-gen/org/edu/le/spaqrl/parser/antlr/internal/InternalSparql.g:1756:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleVAR24011); 

            			newLeafNode(lv_name_2_0, grammarAccess.getVAR2Access().getNameIDTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getVAR2Rule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_2_0, 
                    		"ID");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVAR2"

    // Delegated rules


    protected DFA10 dfa10 = new DFA10(this);
    static final String DFA10_eotS =
        "\25\uffff";
    static final String DFA10_eofS =
        "\25\uffff";
    static final String DFA10_minS =
        "\1\24\1\4\1\uffff\2\4\4\uffff\2\4\1\uffff\1\5\1\15\2\uffff\1\15"+
        "\1\5\1\4\1\15\1\uffff";
    static final String DFA10_maxS =
        "\1\25\1\46\1\uffff\1\4\1\15\4\uffff\2\4\1\uffff\1\5\1\15\2\uffff"+
        "\1\15\1\5\1\4\1\15\1\uffff";
    static final String DFA10_acceptS =
        "\2\uffff\1\2\2\uffff\4\1\2\uffff\1\1\2\uffff\2\1\4\uffff\1\1";
    static final String DFA10_specialS =
        "\25\uffff}>";
    static final String[] DFA10_transitionS = {
            "\1\1\1\2",
            "\1\5\1\10\1\7\1\6\4\uffff\1\4\25\uffff\1\12\1\11\2\uffff\1"+
            "\3",
            "",
            "\1\13",
            "\1\15\10\uffff\1\14",
            "",
            "",
            "",
            "",
            "\1\16",
            "\1\17",
            "",
            "\1\20",
            "\1\21",
            "",
            "",
            "\1\22",
            "\1\23",
            "\1\24",
            "\1\22",
            ""
    };

    static final short[] DFA10_eot = DFA.unpackEncodedString(DFA10_eotS);
    static final short[] DFA10_eof = DFA.unpackEncodedString(DFA10_eofS);
    static final char[] DFA10_min = DFA.unpackEncodedStringToUnsignedChars(DFA10_minS);
    static final char[] DFA10_max = DFA.unpackEncodedStringToUnsignedChars(DFA10_maxS);
    static final short[] DFA10_accept = DFA.unpackEncodedString(DFA10_acceptS);
    static final short[] DFA10_special = DFA.unpackEncodedString(DFA10_specialS);
    static final short[][] DFA10_transition;

    static {
        int numStates = DFA10_transitionS.length;
        DFA10_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA10_transition[i] = DFA.unpackEncodedString(DFA10_transitionS[i]);
        }
    }

    class DFA10 extends DFA {

        public DFA10(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 10;
            this.eot = DFA10_eot;
            this.eof = DFA10_eof;
            this.min = DFA10_min;
            this.max = DFA10_max;
            this.accept = DFA10_accept;
            this.special = DFA10_special;
            this.transition = DFA10_transition;
        }
        public String getDescription() {
            return "491:2: (otherlv_3= ',' ( (lv_parameters_4_0= ruleGraphNode ) ) )?";
        }
    }
 

    public static final BitSet FOLLOW_ruleQuery_in_entryRuleQuery75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQuery85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSelectQuery_in_ruleQuery130 = new BitSet(new long[]{0x0000000000C05002L});
    public static final BitSet FOLLOW_rulePrefix_in_entryRulePrefix166 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePrefix176 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rulePrefix214 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_rulePrefix231 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_rulePrefix248 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_IRI_URI_in_rulePrefix265 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUnNamedPrefix_in_rulePrefix299 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUnNamedPrefix_in_entryRuleUnNamedPrefix334 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleUnNamedPrefix344 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_ruleUnNamedPrefix381 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleUnNamedPrefix393 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_IRI_URI_in_ruleUnNamedPrefix410 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSelectQuery_in_entryRuleSelectQuery451 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSelectQuery461 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrefix_in_ruleSelectQuery508 = new BitSet(new long[]{0x0000000000005000L});
    public static final BitSet FOLLOW_14_in_ruleSelectQuery521 = new BitSet(new long[]{0x0000004000039000L});
    public static final BitSet FOLLOW_15_in_ruleSelectQuery534 = new BitSet(new long[]{0x0000004000021000L});
    public static final BitSet FOLLOW_16_in_ruleSelectQuery552 = new BitSet(new long[]{0x0000004000021000L});
    public static final BitSet FOLLOW_ruleVar_in_ruleSelectQuery576 = new BitSet(new long[]{0x0000007008001000L});
    public static final BitSet FOLLOW_17_in_ruleSelectQuery595 = new BitSet(new long[]{0x0000003008000000L});
    public static final BitSet FOLLOW_ruleDatasetClause_in_ruleSelectQuery617 = new BitSet(new long[]{0x0000003008000000L});
    public static final BitSet FOLLOW_ruleWhereClause_in_ruleSelectQuery639 = new BitSet(new long[]{0x0000000001000002L});
    public static final BitSet FOLLOW_ruleOrderClause_in_ruleSelectQuery660 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLimitClause_in_ruleSelectQuery689 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOffsetClause_in_ruleSelectQuery716 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFilter_in_entryRuleFilter752 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFilter762 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_ruleFilter799 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleExpressionFilter_in_ruleFilter820 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpressionFilter_in_entryRuleExpressionFilter856 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExpressionFilter866 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleExpressionFilter908 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleExpressionFilter925 = new BitSet(new long[]{0x0000004C000010F0L});
    public static final BitSet FOLLOW_ruleGraphNode_in_ruleExpressionFilter946 = new BitSet(new long[]{0x0000000000300000L});
    public static final BitSet FOLLOW_20_in_ruleExpressionFilter959 = new BitSet(new long[]{0x0000004C000010F0L});
    public static final BitSet FOLLOW_ruleGraphNode_in_ruleExpressionFilter980 = new BitSet(new long[]{0x0000000000300000L});
    public static final BitSet FOLLOW_20_in_ruleExpressionFilter995 = new BitSet(new long[]{0x0000004C000010F0L});
    public static final BitSet FOLLOW_ruleGraphNode_in_ruleExpressionFilter1016 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_ruleExpressionFilter1030 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOffsetClause_in_entryRuleOffsetClause1067 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOffsetClause1078 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_ruleOffsetClause1116 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_ruleINTEGER_in_ruleOffsetClause1138 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLimitClause_in_entryRuleLimitClause1184 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLimitClause1195 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_ruleLimitClause1233 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_ruleINTEGER_in_ruleLimitClause1255 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleINTEGER_in_entryRuleINTEGER1301 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleINTEGER1312 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleINTEGER1351 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOrderClause_in_entryRuleOrderClause1395 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOrderClause1405 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_ruleOrderClause1442 = new BitSet(new long[]{0x0000004006001000L});
    public static final BitSet FOLLOW_ruleOrderCondition_in_ruleOrderClause1463 = new BitSet(new long[]{0x0000004006001002L});
    public static final BitSet FOLLOW_ruleOrderCondition_in_entryRuleOrderCondition1500 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOrderCondition1510 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_ruleOrderCondition1549 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_26_in_ruleOrderCondition1567 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_ruleBrackettedExpression_in_ruleOrderCondition1589 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVar_in_ruleOrderCondition1618 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBrackettedExpression_in_entryRuleBrackettedExpression1653 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBrackettedExpression1663 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_ruleBrackettedExpression1700 = new BitSet(new long[]{0x0000004000001000L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleBrackettedExpression1721 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_ruleBrackettedExpression1733 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_entryRuleExpression1769 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExpression1779 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVar_in_ruleExpression1825 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhereClause_in_entryRuleWhereClause1859 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWhereClause1869 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_ruleWhereClause1906 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_28_in_ruleWhereClause1918 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_rulePattern_in_ruleWhereClause1940 = new BitSet(new long[]{0x00000000E0040000L});
    public static final BitSet FOLLOW_29_in_ruleWhereClause1953 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_rulePattern_in_ruleWhereClause1974 = new BitSet(new long[]{0x00000000C0040000L});
    public static final BitSet FOLLOW_30_in_ruleWhereClause1989 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_rulePattern_in_ruleWhereClause2010 = new BitSet(new long[]{0x0000000080040000L});
    public static final BitSet FOLLOW_ruleFilter_in_ruleWhereClause2034 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_ruleWhereClause2047 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePattern_in_entryRulePattern2083 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePattern2093 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_rulePattern2130 = new BitSet(new long[]{0x0000004C000010F0L});
    public static final BitSet FOLLOW_rulePatternOne_in_rulePattern2151 = new BitSet(new long[]{0x0000000180000000L});
    public static final BitSet FOLLOW_32_in_rulePattern2164 = new BitSet(new long[]{0x0000004C000010F0L});
    public static final BitSet FOLLOW_rulePatternOne_in_rulePattern2185 = new BitSet(new long[]{0x0000000180000000L});
    public static final BitSet FOLLOW_32_in_rulePattern2200 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_rulePattern2214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePatternOne_in_entryRulePatternOne2250 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePatternOne2260 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTriplesSameSubject_in_rulePatternOne2305 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTriplesSameSubject_in_entryRuleTriplesSameSubject2340 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTriplesSameSubject2350 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGraphNode_in_ruleTriplesSameSubject2396 = new BitSet(new long[]{0x0000004C000010F0L});
    public static final BitSet FOLLOW_rulePropertyList_in_ruleTriplesSameSubject2417 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_33_in_ruleTriplesSameSubject2430 = new BitSet(new long[]{0x0000004C000010F0L});
    public static final BitSet FOLLOW_rulePropertyList_in_ruleTriplesSameSubject2451 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_rulePropertyList_in_entryRulePropertyList2489 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePropertyList2499 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGraphNode_in_rulePropertyList2545 = new BitSet(new long[]{0x0000004C000010F2L});
    public static final BitSet FOLLOW_ruleGraphNode_in_rulePropertyList2566 = new BitSet(new long[]{0x0000004C000010F2L});
    public static final BitSet FOLLOW_ruleGraphNode_in_entryRuleGraphNode2603 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGraphNode2613 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVar_in_ruleGraphNode2660 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleValue_in_ruleGraphNode2687 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_IRI_URI_in_ruleGraphNode2703 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBlankNode_in_ruleGraphNode2730 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParameter_in_ruleGraphNode2757 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQuote_in_ruleGraphNode2784 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQuote_in_entryRuleQuote2819 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQuote2829 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleQuote2871 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleQuote2888 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParameter_in_entryRuleParameter2924 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleParameter2934 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_ruleParameter2971 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleParameter2988 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBlankNode_in_entryRuleBlankNode3029 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBlankNode3039 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_ruleBlankNode3076 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleBlankNode3093 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleValue_in_entryRuleValue3134 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleValue3144 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleValue3186 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleValue3214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleValue3242 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDatasetClause_in_entryRuleDatasetClause3283 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDatasetClause3293 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_ruleDatasetClause3331 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ruleDefaultGraphClause_in_ruleDatasetClause3352 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNamedGraphClause_in_ruleDatasetClause3380 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDefaultGraphClause_in_entryRuleDefaultGraphClause3416 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDefaultGraphClause3426 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_IRI_URI_in_ruleDefaultGraphClause3467 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNamedGraphClause_in_entryRuleNamedGraphClause3507 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNamedGraphClause3517 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_ruleNamedGraphClause3554 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ruleSourceSelector_in_ruleNamedGraphClause3576 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSourceSelector_in_entryRuleSourceSelector3611 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSourceSelector3621 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_IRI_URI_in_ruleSourceSelector3662 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVar_in_entryRuleVar3702 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVar3712 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVAR1_in_ruleVar3759 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVAR2_in_ruleVar3786 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVAR1_in_entryRuleVAR13821 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVAR13831 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_ruleVAR13868 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleVAR13885 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVAR2_in_entryRuleVAR23926 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVAR23936 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrefix_in_ruleVAR23983 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleVAR23994 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleVAR24011 = new BitSet(new long[]{0x0000000000000002L});

}