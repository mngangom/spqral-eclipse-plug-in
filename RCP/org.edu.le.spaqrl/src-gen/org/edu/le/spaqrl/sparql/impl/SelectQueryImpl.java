/**
 */
package org.edu.le.spaqrl.sparql.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.edu.le.spaqrl.sparql.DatasetClause;
import org.edu.le.spaqrl.sparql.OrderClause;
import org.edu.le.spaqrl.sparql.Prefix;
import org.edu.le.spaqrl.sparql.SelectQuery;
import org.edu.le.spaqrl.sparql.SparqlPackage;
import org.edu.le.spaqrl.sparql.Var;
import org.edu.le.spaqrl.sparql.WhereClause;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Select Query</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.edu.le.spaqrl.sparql.impl.SelectQueryImpl#getPrefix <em>Prefix</em>}</li>
 *   <li>{@link org.edu.le.spaqrl.sparql.impl.SelectQueryImpl#getVar <em>Var</em>}</li>
 *   <li>{@link org.edu.le.spaqrl.sparql.impl.SelectQueryImpl#getDatasetClause <em>Dataset Clause</em>}</li>
 *   <li>{@link org.edu.le.spaqrl.sparql.impl.SelectQueryImpl#getWhereClause <em>Where Clause</em>}</li>
 *   <li>{@link org.edu.le.spaqrl.sparql.impl.SelectQueryImpl#getOrderClause <em>Order Clause</em>}</li>
 *   <li>{@link org.edu.le.spaqrl.sparql.impl.SelectQueryImpl#getLimitClause <em>Limit Clause</em>}</li>
 *   <li>{@link org.edu.le.spaqrl.sparql.impl.SelectQueryImpl#getOffsetClause <em>Offset Clause</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SelectQueryImpl extends MinimalEObjectImpl.Container implements SelectQuery
{
  /**
   * The cached value of the '{@link #getPrefix() <em>Prefix</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPrefix()
   * @generated
   * @ordered
   */
  protected EList<Prefix> prefix;

  /**
   * The cached value of the '{@link #getVar() <em>Var</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVar()
   * @generated
   * @ordered
   */
  protected Var var;

  /**
   * The cached value of the '{@link #getDatasetClause() <em>Dataset Clause</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDatasetClause()
   * @generated
   * @ordered
   */
  protected DatasetClause datasetClause;

  /**
   * The cached value of the '{@link #getWhereClause() <em>Where Clause</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getWhereClause()
   * @generated
   * @ordered
   */
  protected WhereClause whereClause;

  /**
   * The cached value of the '{@link #getOrderClause() <em>Order Clause</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOrderClause()
   * @generated
   * @ordered
   */
  protected OrderClause orderClause;

  /**
   * The default value of the '{@link #getLimitClause() <em>Limit Clause</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLimitClause()
   * @generated
   * @ordered
   */
  protected static final String LIMIT_CLAUSE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLimitClause() <em>Limit Clause</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLimitClause()
   * @generated
   * @ordered
   */
  protected String limitClause = LIMIT_CLAUSE_EDEFAULT;

  /**
   * The default value of the '{@link #getOffsetClause() <em>Offset Clause</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOffsetClause()
   * @generated
   * @ordered
   */
  protected static final String OFFSET_CLAUSE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getOffsetClause() <em>Offset Clause</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOffsetClause()
   * @generated
   * @ordered
   */
  protected String offsetClause = OFFSET_CLAUSE_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SelectQueryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SparqlPackage.Literals.SELECT_QUERY;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Prefix> getPrefix()
  {
    if (prefix == null)
    {
      prefix = new EObjectContainmentEList<Prefix>(Prefix.class, this, SparqlPackage.SELECT_QUERY__PREFIX);
    }
    return prefix;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Var getVar()
  {
    return var;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetVar(Var newVar, NotificationChain msgs)
  {
    Var oldVar = var;
    var = newVar;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SparqlPackage.SELECT_QUERY__VAR, oldVar, newVar);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVar(Var newVar)
  {
    if (newVar != var)
    {
      NotificationChain msgs = null;
      if (var != null)
        msgs = ((InternalEObject)var).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SparqlPackage.SELECT_QUERY__VAR, null, msgs);
      if (newVar != null)
        msgs = ((InternalEObject)newVar).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SparqlPackage.SELECT_QUERY__VAR, null, msgs);
      msgs = basicSetVar(newVar, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SparqlPackage.SELECT_QUERY__VAR, newVar, newVar));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DatasetClause getDatasetClause()
  {
    return datasetClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDatasetClause(DatasetClause newDatasetClause, NotificationChain msgs)
  {
    DatasetClause oldDatasetClause = datasetClause;
    datasetClause = newDatasetClause;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SparqlPackage.SELECT_QUERY__DATASET_CLAUSE, oldDatasetClause, newDatasetClause);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDatasetClause(DatasetClause newDatasetClause)
  {
    if (newDatasetClause != datasetClause)
    {
      NotificationChain msgs = null;
      if (datasetClause != null)
        msgs = ((InternalEObject)datasetClause).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SparqlPackage.SELECT_QUERY__DATASET_CLAUSE, null, msgs);
      if (newDatasetClause != null)
        msgs = ((InternalEObject)newDatasetClause).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SparqlPackage.SELECT_QUERY__DATASET_CLAUSE, null, msgs);
      msgs = basicSetDatasetClause(newDatasetClause, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SparqlPackage.SELECT_QUERY__DATASET_CLAUSE, newDatasetClause, newDatasetClause));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WhereClause getWhereClause()
  {
    return whereClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetWhereClause(WhereClause newWhereClause, NotificationChain msgs)
  {
    WhereClause oldWhereClause = whereClause;
    whereClause = newWhereClause;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SparqlPackage.SELECT_QUERY__WHERE_CLAUSE, oldWhereClause, newWhereClause);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setWhereClause(WhereClause newWhereClause)
  {
    if (newWhereClause != whereClause)
    {
      NotificationChain msgs = null;
      if (whereClause != null)
        msgs = ((InternalEObject)whereClause).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SparqlPackage.SELECT_QUERY__WHERE_CLAUSE, null, msgs);
      if (newWhereClause != null)
        msgs = ((InternalEObject)newWhereClause).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SparqlPackage.SELECT_QUERY__WHERE_CLAUSE, null, msgs);
      msgs = basicSetWhereClause(newWhereClause, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SparqlPackage.SELECT_QUERY__WHERE_CLAUSE, newWhereClause, newWhereClause));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OrderClause getOrderClause()
  {
    return orderClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetOrderClause(OrderClause newOrderClause, NotificationChain msgs)
  {
    OrderClause oldOrderClause = orderClause;
    orderClause = newOrderClause;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SparqlPackage.SELECT_QUERY__ORDER_CLAUSE, oldOrderClause, newOrderClause);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOrderClause(OrderClause newOrderClause)
  {
    if (newOrderClause != orderClause)
    {
      NotificationChain msgs = null;
      if (orderClause != null)
        msgs = ((InternalEObject)orderClause).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SparqlPackage.SELECT_QUERY__ORDER_CLAUSE, null, msgs);
      if (newOrderClause != null)
        msgs = ((InternalEObject)newOrderClause).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SparqlPackage.SELECT_QUERY__ORDER_CLAUSE, null, msgs);
      msgs = basicSetOrderClause(newOrderClause, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SparqlPackage.SELECT_QUERY__ORDER_CLAUSE, newOrderClause, newOrderClause));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLimitClause()
  {
    return limitClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLimitClause(String newLimitClause)
  {
    String oldLimitClause = limitClause;
    limitClause = newLimitClause;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SparqlPackage.SELECT_QUERY__LIMIT_CLAUSE, oldLimitClause, limitClause));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getOffsetClause()
  {
    return offsetClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOffsetClause(String newOffsetClause)
  {
    String oldOffsetClause = offsetClause;
    offsetClause = newOffsetClause;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SparqlPackage.SELECT_QUERY__OFFSET_CLAUSE, oldOffsetClause, offsetClause));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SparqlPackage.SELECT_QUERY__PREFIX:
        return ((InternalEList<?>)getPrefix()).basicRemove(otherEnd, msgs);
      case SparqlPackage.SELECT_QUERY__VAR:
        return basicSetVar(null, msgs);
      case SparqlPackage.SELECT_QUERY__DATASET_CLAUSE:
        return basicSetDatasetClause(null, msgs);
      case SparqlPackage.SELECT_QUERY__WHERE_CLAUSE:
        return basicSetWhereClause(null, msgs);
      case SparqlPackage.SELECT_QUERY__ORDER_CLAUSE:
        return basicSetOrderClause(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SparqlPackage.SELECT_QUERY__PREFIX:
        return getPrefix();
      case SparqlPackage.SELECT_QUERY__VAR:
        return getVar();
      case SparqlPackage.SELECT_QUERY__DATASET_CLAUSE:
        return getDatasetClause();
      case SparqlPackage.SELECT_QUERY__WHERE_CLAUSE:
        return getWhereClause();
      case SparqlPackage.SELECT_QUERY__ORDER_CLAUSE:
        return getOrderClause();
      case SparqlPackage.SELECT_QUERY__LIMIT_CLAUSE:
        return getLimitClause();
      case SparqlPackage.SELECT_QUERY__OFFSET_CLAUSE:
        return getOffsetClause();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SparqlPackage.SELECT_QUERY__PREFIX:
        getPrefix().clear();
        getPrefix().addAll((Collection<? extends Prefix>)newValue);
        return;
      case SparqlPackage.SELECT_QUERY__VAR:
        setVar((Var)newValue);
        return;
      case SparqlPackage.SELECT_QUERY__DATASET_CLAUSE:
        setDatasetClause((DatasetClause)newValue);
        return;
      case SparqlPackage.SELECT_QUERY__WHERE_CLAUSE:
        setWhereClause((WhereClause)newValue);
        return;
      case SparqlPackage.SELECT_QUERY__ORDER_CLAUSE:
        setOrderClause((OrderClause)newValue);
        return;
      case SparqlPackage.SELECT_QUERY__LIMIT_CLAUSE:
        setLimitClause((String)newValue);
        return;
      case SparqlPackage.SELECT_QUERY__OFFSET_CLAUSE:
        setOffsetClause((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SparqlPackage.SELECT_QUERY__PREFIX:
        getPrefix().clear();
        return;
      case SparqlPackage.SELECT_QUERY__VAR:
        setVar((Var)null);
        return;
      case SparqlPackage.SELECT_QUERY__DATASET_CLAUSE:
        setDatasetClause((DatasetClause)null);
        return;
      case SparqlPackage.SELECT_QUERY__WHERE_CLAUSE:
        setWhereClause((WhereClause)null);
        return;
      case SparqlPackage.SELECT_QUERY__ORDER_CLAUSE:
        setOrderClause((OrderClause)null);
        return;
      case SparqlPackage.SELECT_QUERY__LIMIT_CLAUSE:
        setLimitClause(LIMIT_CLAUSE_EDEFAULT);
        return;
      case SparqlPackage.SELECT_QUERY__OFFSET_CLAUSE:
        setOffsetClause(OFFSET_CLAUSE_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SparqlPackage.SELECT_QUERY__PREFIX:
        return prefix != null && !prefix.isEmpty();
      case SparqlPackage.SELECT_QUERY__VAR:
        return var != null;
      case SparqlPackage.SELECT_QUERY__DATASET_CLAUSE:
        return datasetClause != null;
      case SparqlPackage.SELECT_QUERY__WHERE_CLAUSE:
        return whereClause != null;
      case SparqlPackage.SELECT_QUERY__ORDER_CLAUSE:
        return orderClause != null;
      case SparqlPackage.SELECT_QUERY__LIMIT_CLAUSE:
        return LIMIT_CLAUSE_EDEFAULT == null ? limitClause != null : !LIMIT_CLAUSE_EDEFAULT.equals(limitClause);
      case SparqlPackage.SELECT_QUERY__OFFSET_CLAUSE:
        return OFFSET_CLAUSE_EDEFAULT == null ? offsetClause != null : !OFFSET_CLAUSE_EDEFAULT.equals(offsetClause);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (limitClause: ");
    result.append(limitClause);
    result.append(", offsetClause: ");
    result.append(offsetClause);
    result.append(')');
    return result.toString();
  }

} //SelectQueryImpl
