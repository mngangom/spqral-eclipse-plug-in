/**
 */
package org.edu.le.spaqrl.sparql.impl;

import org.eclipse.emf.ecore.EClass;

import org.edu.le.spaqrl.sparql.SparqlPackage;
import org.edu.le.spaqrl.sparql.VAR2;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>VAR2</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class VAR2Impl extends VarImpl implements VAR2
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected VAR2Impl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SparqlPackage.Literals.VAR2;
  }

} //VAR2Impl
