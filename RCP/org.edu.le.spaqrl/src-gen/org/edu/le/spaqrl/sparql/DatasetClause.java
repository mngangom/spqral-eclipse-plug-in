/**
 */
package org.edu.le.spaqrl.sparql;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dataset Clause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.edu.le.spaqrl.sparql.DatasetClause#getDefaultGraphClause <em>Default Graph Clause</em>}</li>
 *   <li>{@link org.edu.le.spaqrl.sparql.DatasetClause#getNamedGraphClause <em>Named Graph Clause</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.edu.le.spaqrl.sparql.SparqlPackage#getDatasetClause()
 * @model
 * @generated
 */
public interface DatasetClause extends EObject
{
  /**
   * Returns the value of the '<em><b>Default Graph Clause</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Default Graph Clause</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Default Graph Clause</em>' containment reference.
   * @see #setDefaultGraphClause(DefaultGraphClause)
   * @see org.edu.le.spaqrl.sparql.SparqlPackage#getDatasetClause_DefaultGraphClause()
   * @model containment="true"
   * @generated
   */
  DefaultGraphClause getDefaultGraphClause();

  /**
   * Sets the value of the '{@link org.edu.le.spaqrl.sparql.DatasetClause#getDefaultGraphClause <em>Default Graph Clause</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Default Graph Clause</em>' containment reference.
   * @see #getDefaultGraphClause()
   * @generated
   */
  void setDefaultGraphClause(DefaultGraphClause value);

  /**
   * Returns the value of the '<em><b>Named Graph Clause</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Named Graph Clause</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Named Graph Clause</em>' containment reference.
   * @see #setNamedGraphClause(NamedGraphClause)
   * @see org.edu.le.spaqrl.sparql.SparqlPackage#getDatasetClause_NamedGraphClause()
   * @model containment="true"
   * @generated
   */
  NamedGraphClause getNamedGraphClause();

  /**
   * Sets the value of the '{@link org.edu.le.spaqrl.sparql.DatasetClause#getNamedGraphClause <em>Named Graph Clause</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Named Graph Clause</em>' containment reference.
   * @see #getNamedGraphClause()
   * @generated
   */
  void setNamedGraphClause(NamedGraphClause value);

} // DatasetClause
