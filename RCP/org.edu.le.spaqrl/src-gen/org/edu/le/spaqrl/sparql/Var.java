/**
 */
package org.edu.le.spaqrl.sparql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Var</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.edu.le.spaqrl.sparql.SparqlPackage#getVar()
 * @model
 * @generated
 */
public interface Var extends OrderCondition, Expression, GraphNode
{
} // Var
