/**
 */
package org.edu.le.spaqrl.sparql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>VAR2</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.edu.le.spaqrl.sparql.SparqlPackage#getVAR2()
 * @model
 * @generated
 */
public interface VAR2 extends Var
{
} // VAR2
