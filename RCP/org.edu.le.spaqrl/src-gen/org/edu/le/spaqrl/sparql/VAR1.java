/**
 */
package org.edu.le.spaqrl.sparql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>VAR1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.edu.le.spaqrl.sparql.VAR1#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.edu.le.spaqrl.sparql.SparqlPackage#getVAR1()
 * @model
 * @generated
 */
public interface VAR1 extends Var
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.edu.le.spaqrl.sparql.SparqlPackage#getVAR1_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.edu.le.spaqrl.sparql.VAR1#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

} // VAR1
