/**
 */
package org.edu.le.spaqrl.sparql;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pattern One</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.edu.le.spaqrl.sparql.PatternOne#getTriplesSameSubject <em>Triples Same Subject</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.edu.le.spaqrl.sparql.SparqlPackage#getPatternOne()
 * @model
 * @generated
 */
public interface PatternOne extends EObject
{
  /**
   * Returns the value of the '<em><b>Triples Same Subject</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Triples Same Subject</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Triples Same Subject</em>' containment reference.
   * @see #setTriplesSameSubject(TriplesSameSubject)
   * @see org.edu.le.spaqrl.sparql.SparqlPackage#getPatternOne_TriplesSameSubject()
   * @model containment="true"
   * @generated
   */
  TriplesSameSubject getTriplesSameSubject();

  /**
   * Sets the value of the '{@link org.edu.le.spaqrl.sparql.PatternOne#getTriplesSameSubject <em>Triples Same Subject</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Triples Same Subject</em>' containment reference.
   * @see #getTriplesSameSubject()
   * @generated
   */
  void setTriplesSameSubject(TriplesSameSubject value);

} // PatternOne
