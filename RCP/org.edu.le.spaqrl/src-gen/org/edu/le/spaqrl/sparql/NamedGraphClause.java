/**
 */
package org.edu.le.spaqrl.sparql;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Named Graph Clause</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.edu.le.spaqrl.sparql.SparqlPackage#getNamedGraphClause()
 * @model
 * @generated
 */
public interface NamedGraphClause extends EObject
{
} // NamedGraphClause
