/**
 */
package org.edu.le.spaqrl.sparql.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.edu.le.spaqrl.sparql.DefaultGraphClause;
import org.edu.le.spaqrl.sparql.SparqlPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Default Graph Clause</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.edu.le.spaqrl.sparql.impl.DefaultGraphClauseImpl#getIref <em>Iref</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DefaultGraphClauseImpl extends MinimalEObjectImpl.Container implements DefaultGraphClause
{
  /**
   * The default value of the '{@link #getIref() <em>Iref</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIref()
   * @generated
   * @ordered
   */
  protected static final String IREF_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getIref() <em>Iref</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIref()
   * @generated
   * @ordered
   */
  protected String iref = IREF_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DefaultGraphClauseImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SparqlPackage.Literals.DEFAULT_GRAPH_CLAUSE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getIref()
  {
    return iref;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIref(String newIref)
  {
    String oldIref = iref;
    iref = newIref;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SparqlPackage.DEFAULT_GRAPH_CLAUSE__IREF, oldIref, iref));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SparqlPackage.DEFAULT_GRAPH_CLAUSE__IREF:
        return getIref();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SparqlPackage.DEFAULT_GRAPH_CLAUSE__IREF:
        setIref((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SparqlPackage.DEFAULT_GRAPH_CLAUSE__IREF:
        setIref(IREF_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SparqlPackage.DEFAULT_GRAPH_CLAUSE__IREF:
        return IREF_EDEFAULT == null ? iref != null : !IREF_EDEFAULT.equals(iref);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (iref: ");
    result.append(iref);
    result.append(')');
    return result.toString();
  }

} //DefaultGraphClauseImpl
