/**
 */
package org.edu.le.spaqrl.sparql;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Select Query</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.edu.le.spaqrl.sparql.SelectQuery#getPrefix <em>Prefix</em>}</li>
 *   <li>{@link org.edu.le.spaqrl.sparql.SelectQuery#getVar <em>Var</em>}</li>
 *   <li>{@link org.edu.le.spaqrl.sparql.SelectQuery#getDatasetClause <em>Dataset Clause</em>}</li>
 *   <li>{@link org.edu.le.spaqrl.sparql.SelectQuery#getWhereClause <em>Where Clause</em>}</li>
 *   <li>{@link org.edu.le.spaqrl.sparql.SelectQuery#getOrderClause <em>Order Clause</em>}</li>
 *   <li>{@link org.edu.le.spaqrl.sparql.SelectQuery#getLimitClause <em>Limit Clause</em>}</li>
 *   <li>{@link org.edu.le.spaqrl.sparql.SelectQuery#getOffsetClause <em>Offset Clause</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.edu.le.spaqrl.sparql.SparqlPackage#getSelectQuery()
 * @model
 * @generated
 */
public interface SelectQuery extends EObject
{
  /**
   * Returns the value of the '<em><b>Prefix</b></em>' containment reference list.
   * The list contents are of type {@link org.edu.le.spaqrl.sparql.Prefix}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Prefix</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Prefix</em>' containment reference list.
   * @see org.edu.le.spaqrl.sparql.SparqlPackage#getSelectQuery_Prefix()
   * @model containment="true"
   * @generated
   */
  EList<Prefix> getPrefix();

  /**
   * Returns the value of the '<em><b>Var</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Var</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Var</em>' containment reference.
   * @see #setVar(Var)
   * @see org.edu.le.spaqrl.sparql.SparqlPackage#getSelectQuery_Var()
   * @model containment="true"
   * @generated
   */
  Var getVar();

  /**
   * Sets the value of the '{@link org.edu.le.spaqrl.sparql.SelectQuery#getVar <em>Var</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Var</em>' containment reference.
   * @see #getVar()
   * @generated
   */
  void setVar(Var value);

  /**
   * Returns the value of the '<em><b>Dataset Clause</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dataset Clause</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dataset Clause</em>' containment reference.
   * @see #setDatasetClause(DatasetClause)
   * @see org.edu.le.spaqrl.sparql.SparqlPackage#getSelectQuery_DatasetClause()
   * @model containment="true"
   * @generated
   */
  DatasetClause getDatasetClause();

  /**
   * Sets the value of the '{@link org.edu.le.spaqrl.sparql.SelectQuery#getDatasetClause <em>Dataset Clause</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dataset Clause</em>' containment reference.
   * @see #getDatasetClause()
   * @generated
   */
  void setDatasetClause(DatasetClause value);

  /**
   * Returns the value of the '<em><b>Where Clause</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Where Clause</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Where Clause</em>' containment reference.
   * @see #setWhereClause(WhereClause)
   * @see org.edu.le.spaqrl.sparql.SparqlPackage#getSelectQuery_WhereClause()
   * @model containment="true"
   * @generated
   */
  WhereClause getWhereClause();

  /**
   * Sets the value of the '{@link org.edu.le.spaqrl.sparql.SelectQuery#getWhereClause <em>Where Clause</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Where Clause</em>' containment reference.
   * @see #getWhereClause()
   * @generated
   */
  void setWhereClause(WhereClause value);

  /**
   * Returns the value of the '<em><b>Order Clause</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Order Clause</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Order Clause</em>' containment reference.
   * @see #setOrderClause(OrderClause)
   * @see org.edu.le.spaqrl.sparql.SparqlPackage#getSelectQuery_OrderClause()
   * @model containment="true"
   * @generated
   */
  OrderClause getOrderClause();

  /**
   * Sets the value of the '{@link org.edu.le.spaqrl.sparql.SelectQuery#getOrderClause <em>Order Clause</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Order Clause</em>' containment reference.
   * @see #getOrderClause()
   * @generated
   */
  void setOrderClause(OrderClause value);

  /**
   * Returns the value of the '<em><b>Limit Clause</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Limit Clause</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Limit Clause</em>' attribute.
   * @see #setLimitClause(String)
   * @see org.edu.le.spaqrl.sparql.SparqlPackage#getSelectQuery_LimitClause()
   * @model
   * @generated
   */
  String getLimitClause();

  /**
   * Sets the value of the '{@link org.edu.le.spaqrl.sparql.SelectQuery#getLimitClause <em>Limit Clause</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Limit Clause</em>' attribute.
   * @see #getLimitClause()
   * @generated
   */
  void setLimitClause(String value);

  /**
   * Returns the value of the '<em><b>Offset Clause</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Offset Clause</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Offset Clause</em>' attribute.
   * @see #setOffsetClause(String)
   * @see org.edu.le.spaqrl.sparql.SparqlPackage#getSelectQuery_OffsetClause()
   * @model
   * @generated
   */
  String getOffsetClause();

  /**
   * Sets the value of the '{@link org.edu.le.spaqrl.sparql.SelectQuery#getOffsetClause <em>Offset Clause</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Offset Clause</em>' attribute.
   * @see #getOffsetClause()
   * @generated
   */
  void setOffsetClause(String value);

} // SelectQuery
