/**
 */
package org.edu.le.spaqrl.sparql.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.edu.le.spaqrl.sparql.PatternOne;
import org.edu.le.spaqrl.sparql.SparqlPackage;
import org.edu.le.spaqrl.sparql.TriplesSameSubject;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Pattern One</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.edu.le.spaqrl.sparql.impl.PatternOneImpl#getTriplesSameSubject <em>Triples Same Subject</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PatternOneImpl extends MinimalEObjectImpl.Container implements PatternOne
{
  /**
   * The cached value of the '{@link #getTriplesSameSubject() <em>Triples Same Subject</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTriplesSameSubject()
   * @generated
   * @ordered
   */
  protected TriplesSameSubject triplesSameSubject;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PatternOneImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SparqlPackage.Literals.PATTERN_ONE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TriplesSameSubject getTriplesSameSubject()
  {
    return triplesSameSubject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTriplesSameSubject(TriplesSameSubject newTriplesSameSubject, NotificationChain msgs)
  {
    TriplesSameSubject oldTriplesSameSubject = triplesSameSubject;
    triplesSameSubject = newTriplesSameSubject;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SparqlPackage.PATTERN_ONE__TRIPLES_SAME_SUBJECT, oldTriplesSameSubject, newTriplesSameSubject);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTriplesSameSubject(TriplesSameSubject newTriplesSameSubject)
  {
    if (newTriplesSameSubject != triplesSameSubject)
    {
      NotificationChain msgs = null;
      if (triplesSameSubject != null)
        msgs = ((InternalEObject)triplesSameSubject).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SparqlPackage.PATTERN_ONE__TRIPLES_SAME_SUBJECT, null, msgs);
      if (newTriplesSameSubject != null)
        msgs = ((InternalEObject)newTriplesSameSubject).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SparqlPackage.PATTERN_ONE__TRIPLES_SAME_SUBJECT, null, msgs);
      msgs = basicSetTriplesSameSubject(newTriplesSameSubject, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SparqlPackage.PATTERN_ONE__TRIPLES_SAME_SUBJECT, newTriplesSameSubject, newTriplesSameSubject));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SparqlPackage.PATTERN_ONE__TRIPLES_SAME_SUBJECT:
        return basicSetTriplesSameSubject(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SparqlPackage.PATTERN_ONE__TRIPLES_SAME_SUBJECT:
        return getTriplesSameSubject();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SparqlPackage.PATTERN_ONE__TRIPLES_SAME_SUBJECT:
        setTriplesSameSubject((TriplesSameSubject)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SparqlPackage.PATTERN_ONE__TRIPLES_SAME_SUBJECT:
        setTriplesSameSubject((TriplesSameSubject)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SparqlPackage.PATTERN_ONE__TRIPLES_SAME_SUBJECT:
        return triplesSameSubject != null;
    }
    return super.eIsSet(featureID);
  }

} //PatternOneImpl
