/**
 */
package org.edu.le.spaqrl.sparql.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.edu.le.spaqrl.sparql.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.edu.le.spaqrl.sparql.SparqlPackage
 * @generated
 */
public class SparqlAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static SparqlPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SparqlAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = SparqlPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SparqlSwitch<Adapter> modelSwitch =
    new SparqlSwitch<Adapter>()
    {
      @Override
      public Adapter caseQuery(Query object)
      {
        return createQueryAdapter();
      }
      @Override
      public Adapter casePrefix(Prefix object)
      {
        return createPrefixAdapter();
      }
      @Override
      public Adapter caseUnNamedPrefix(UnNamedPrefix object)
      {
        return createUnNamedPrefixAdapter();
      }
      @Override
      public Adapter caseSelectQuery(SelectQuery object)
      {
        return createSelectQueryAdapter();
      }
      @Override
      public Adapter caseFilter(Filter object)
      {
        return createFilterAdapter();
      }
      @Override
      public Adapter caseExpressionFilter(ExpressionFilter object)
      {
        return createExpressionFilterAdapter();
      }
      @Override
      public Adapter caseOrderClause(OrderClause object)
      {
        return createOrderClauseAdapter();
      }
      @Override
      public Adapter caseOrderCondition(OrderCondition object)
      {
        return createOrderConditionAdapter();
      }
      @Override
      public Adapter caseBrackettedExpression(BrackettedExpression object)
      {
        return createBrackettedExpressionAdapter();
      }
      @Override
      public Adapter caseExpression(Expression object)
      {
        return createExpressionAdapter();
      }
      @Override
      public Adapter caseWhereClause(WhereClause object)
      {
        return createWhereClauseAdapter();
      }
      @Override
      public Adapter casePattern(Pattern object)
      {
        return createPatternAdapter();
      }
      @Override
      public Adapter casePatternOne(PatternOne object)
      {
        return createPatternOneAdapter();
      }
      @Override
      public Adapter caseTriplesSameSubject(TriplesSameSubject object)
      {
        return createTriplesSameSubjectAdapter();
      }
      @Override
      public Adapter casePropertyList(PropertyList object)
      {
        return createPropertyListAdapter();
      }
      @Override
      public Adapter caseGraphNode(GraphNode object)
      {
        return createGraphNodeAdapter();
      }
      @Override
      public Adapter caseQuote(Quote object)
      {
        return createQuoteAdapter();
      }
      @Override
      public Adapter caseParameter(Parameter object)
      {
        return createParameterAdapter();
      }
      @Override
      public Adapter caseBlankNode(BlankNode object)
      {
        return createBlankNodeAdapter();
      }
      @Override
      public Adapter caseValue(Value object)
      {
        return createValueAdapter();
      }
      @Override
      public Adapter caseDatasetClause(DatasetClause object)
      {
        return createDatasetClauseAdapter();
      }
      @Override
      public Adapter caseDefaultGraphClause(DefaultGraphClause object)
      {
        return createDefaultGraphClauseAdapter();
      }
      @Override
      public Adapter caseNamedGraphClause(NamedGraphClause object)
      {
        return createNamedGraphClauseAdapter();
      }
      @Override
      public Adapter caseSourceSelector(SourceSelector object)
      {
        return createSourceSelectorAdapter();
      }
      @Override
      public Adapter caseVar(Var object)
      {
        return createVarAdapter();
      }
      @Override
      public Adapter caseVAR1(VAR1 object)
      {
        return createVAR1Adapter();
      }
      @Override
      public Adapter caseVAR2(VAR2 object)
      {
        return createVAR2Adapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.Query <em>Query</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.Query
   * @generated
   */
  public Adapter createQueryAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.Prefix <em>Prefix</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.Prefix
   * @generated
   */
  public Adapter createPrefixAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.UnNamedPrefix <em>Un Named Prefix</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.UnNamedPrefix
   * @generated
   */
  public Adapter createUnNamedPrefixAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.SelectQuery <em>Select Query</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.SelectQuery
   * @generated
   */
  public Adapter createSelectQueryAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.Filter <em>Filter</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.Filter
   * @generated
   */
  public Adapter createFilterAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.ExpressionFilter <em>Expression Filter</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.ExpressionFilter
   * @generated
   */
  public Adapter createExpressionFilterAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.OrderClause <em>Order Clause</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.OrderClause
   * @generated
   */
  public Adapter createOrderClauseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.OrderCondition <em>Order Condition</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.OrderCondition
   * @generated
   */
  public Adapter createOrderConditionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.BrackettedExpression <em>Bracketted Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.BrackettedExpression
   * @generated
   */
  public Adapter createBrackettedExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.Expression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.Expression
   * @generated
   */
  public Adapter createExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.WhereClause <em>Where Clause</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.WhereClause
   * @generated
   */
  public Adapter createWhereClauseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.Pattern <em>Pattern</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.Pattern
   * @generated
   */
  public Adapter createPatternAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.PatternOne <em>Pattern One</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.PatternOne
   * @generated
   */
  public Adapter createPatternOneAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.TriplesSameSubject <em>Triples Same Subject</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.TriplesSameSubject
   * @generated
   */
  public Adapter createTriplesSameSubjectAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.PropertyList <em>Property List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.PropertyList
   * @generated
   */
  public Adapter createPropertyListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.GraphNode <em>Graph Node</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.GraphNode
   * @generated
   */
  public Adapter createGraphNodeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.Quote <em>Quote</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.Quote
   * @generated
   */
  public Adapter createQuoteAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.Parameter <em>Parameter</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.Parameter
   * @generated
   */
  public Adapter createParameterAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.BlankNode <em>Blank Node</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.BlankNode
   * @generated
   */
  public Adapter createBlankNodeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.Value <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.Value
   * @generated
   */
  public Adapter createValueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.DatasetClause <em>Dataset Clause</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.DatasetClause
   * @generated
   */
  public Adapter createDatasetClauseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.DefaultGraphClause <em>Default Graph Clause</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.DefaultGraphClause
   * @generated
   */
  public Adapter createDefaultGraphClauseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.NamedGraphClause <em>Named Graph Clause</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.NamedGraphClause
   * @generated
   */
  public Adapter createNamedGraphClauseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.SourceSelector <em>Source Selector</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.SourceSelector
   * @generated
   */
  public Adapter createSourceSelectorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.Var <em>Var</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.Var
   * @generated
   */
  public Adapter createVarAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.VAR1 <em>VAR1</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.VAR1
   * @generated
   */
  public Adapter createVAR1Adapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.edu.le.spaqrl.sparql.VAR2 <em>VAR2</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.edu.le.spaqrl.sparql.VAR2
   * @generated
   */
  public Adapter createVAR2Adapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //SparqlAdapterFactory
