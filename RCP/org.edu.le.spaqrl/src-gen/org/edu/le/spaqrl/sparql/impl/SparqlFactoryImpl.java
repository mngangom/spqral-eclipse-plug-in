/**
 */
package org.edu.le.spaqrl.sparql.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.edu.le.spaqrl.sparql.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SparqlFactoryImpl extends EFactoryImpl implements SparqlFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static SparqlFactory init()
  {
    try
    {
      SparqlFactory theSparqlFactory = (SparqlFactory)EPackage.Registry.INSTANCE.getEFactory(SparqlPackage.eNS_URI);
      if (theSparqlFactory != null)
      {
        return theSparqlFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new SparqlFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SparqlFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case SparqlPackage.QUERY: return createQuery();
      case SparqlPackage.PREFIX: return createPrefix();
      case SparqlPackage.UN_NAMED_PREFIX: return createUnNamedPrefix();
      case SparqlPackage.SELECT_QUERY: return createSelectQuery();
      case SparqlPackage.FILTER: return createFilter();
      case SparqlPackage.EXPRESSION_FILTER: return createExpressionFilter();
      case SparqlPackage.ORDER_CLAUSE: return createOrderClause();
      case SparqlPackage.ORDER_CONDITION: return createOrderCondition();
      case SparqlPackage.BRACKETTED_EXPRESSION: return createBrackettedExpression();
      case SparqlPackage.EXPRESSION: return createExpression();
      case SparqlPackage.WHERE_CLAUSE: return createWhereClause();
      case SparqlPackage.PATTERN: return createPattern();
      case SparqlPackage.PATTERN_ONE: return createPatternOne();
      case SparqlPackage.TRIPLES_SAME_SUBJECT: return createTriplesSameSubject();
      case SparqlPackage.PROPERTY_LIST: return createPropertyList();
      case SparqlPackage.GRAPH_NODE: return createGraphNode();
      case SparqlPackage.QUOTE: return createQuote();
      case SparqlPackage.PARAMETER: return createParameter();
      case SparqlPackage.BLANK_NODE: return createBlankNode();
      case SparqlPackage.VALUE: return createValue();
      case SparqlPackage.DATASET_CLAUSE: return createDatasetClause();
      case SparqlPackage.DEFAULT_GRAPH_CLAUSE: return createDefaultGraphClause();
      case SparqlPackage.NAMED_GRAPH_CLAUSE: return createNamedGraphClause();
      case SparqlPackage.SOURCE_SELECTOR: return createSourceSelector();
      case SparqlPackage.VAR: return createVar();
      case SparqlPackage.VAR1: return createVAR1();
      case SparqlPackage.VAR2: return createVAR2();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Query createQuery()
  {
    QueryImpl query = new QueryImpl();
    return query;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Prefix createPrefix()
  {
    PrefixImpl prefix = new PrefixImpl();
    return prefix;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnNamedPrefix createUnNamedPrefix()
  {
    UnNamedPrefixImpl unNamedPrefix = new UnNamedPrefixImpl();
    return unNamedPrefix;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SelectQuery createSelectQuery()
  {
    SelectQueryImpl selectQuery = new SelectQueryImpl();
    return selectQuery;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Filter createFilter()
  {
    FilterImpl filter = new FilterImpl();
    return filter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExpressionFilter createExpressionFilter()
  {
    ExpressionFilterImpl expressionFilter = new ExpressionFilterImpl();
    return expressionFilter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OrderClause createOrderClause()
  {
    OrderClauseImpl orderClause = new OrderClauseImpl();
    return orderClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OrderCondition createOrderCondition()
  {
    OrderConditionImpl orderCondition = new OrderConditionImpl();
    return orderCondition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BrackettedExpression createBrackettedExpression()
  {
    BrackettedExpressionImpl brackettedExpression = new BrackettedExpressionImpl();
    return brackettedExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression createExpression()
  {
    ExpressionImpl expression = new ExpressionImpl();
    return expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WhereClause createWhereClause()
  {
    WhereClauseImpl whereClause = new WhereClauseImpl();
    return whereClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Pattern createPattern()
  {
    PatternImpl pattern = new PatternImpl();
    return pattern;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PatternOne createPatternOne()
  {
    PatternOneImpl patternOne = new PatternOneImpl();
    return patternOne;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TriplesSameSubject createTriplesSameSubject()
  {
    TriplesSameSubjectImpl triplesSameSubject = new TriplesSameSubjectImpl();
    return triplesSameSubject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PropertyList createPropertyList()
  {
    PropertyListImpl propertyList = new PropertyListImpl();
    return propertyList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GraphNode createGraphNode()
  {
    GraphNodeImpl graphNode = new GraphNodeImpl();
    return graphNode;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Quote createQuote()
  {
    QuoteImpl quote = new QuoteImpl();
    return quote;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Parameter createParameter()
  {
    ParameterImpl parameter = new ParameterImpl();
    return parameter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BlankNode createBlankNode()
  {
    BlankNodeImpl blankNode = new BlankNodeImpl();
    return blankNode;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Value createValue()
  {
    ValueImpl value = new ValueImpl();
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DatasetClause createDatasetClause()
  {
    DatasetClauseImpl datasetClause = new DatasetClauseImpl();
    return datasetClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DefaultGraphClause createDefaultGraphClause()
  {
    DefaultGraphClauseImpl defaultGraphClause = new DefaultGraphClauseImpl();
    return defaultGraphClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NamedGraphClause createNamedGraphClause()
  {
    NamedGraphClauseImpl namedGraphClause = new NamedGraphClauseImpl();
    return namedGraphClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SourceSelector createSourceSelector()
  {
    SourceSelectorImpl sourceSelector = new SourceSelectorImpl();
    return sourceSelector;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Var createVar()
  {
    VarImpl var = new VarImpl();
    return var;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VAR1 createVAR1()
  {
    VAR1Impl var1 = new VAR1Impl();
    return var1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VAR2 createVAR2()
  {
    VAR2Impl var2 = new VAR2Impl();
    return var2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SparqlPackage getSparqlPackage()
  {
    return (SparqlPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static SparqlPackage getPackage()
  {
    return SparqlPackage.eINSTANCE;
  }

} //SparqlFactoryImpl
