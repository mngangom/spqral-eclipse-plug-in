/**
 */
package org.edu.le.spaqrl.sparql;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Query</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.edu.le.spaqrl.sparql.Query#getSelectQuery <em>Select Query</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.edu.le.spaqrl.sparql.SparqlPackage#getQuery()
 * @model
 * @generated
 */
public interface Query extends EObject
{
  /**
   * Returns the value of the '<em><b>Select Query</b></em>' containment reference list.
   * The list contents are of type {@link org.edu.le.spaqrl.sparql.SelectQuery}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Select Query</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Select Query</em>' containment reference list.
   * @see org.edu.le.spaqrl.sparql.SparqlPackage#getQuery_SelectQuery()
   * @model containment="true"
   * @generated
   */
  EList<SelectQuery> getSelectQuery();

} // Query
