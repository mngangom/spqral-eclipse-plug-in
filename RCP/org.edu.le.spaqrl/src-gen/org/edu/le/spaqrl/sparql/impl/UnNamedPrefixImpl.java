/**
 */
package org.edu.le.spaqrl.sparql.impl;

import org.eclipse.emf.ecore.EClass;

import org.edu.le.spaqrl.sparql.SparqlPackage;
import org.edu.le.spaqrl.sparql.UnNamedPrefix;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Un Named Prefix</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class UnNamedPrefixImpl extends PrefixImpl implements UnNamedPrefix
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected UnNamedPrefixImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SparqlPackage.Literals.UN_NAMED_PREFIX;
  }

} //UnNamedPrefixImpl
