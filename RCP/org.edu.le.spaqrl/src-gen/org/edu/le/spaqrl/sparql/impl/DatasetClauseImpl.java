/**
 */
package org.edu.le.spaqrl.sparql.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.edu.le.spaqrl.sparql.DatasetClause;
import org.edu.le.spaqrl.sparql.DefaultGraphClause;
import org.edu.le.spaqrl.sparql.NamedGraphClause;
import org.edu.le.spaqrl.sparql.SparqlPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dataset Clause</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.edu.le.spaqrl.sparql.impl.DatasetClauseImpl#getDefaultGraphClause <em>Default Graph Clause</em>}</li>
 *   <li>{@link org.edu.le.spaqrl.sparql.impl.DatasetClauseImpl#getNamedGraphClause <em>Named Graph Clause</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DatasetClauseImpl extends MinimalEObjectImpl.Container implements DatasetClause
{
  /**
   * The cached value of the '{@link #getDefaultGraphClause() <em>Default Graph Clause</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDefaultGraphClause()
   * @generated
   * @ordered
   */
  protected DefaultGraphClause defaultGraphClause;

  /**
   * The cached value of the '{@link #getNamedGraphClause() <em>Named Graph Clause</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNamedGraphClause()
   * @generated
   * @ordered
   */
  protected NamedGraphClause namedGraphClause;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DatasetClauseImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SparqlPackage.Literals.DATASET_CLAUSE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DefaultGraphClause getDefaultGraphClause()
  {
    return defaultGraphClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDefaultGraphClause(DefaultGraphClause newDefaultGraphClause, NotificationChain msgs)
  {
    DefaultGraphClause oldDefaultGraphClause = defaultGraphClause;
    defaultGraphClause = newDefaultGraphClause;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SparqlPackage.DATASET_CLAUSE__DEFAULT_GRAPH_CLAUSE, oldDefaultGraphClause, newDefaultGraphClause);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDefaultGraphClause(DefaultGraphClause newDefaultGraphClause)
  {
    if (newDefaultGraphClause != defaultGraphClause)
    {
      NotificationChain msgs = null;
      if (defaultGraphClause != null)
        msgs = ((InternalEObject)defaultGraphClause).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SparqlPackage.DATASET_CLAUSE__DEFAULT_GRAPH_CLAUSE, null, msgs);
      if (newDefaultGraphClause != null)
        msgs = ((InternalEObject)newDefaultGraphClause).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SparqlPackage.DATASET_CLAUSE__DEFAULT_GRAPH_CLAUSE, null, msgs);
      msgs = basicSetDefaultGraphClause(newDefaultGraphClause, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SparqlPackage.DATASET_CLAUSE__DEFAULT_GRAPH_CLAUSE, newDefaultGraphClause, newDefaultGraphClause));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NamedGraphClause getNamedGraphClause()
  {
    return namedGraphClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetNamedGraphClause(NamedGraphClause newNamedGraphClause, NotificationChain msgs)
  {
    NamedGraphClause oldNamedGraphClause = namedGraphClause;
    namedGraphClause = newNamedGraphClause;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SparqlPackage.DATASET_CLAUSE__NAMED_GRAPH_CLAUSE, oldNamedGraphClause, newNamedGraphClause);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNamedGraphClause(NamedGraphClause newNamedGraphClause)
  {
    if (newNamedGraphClause != namedGraphClause)
    {
      NotificationChain msgs = null;
      if (namedGraphClause != null)
        msgs = ((InternalEObject)namedGraphClause).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SparqlPackage.DATASET_CLAUSE__NAMED_GRAPH_CLAUSE, null, msgs);
      if (newNamedGraphClause != null)
        msgs = ((InternalEObject)newNamedGraphClause).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SparqlPackage.DATASET_CLAUSE__NAMED_GRAPH_CLAUSE, null, msgs);
      msgs = basicSetNamedGraphClause(newNamedGraphClause, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SparqlPackage.DATASET_CLAUSE__NAMED_GRAPH_CLAUSE, newNamedGraphClause, newNamedGraphClause));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SparqlPackage.DATASET_CLAUSE__DEFAULT_GRAPH_CLAUSE:
        return basicSetDefaultGraphClause(null, msgs);
      case SparqlPackage.DATASET_CLAUSE__NAMED_GRAPH_CLAUSE:
        return basicSetNamedGraphClause(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SparqlPackage.DATASET_CLAUSE__DEFAULT_GRAPH_CLAUSE:
        return getDefaultGraphClause();
      case SparqlPackage.DATASET_CLAUSE__NAMED_GRAPH_CLAUSE:
        return getNamedGraphClause();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SparqlPackage.DATASET_CLAUSE__DEFAULT_GRAPH_CLAUSE:
        setDefaultGraphClause((DefaultGraphClause)newValue);
        return;
      case SparqlPackage.DATASET_CLAUSE__NAMED_GRAPH_CLAUSE:
        setNamedGraphClause((NamedGraphClause)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SparqlPackage.DATASET_CLAUSE__DEFAULT_GRAPH_CLAUSE:
        setDefaultGraphClause((DefaultGraphClause)null);
        return;
      case SparqlPackage.DATASET_CLAUSE__NAMED_GRAPH_CLAUSE:
        setNamedGraphClause((NamedGraphClause)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SparqlPackage.DATASET_CLAUSE__DEFAULT_GRAPH_CLAUSE:
        return defaultGraphClause != null;
      case SparqlPackage.DATASET_CLAUSE__NAMED_GRAPH_CLAUSE:
        return namedGraphClause != null;
    }
    return super.eIsSet(featureID);
  }

} //DatasetClauseImpl
