/**
 */
package org.edu.le.spaqrl.sparql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Source Selector</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.edu.le.spaqrl.sparql.SourceSelector#getIref <em>Iref</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.edu.le.spaqrl.sparql.SparqlPackage#getSourceSelector()
 * @model
 * @generated
 */
public interface SourceSelector extends NamedGraphClause
{
  /**
   * Returns the value of the '<em><b>Iref</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Iref</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Iref</em>' attribute.
   * @see #setIref(String)
   * @see org.edu.le.spaqrl.sparql.SparqlPackage#getSourceSelector_Iref()
   * @model
   * @generated
   */
  String getIref();

  /**
   * Sets the value of the '{@link org.edu.le.spaqrl.sparql.SourceSelector#getIref <em>Iref</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Iref</em>' attribute.
   * @see #getIref()
   * @generated
   */
  void setIref(String value);

} // SourceSelector
