/**
 */
package org.edu.le.spaqrl.sparql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Un Named Prefix</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.edu.le.spaqrl.sparql.SparqlPackage#getUnNamedPrefix()
 * @model
 * @generated
 */
public interface UnNamedPrefix extends Prefix
{
} // UnNamedPrefix
