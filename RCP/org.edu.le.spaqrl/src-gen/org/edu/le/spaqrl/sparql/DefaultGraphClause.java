/**
 */
package org.edu.le.spaqrl.sparql;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Default Graph Clause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.edu.le.spaqrl.sparql.DefaultGraphClause#getIref <em>Iref</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.edu.le.spaqrl.sparql.SparqlPackage#getDefaultGraphClause()
 * @model
 * @generated
 */
public interface DefaultGraphClause extends EObject
{
  /**
   * Returns the value of the '<em><b>Iref</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Iref</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Iref</em>' attribute.
   * @see #setIref(String)
   * @see org.edu.le.spaqrl.sparql.SparqlPackage#getDefaultGraphClause_Iref()
   * @model
   * @generated
   */
  String getIref();

  /**
   * Sets the value of the '{@link org.edu.le.spaqrl.sparql.DefaultGraphClause#getIref <em>Iref</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Iref</em>' attribute.
   * @see #getIref()
   * @generated
   */
  void setIref(String value);

} // DefaultGraphClause
