/**
 */
package org.edu.le.spaqrl.sparql;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Graph Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.edu.le.spaqrl.sparql.SparqlPackage#getGraphNode()
 * @model
 * @generated
 */
public interface GraphNode extends EObject
{
} // GraphNode
