/**
 */
package org.edu.le.spaqrl.sparql;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.edu.le.spaqrl.sparql.SparqlFactory
 * @model kind="package"
 * @generated
 */
public interface SparqlPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "sparql";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.edu.org/le/spaqrl/Sparql";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "sparql";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  SparqlPackage eINSTANCE = org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl.init();

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.QueryImpl <em>Query</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.QueryImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getQuery()
   * @generated
   */
  int QUERY = 0;

  /**
   * The feature id for the '<em><b>Select Query</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUERY__SELECT_QUERY = 0;

  /**
   * The number of structural features of the '<em>Query</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUERY_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.OrderConditionImpl <em>Order Condition</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.OrderConditionImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getOrderCondition()
   * @generated
   */
  int ORDER_CONDITION = 7;

  /**
   * The feature id for the '<em><b>Bracketted Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORDER_CONDITION__BRACKETTED_EXPRESSION = 0;

  /**
   * The number of structural features of the '<em>Order Condition</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORDER_CONDITION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.VarImpl <em>Var</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.VarImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getVar()
   * @generated
   */
  int VAR = 24;

  /**
   * The feature id for the '<em><b>Bracketted Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR__BRACKETTED_EXPRESSION = ORDER_CONDITION__BRACKETTED_EXPRESSION;

  /**
   * The number of structural features of the '<em>Var</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_FEATURE_COUNT = ORDER_CONDITION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.VAR2Impl <em>VAR2</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.VAR2Impl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getVAR2()
   * @generated
   */
  int VAR2 = 26;

  /**
   * The feature id for the '<em><b>Bracketted Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR2__BRACKETTED_EXPRESSION = VAR__BRACKETTED_EXPRESSION;

  /**
   * The number of structural features of the '<em>VAR2</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR2_FEATURE_COUNT = VAR_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.PrefixImpl <em>Prefix</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.PrefixImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getPrefix()
   * @generated
   */
  int PREFIX = 1;

  /**
   * The feature id for the '<em><b>Bracketted Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PREFIX__BRACKETTED_EXPRESSION = VAR2__BRACKETTED_EXPRESSION;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PREFIX__NAME = VAR2_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Iref</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PREFIX__IREF = VAR2_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Prefix</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PREFIX_FEATURE_COUNT = VAR2_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.UnNamedPrefixImpl <em>Un Named Prefix</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.UnNamedPrefixImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getUnNamedPrefix()
   * @generated
   */
  int UN_NAMED_PREFIX = 2;

  /**
   * The feature id for the '<em><b>Bracketted Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UN_NAMED_PREFIX__BRACKETTED_EXPRESSION = PREFIX__BRACKETTED_EXPRESSION;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UN_NAMED_PREFIX__NAME = PREFIX__NAME;

  /**
   * The feature id for the '<em><b>Iref</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UN_NAMED_PREFIX__IREF = PREFIX__IREF;

  /**
   * The number of structural features of the '<em>Un Named Prefix</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UN_NAMED_PREFIX_FEATURE_COUNT = PREFIX_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.SelectQueryImpl <em>Select Query</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.SelectQueryImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getSelectQuery()
   * @generated
   */
  int SELECT_QUERY = 3;

  /**
   * The feature id for the '<em><b>Prefix</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_QUERY__PREFIX = 0;

  /**
   * The feature id for the '<em><b>Var</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_QUERY__VAR = 1;

  /**
   * The feature id for the '<em><b>Dataset Clause</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_QUERY__DATASET_CLAUSE = 2;

  /**
   * The feature id for the '<em><b>Where Clause</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_QUERY__WHERE_CLAUSE = 3;

  /**
   * The feature id for the '<em><b>Order Clause</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_QUERY__ORDER_CLAUSE = 4;

  /**
   * The feature id for the '<em><b>Limit Clause</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_QUERY__LIMIT_CLAUSE = 5;

  /**
   * The feature id for the '<em><b>Offset Clause</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_QUERY__OFFSET_CLAUSE = 6;

  /**
   * The number of structural features of the '<em>Select Query</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_QUERY_FEATURE_COUNT = 7;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.FilterImpl <em>Filter</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.FilterImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getFilter()
   * @generated
   */
  int FILTER = 4;

  /**
   * The feature id for the '<em><b>Expression Filter</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FILTER__EXPRESSION_FILTER = 0;

  /**
   * The number of structural features of the '<em>Filter</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FILTER_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.ExpressionFilterImpl <em>Expression Filter</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.ExpressionFilterImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getExpressionFilter()
   * @generated
   */
  int EXPRESSION_FILTER = 5;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_FILTER__NAME = 0;

  /**
   * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_FILTER__PARAMETERS = 1;

  /**
   * The number of structural features of the '<em>Expression Filter</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_FILTER_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.OrderClauseImpl <em>Order Clause</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.OrderClauseImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getOrderClause()
   * @generated
   */
  int ORDER_CLAUSE = 6;

  /**
   * The feature id for the '<em><b>Order Condition</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORDER_CLAUSE__ORDER_CONDITION = 0;

  /**
   * The number of structural features of the '<em>Order Clause</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORDER_CLAUSE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.BrackettedExpressionImpl <em>Bracketted Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.BrackettedExpressionImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getBrackettedExpression()
   * @generated
   */
  int BRACKETTED_EXPRESSION = 8;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BRACKETTED_EXPRESSION__EXPRESSION = 0;

  /**
   * The number of structural features of the '<em>Bracketted Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BRACKETTED_EXPRESSION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.ExpressionImpl <em>Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.ExpressionImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getExpression()
   * @generated
   */
  int EXPRESSION = 9;

  /**
   * The number of structural features of the '<em>Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.WhereClauseImpl <em>Where Clause</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.WhereClauseImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getWhereClause()
   * @generated
   */
  int WHERE_CLAUSE = 10;

  /**
   * The feature id for the '<em><b>Pattern</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHERE_CLAUSE__PATTERN = 0;

  /**
   * The feature id for the '<em><b>Filter</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHERE_CLAUSE__FILTER = 1;

  /**
   * The number of structural features of the '<em>Where Clause</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHERE_CLAUSE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.PatternImpl <em>Pattern</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.PatternImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getPattern()
   * @generated
   */
  int PATTERN = 11;

  /**
   * The feature id for the '<em><b>Patterns</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATTERN__PATTERNS = 0;

  /**
   * The number of structural features of the '<em>Pattern</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATTERN_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.PatternOneImpl <em>Pattern One</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.PatternOneImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getPatternOne()
   * @generated
   */
  int PATTERN_ONE = 12;

  /**
   * The feature id for the '<em><b>Triples Same Subject</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATTERN_ONE__TRIPLES_SAME_SUBJECT = 0;

  /**
   * The number of structural features of the '<em>Pattern One</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATTERN_ONE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.TriplesSameSubjectImpl <em>Triples Same Subject</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.TriplesSameSubjectImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getTriplesSameSubject()
   * @generated
   */
  int TRIPLES_SAME_SUBJECT = 13;

  /**
   * The feature id for the '<em><b>Subject</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRIPLES_SAME_SUBJECT__SUBJECT = 0;

  /**
   * The feature id for the '<em><b>Property List</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRIPLES_SAME_SUBJECT__PROPERTY_LIST = 1;

  /**
   * The number of structural features of the '<em>Triples Same Subject</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRIPLES_SAME_SUBJECT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.PropertyListImpl <em>Property List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.PropertyListImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getPropertyList()
   * @generated
   */
  int PROPERTY_LIST = 14;

  /**
   * The feature id for the '<em><b>Property</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_LIST__PROPERTY = 0;

  /**
   * The feature id for the '<em><b>Object</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_LIST__OBJECT = 1;

  /**
   * The number of structural features of the '<em>Property List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_LIST_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.GraphNodeImpl <em>Graph Node</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.GraphNodeImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getGraphNode()
   * @generated
   */
  int GRAPH_NODE = 15;

  /**
   * The number of structural features of the '<em>Graph Node</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GRAPH_NODE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.QuoteImpl <em>Quote</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.QuoteImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getQuote()
   * @generated
   */
  int QUOTE = 16;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUOTE__NAME = GRAPH_NODE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Quote</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUOTE_FEATURE_COUNT = GRAPH_NODE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.ParameterImpl <em>Parameter</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.ParameterImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getParameter()
   * @generated
   */
  int PARAMETER = 17;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER__NAME = GRAPH_NODE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Parameter</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER_FEATURE_COUNT = GRAPH_NODE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.BlankNodeImpl <em>Blank Node</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.BlankNodeImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getBlankNode()
   * @generated
   */
  int BLANK_NODE = 18;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BLANK_NODE__NAME = GRAPH_NODE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Blank Node</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BLANK_NODE_FEATURE_COUNT = GRAPH_NODE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.ValueImpl <em>Value</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.ValueImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getValue()
   * @generated
   */
  int VALUE = 19;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE__NAME = GRAPH_NODE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>String Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE__STRING_VALUE = GRAPH_NODE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Integer Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE__INTEGER_VALUE = GRAPH_NODE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_FEATURE_COUNT = GRAPH_NODE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.DatasetClauseImpl <em>Dataset Clause</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.DatasetClauseImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getDatasetClause()
   * @generated
   */
  int DATASET_CLAUSE = 20;

  /**
   * The feature id for the '<em><b>Default Graph Clause</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATASET_CLAUSE__DEFAULT_GRAPH_CLAUSE = 0;

  /**
   * The feature id for the '<em><b>Named Graph Clause</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATASET_CLAUSE__NAMED_GRAPH_CLAUSE = 1;

  /**
   * The number of structural features of the '<em>Dataset Clause</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATASET_CLAUSE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.DefaultGraphClauseImpl <em>Default Graph Clause</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.DefaultGraphClauseImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getDefaultGraphClause()
   * @generated
   */
  int DEFAULT_GRAPH_CLAUSE = 21;

  /**
   * The feature id for the '<em><b>Iref</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFAULT_GRAPH_CLAUSE__IREF = 0;

  /**
   * The number of structural features of the '<em>Default Graph Clause</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFAULT_GRAPH_CLAUSE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.NamedGraphClauseImpl <em>Named Graph Clause</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.NamedGraphClauseImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getNamedGraphClause()
   * @generated
   */
  int NAMED_GRAPH_CLAUSE = 22;

  /**
   * The number of structural features of the '<em>Named Graph Clause</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAMED_GRAPH_CLAUSE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.SourceSelectorImpl <em>Source Selector</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.SourceSelectorImpl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getSourceSelector()
   * @generated
   */
  int SOURCE_SELECTOR = 23;

  /**
   * The feature id for the '<em><b>Iref</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE_SELECTOR__IREF = NAMED_GRAPH_CLAUSE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Source Selector</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE_SELECTOR_FEATURE_COUNT = NAMED_GRAPH_CLAUSE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.edu.le.spaqrl.sparql.impl.VAR1Impl <em>VAR1</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.edu.le.spaqrl.sparql.impl.VAR1Impl
   * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getVAR1()
   * @generated
   */
  int VAR1 = 25;

  /**
   * The feature id for the '<em><b>Bracketted Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR1__BRACKETTED_EXPRESSION = VAR__BRACKETTED_EXPRESSION;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR1__NAME = VAR_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>VAR1</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR1_FEATURE_COUNT = VAR_FEATURE_COUNT + 1;


  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.Query <em>Query</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Query</em>'.
   * @see org.edu.le.spaqrl.sparql.Query
   * @generated
   */
  EClass getQuery();

  /**
   * Returns the meta object for the containment reference list '{@link org.edu.le.spaqrl.sparql.Query#getSelectQuery <em>Select Query</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Select Query</em>'.
   * @see org.edu.le.spaqrl.sparql.Query#getSelectQuery()
   * @see #getQuery()
   * @generated
   */
  EReference getQuery_SelectQuery();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.Prefix <em>Prefix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Prefix</em>'.
   * @see org.edu.le.spaqrl.sparql.Prefix
   * @generated
   */
  EClass getPrefix();

  /**
   * Returns the meta object for the attribute '{@link org.edu.le.spaqrl.sparql.Prefix#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.edu.le.spaqrl.sparql.Prefix#getName()
   * @see #getPrefix()
   * @generated
   */
  EAttribute getPrefix_Name();

  /**
   * Returns the meta object for the attribute '{@link org.edu.le.spaqrl.sparql.Prefix#getIref <em>Iref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Iref</em>'.
   * @see org.edu.le.spaqrl.sparql.Prefix#getIref()
   * @see #getPrefix()
   * @generated
   */
  EAttribute getPrefix_Iref();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.UnNamedPrefix <em>Un Named Prefix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Un Named Prefix</em>'.
   * @see org.edu.le.spaqrl.sparql.UnNamedPrefix
   * @generated
   */
  EClass getUnNamedPrefix();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.SelectQuery <em>Select Query</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Select Query</em>'.
   * @see org.edu.le.spaqrl.sparql.SelectQuery
   * @generated
   */
  EClass getSelectQuery();

  /**
   * Returns the meta object for the containment reference list '{@link org.edu.le.spaqrl.sparql.SelectQuery#getPrefix <em>Prefix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Prefix</em>'.
   * @see org.edu.le.spaqrl.sparql.SelectQuery#getPrefix()
   * @see #getSelectQuery()
   * @generated
   */
  EReference getSelectQuery_Prefix();

  /**
   * Returns the meta object for the containment reference '{@link org.edu.le.spaqrl.sparql.SelectQuery#getVar <em>Var</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Var</em>'.
   * @see org.edu.le.spaqrl.sparql.SelectQuery#getVar()
   * @see #getSelectQuery()
   * @generated
   */
  EReference getSelectQuery_Var();

  /**
   * Returns the meta object for the containment reference '{@link org.edu.le.spaqrl.sparql.SelectQuery#getDatasetClause <em>Dataset Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Dataset Clause</em>'.
   * @see org.edu.le.spaqrl.sparql.SelectQuery#getDatasetClause()
   * @see #getSelectQuery()
   * @generated
   */
  EReference getSelectQuery_DatasetClause();

  /**
   * Returns the meta object for the containment reference '{@link org.edu.le.spaqrl.sparql.SelectQuery#getWhereClause <em>Where Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Where Clause</em>'.
   * @see org.edu.le.spaqrl.sparql.SelectQuery#getWhereClause()
   * @see #getSelectQuery()
   * @generated
   */
  EReference getSelectQuery_WhereClause();

  /**
   * Returns the meta object for the containment reference '{@link org.edu.le.spaqrl.sparql.SelectQuery#getOrderClause <em>Order Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Order Clause</em>'.
   * @see org.edu.le.spaqrl.sparql.SelectQuery#getOrderClause()
   * @see #getSelectQuery()
   * @generated
   */
  EReference getSelectQuery_OrderClause();

  /**
   * Returns the meta object for the attribute '{@link org.edu.le.spaqrl.sparql.SelectQuery#getLimitClause <em>Limit Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Limit Clause</em>'.
   * @see org.edu.le.spaqrl.sparql.SelectQuery#getLimitClause()
   * @see #getSelectQuery()
   * @generated
   */
  EAttribute getSelectQuery_LimitClause();

  /**
   * Returns the meta object for the attribute '{@link org.edu.le.spaqrl.sparql.SelectQuery#getOffsetClause <em>Offset Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Offset Clause</em>'.
   * @see org.edu.le.spaqrl.sparql.SelectQuery#getOffsetClause()
   * @see #getSelectQuery()
   * @generated
   */
  EAttribute getSelectQuery_OffsetClause();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.Filter <em>Filter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Filter</em>'.
   * @see org.edu.le.spaqrl.sparql.Filter
   * @generated
   */
  EClass getFilter();

  /**
   * Returns the meta object for the containment reference '{@link org.edu.le.spaqrl.sparql.Filter#getExpressionFilter <em>Expression Filter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression Filter</em>'.
   * @see org.edu.le.spaqrl.sparql.Filter#getExpressionFilter()
   * @see #getFilter()
   * @generated
   */
  EReference getFilter_ExpressionFilter();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.ExpressionFilter <em>Expression Filter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression Filter</em>'.
   * @see org.edu.le.spaqrl.sparql.ExpressionFilter
   * @generated
   */
  EClass getExpressionFilter();

  /**
   * Returns the meta object for the attribute '{@link org.edu.le.spaqrl.sparql.ExpressionFilter#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.edu.le.spaqrl.sparql.ExpressionFilter#getName()
   * @see #getExpressionFilter()
   * @generated
   */
  EAttribute getExpressionFilter_Name();

  /**
   * Returns the meta object for the containment reference list '{@link org.edu.le.spaqrl.sparql.ExpressionFilter#getParameters <em>Parameters</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Parameters</em>'.
   * @see org.edu.le.spaqrl.sparql.ExpressionFilter#getParameters()
   * @see #getExpressionFilter()
   * @generated
   */
  EReference getExpressionFilter_Parameters();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.OrderClause <em>Order Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Order Clause</em>'.
   * @see org.edu.le.spaqrl.sparql.OrderClause
   * @generated
   */
  EClass getOrderClause();

  /**
   * Returns the meta object for the containment reference '{@link org.edu.le.spaqrl.sparql.OrderClause#getOrderCondition <em>Order Condition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Order Condition</em>'.
   * @see org.edu.le.spaqrl.sparql.OrderClause#getOrderCondition()
   * @see #getOrderClause()
   * @generated
   */
  EReference getOrderClause_OrderCondition();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.OrderCondition <em>Order Condition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Order Condition</em>'.
   * @see org.edu.le.spaqrl.sparql.OrderCondition
   * @generated
   */
  EClass getOrderCondition();

  /**
   * Returns the meta object for the containment reference '{@link org.edu.le.spaqrl.sparql.OrderCondition#getBrackettedExpression <em>Bracketted Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Bracketted Expression</em>'.
   * @see org.edu.le.spaqrl.sparql.OrderCondition#getBrackettedExpression()
   * @see #getOrderCondition()
   * @generated
   */
  EReference getOrderCondition_BrackettedExpression();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.BrackettedExpression <em>Bracketted Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Bracketted Expression</em>'.
   * @see org.edu.le.spaqrl.sparql.BrackettedExpression
   * @generated
   */
  EClass getBrackettedExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.edu.le.spaqrl.sparql.BrackettedExpression#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see org.edu.le.spaqrl.sparql.BrackettedExpression#getExpression()
   * @see #getBrackettedExpression()
   * @generated
   */
  EReference getBrackettedExpression_Expression();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.Expression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression</em>'.
   * @see org.edu.le.spaqrl.sparql.Expression
   * @generated
   */
  EClass getExpression();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.WhereClause <em>Where Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Where Clause</em>'.
   * @see org.edu.le.spaqrl.sparql.WhereClause
   * @generated
   */
  EClass getWhereClause();

  /**
   * Returns the meta object for the containment reference '{@link org.edu.le.spaqrl.sparql.WhereClause#getPattern <em>Pattern</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Pattern</em>'.
   * @see org.edu.le.spaqrl.sparql.WhereClause#getPattern()
   * @see #getWhereClause()
   * @generated
   */
  EReference getWhereClause_Pattern();

  /**
   * Returns the meta object for the containment reference '{@link org.edu.le.spaqrl.sparql.WhereClause#getFilter <em>Filter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Filter</em>'.
   * @see org.edu.le.spaqrl.sparql.WhereClause#getFilter()
   * @see #getWhereClause()
   * @generated
   */
  EReference getWhereClause_Filter();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.Pattern <em>Pattern</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Pattern</em>'.
   * @see org.edu.le.spaqrl.sparql.Pattern
   * @generated
   */
  EClass getPattern();

  /**
   * Returns the meta object for the containment reference list '{@link org.edu.le.spaqrl.sparql.Pattern#getPatterns <em>Patterns</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Patterns</em>'.
   * @see org.edu.le.spaqrl.sparql.Pattern#getPatterns()
   * @see #getPattern()
   * @generated
   */
  EReference getPattern_Patterns();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.PatternOne <em>Pattern One</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Pattern One</em>'.
   * @see org.edu.le.spaqrl.sparql.PatternOne
   * @generated
   */
  EClass getPatternOne();

  /**
   * Returns the meta object for the containment reference '{@link org.edu.le.spaqrl.sparql.PatternOne#getTriplesSameSubject <em>Triples Same Subject</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Triples Same Subject</em>'.
   * @see org.edu.le.spaqrl.sparql.PatternOne#getTriplesSameSubject()
   * @see #getPatternOne()
   * @generated
   */
  EReference getPatternOne_TriplesSameSubject();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.TriplesSameSubject <em>Triples Same Subject</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Triples Same Subject</em>'.
   * @see org.edu.le.spaqrl.sparql.TriplesSameSubject
   * @generated
   */
  EClass getTriplesSameSubject();

  /**
   * Returns the meta object for the containment reference '{@link org.edu.le.spaqrl.sparql.TriplesSameSubject#getSubject <em>Subject</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Subject</em>'.
   * @see org.edu.le.spaqrl.sparql.TriplesSameSubject#getSubject()
   * @see #getTriplesSameSubject()
   * @generated
   */
  EReference getTriplesSameSubject_Subject();

  /**
   * Returns the meta object for the containment reference list '{@link org.edu.le.spaqrl.sparql.TriplesSameSubject#getPropertyList <em>Property List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Property List</em>'.
   * @see org.edu.le.spaqrl.sparql.TriplesSameSubject#getPropertyList()
   * @see #getTriplesSameSubject()
   * @generated
   */
  EReference getTriplesSameSubject_PropertyList();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.PropertyList <em>Property List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Property List</em>'.
   * @see org.edu.le.spaqrl.sparql.PropertyList
   * @generated
   */
  EClass getPropertyList();

  /**
   * Returns the meta object for the containment reference '{@link org.edu.le.spaqrl.sparql.PropertyList#getProperty <em>Property</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Property</em>'.
   * @see org.edu.le.spaqrl.sparql.PropertyList#getProperty()
   * @see #getPropertyList()
   * @generated
   */
  EReference getPropertyList_Property();

  /**
   * Returns the meta object for the containment reference '{@link org.edu.le.spaqrl.sparql.PropertyList#getObject <em>Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Object</em>'.
   * @see org.edu.le.spaqrl.sparql.PropertyList#getObject()
   * @see #getPropertyList()
   * @generated
   */
  EReference getPropertyList_Object();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.GraphNode <em>Graph Node</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Graph Node</em>'.
   * @see org.edu.le.spaqrl.sparql.GraphNode
   * @generated
   */
  EClass getGraphNode();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.Quote <em>Quote</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Quote</em>'.
   * @see org.edu.le.spaqrl.sparql.Quote
   * @generated
   */
  EClass getQuote();

  /**
   * Returns the meta object for the attribute '{@link org.edu.le.spaqrl.sparql.Quote#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.edu.le.spaqrl.sparql.Quote#getName()
   * @see #getQuote()
   * @generated
   */
  EAttribute getQuote_Name();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.Parameter <em>Parameter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Parameter</em>'.
   * @see org.edu.le.spaqrl.sparql.Parameter
   * @generated
   */
  EClass getParameter();

  /**
   * Returns the meta object for the attribute '{@link org.edu.le.spaqrl.sparql.Parameter#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.edu.le.spaqrl.sparql.Parameter#getName()
   * @see #getParameter()
   * @generated
   */
  EAttribute getParameter_Name();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.BlankNode <em>Blank Node</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Blank Node</em>'.
   * @see org.edu.le.spaqrl.sparql.BlankNode
   * @generated
   */
  EClass getBlankNode();

  /**
   * Returns the meta object for the attribute '{@link org.edu.le.spaqrl.sparql.BlankNode#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.edu.le.spaqrl.sparql.BlankNode#getName()
   * @see #getBlankNode()
   * @generated
   */
  EAttribute getBlankNode_Name();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.Value <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Value</em>'.
   * @see org.edu.le.spaqrl.sparql.Value
   * @generated
   */
  EClass getValue();

  /**
   * Returns the meta object for the attribute '{@link org.edu.le.spaqrl.sparql.Value#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.edu.le.spaqrl.sparql.Value#getName()
   * @see #getValue()
   * @generated
   */
  EAttribute getValue_Name();

  /**
   * Returns the meta object for the attribute '{@link org.edu.le.spaqrl.sparql.Value#getStringValue <em>String Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>String Value</em>'.
   * @see org.edu.le.spaqrl.sparql.Value#getStringValue()
   * @see #getValue()
   * @generated
   */
  EAttribute getValue_StringValue();

  /**
   * Returns the meta object for the attribute '{@link org.edu.le.spaqrl.sparql.Value#getIntegerValue <em>Integer Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Integer Value</em>'.
   * @see org.edu.le.spaqrl.sparql.Value#getIntegerValue()
   * @see #getValue()
   * @generated
   */
  EAttribute getValue_IntegerValue();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.DatasetClause <em>Dataset Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Dataset Clause</em>'.
   * @see org.edu.le.spaqrl.sparql.DatasetClause
   * @generated
   */
  EClass getDatasetClause();

  /**
   * Returns the meta object for the containment reference '{@link org.edu.le.spaqrl.sparql.DatasetClause#getDefaultGraphClause <em>Default Graph Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Default Graph Clause</em>'.
   * @see org.edu.le.spaqrl.sparql.DatasetClause#getDefaultGraphClause()
   * @see #getDatasetClause()
   * @generated
   */
  EReference getDatasetClause_DefaultGraphClause();

  /**
   * Returns the meta object for the containment reference '{@link org.edu.le.spaqrl.sparql.DatasetClause#getNamedGraphClause <em>Named Graph Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Named Graph Clause</em>'.
   * @see org.edu.le.spaqrl.sparql.DatasetClause#getNamedGraphClause()
   * @see #getDatasetClause()
   * @generated
   */
  EReference getDatasetClause_NamedGraphClause();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.DefaultGraphClause <em>Default Graph Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Default Graph Clause</em>'.
   * @see org.edu.le.spaqrl.sparql.DefaultGraphClause
   * @generated
   */
  EClass getDefaultGraphClause();

  /**
   * Returns the meta object for the attribute '{@link org.edu.le.spaqrl.sparql.DefaultGraphClause#getIref <em>Iref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Iref</em>'.
   * @see org.edu.le.spaqrl.sparql.DefaultGraphClause#getIref()
   * @see #getDefaultGraphClause()
   * @generated
   */
  EAttribute getDefaultGraphClause_Iref();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.NamedGraphClause <em>Named Graph Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Named Graph Clause</em>'.
   * @see org.edu.le.spaqrl.sparql.NamedGraphClause
   * @generated
   */
  EClass getNamedGraphClause();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.SourceSelector <em>Source Selector</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Source Selector</em>'.
   * @see org.edu.le.spaqrl.sparql.SourceSelector
   * @generated
   */
  EClass getSourceSelector();

  /**
   * Returns the meta object for the attribute '{@link org.edu.le.spaqrl.sparql.SourceSelector#getIref <em>Iref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Iref</em>'.
   * @see org.edu.le.spaqrl.sparql.SourceSelector#getIref()
   * @see #getSourceSelector()
   * @generated
   */
  EAttribute getSourceSelector_Iref();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.Var <em>Var</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Var</em>'.
   * @see org.edu.le.spaqrl.sparql.Var
   * @generated
   */
  EClass getVar();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.VAR1 <em>VAR1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>VAR1</em>'.
   * @see org.edu.le.spaqrl.sparql.VAR1
   * @generated
   */
  EClass getVAR1();

  /**
   * Returns the meta object for the attribute '{@link org.edu.le.spaqrl.sparql.VAR1#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.edu.le.spaqrl.sparql.VAR1#getName()
   * @see #getVAR1()
   * @generated
   */
  EAttribute getVAR1_Name();

  /**
   * Returns the meta object for class '{@link org.edu.le.spaqrl.sparql.VAR2 <em>VAR2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>VAR2</em>'.
   * @see org.edu.le.spaqrl.sparql.VAR2
   * @generated
   */
  EClass getVAR2();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  SparqlFactory getSparqlFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.QueryImpl <em>Query</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.QueryImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getQuery()
     * @generated
     */
    EClass QUERY = eINSTANCE.getQuery();

    /**
     * The meta object literal for the '<em><b>Select Query</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference QUERY__SELECT_QUERY = eINSTANCE.getQuery_SelectQuery();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.PrefixImpl <em>Prefix</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.PrefixImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getPrefix()
     * @generated
     */
    EClass PREFIX = eINSTANCE.getPrefix();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PREFIX__NAME = eINSTANCE.getPrefix_Name();

    /**
     * The meta object literal for the '<em><b>Iref</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PREFIX__IREF = eINSTANCE.getPrefix_Iref();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.UnNamedPrefixImpl <em>Un Named Prefix</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.UnNamedPrefixImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getUnNamedPrefix()
     * @generated
     */
    EClass UN_NAMED_PREFIX = eINSTANCE.getUnNamedPrefix();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.SelectQueryImpl <em>Select Query</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.SelectQueryImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getSelectQuery()
     * @generated
     */
    EClass SELECT_QUERY = eINSTANCE.getSelectQuery();

    /**
     * The meta object literal for the '<em><b>Prefix</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SELECT_QUERY__PREFIX = eINSTANCE.getSelectQuery_Prefix();

    /**
     * The meta object literal for the '<em><b>Var</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SELECT_QUERY__VAR = eINSTANCE.getSelectQuery_Var();

    /**
     * The meta object literal for the '<em><b>Dataset Clause</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SELECT_QUERY__DATASET_CLAUSE = eINSTANCE.getSelectQuery_DatasetClause();

    /**
     * The meta object literal for the '<em><b>Where Clause</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SELECT_QUERY__WHERE_CLAUSE = eINSTANCE.getSelectQuery_WhereClause();

    /**
     * The meta object literal for the '<em><b>Order Clause</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SELECT_QUERY__ORDER_CLAUSE = eINSTANCE.getSelectQuery_OrderClause();

    /**
     * The meta object literal for the '<em><b>Limit Clause</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SELECT_QUERY__LIMIT_CLAUSE = eINSTANCE.getSelectQuery_LimitClause();

    /**
     * The meta object literal for the '<em><b>Offset Clause</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SELECT_QUERY__OFFSET_CLAUSE = eINSTANCE.getSelectQuery_OffsetClause();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.FilterImpl <em>Filter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.FilterImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getFilter()
     * @generated
     */
    EClass FILTER = eINSTANCE.getFilter();

    /**
     * The meta object literal for the '<em><b>Expression Filter</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FILTER__EXPRESSION_FILTER = eINSTANCE.getFilter_ExpressionFilter();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.ExpressionFilterImpl <em>Expression Filter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.ExpressionFilterImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getExpressionFilter()
     * @generated
     */
    EClass EXPRESSION_FILTER = eINSTANCE.getExpressionFilter();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute EXPRESSION_FILTER__NAME = eINSTANCE.getExpressionFilter_Name();

    /**
     * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRESSION_FILTER__PARAMETERS = eINSTANCE.getExpressionFilter_Parameters();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.OrderClauseImpl <em>Order Clause</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.OrderClauseImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getOrderClause()
     * @generated
     */
    EClass ORDER_CLAUSE = eINSTANCE.getOrderClause();

    /**
     * The meta object literal for the '<em><b>Order Condition</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ORDER_CLAUSE__ORDER_CONDITION = eINSTANCE.getOrderClause_OrderCondition();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.OrderConditionImpl <em>Order Condition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.OrderConditionImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getOrderCondition()
     * @generated
     */
    EClass ORDER_CONDITION = eINSTANCE.getOrderCondition();

    /**
     * The meta object literal for the '<em><b>Bracketted Expression</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ORDER_CONDITION__BRACKETTED_EXPRESSION = eINSTANCE.getOrderCondition_BrackettedExpression();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.BrackettedExpressionImpl <em>Bracketted Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.BrackettedExpressionImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getBrackettedExpression()
     * @generated
     */
    EClass BRACKETTED_EXPRESSION = eINSTANCE.getBrackettedExpression();

    /**
     * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BRACKETTED_EXPRESSION__EXPRESSION = eINSTANCE.getBrackettedExpression_Expression();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.ExpressionImpl <em>Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.ExpressionImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getExpression()
     * @generated
     */
    EClass EXPRESSION = eINSTANCE.getExpression();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.WhereClauseImpl <em>Where Clause</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.WhereClauseImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getWhereClause()
     * @generated
     */
    EClass WHERE_CLAUSE = eINSTANCE.getWhereClause();

    /**
     * The meta object literal for the '<em><b>Pattern</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference WHERE_CLAUSE__PATTERN = eINSTANCE.getWhereClause_Pattern();

    /**
     * The meta object literal for the '<em><b>Filter</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference WHERE_CLAUSE__FILTER = eINSTANCE.getWhereClause_Filter();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.PatternImpl <em>Pattern</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.PatternImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getPattern()
     * @generated
     */
    EClass PATTERN = eINSTANCE.getPattern();

    /**
     * The meta object literal for the '<em><b>Patterns</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PATTERN__PATTERNS = eINSTANCE.getPattern_Patterns();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.PatternOneImpl <em>Pattern One</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.PatternOneImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getPatternOne()
     * @generated
     */
    EClass PATTERN_ONE = eINSTANCE.getPatternOne();

    /**
     * The meta object literal for the '<em><b>Triples Same Subject</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PATTERN_ONE__TRIPLES_SAME_SUBJECT = eINSTANCE.getPatternOne_TriplesSameSubject();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.TriplesSameSubjectImpl <em>Triples Same Subject</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.TriplesSameSubjectImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getTriplesSameSubject()
     * @generated
     */
    EClass TRIPLES_SAME_SUBJECT = eINSTANCE.getTriplesSameSubject();

    /**
     * The meta object literal for the '<em><b>Subject</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TRIPLES_SAME_SUBJECT__SUBJECT = eINSTANCE.getTriplesSameSubject_Subject();

    /**
     * The meta object literal for the '<em><b>Property List</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TRIPLES_SAME_SUBJECT__PROPERTY_LIST = eINSTANCE.getTriplesSameSubject_PropertyList();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.PropertyListImpl <em>Property List</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.PropertyListImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getPropertyList()
     * @generated
     */
    EClass PROPERTY_LIST = eINSTANCE.getPropertyList();

    /**
     * The meta object literal for the '<em><b>Property</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROPERTY_LIST__PROPERTY = eINSTANCE.getPropertyList_Property();

    /**
     * The meta object literal for the '<em><b>Object</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROPERTY_LIST__OBJECT = eINSTANCE.getPropertyList_Object();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.GraphNodeImpl <em>Graph Node</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.GraphNodeImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getGraphNode()
     * @generated
     */
    EClass GRAPH_NODE = eINSTANCE.getGraphNode();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.QuoteImpl <em>Quote</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.QuoteImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getQuote()
     * @generated
     */
    EClass QUOTE = eINSTANCE.getQuote();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute QUOTE__NAME = eINSTANCE.getQuote_Name();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.ParameterImpl <em>Parameter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.ParameterImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getParameter()
     * @generated
     */
    EClass PARAMETER = eINSTANCE.getParameter();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PARAMETER__NAME = eINSTANCE.getParameter_Name();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.BlankNodeImpl <em>Blank Node</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.BlankNodeImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getBlankNode()
     * @generated
     */
    EClass BLANK_NODE = eINSTANCE.getBlankNode();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BLANK_NODE__NAME = eINSTANCE.getBlankNode_Name();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.ValueImpl <em>Value</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.ValueImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getValue()
     * @generated
     */
    EClass VALUE = eINSTANCE.getValue();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VALUE__NAME = eINSTANCE.getValue_Name();

    /**
     * The meta object literal for the '<em><b>String Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VALUE__STRING_VALUE = eINSTANCE.getValue_StringValue();

    /**
     * The meta object literal for the '<em><b>Integer Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VALUE__INTEGER_VALUE = eINSTANCE.getValue_IntegerValue();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.DatasetClauseImpl <em>Dataset Clause</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.DatasetClauseImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getDatasetClause()
     * @generated
     */
    EClass DATASET_CLAUSE = eINSTANCE.getDatasetClause();

    /**
     * The meta object literal for the '<em><b>Default Graph Clause</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DATASET_CLAUSE__DEFAULT_GRAPH_CLAUSE = eINSTANCE.getDatasetClause_DefaultGraphClause();

    /**
     * The meta object literal for the '<em><b>Named Graph Clause</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DATASET_CLAUSE__NAMED_GRAPH_CLAUSE = eINSTANCE.getDatasetClause_NamedGraphClause();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.DefaultGraphClauseImpl <em>Default Graph Clause</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.DefaultGraphClauseImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getDefaultGraphClause()
     * @generated
     */
    EClass DEFAULT_GRAPH_CLAUSE = eINSTANCE.getDefaultGraphClause();

    /**
     * The meta object literal for the '<em><b>Iref</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DEFAULT_GRAPH_CLAUSE__IREF = eINSTANCE.getDefaultGraphClause_Iref();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.NamedGraphClauseImpl <em>Named Graph Clause</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.NamedGraphClauseImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getNamedGraphClause()
     * @generated
     */
    EClass NAMED_GRAPH_CLAUSE = eINSTANCE.getNamedGraphClause();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.SourceSelectorImpl <em>Source Selector</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.SourceSelectorImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getSourceSelector()
     * @generated
     */
    EClass SOURCE_SELECTOR = eINSTANCE.getSourceSelector();

    /**
     * The meta object literal for the '<em><b>Iref</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SOURCE_SELECTOR__IREF = eINSTANCE.getSourceSelector_Iref();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.VarImpl <em>Var</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.VarImpl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getVar()
     * @generated
     */
    EClass VAR = eINSTANCE.getVar();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.VAR1Impl <em>VAR1</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.VAR1Impl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getVAR1()
     * @generated
     */
    EClass VAR1 = eINSTANCE.getVAR1();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VAR1__NAME = eINSTANCE.getVAR1_Name();

    /**
     * The meta object literal for the '{@link org.edu.le.spaqrl.sparql.impl.VAR2Impl <em>VAR2</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.edu.le.spaqrl.sparql.impl.VAR2Impl
     * @see org.edu.le.spaqrl.sparql.impl.SparqlPackageImpl#getVAR2()
     * @generated
     */
    EClass VAR2 = eINSTANCE.getVAR2();

  }

} //SparqlPackage
