/**
 */
package org.edu.le.spaqrl.sparql.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.edu.le.spaqrl.sparql.NamedGraphClause;
import org.edu.le.spaqrl.sparql.SparqlPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Named Graph Clause</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class NamedGraphClauseImpl extends MinimalEObjectImpl.Container implements NamedGraphClause
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected NamedGraphClauseImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SparqlPackage.Literals.NAMED_GRAPH_CLAUSE;
  }

} //NamedGraphClauseImpl
